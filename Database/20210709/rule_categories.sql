-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 
-- サーバのバージョン： 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oss`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `rule_categories`
--

CREATE TABLE IF NOT EXISTS `rule_categories` (
  `id` int(11) NOT NULL,
  `category` int(11) NOT NULL COMMENT '規約区分',
  `category_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL COMMENT '規約名称',
  `enable` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0:無効, 1:有効',
  `created` datetime NOT NULL COMMENT '作成日',
  `modified` datetime NOT NULL COMMENT '更新日'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='規約区分テーブル';

--
-- テーブルのデータのダンプ `rule_categories`
--

INSERT INTO `rule_categories` (`id`, `category`, `category_name`, `enable`, `created`, `modified`) VALUES
(1, 1, '就業規則', 1, '2021-06-28 00:00:00', '2021-06-28 00:00:00'),
(2, 2, '出張に関する規定', 1, '2021-06-28 00:00:00', '2021-06-28 00:00:00'),
(3, 3, '賃金規定', 1, '2021-06-28 00:00:00', '2021-06-28 00:00:00'),
(4, 4, '勤務に関する規定', 1, '2021-06-28 00:00:00', '2021-06-28 00:00:00'),
(5, 5, '表彰に関する規定', 1, '2021-06-28 00:00:00', '2021-06-28 00:00:00'),
(6, 6, '休暇に関する規定', 1, '2021-06-28 00:00:00', '2021-06-28 00:00:00'),
(7, 7, '慶弔見舞金規定', 1, '2021-06-28 14:08:56', '2021-06-28 14:08:56'),
(8, 8, '勤務場所に関する規定', 1, '2021-06-28 14:08:56', '2021-06-28 14:08:56'),
(9, 9, '社内の資産に関する規定', 1, '2021-06-28 14:09:31', '2021-06-28 14:09:31'),
(10, 10, '社内の制度に関する規定', 1, '2021-06-28 14:09:31', '2021-06-28 14:09:31'),
(11, 11, '安全衛生に関する規定', 1, '2021-06-28 14:10:04', '2021-06-28 14:10:04'),
(12, 12, 'セキュリティに関する規定', 1, '2021-06-28 14:10:04', '2021-06-28 14:10:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rule_categories`
--
ALTER TABLE `rule_categories`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rule_categories`
--
ALTER TABLE `rule_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
