-- [2021/12/01] 勤怠テーブルへの交通費、在宅勤務を追加
ALTER TABLE `kintais` ADD `transport` INT NULL COMMENT '交通費' AFTER `status`;
ALTER TABLE `kintai_details` ADD `remote_work` TINYINT NOT NULL DEFAULT '0' COMMENT '在宅勤務' AFTER `remarks`;
