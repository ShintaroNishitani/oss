-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 
-- サーバのバージョン： 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oss`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `rule_details`
--

DROP TABLE IF EXISTS `rule_details`;
CREATE TABLE IF NOT EXISTS `rule_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rule_id` int(11) NOT NULL COMMENT '関連ドキュメント番号',
  `index` int(11) NOT NULL DEFAULT '1' COMMENT 'ドキュメント内通番',
  `doc_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ドキュメント名称',
  `file_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ファイル名',
  `remark` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'コメント',
  `enable` tinyint(4) NOT NULL DEFAULT '1' COMMENT '有効フラグ',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='規約テーブル';

--
-- テーブルのデータのダンプ `rule_details`
--

INSERT INTO `rule_details` (`id`, `rule_id`, `index`, `doc_name`, `file_name`, `remark`, `enable`, `created`, `modified`) VALUES
(1, 17, 1, '情報セキュリティ管理規定_附則', '012-0002_情報セキュリティ管理規定_附則.pdf', NULL, 1, '2021-10-27 12:00:00', '2021-10-27 12:00:00'),
(2, 17, 2, '情報セキュリティ管理体制', '012-0002_情報セキュリティ管理体制.pdf', NULL, 1, '2021-10-27 12:00:00', '2021-10-27 12:00:00'),
(3, 21, 1, '特定個人情報基本方針', '012-0004_特定個人情報基本方針.pdf', NULL, 1, '2021-10-27 12:00:00', '2021-10-27 12:00:00'),
(4, 21, 2, '安全管理措置に関するマニュアル', '012-0004_安全管理措置に関するマニュアル.pdf', NULL, 1, '2021-10-27 12:00:00', '2021-10-27 12:00:00'),
(5, 21, 3, '委託に関するマニュアル', '012-0004_委託に関するマニュアル.pdf', NULL, 1, '2021-10-27 12:00:00', '2021-10-27 12:00:00'),
(6, 21, 4, '事務取扱担当者に関するマニュアル', '012-0004_事務取扱担当者に関するマニュアル.pdf', NULL, 1, '2021-10-27 12:00:00', '2021-10-27 12:00:00'),
(7, 21, 5, '従業員に関するマニュアル', '012-0004_従業員に関するマニュアル.pdf', NULL, 1, '2021-10-27 12:00:00', '2021-10-27 12:00:00');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
