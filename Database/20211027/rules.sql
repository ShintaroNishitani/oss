-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 
-- サーバのバージョン： 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oss`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `rules`
--

DROP TABLE IF EXISTS `rules`;
CREATE TABLE IF NOT EXISTS `rules` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category` int(11) NOT NULL COMMENT '規約区分',
  `doc_no` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ドキュメント番号',
  `index` int(11) NOT NULL DEFAULT '1' COMMENT 'ドキュメント内通番',
  `doc_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ドキュメント名称',
  `file_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ファイル名',
  `remark` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'コメント',
  `enable` tinyint(4) NOT NULL DEFAULT '1' COMMENT '有効フラグ',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='規約テーブル';

--
-- テーブルのデータのダンプ `rules`
--

INSERT INTO `rules` (`id`, `category`, `doc_no`, `index`, `doc_name`, `file_name`, `remark`, `enable`, `created`, `modified`) VALUES
(1, 1, '001-0001', 1, '就業規則', '001-0001_就業規則.pdf', NULL, 1, '2021-06-30 11:45:19', '2021-06-30 11:45:19'),
(2, 2, '002-0001', 1, '国内出張旅費規程', '002-0001_国内出張旅費規程.pdf', '', 1, '2021-06-30 11:54:38', '2021-06-30 11:54:38'),
(3, 2, '002-0001', 2, '国内出張旅費規程_旅費区分', '002-0001_国内出張旅費規程_旅費区分.pdf', NULL, 1, '2021-06-30 11:54:38', '2021-06-30 11:54:38'),
(4, 2, '002-0002', 1, '海外出張旅費規程', '002-0002_海外出張旅費規程.pdf', NULL, 1, '2021-06-30 11:56:15', '2021-06-30 11:56:15'),
(5, 2, '002-0002', 2, '海外出張旅費規程_旅費区分', '002-0002_海外出張旅費規程_旅費区分.pdf', NULL, 1, '2021-06-30 11:56:15', '2021-06-30 11:56:15'),
(6, 3, '003-0001', 1, '賃金規定', '003-0001_賃金規定.pdf', NULL, 1, '2021-06-30 11:57:44', '2021-06-30 11:57:44'),
(7, 4, '004-0002', 1, '在宅勤務制度規定', '004-0002_在宅勤務制度規定.pdf', NULL, 1, '2021-06-30 11:58:20', '2021-06-30 11:58:20'),
(8, 4, '004-0001', 1, '時間外勤務及び休日勤務取扱規程', '004-0001_時間外勤務及び休日勤務取扱規程.pdf', NULL, 1, '2021-06-30 11:59:31', '2021-06-30 11:59:31'),
(9, 4, '004-0003', 1, 'フレックスタイム制度取扱規定', '004-0003_フレックスタイム制度取扱規定.pdf', NULL, 1, '2021-06-30 11:59:31', '2021-06-30 11:59:31'),
(10, 5, '005-0001', 1, 'リフレッシュ休暇規定', '005-0001_リフレッシュ休暇規定.pdf', NULL, 1, '2021-06-30 12:00:47', '2021-06-30 12:00:47'),
(11, 5, '005-0002', 1, '表彰規程', '005-0002_表彰規程.pdf', NULL, 1, '2021-06-30 12:00:47', '2021-06-30 12:00:47'),
(12, 6, '006-0001', 1, '育児・介護規定', '006-0001_育児・介護規定.pdf', NULL, 1, '2021-06-30 12:01:40', '2021-06-30 12:01:40'),
(13, 7, '007-0001', 1, '慶弔見舞金規定', '007-0001_慶弔見舞金規定.pdf', NULL, 1, '2021-06-30 12:03:22', '2021-06-30 12:03:22'),
(14, 8, '008-0004', 1, '派遣規定', '008-0004_派遣規定.pdf', NULL, 1, '2021-06-30 12:09:59', '2021-06-30 12:09:59'),
(15, 10, '010-0005', 1, '労働災害補償規程', '010-0005_労働災害補償規程.pdf', NULL, 1, '2021-06-30 12:09:59', '2021-06-30 12:09:59'),
(16, 12, '012-0001', 1, '個人情報保護規定', '012-0001_個人情報保護規定.pdf', NULL, 1, '2021-06-30 12:09:59', '2021-06-30 12:09:59'),
(17, 12, '012-0002', 1, '情報セキュリティ管理規定', '012-0002_情報セキュリティ管理規定.pdf', NULL, 1, '2021-06-30 12:09:59', '2021-06-30 12:09:59'),
(20, 12, '012-0003', 1, '情報機器管理規程', '012-0003_情報機器管理規程.pdf', NULL, 1, '2021-06-30 12:09:59', '2021-06-30 12:09:59'),
(21, 12, '012-0004', 1, '特定個人情報等取扱規定', '012-0004_特定個人情報等取扱規定.pdf', NULL, 1, '2021-06-30 12:09:59', '2021-06-30 12:09:59');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
