-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 
-- サーバのバージョン： 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oss`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `asset_lend_inventories`
--

CREATE TABLE IF NOT EXISTS `asset_lend_inventories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_lend_id` int(11) NOT NULL COMMENT '資産貸出書ID',
  `year` int(11) NOT NULL COMMENT '棚卸年',
  `date` int(11) NOT NULL COMMENT '棚卸月',
  `executer_id` int(11) NOT NULL COMMENT '棚卸確認者',
  `execute_date` date NOT NULL COMMENT '棚卸実施日',
  `inventory_status` int(1) DEFAULT NULL COMMENT '棚卸ステータス',
  `corrective_action` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '是正措置',
  `corrective_limit_date` date NOT NULL COMMENT '是正措置期限',
  `corrective_check_staff_id` int(11) NOT NULL COMMENT '是正措置確認者',
  `corrective_check_date` date NOT NULL COMMENT '是正措置確認日',
  `enable` tinyint(4) NOT NULL COMMENT '0:無効, 1:有効',
  `created` datetime DEFAULT NULL COMMENT '作成日',
  `modified` datetime DEFAULT NULL COMMENT '更新日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
