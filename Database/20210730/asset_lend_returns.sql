-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 
-- サーバのバージョン： 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oss`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `asset_lend_returns`
--

DROP TABLE IF EXISTS `asset_lend_returns`;
CREATE TABLE IF NOT EXISTS `asset_lend_returns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_lend_id` int(11) NOT NULL COMMENT '資産貸出書番号ID',
  `means` int(11) DEFAULT NULL COMMENT '返却方法',
  `transfer` text COLLATE utf8_unicode_ci COMMENT '返却移送手段',
  `delete_staff_id` int(11) DEFAULT NULL COMMENT '削除確認者',
  `delete_date` date DEFAULT NULL COMMENT '削除確認日',
  `return_staff_id` int(11) DEFAULT NULL COMMENT '返却者',
  `return_date` date DEFAULT NULL COMMENT '返却日',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '返却済ステータス',
  `enable` tinyint(4) NOT NULL COMMENT '0:無効, 1:有効',
  `created` datetime DEFAULT NULL COMMENT '作成日',
  `modified` datetime DEFAULT NULL COMMENT '更新日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
