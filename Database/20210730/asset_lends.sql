-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 
-- サーバのバージョン： 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oss`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `asset_lends`
--

DROP TABLE IF EXISTS `asset_lends`;
CREATE TABLE `asset_lends` (
  `id` int(11) NOT NULL,
  `asset_lend_number` varchar(16) COLLATE utf8_unicode_ci NOT NULL COMMENT '資産貸出書番号',
  `asset_manager_id` int(11) NOT NULL COMMENT '情報管理責任者',
  `customer_id` int(11) NOT NULL COMMENT '貸借顧客ID',
  `division` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT '区分',
  `lend_charge_department` char(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '貸与責任者 部署',
  `lend_charge_section` varchar(12) COLLATE utf8_unicode_ci NOT NULL COMMENT '貸与責任者 課',
  `lend_charge_position` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '貸与責任者 役職',
  `lend_charge_person` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '貸与責任者 氏名',
  `lend_mng_department` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '貸与管理者 部署',
  `lend_mng_section` varchar(15) COLLATE utf8_unicode_ci NOT NULL COMMENT '貸与管理者 課',
  `lend_mng_position` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '貸与管理者 役職',
  `lend_mng_person` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '貸与管理者 氏名',
  `lend_date` date NOT NULL COMMENT '貸与日',
  `receive_user_id` int(11) NOT NULL COMMENT '受領者',
  `receive_date` date NOT NULL COMMENT '受領日',
  `lend_start_date` date NOT NULL COMMENT '貸借期間開始日',
  `lend_end_date` date NOT NULL COMMENT '貸借期間終了日',
  `auto_extend` int(1) NOT NULL COMMENT '自動延長フラグ',
  `lend_object` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '貸借物件',
  `lend_approach` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT '貸借方法',
  `lend_purpose` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT '使用目的',
  `delivery_approach` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '貸借移送手段',
  `storage_area` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '保管場所',
  `check_return` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT '返却要否',
  `enable` tinyint(4) NOT NULL COMMENT '0:無効, 1:有効',
  `created` datetime DEFAULT NULL COMMENT '作成日',
  `modified` datetime DEFAULT NULL COMMENT '更新日'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- テーブルのデータのダンプ `asset_lends`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asset_lends`
--
ALTER TABLE `asset_lends`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asset_lends`
--
ALTER TABLE `asset_lends`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
