-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 
-- サーバのバージョン： 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oss`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `inner_asset_lends`
--

DROP TABLE IF EXISTS `inner_asset_lends`;
CREATE TABLE `inner_asset_lends` (
  `id` int(11) NOT NULL,
  `lend_manager_id` int(11) NOT NULL COMMENT '管理責任者',
  `division` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT '区分',
  `rent_date` date NOT NULL COMMENT '借受日',
  `rent_staff_id` int(11) DEFAULT NULL COMMENT '使用者',
  `rent_start_date` date NOT NULL COMMENT '貸借期間開始日',
  `rent_end_date` date NOT NULL COMMENT '借受期間終了日',
  `auto_extend` int(1) NOT NULL COMMENT '自動延長フラグ',
  `rent_object` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '借受物件',
  `rent_approach` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT '借受方法',
  `rent_purpose` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT '使用目的',
  `delivery_approach` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '借受移送手段',
  `storage_area` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '保管場所',
  `check_return` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT '返却要否',
  `return_date` date DEFAULT NULL COMMENT '返却日',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '返却済ステータス',  
  `enable` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0:無効, 1:有効',
  `created` datetime DEFAULT NULL COMMENT '作成日',
  `modified` datetime DEFAULT NULL COMMENT '更新日'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


--
-- Indexes for dumped tables
--

--
-- Indexes for table `inner_asset_lends`
--
ALTER TABLE `inner_asset_lends`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `inner_asset_lends`
--
ALTER TABLE `inner_asset_lends`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
