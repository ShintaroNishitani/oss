
--
-- Database: `oss`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `transport_details`
--

CREATE TABLE IF NOT EXISTS `transport_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL DEFAULT 0 COMMENT 'スタッフ',
  `idx` int(11) NOT NULL DEFAULT '0' COMMENT '通番',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '名称',
  `place` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '行き先',
  `purpose` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '目的',
  `transport` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '利用交通機関',
  `arrival` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '出発',
  `departure` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '到着',
  `unit` int(11) NOT NULL COMMENT '単価',
  `num` int(11) NOT NULL COMMENT '数量',
  `customer` tinyint(4) NOT NULL COMMENT '客先請求可否',
  `remark` text COLLATE utf8_unicode_ci COMMENT '備考',
  `enable` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='交通費テンプレート';
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
