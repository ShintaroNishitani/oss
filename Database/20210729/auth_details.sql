--
-- テーブルのデータのダンプ `auth_details`
--

INSERT INTO `auth_details` (`authority_id`, `name`, `action`, `auth`, `created`, `modified`) VALUES
( 1, 'Transports', 's_template', 1, '2021-07-07 17:06:25', '2021-07-07 17:06:25'),
( 2, 'Transports', 's_template', 1, '2021-07-07 17:06:25', '2021-07-07 17:06:25'),
( 3, 'Transports', 's_template', 1, '2021-07-07 17:06:25', '2021-07-07 17:06:25'),
( 4, 'Transports', 's_template', 1, '2021-07-07 17:06:25', '2021-07-07 17:06:25'),
( 5, 'Transports', 's_template', 1, '2021-07-07 17:06:25', '2021-07-07 17:06:25'),
( 6, 'Transports', 's_template', 1, '2021-07-07 17:06:25', '2021-07-07 17:06:25'),
( 7, 'Transports', 's_template', 1, '2021-07-07 17:06:25', '2021-07-07 17:06:25'),
( 8, 'Transports', 's_template', 1, '2021-07-07 17:06:25', '2021-07-07 17:06:25'),
( 9, 'Transports', 's_template', 1, '2021-07-07 17:06:25', '2021-07-07 17:06:25'),
( 10, 'Transports', 's_template', 1, '2021-07-07 17:06:25', '2021-07-07 17:06:25'),
( 1, 'Transports', 's_search_template', 1, '2021-07-07 17:06:25', '2021-07-07 17:06:25'),
( 2, 'Transports', 's_search_template', 1, '2021-07-07 17:06:25', '2021-07-07 17:06:25'),
( 3, 'Transports', 's_search_template', 1, '2021-07-07 17:06:25', '2021-07-07 17:06:25'),
( 4, 'Transports', 's_search_template', 1, '2021-07-07 17:06:25', '2021-07-07 17:06:25'),
( 5, 'Transports', 's_search_template', 1, '2021-07-07 17:06:25', '2021-07-07 17:06:25'),
( 6, 'Transports', 's_search_template', 1, '2021-07-07 17:06:25', '2021-07-07 17:06:25'),
( 7, 'Transports', 's_search_template', 1, '2021-07-07 17:06:25', '2021-07-07 17:06:25'),
( 8, 'Transports', 's_search_template', 1, '2021-07-07 17:06:25', '2021-07-07 17:06:25'),
( 9, 'Transports', 's_search_template', 1, '2021-07-07 17:06:25', '2021-07-07 17:06:25'),
( 10, 'Transports', 's_search_template', 1, '2021-07-07 17:06:25', '2021-07-07 17:06:25');

