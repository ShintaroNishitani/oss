-- [2021/12/xx] 勤怠テーブルへの36協定時間外労働計を追加
ALTER TABLE `kintais` ADD `extra_time` DOUBLE NOT NULL DEFAULT '0' COMMENT '36協定時間外労働計' AFTER `over_time`;
