<script>
    $(function(){
        var columns = ['#TransHolidayTransFrom', '#TransHolidayTransTo'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });   
    });

    function TransHolidayList() {
        $.ajax({
            type: "get",
            url: "<?php echo $this->Html->url(array('action' => 'list'));?>",
            data: $('#TransHolidayListForm').serialize(),
            success: function(html){
                $('#TransHolidayList').html(html);
            }
        });
        return false;
    }
</script>


<blockquote><p><?php echo $this->Html->image("trans_holiday.png")?><?php echo ' 振休情報';?></p></blockquote>
<?php echo "あなたの振休残日数は $trans_count 日です。" ?>
<?php echo $this->Form->create('TransHoliday', array("action" => "index", 'type'=>'get'))?>
<table class="table-condensed well">
    <tr>
        <th style="min-width:50px;">振休表示指定</th>
        <td><?php echo  $this->Form->select("TransHoliday.select_trans",
                                            $select_trans_option ,
                                            array("class" => "form-control input-sm",
                                                  "value" => $select_trans));?></td>
        <th style="min-width:50px;">振休発生日指定</th>
        <td>
            <div style="display:inline-flex">
            <?php echo $this->Form->text("TransHoliday.trans_from", array("class" => "form-control input-sm", 'style'=>SIZE_S, "value"=>$trans_from));?>
            ～
            <?php echo $this->Form->text("TransHoliday.trans_to", array("class" => "form-control input-sm", 'style'=>SIZE_S, "value"=>$trans_to));?>
            </div>
        </td> 
        
        <td class="center" colspan="8">
            <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> 検索</button>
        </td>
    </tr>
</table>
<?php echo $this->Form->end()?>


<table class="table-bordered table-condensed table-striped table-hover">
<tr>
    <th style="min-width:100px;"><?php echo $this->Paginator->sort('accrual_date','振休発生日');?></th>
    <th style="min-width:100px;"><?php echo $this->Paginator->sort('accrual_date','振休消化日1');?></th>
    <th style="min-width:100px;"><?php echo $this->Paginator->sort('accrual_date','振休消化日2');?></th>
</tr>
<?php foreach ($holiday_data as $datas): 
    $click = sprintf(' onclick="editTransHoliday(%d);"', $datas['TransHoliday']['id']);
?>
<tr>
    
    <td class="center" ><?php echo h($datas['TransHoliday']['accrual_date'])?></td>
    <?php if($datas['TransHoliday']['digestion_date1'] == "0000-00-00"): ?>
        <td class="center" <?php echo $click;?>><?php echo h("-------------")?></td>
    <?php else: ?>
        <td class="center" <?php echo $click;?>><?php echo h($datas['TransHoliday']['digestion_date1'])?></td>
    <?php endif; ?>
    <?php if($datas['TransHoliday']['digestion_date2'] == "0000-00-00"): ?>
        <td class="center" <?php echo $click;?>><?php echo h("-------------")?></td>
    <?php else: ?>
        <td class="center" <?php echo $click;?>><?php echo h($datas['TransHoliday']['digestion_date2'])?></td>
    <?php endif;?>
</tr>
<?php 
endforeach; 
?>


</table>
<?php echo $this->Element("a_pagination", array('count'=>true));?>

