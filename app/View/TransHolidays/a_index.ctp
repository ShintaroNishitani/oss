<script>
    /**
     * [editTransHoliday description]
     * @param  {[type]} id [description]
     * @return {[type]}    [description]
     */
    function editTransHoliday(id){
        var url = "/a/trans_holidays/edit/" + id;
        loadBaseWindow(url);
        return false;
    }

    /**
     * [deleteTransHoliday description]
     * @param  {[type]} id [description]
     * @return {[type]}    [description]
     */
    function deleteTransHoliday(id, date){
        if( date == 0000-00-00 ){
            if(window.confirm('削除しますがよろしいですか？')){
                var url = "/a/trans_holidays/delete/" + id;
                location.replace(url);
            }
        }else{
            if(window.confirm('勤怠を先に更新してください')){
            }
        }
    }    
</script>


<blockquote><p><?php echo $this->Html->image("trans_holiday.png")?><?php echo ' 振休管理';?></p></blockquote>

<a href="javascript:void(0);" onclick="editTransHoliday(<?php echo $id;?> );"><i class="glyphicon glyphicon-plus-sign"></i> 新規追加</a>


<?php echo $this->Form->create("TransHoliday", array("action" => "index", 'type'=>'get'))?>
<table class="table-condensed well">
    <tr>
        <th style="min-width:50px;">社員名</th>
        <td><?php echo  $this->Form->select("TransHoliday.select_staff", $staffs, array("class" => "form-control input-sm", "value"=>$select_staff));?></td>
        <th style="min-width:50px;">振休表示指定</th>
        <td><?php echo  $this->Form->select("TransHoliday.select_trans",
                                            $select_trans_option ,
                                            array("class" => "form-control input-sm",
                                                  "value" => $select_trans));?></td>
        <td class="center" colspan="8">
            <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> 検索</button>
        </td>
    </tr>
</table>
<?php echo $this->Form->end()?>

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
    <th style="min-width:100px;"><?php echo $this->Paginator->sort('staff_id','社員名');?></th>
    <th style="min-width:100px;"><?php echo $this->Paginator->sort('accrual_date','振休発生日');?></th>
    <th style="min-width:100px;"><?php echo $this->Paginator->sort('digestion_date1','振休消化日1');?></th>
    <th style="min-width:100px;"><?php echo $this->Paginator->sort('digestion_date2','振休消化日2');?></th>
    <th></th>
</tr>
<?php foreach ($holiday_data as $datas): 
    $click = sprintf(' onclick="editTransHoliday(%d);"', $datas['TransHoliday']['id']);
?>
<tr>
    <td class="center" <?php echo $click;?>><?php echo h($datas['Staff']['name'])?></td>
    <td class="center" <?php echo $click;?>><?php echo h($datas['TransHoliday']['accrual_date'])?></td>
    <?php if($datas['TransHoliday']['digestion_date1'] == "0000-00-00"): ?>
        <td class="center" <?php echo $click;?>><?php echo h("-------------")?></td>
    <?php else: ?>
        <td class="center" <?php echo $click;?>><?php echo h($datas['TransHoliday']['digestion_date1'])?></td>
    <?php endif; ?>
    <?php if($datas['TransHoliday']['digestion_date2'] == "0000-00-00"): ?>
        <td class="center" <?php echo $click;?>><?php echo h("-------------")?></td>
    <?php else: ?>
        <td class="center" <?php echo $click;?>><?php echo h($datas['TransHoliday']['digestion_date2'])?></td>
    <?php endif;?>
    <td class="center">
        <a href="javascript:void(0);" onclick="deleteTransHoliday(<?php echo $datas['TransHoliday']["id"];?>,<?php echo $datas['TransHoliday']['digestion_date1'];?>);">
        <i class="glyphicon glyphicon-remove-circle"></i></a>
    </td>
</tr>
<?php 
endforeach; 
?>
</table>

<?php echo $this->Element("a_pagination", array('count'=>true));?>

