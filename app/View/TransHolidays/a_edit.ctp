<script>
    $(function(){
        var columns = ['#TransHolidayAccrualDate', '#TransHolidayDigestionDate1' , '#TransHolidayDigestionDate2'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });   
    });

</script>

<blockquote><p><?php echo '振休登録';?></p></blockquote>
<p class="text-info"><?php echo '振休情報を入力してください。';?></p>

<?php echo $this->Form->create('TransHoliday', array("name"=>"TransHoliday", "action" => "update", "onsubmit" => "return updateTransHoliday();"))?>
<?php echo $this->Form->hidden("TransHoliday.staff_id")?>
<?php echo $this->Form->hidden("TransHoliday.id")?>
<?php echo $this->Form->hidden("TransHoliday.enable", array("value"=>1))?>

<table class="form">
<tr>
    <th>社員名*</th>
    <td><?php echo  $this->Form->select("TransHoliday.staff_id", $staffs, array("class" => "form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>振休発生日*</th>
    <td><?php echo  $this->Form->text("TransHoliday.accrual_date", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
    <th>振休消化日1*</th>
    <td><?php echo  $this->Form->text("TransHoliday.digestion_date1", array('default' => '0000-00-00', "empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
    <th>振休消化日2*</th>
    <td><?php echo $this->Form->text("TransHoliday.digestion_date2", array('default' => '0000-00-00', "empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
    <td colspan="2" class="center">
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>        
</table>

<?php echo $this->Form->end();?>