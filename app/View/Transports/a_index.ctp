<script>
    $(document).ready(function(){
        loadConfirmList();
    });

    function loadConfirmList(){
        var url = "/a/confirms/list/" + <?php echo CONFIRM_TYPE_1?> + "/" + <?php echo $datas['Transport']['id']?>;
        $('#ConfirmList').load(url);
    }

    function printTransport(id){
        var url = "/a/transports/print/" + id;
        location.replace(url);
    }    
</script>


<blockquote><p><?php echo $this->Html->image("train.png")?> 交通費請求</p></blockquote>

<div id="ConfirmList"><!-- 申請リスト --></div> 

<div class="center">
<h5>
<a href="/a/transports/index/<?php echo $b_year;?>/<?php echo $b_month;?>/<?php echo $staff_id;?>"><i class="glyphicon glyphicon-backward"></i> 前月 </a>
<b><?php echo sprintf("%s年 %s月", $year, $month);?></b>
<a href="/a/transports/index/<?php echo $n_year;?>/<?php echo $n_month;?>/<?php echo $staff_id;?>"> 翌月 <i class="glyphicon glyphicon-forward"></i></a>
</h5>
</div>

<a href="javascript:void(0);" onclick="printTransport(<?php echo $datas['Transport']['id'];?>);"><i class="glyphicon glyphicon-print"></i> 出力</a>

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
    <th>No</th>
    <th style="min-width:90px;">日付</th>
    <th style="min-width:200px;">行先</th>
    <th style="min-width:200px;">目的</th>
    <th style="min-width:300px;">利用交通機関</th>
    <th style="min-width:50px;">単価</th>
    <th style="min-width:50px;">数量</th>
    <th style="min-width:50px;">金額</th>
    <th style="min-width:60px;">客先請求</th>
</tr>
<?php
$no = 1;
$unit = 0;
$num = 0;
$price = 0;
if (!empty($datas['TransportDetail'])) {
foreach ($datas['TransportDetail'] as $data):
    $click = "";
    $unit += $data['unit'];
    $num += $data['num'];
    $price += $data['price'];
?>
<tr>
    <td <?php echo $click;?>><?php echo h($no++)?></td>
    <td class="center" <?php echo $click;?>><?php echo h($data['date'])?></td>
    <td <?php echo $click;?>><?php echo h($data['place'])?></td>
    <td <?php echo $click;?>><?php echo h($data['purpose'])?></td>
    <td <?php echo $click;?>>
        <?php echo h($data['transport'] . ' ' . $data['arrival'] . '～' . $data['departure'])?>
    </td>
    <td class="number" <?php echo $click;?>><?php echo h(number_format($data['unit']))?></td>
    <td class="number" <?php echo $click;?>><?php echo h(number_format($data['num']))?></td>
    <td class="number" <?php echo $click;?>><?php echo h(number_format($data['price']))?></td>
    <td class="center" <?php echo $click;?>>
        <?php 
            $items = Configure::read("transport_customer");
            echo h($items[$data['customer']]);
        ?>
    </td>
</tr>
<?php 
endforeach; 
}
?>
<tr>
    <th colspan="5" class="center">合計</th>
    <th class="number"><?php echo h(number_format($unit))?></th>
    <th class="number"><?php echo h(number_format($num))?></th>
    <th class="number"><?php echo h(number_format($price))?></th>
    <th colspan="2">&nbsp</th>
</tr>
</table>

