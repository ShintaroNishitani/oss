<script>
    $(function(){
        $('#TransportTemplatePlace').autocomplete({
            source: '/a/transports/auto_item/' + <?php echo CONFIRM_TYPE_1;?> + "/" + <?php echo TRANSPORT_SUB_0;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#TransportTemplatePlace').html(ui.item.details);
            }}            
        });   
        $('#TransportTemplatePurpose').autocomplete({
            source: '/a/transports/auto_item/' + <?php echo CONFIRM_TYPE_1;?> + "/" + <?php echo TRANSPORT_SUB_1;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#TransportTemplatePurpose').html(ui.item.details);
            }}            
        });   
        $('#TransportTemplateTransport').autocomplete({
            source: '/a/transports/auto_item/' + <?php echo CONFIRM_TYPE_1;?> + "/" + <?php echo TRANSPORT_SUB_2;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#TransportTemplateTransport').html(ui.item.details);
            }}            
        });  
        $('#TransportTemplateArrival').autocomplete({
            source: '/a/transports/auto_item/' + <?php echo CONFIRM_TYPE_1;?> + "/" + <?php echo TRANSPORT_SUB_3;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#TransportTemplateArrival').html(ui.item.details);
            }}            
        });         
        $('#TransportTemplateDeparture').autocomplete({
            source: '/a/transports/auto_item/' + <?php echo CONFIRM_TYPE_1;?> + "/" + <?php echo TRANSPORT_SUB_3;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#TransportTemplateDeparture').html(ui.item.details);
            }}            
        });              
    });


    function updateTransportTemplate(){
        var element = document.getElementById("TransportTemplatePrice");
        if(0 == element.value){
            alert("金額を設定してください");
            return false;
        }
    }      
</script>

<blockquote><p>交通費テンプレート登録</p></blockquote>
<p class="text-info">交通費情報を入力してください。</p>
<?php echo $this->Form->create('Transport', array("name"=>"TransportTemplate", "url" => array('action' =>"template")))?>

<?php 
	for($i = 0; $i < 8; $i++){ 
?>
<?php 
	$id = !empty($data[$i]['TransportTemplate']['id']) ? $data[$i]['TransportTemplate']['id'] : "0";
	echo $this->Form->hidden("TransportTemplate.{$i}.id", array('value'=> $id));
?>
<?php  
	$val = !empty($data[$i]['TransportTemplate']['idx']) ? $data[$i]['TransportTemplate']['idx'] : ($i + 1);
	echo $this->Form->hidden("TransportTemplate.{$i}.idx", array('value'=> $val));
?>
<?php 
	$sid = !empty($data[$i]['TransportTemplate']['staff_id']) ? $data[$i]['TransportTemplate']['staff_id'] : $staff_id;
	echo $this->Form->hidden("TransportTemplate.{$i}.staff_id", array('value' => $sid));
?>
<?php echo $this->Form->hidden("TransportTemplate.{$i}.enable", array('value'=> 1))?>

<p>

■テンプレート<?php echo($i + 1)?>
<table class="form">
<tr>
    <th>名称*</th>
    <td>
    <?php 
		$name = isSet($data[$i]['TransportTemplate']['name']) ? $data[$i]['TransportTemplate']['name'] : "テンプレート". ($i + 1);
    	echo $this->Form->text("TransportTemplate.{$i}.name", array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M, 'value' => $name));
    ?></td>
</tr>
<tr>
    <th>行先</th>
    <td>
    <?php 
    	$val = isSet($data[$i]['TransportTemplate']['place']) ? $data[$i]['TransportTemplate']['place'] : "";
    	echo $this->Form->text("TransportTemplate.{$i}.place", array('value' => $val, "class"=>"form-control input-sm jpn", 'style'=>SIZE_M));
    ?>
    </td>
</tr>
<tr>    
    <th>目的</th>
    <td>
    <?php 
        $val = isSet($data[$i]['TransportTemplate']['purpose']) ? $data[$i]['TransportTemplate']['purpose'] : "";
    	echo $this->Form->text("TransportTemplate.{$i}.purpose", array('value' => $val, "class"=>"form-control input-sm jpn", 'style'=>SIZE_M));
    ?>
    </td>
</tr>
<tr>
    <th>利用交通機関</th>
    <td>
    <?php 
        $val = isSet($data[$i]['TransportTemplate']['transport']) ? $data[$i]['TransportTemplate']['transport'] : "";
    	echo $this->Form->text("TransportTemplate.{$i}.transport", array('value' => $val, "class"=>"form-control input-sm jpn", 'style'=>SIZE_M));
    ?>
    </td>
</tr>
<tr>
    <th>出発</th>
    <td>
    <?php 
        $val = isSet($data[$i]['TransportTemplate']['arrival']) ? $data[$i]['TransportTemplate']['arrival'] : "";
    	echo $this->Form->text("TransportTemplate.{$i}.arrival", array('value' => $val, "class"=>"form-control input-sm jpn", 'style'=>SIZE_M));
    ?>
    </td>
</tr> 
<tr>   
    <th>到着</th>
    <td>
    <?php 
        $val = isSet($data[$i]['TransportTemplate']['departure']) ? $data[$i]['TransportTemplate']['departure'] : "";
    	echo $this->Form->text("TransportTemplate.{$i}.departure", array('value' => $val, "class"=>"form-control input-sm jpn", 'style'=>SIZE_M));
    ?>
    </td>
</tr>
<tr>
    <th>単価</th>
    <td>
    <?php 
        $val = isSet($data[$i]['TransportTemplate']['unit']) ? $data[$i]['TransportTemplate']['unit'] : "0";
    	echo $this->Form->text("TransportTemplate.{$i}.unit", array('value' => $val, "class"=>"form-control input-sm number", 'style'=>SIZE_S, 'default'=>0, 'type'=>'number'));
    ?>
    </td>
</tr>
<tr>
    <th>数量</th>
    <td>
    <?php 
    	$val = isSet($data[$i]['TransportTemplate']['num']) ? $data[$i]['TransportTemplate']['num'] : "0";
    	echo $this->Form->text("TransportTemplate.{$i}.num", array('value' => $val,"class"=>"form-control input-sm number", 'style'=>SIZE_S, 'default'=>1, 'type'=>'number'));
    ?>
    </td>
</tr>
<tr>
    <th>客先請求</th>
    <td>
    <?php 
    	$val = isSet($data[$i]['TransportTemplate']['customer']) ? $data[$i]['TransportTemplate']['customer'] : "";
    	echo $this->Form->select("TransportTemplate.{$i}.customer", Configure::read("transport_customer"), array('value' => $val,"class"=>"form-control input-sm", 'style'=>SIZE_S, 'empty'=>false));
    ?>
    </td>
</tr>
</table>
</p>
<br>
<?php }?>

<tr>
    <td colspan="6" class="center">
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>   
<?php echo $this->Form->end();?>