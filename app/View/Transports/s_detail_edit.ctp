<script>
    $(function(){
        var columns = ['#TransportDetailDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });   
        $('#TransportDetailPlace').autocomplete({
            source: '/a/transports/auto_item/' + <?php echo CONFIRM_TYPE_1;?> + "/" + <?php echo TRANSPORT_SUB_0;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#TransportDetailPlace').html(ui.item.details);
            }}            
        });   
        $('#TransportDetailPurpose').autocomplete({
            source: '/a/transports/auto_item/' + <?php echo CONFIRM_TYPE_1;?> + "/" + <?php echo TRANSPORT_SUB_1;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#TransportDetailPurpose').html(ui.item.details);
            }}            
        });   
        $('#TransportDetailTransport').autocomplete({
            source: '/a/transports/auto_item/' + <?php echo CONFIRM_TYPE_1;?> + "/" + <?php echo TRANSPORT_SUB_2;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#TransportDetailTransport').html(ui.item.details);
            }}            
        });  
        $('#TransportDetailArrival').autocomplete({
            source: '/a/transports/auto_item/' + <?php echo CONFIRM_TYPE_1;?> + "/" + <?php echo TRANSPORT_SUB_3;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#TransportDetailArrival').html(ui.item.details);
            }}            
        });         
        $('#TransportDetailDeparture').autocomplete({
            source: '/a/transports/auto_item/' + <?php echo CONFIRM_TYPE_1;?> + "/" + <?php echo TRANSPORT_SUB_3;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#TransportDetailDeparture').html(ui.item.details);
            }}            
        });              
    });
    
	$(function() {
		// ------------------------------------
		// 技術者検索で
		// ------------------------------------
		$('#TransportTemplateId').change(function() {
			$.ajax({
				type: 'get',
				dataType: 'json',
				async: false,
				url: '/s/transports/search_template/',
				data: {
					'id' : $(this).val(),
				},
			}).done(function(data){
				var list = null;
				if(data.status) {
					// 成功時
					//画面の各項目に反映
					$('#TransportDetailPlace').val(data.result['TransportTemplate']['place']);
					$('#TransportDetailPurpose').val(data.result['TransportTemplate']['purpose']);
					$('#TransportDetailTransport').val(data.result['TransportTemplate']['transport']);
					$('#TransportDetailArrival').val(data.result['TransportTemplate']['arrival']);
					$('#TransportDetailDeparture').val(data.result['TransportTemplate']['departure']);
					$('#TransportDetailUnit').val(data.result['TransportTemplate']['unit']);
					$('#TransportDetailNum').val(data.result['TransportTemplate']['num']);
					$('#TransportDetailCustomer').val(data.result['TransportTemplate']['customer']);

					calcPrice();
				} else {
					// 失敗時
					//alert(data.message);
				}
			}).fail(function(data){
				// アクション側でExceptionが投げられた場合はここに来る。
				// alert('error!!!');
			});
		});
	});	    

    function calcPrice() {
        var unit = document.getElementById('TransportDetailUnit');
        var num = document.getElementById('TransportDetailNum');
        var price  = document.getElementById('TransportDetailPrice');

        price.value = Math.round(unit.value * num.value);
    }    

    function updateTransportDetail(){
        var element = document.getElementById("TransportDetailPrice");
        if(0 == element.value){
            alert("金額を設定してください");
            return false;
        }
    }      
</script>

<blockquote><p>交通費登録</p></blockquote>
<p class="text-info">交通費情報を入力してください。
<?php echo $this->Form->select('TransportTemplate.id', $templates, array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?>
</p>

<?php echo $this->Form->create('Transport', array("name"=>"TransportDetail", 'url' => array("action" => "detail_update"),
                                                        "onsubmit" => "return updateTransportDetail();"))?>
<?php echo $this->Form->hidden("TransportDetail.id")?>
<?php echo $this->Form->hidden("TransportDetail.transport_id", array("value"=>$id))?>
<?php echo $this->Form->hidden("TransportDetail.staff_id", array("value"=>$staff_id))?>
<?php echo $this->Form->hidden("TransportDetail.year", array("value"=>$year))?>
<?php echo $this->Form->hidden("TransportDetail.month", array("value"=>$month))?>
<?php echo $this->Form->hidden("TransportDetail.enable", array("value"=>1))?>

<table class="form">
<tr>
    <th>日付*</th>
    <td><?php echo $this->Form->text("TransportDetail.date", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
    <th>行先*</th>
    <td><?php echo $this->Form->text('TransportDetail.place', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>    
    <th>目的*</th>
    <td><?php echo $this->Form->text('TransportDetail.purpose', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>利用交通機関*</th>
    <td><?php echo $this->Form->text('TransportDetail.transport', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>出発*</th>
    <td><?php echo $this->Form->text('TransportDetail.arrival', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr> 
<tr>   
    <th>到着*</th>
    <td><?php echo $this->Form->text('TransportDetail.departure', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>単価*</th>
    <td><?php echo $this->Form->text('TransportDetail.unit', array("class"=>"form-control input-sm number", 'style'=>SIZE_S, 'default'=>0, 'onBlur'=>'calcPrice();', 'type'=>'number'));?></td>
</tr>
<tr>
    <th>数量*</th>
    <td><?php echo $this->Form->text('TransportDetail.num', array("class"=>"form-control input-sm number", 'style'=>SIZE_S, 'default'=>1, 'onBlur'=>'calcPrice();', 'type'=>'number'));?></td>
</tr>
<tr>    
    <th>金額*</th>
    <td><?php echo $this->Form->text('TransportDetail.price', array("class"=>"form-control input-sm number", 'style'=>SIZE_S, 'default'=>0, 'onBlur'=>'calcPrice();', 'type'=>'number'));?></td>
</tr>
<tr>
    <th>客先請求*</th>
    <td><?php echo $this->Form->select('TransportDetail.customer', Configure::read("transport_customer"), array("class"=>"form-control input-sm", 'style'=>SIZE_S, 'empty'=>false));?></td>
</tr>
<tr>
    <td colspan="6" class="center">
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>        
</table>

<?php echo $this->Form->end();?>