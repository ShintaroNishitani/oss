<?php
	require_once('tcpdf/config/lang/eng.php');
	require_once('tcpdf/tcpdf.php');

	$transport_customer = Configure::read("transport_customer");

	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf->AddFont('msmincho');
	$pdf->setFontSubsetting(true);
	
	// ヘッダー、フッターなし
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);

	//塗りつぶし色指定
	$pdf->SetFillColor(220);

	$border_n = 0;
	$border_y = 1;
	$border_u = 'B';

	$fill_n = 0;
	$fill_y = 1;

	$ln_n = 0;
	$ln_y = 1;

	$reseth_n = false;
	$reseth_y = true;

	$pdf->AddPage('LANDSCAPE');

	$pdf->SetFont('msmincho', '', 14);

	$title = sprintf("交通費請求 %04d年%02d月（%s）", $datas['Transport']['year'], $datas['Transport']['month'], $datas['Staff']['name']);
	$pdf->MultiCell(100, 0, $title, $border_u, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetFont('msmincho', '', 8);

	$check = "";
	if ($confirm['Confirm']['check_staff_id']) {
		$date = explode(' ', $confirm['Confirm']['check_date']);
		$check = $date[0]."\n".$staffs[$confirm['Confirm']['check_staff_id']];
	}

	$receive = "";
	if ($confirm['Confirm']['receive_date']) {
		$receive = $confirm['Confirm']['receive_date']."\n".$staffs[$confirm['Confirm']['request_staff_id']];
	}

	$pdf->SetX(235);
	$pdf->MultiCell(25, 3.5, '承認', $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->MultiCell(25, 3.5, '受領', $border_y, 'C', $fill_y, $ln_y, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetX(235);
	$pdf->MultiCell(25, 7.5, $check, $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->MultiCell(25, 7.5, $receive, $border_y, 'C', $fill_n, $ln_y, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->Ln();

	// 詳細
	$height = 5.5;

	$s1 = 12;
	$s2 = 15;
	$m1 = 20;
	$m2 = 25;
	$l1 = 57;

	$pdf->MultiCell($s1, $height, "No", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
	$pdf->MultiCell($m1, $height, "日付", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
	$pdf->MultiCell($l1, $height, "行先", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
	$pdf->MultiCell($l1, $height, "目的", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
	$pdf->MultiCell($l1, $height, "利用交通機関", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
	$pdf->MultiCell($m1, $height, "単価", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
	$pdf->MultiCell($s1, $height, "数量", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
	$pdf->MultiCell($m1, $height, "金額", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
	$pdf->MultiCell($m1, $height, "客先請求", $border_y, 'C', $fill_y, $ln_y, '', '', $reseth_y, 0, false, true, $height, 'M');

	$height = 7.5;

	$price = 0;
	$num = 0;
	$unit = 0;
	$no = 1;
	foreach ($datas['TransportDetail'] as $data) {
		$price += $data['price'];
		$num += $data['num'];
		$unit += $data['unit'];
		$pdf->MultiCell($s1, $height, $no, $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
		$pdf->MultiCell($m1, $height, $data['date'], $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
		$pdf->MultiCell($l1, $height, $data['place'], $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
		$pdf->MultiCell($l1, $height, $data['purpose'], $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
		$transport = $data['transport'] ." " . $data['arrival'] . "-" . $data['departure'];
		$pdf->MultiCell($l1, $height, $transport, $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
		$pdf->MultiCell($m1, $height, "\\".number_format($data['unit']), $border_y, 'R', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
		$pdf->MultiCell($s1, $height, number_format($data['num']), $border_y, 'R', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
		$pdf->MultiCell($m1, $height, "\\".number_format($data['price']), $border_y, 'R', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
		$pdf->MultiCell($m1, $height, $transport_customer[$data['customer']], $border_y, 'C', $fill_n, $ln_y, '', '', $reseth_y, 0, false, true, $height, 'M');
		$no++;
	}

	// 空行
	for ($no = $no; $no <= 20; $no++) {
		$pdf->MultiCell($s1, $height, $no, $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
		$pdf->MultiCell($m1, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
		$pdf->MultiCell($l1, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
		$pdf->MultiCell($l1, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
		$pdf->MultiCell($l1, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
		$pdf->MultiCell($m1, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
		$pdf->MultiCell($s1, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
		$pdf->MultiCell($m1, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
		$pdf->MultiCell($m1, $height, "", $border_y, 'C', $fill_n, $ln_y, '', '', $reseth_y, 0, false, true, $height, 'M');
	}

	$height = 5.5;

	// 合計
	$pdf->MultiCell($s1 + $m1 + $l1 + $l1 + $l1, $height, "合計", 1, 'C', 1, 0, '', '', true, 0, false, true, $height, 'M');
	$pdf->MultiCell($m1, $height, "\\".number_format($unit), $border_y, 'R', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
	$pdf->MultiCell($s1, $height, number_format($num), $border_y, 'R', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
	$pdf->MultiCell($m1, $height, "\\".number_format($price), $border_y, 'R', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M');
	$pdf->MultiCell($m1, $height, "", $border_y, 'C', $fill_n, $ln_y, '', '', $reseth_y, 0, false, true, $height, 'M');

	// 出力
 	ob_end_clean();
  	$file_name = $title;
	$pdf->Output("$file_name.pdf", "D");

?>
