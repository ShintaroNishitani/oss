<script>
    $(function(){
        var columns = ['#HolidayDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });   
    });    
</script>

<blockquote><p>休日登録</p></blockquote>
<p class="text-info">休日情報を入力してください。</p>

<?php echo $this->Form->create('Holiday', array("name"=>"Holiday", "action" => "update",
                                                        "onsubmit" => "return updateHoliday();"))?>
<?php echo $this->Form->hidden("Holiday.id")?>
<?php echo $this->Form->hidden("Holiday.enable", array("value"=>1))?>

<table class="form">
<tr>    
    <th>名前*</th>
    <td><?php echo $this->Form->text('Holiday.name', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>日付*</th>
    <td><?php echo $this->Form->text("Holiday.date", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
    <td colspan="6" class="center">
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>        
</table>

<?php echo $this->Form->end();?>