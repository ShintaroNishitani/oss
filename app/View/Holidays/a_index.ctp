<script>

    function editHoliday(id){
        if (id == undefined) {
            id = 0;
        }
        var url = '/a/holidays/edit/' + id;
        loadBaseWindow(url);
        return false;
    }

    function deleteCost(id){
        if(window.confirm('削除しますがよろしいですか？')){
            var url = "/a/holidays/delete/" + id;
            location.replace(url);
        }
    }

    function autoSet(){
        var url = '/a/holidays/autoset/';
        loadBaseWindow(url);
        return false;
    }
</script>


<blockquote><p><?php echo $this->Html->image("holiday.png")?> 休日設定</p></blockquote>
<p class="guide"><?php echo '休日を登録してください。' ?></p>


<a href="javascript:void(0);" onclick="editHoliday();"><i class="glyphicon glyphicon-plus-sign"></i> 新規追加</a>
&nbsp;&nbsp;&nbsp;&nbsp;
<a href="javascript:void(0);" onclick="autoSet();"><i class="glyphicon glyphicon-open"></i> 自動設定</a>

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
    <th>No</th>
    <th style="min-width:200px;">名前</th>
    <th style="min-width:90px;">日付</th>
    <th></th>
</tr>
<?php
$no = 1;
foreach ($datas as $data):
    $click = sprintf(' onclick="editHoliday(%d);"', $data['Holiday']['id']);
?>
<tr>
    <td <?php echo $click;?>><?php echo h($no++)?></td>
    <td <?php echo $click;?>><?php echo h($data['Holiday']['name'])?></td>
    <td class="center" <?php echo $click;?>><?php echo h($data['Holiday']['date'])?></td>
    <td class="center">
        <a href="javascript:void(0);" onclick="deleteCost(<?php echo $data['Holiday']["id"];?>);">
        <i class="glyphicon glyphicon-remove-circle"></i></a>
    </td>
</tr>
<?php
endforeach;
?>
</table>

<?php echo $this->Element("a_pagination", array('count'=>true));?>

