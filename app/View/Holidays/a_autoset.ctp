<blockquote><p>休日自動設定</p></blockquote>
<p class="text-info">年を入力してください。</p>

<?php echo $this->Form->create('Holiday', array("name"=>"Holiday", "action" => "autoset_upload"))?>

<table class="form">
<tr>
    <th>年*</th>
    <td>
    	<?php echo $this->Form->text("Holiday.year", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S, 'default'=>$year));?>
    </td>
</tr>
<tr>
    <td colspan="6" class="center">
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>
</table>

<?php echo $this->Form->end();?>
