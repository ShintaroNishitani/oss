<div class="pagination pagination-large">
	<ul class="pagination">    
		<?php echo $this->Paginator->first(__('first')); ?>                                  
	    <?php echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a')); ?>
	    <?php echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1, 'ellipsis' => '<li class="disabled"><a>...</a></li>')); ?>                              
	    <?php echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a')); ?>
	    <?php echo $this->Paginator->last(__('last')); ?>  
	    &nbsp&nbsp&nbsp&nbsp
		<?php
			if ($count) {
			 	echo $this->Paginator->counter(array('format' => '{:page}/{:pages}ページを表示 '));
			 	echo $this->Paginator->counter(array('format' => '[全%count%件] ' ));
			}
		?>	    
	</ul>                                          
</div> 
