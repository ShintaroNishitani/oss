<script>
	$(document).ready(function(){
		loadFileList();
		<?php if($search):?>
			$("#HumanResourceId").select2();
	        $('#HumanResourceId').change( function() {
	            $('#HumanResourceName').val($('#HumanResourceId option:selected').text());
	        } );
		<?php endif;?>
		$("#HumanResourceCompanyId").select2();
	});
	$(function(){
		var columns = ['#HumanResourceBirthday'];
		$.each(columns, function(i, value) {
			$(value).datepicker({
				dateFormat: "yy-mm-dd"
			});
		});
	});
	
	
	$(function() {
		// ------------------------------------
		// 技術者検索で
		// ------------------------------------
		$('#HumanResourceId').change(function() {
			$.ajax({
				type: 'get',
				dataType: 'json',
				async: false,
				url: '/s/human_resources/search_human/',
				data: {
					'id' : $(this).val(),
				},
			}).done(function(data){
				var list = null;
				if(data.status) {
					// 成功時
					//画面の各項目に反映
					$('#HumanResourceId').val(data.result['HumanResource']['id']);
					$('#HumanResourceName').val(data.result['HumanResource']['name']);
					$('#HumanResourceSex').val(data.result['HumanResource']['sex']);
					$('#HumanResourceBirthday').val(data.result['HumanResource']['birthday']);
					$('#HumanResourceAge').val(data.result['HumanResource']['age']);
					$('#HumanResourceStation').val(data.result['HumanResource']['station']);
					$('#HumanResourceCompanyId').val(data.result['HumanResource']['company_id']);
					$("#HumanResourceCompanyId").select2();
					$('#HumanResourceSkill').val(data.result['HumanResource']['skill']);
					$('#HumanResourceExperienceYear').val(data.result['HumanResource']['experience_year']);
					$('#HumanResourcePrice').val(data.result['HumanResource']['price']);
					$('#HumanResourceDistribute').val(data.result['HumanResource']['distribute']);
					$('#HumanResourceEmploymentType').val(data.result['HumanResource']['employment_type']);
					$('#HumanResourceTermS').val(data.result['HumanResource']['term_s']);
					$('#HumanResourceTimeFromId').val(data.result['HumanResource']['time_from_id']);
					$('#HumanResourceTimeFrom').val(data.result['HumanResource']['time_from']);
					$('#HumanResourceTimeToId').val(data.result['HumanResource']['time_to_id']);
					$('#HumanResourceTimTo').val(data.result['HumanResource']['time_to']);
					$('#HumanResourceStatus').val(data.result['HumanResource']['status']);
					$('#HumanResourceRemark').val(data.result['HumanResource']['remark']);


				} else {
					// 失敗時
					//alert(data.message);
				}
			}).fail(function(data){
				// アクション側でExceptionが投げられた場合はここに来る。
				// alert('error!!!');
			});
		});
	});	

	function loadFileList(){
	<?php if (!empty($this->data['HumanResource'])) : ?>
			url = '/a/file_lists/list/human_resources/'+ <?php echo $this->data['HumanResource']['id']; ?>;
			$('#FileList').load(url);
		<?php endif; ?>
	}

	function searchHuman(){
		var url = '/s/human_resources/search_human/';
		loadBaseWindow(url);
		return false;
	}
	
	//年齢簡易計算
    function calcAge() {
    	var obj = document.getElementById('HumanResourceBirthday');
    	var birdthday = obj.value;
    	if("" == birdthday){
    		return;
    	}
    	birdthdayEdit = birdthday.replace(/-/g, "");
    	
   		//今日の日付データを変数hidukeに格納
		var hiduke=new Date(); 
		//年・月・日を取得する
		var year = hiduke.getFullYear();
		var month = hiduke.getMonth()+1;
		var day = hiduke.getDate();
		var today = (year*10000) + (month * 10) + day;

		var age = Math.round((today - birdthdayEdit)/10000);
        document.getElementById('HumanResourceAge').value = age;
    }
    
	function setAutoName() {
	
    	var obj = document.getElementById('HumanResourceName');
    	var name = obj.value;
    	//if("" == name){
	     	var obj1 = document.getElementById('HumanResourceCompanyId');
	    	var idx = obj1.selectedIndex;       //インデックス番号を取得
	  		var txt  = obj1.options[idx].text;  //ラベルを取得
	  		
	  		if("" == txt)
	  		{
	  			return;
	  		}
    		
    		var obj2 = document.getElementById('HumanResourceAge');
    		var age = obj2.value;
    		if("" != age)
    		{
    			txt = txt + "_" + age;
    		}
    		
    		
    		var obj3 = document.getElementById('HumanResourceStation');
    		var station = obj3.value;
    		if("" != station)
    		{
    			txt = txt + "_" + station;
    		}
    		
    		obj.value = txt;
    	//}
	}    
</script>

<?php echo $this->Form->hidden("HumanResource.enable", array("value"=>1))?>
<table class="form">
<tr>
	<th>技術者名*</th>
	<td>
		<?php if($search):?>
			<?php echo $this->Form->select('HumanResource.id', $humans, array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?>
			<!-- <button type="button" onclick="searchProject()" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-search"></i> 検索</button> -->
		<?php else: ?>
			<?php echo $this->Form->hidden("HumanResource.id")?>
		<?php endif;?>
		<?php echo $this->Form->text('HumanResource.name', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?>
	</td>
	<th>性別</th>
	<td>
		<?php echo $this->Form->select('HumanResource.sex', Configure::read("sex"),array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S, 'empty'=>false));?>
	</td>	
</tr>
<tr>
	<th>生年月日</th>
	<td>
		<div style="display:inline-flex">
			<?php echo $this->Form->text('HumanResource.birthday', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S, 'onBlur'=>'calcAge();'));?>
			<?php echo $this->Form->text('HumanResource.age', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
			<label class="unit">歳</label>
		</div>
	</td>
	<th>最寄り駅</th>
	<td><?php echo $this->Form->text('HumanResource.station', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
	<th>提案元企業*</th>
	<td colspan=2>
		<div style="display:inline-flex">
			<?php echo $this->Form->select('HumanResource.company_id', $companies,array("class"=>"form-control input-sm jpn", 'style'=>SIZE_L));?>
			<!-- <button type="button" onclick="searchCompany()" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-search"></i> 検索</button> -->
			<button type="button" onclick="setAutoName()" class="btn btn-xs"><i class="glyphicon glyphicon-set"></i> 技術者名作成</button>
		</div>
	</td>
</tr>
<tr>
	<th>保有スキル</th>
	<td colspan=3>
	<?php echo $this->Form->text('HumanResource.skill', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_L));?>
	</td>
</tr>
<tr>
	<th>経験年数</th>
	<td>
		<div style="display:inline-flex">
			<?php echo $this->Form->text('HumanResource.experience_year', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
			<label class="unit">年</label>
		</div>			
	</td>
</tr>
<tr>
	<th>希望単価</th>
	<td>
		<div style="display:inline-flex">
			<?php echo $this->Form->text('HumanResource.price', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
			<label class="unit">円</label>
		</div>
	</td>
	<th>商流</th>
	<td><?php echo $this->Form->text('HumanResource.distribute', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
	<th>雇用形態</th>
	<td>
		<?php echo $this->Form->select('HumanResource.employment_type', Configure::read("employment_type"),array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
	</td>
	<th>稼働日</th>
	<td><?php echo $this->Form->text('HumanResource.term_s', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?></td>
</tr>
<tr>
	<th>希望精算幅</br>（上限）</th>
	<td>
		<div style="display:inline-flex">
			<?php echo $this->Form->select('HumanResource.time_from_id', Configure::read("maxmin_time"),array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
			<?php echo $this->Form->text('HumanResource.time_from', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
			<label class="unit">H</label>
		</div>
	</td>
	<th>希望精算幅</br>（下限）</th>
	<td>
		<div style="display:inline-flex">
			<?php echo $this->Form->select('HumanResource.time_to_id', Configure::read("maxmin_time"),array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
			<?php echo $this->Form->text('HumanResource.time_to', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
			<label class="unit">H</label>
		</div>
	</td>
</tr>
<tr>
	<th>ステータス</th>
	<td>
		<?php
			echo  $this->Form->select("HumanResource.status", array(0=>'有効',1=>'終了'), array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S,));
		 ?>
	</td>
</tr>
<tr>
	<th>備考</th>
	<td colspan="3"><?php echo $this->Form->textarea("HumanResource.remark", array("empty" => false, "class"=>"form-control input-sm jpn", 'style'=>"width:500px;height:100px"));?></td>
</tr>
</table>
