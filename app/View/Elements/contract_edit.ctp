<script>
	$(document).ready(function(){
	});
	$(function(){
		var columns = ['#ContractTermS', '#ContractTermE' , '#ContractContractDate'];
		$.each(columns, function(i, value) {
			$(value).datepicker({
				dateFormat: "yy-mm-dd"
			});
		});
	});
	
    function calcPriceCommon(price, worktime) {
    	if((null == price) || (null == worktime))
    	{
    		return "";
    	}
    	if(("" == price) || ("" == worktime))
    	{
    		return "";
    	}
    	if(0 == worktime)
    	{
    		return "";
    	}

		var value = Math.round(price / worktime);
        return  Math.round(value/10)*10;
    }
    	
    function calcPriceRcvUnder() {
    	var obj = document.getElementById('ContractDetailReceiveTimeFromId');
    	var idx = obj.selectedIndex;       //インデックス番号を取得
  		//var val = obj.options[idx].value;  //value値を取得
  		var txt  = obj.options[idx].text;  //ラベルを取得
  		if("" == txt)
  		{
  			txt = document.getElementById('ContractDetailReceiveTimeFrom').value;
   		}		
        var price  = document.getElementById('ContractDetailReceivePrice').value;
		//単価/下限
		document.getElementById('ContractDetailReceiveDownPrice').value = calcPriceCommon(price, txt);
    }
    
    function calcPriceRcvOver() {
    	var obj = document.getElementById('ContractDetailReceiveTimeToId');
    	var idx = obj.selectedIndex;       //インデックス番号を取得
  		//var val = obj.options[idx].value;  //value値を取得
  		var txt  = obj.options[idx].text;  //ラベルを取得
    	if("" == txt)
  		{
  			txt = document.getElementById('ContractDetailReceiveTimeTo').value;
   		}			
        var price  = document.getElementById('ContractDetailReceivePrice').value;

		//単価/上限
		document.getElementById('ContractDetailReceiveUpPrice').value = calcPriceCommon(price, txt); 
    }
    
    function calcPriceOdrUnder() {
    	var obj = document.getElementById('ContractDetailOrderTimeFromId');
    	var idx = obj.selectedIndex;       //インデックス番号を取得
  		//var val = obj.options[idx].value;  //value値を取得
  		var txt  = obj.options[idx].text;  //ラベルを取得
    	if("" == txt)
  		{
  			txt = document.getElementById('ContractDetailOrderTimeFrom').value;
   		}	
  		
        var price  = document.getElementById('ContractDetailOrderPrice').value;
		//単価/下限
		document.getElementById('ContractDetailOrderDownPrice').value = calcPriceCommon(price, txt);
    }
    
    function calcPriceOdrOver() {
    	var obj = document.getElementById('ContractDetailOrderTimeToId');
    	var idx = obj.selectedIndex;       //インデックス番号を取得
  		//var val = obj.options[idx].value;  //value値を取得
  		var txt  = obj.options[idx].text;  //ラベルを取得
      	if("" == txt)
  		{
  			txt = document.getElementById('ContractDetailOrderTimeTo').value;
   		}			
        var price  = document.getElementById('ContractDetailOrderPrice').value;

		//単価/上限
		document.getElementById('ContractDetailOrderUpPrice').value = calcPriceCommon(price, txt); 
    }    
</script>

<table class="form">
<?php echo $this->Form->hidden("Contract.id")?>
<?php echo $this->Form->hidden("Contract.enable", array("value"=>1))?>
<?php echo $this->Form->hidden("ContractDetail.id")?>
<?php echo $this->Form->hidden("ContractDetail.enable", array("value"=>1))?>
<tr>
    <th>受注下限時間</th>
    <td>
    	<div style="display:inline-flex">
	    	<?php echo $this->Form->select('ContractDetail.receive_time_from_id', Configure::read("maxmin_time"),array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S, 'onBlur'=>'calcPriceRcvUnder();'));?>
	    	<?php echo $this->Form->text('ContractDetail.receive_time_from', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
	    	<label class="unit">H</label>
	    </div>
    </td>
    <th>受注上限時間</th>
    <td>
    	<div style="display:inline-flex">
	    	<?php echo $this->Form->select('ContractDetail.receive_time_to_id', Configure::read("maxmin_time"),array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S, 'onBlur'=>'calcPriceRcvOver();'));?>
	    	<?php echo $this->Form->text('ContractDetail.receive_time_to', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
	    	<label class="unit">H</label>
	    </div>
    </td>
    <th>受注単価</th>
    <td>
    	<div style="display:inline-flex">
    		<?php echo $this->Form->text('ContractDetail.receive_price', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S, 'onBlur'=>'calcPriceRcvUnder();calcPriceRcvOver();'));?>
    		<label class="unit">円</label>
    	</div>
    </td>
</tr>
<tr>
    <th>受注控除単価</th>
    <td>
    	<div style="display:inline-flex">
    		<?php echo $this->Form->text('ContractDetail.receive_down_price', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
    		<label class="unit">円</label>
    	</div>
    </td>
    <th>受注超過単価</th>
    <td>
    	<div style="display:inline-flex">
    		<?php echo $this->Form->text('ContractDetail.receive_up_price', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
    		<label class="unit">円</label>
    	</div>
    </td>

    <th>受注入金サイト</th>
    <td>
    	<div style="display:inline-flex">
    		<?php echo $this->Form->text('Contract.in_site', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?>
    	</div>
    </td>
</tr>
</table>
<hr>
<table class="form">
<tr>
    <th>発注下限時間</th>
    <td>
    	<div style="display:inline-flex">
	    	<?php echo $this->Form->select('ContractDetail.order_time_from_id', Configure::read("maxmin_time"),array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S, 'onBlur'=>'calcPriceOdrUnder();'));?>
	    	<?php echo $this->Form->text('ContractDetail.order_time_from', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
	    	<label class="unit">H</label>
	    </div>
    </td>
    <th>発注上限時間</th>
    <td>
    	<div style="display:inline-flex">
	    	<?php echo $this->Form->select('ContractDetail.order_time_to_id', Configure::read("maxmin_time"),array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S, 'onBlur'=>'calcPriceOdrOver();'));?>
	    	<?php echo $this->Form->text('ContractDetail.order_time_to', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
	    	<label class="unit">H</label>
	    </div>
    </td>
    <th>発注単価</th>
    <td>
    	<div style="display:inline-flex">
    		<?php echo $this->Form->text('ContractDetail.order_price', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S, 'onBlur'=>'calcPriceOdrUnder();calcPriceOdrOver();'));?>
    		<label class="unit">円</label>
    	</div>
    </td>
</tr>
<tr>
    <th>発注控除単価</th>
    <td>
    	<div style="display:inline-flex">
    		<?php echo $this->Form->text('ContractDetail.order_down_price', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
    		<label class="unit">円</label>
    	</div>
    </td>
    <th>発注超過単価</th>
    <td>
    	<div style="display:inline-flex">
    		<?php echo $this->Form->text('ContractDetail.order_up_price', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
    		<label class="unit">円</label>
    	</div>
    </td>
    <th>発注支払サイト</th>
    <td>
    	<div style="display:inline-flex">
    		<?php echo $this->Form->text('Contract.out_site', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?>
    	</div>
    </td>
</tr>
</table>
<hr>
<table class="form">
<tr>
    <th>契約形態</th>
    <td>
    	<?php echo $this->Form->select('ContractDetail.contract_type', Configure::read("contract_type"),array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
    </td>
    <th>勤怠時間精算単位</th>
    <td>
    	<div style="display:inline-flex">
    		<?php echo $this->Form->text('ContractDetail.kintai_time', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
    		<label class="unit">分</label>
    	</div>
    </td>
    <th>契約期間*</th>
    <td>
    	<div style="display:inline-flex">
	    	<?php echo $this->Form->text('Contract.term_s', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
	    	<label class="unit">～</label>
	    	<?php echo $this->Form->text('Contract.term_e', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
	    </div>
    </td>
    <th>契約日付*</th>
    <td>
		<?php echo $this->Form->text('Contract.contract_date', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
    </td>
</tr>
<tr>
    <th>備考</th>
    <td colspan=7>
   		<?php echo $this->Form->textarea('Contract.remark', array("class"=>"form-control input-sm jpn"));?>
    </td>
</tr>
</table>
