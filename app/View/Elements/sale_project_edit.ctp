<script>
	$(document).ready(function(){
		<?php if($search):?>
			$("#SaleProjectId").select2();
	        $('#SaleProjectId').change( function() {
	            $('#SaleProjectName').val($('#SaleProjectId option:selected').text());
	        } );
		<?php endif;?>
		$("#SaleProjectCompanyId").select2();
	});
	$(function(){
		var columns = ['#SaleProjectTermS', '#SaleProjectTermE'];
		$.each(columns, function(i, value) {
			$(value).datepicker({
				dateFormat: "yy-mm-dd"
			});
		});
	});
	$(function() {
		// ------------------------------------
		// 技術者検索で
		// ------------------------------------
		$('#SaleProjectId').change(function() {
			$.ajax({
				type: 'get',
				dataType: 'json',
				async: false,
				url: '/s/sale_projects/search_project/',
				data: {
					'id' : $(this).val(),
				},
			}).done(function(data){
				var list = null;
				if(data.status) {
					// 成功時

					//画面の各項目に反映
					$('#SaleProjectId').val(data.result['SaleProject']['id']);
					$('#SaleProjectName').val(data.result['SaleProject']['name']);
					$('#SaleProjectCompanyId').val(data.result['SaleProject']['company_id']);
					$("#SaleProjectCompanyId").select2();
					$('#SaleProjectContent').val(data.result['SaleProject']['content']);
					$('#SaleProjectLanguage').val(data.result['SaleProject']['language']);
					$('#SaleProjectProcessS').val(data.result['SaleProject']['process_s']);
					$('#SaleProjectProcessE').val(data.result['SaleProject']['process_e']);
					$('#SaleProjectTermId').val(data.result['SaleProject']['term_id']);
					if(1 == data.result['SaleProject']['extension'])
					{
						$('#SaleProjectExtension').prop('checked',true);
					}
					$('#SaleProjectTermS').val(data.result['SaleProject']['term_s']);
					$('#SaleProjectTermE').val(data.result['SaleProject']['term_e']);
					if(1 == data.result['SaleProject']['skill_level'])
					{
						$('#SaleProjectSkillLevel').prop('checked',true);
					}
					$('#SaleProjectPrice').val(data.result['SaleProject']['price']);
					$('#SaleProjectPlace').val(data.result['SaleProject']['place']);
					$('#SaleProjectRequiredSkill').val(data.result['SaleProject']['required_skill']);
					$('#SaleProjectPlusSkill').val(data.result['SaleProject']['plus_skill']);
					$('#SaleProjectOs').val(data.result['SaleProject']['os']);
					$('#SaleProjectDb').val(data.result['SaleProject']['db']);
					$('#SaleProjectFw').val(data.result['SaleProject']['fw']);
					$('#SaleProjectTimeFromId').val(data.result['SaleProject']['time_from_id']);
					$('#SaleProjectTimeFrom').val(data.result['SaleProject']['time_from']);
					$('#SaleProjectTimeToId').val(data.result['SaleProject']['time_to_id']);
					$('#SaleProjectTimeTo').val(data.result['SaleProject']['time_to']);
					$('#SaleProjectNum').val(data.result['SaleProject']['num']);
					$('#SaleProjectInterview').val(data.result['SaleProject']['interview']);
					$('#SaleProjectStatus').val(data.result['SaleProject']['status']);
					$('#SaleProjectRemark').val(data.result['SaleProject']['remark']);
										
					var ohf = data.result['SaleProject']['office_hour_from'];
					var ary = ohf.split(":"); 
					$('#SaleProjectOfficeHourFromHour').val(ary[0]);
					$('#SaleProjectOfficeHourFromMin').val(ary[1]);

					var oht = data.result['SaleProject']['office_hour_to'];
					ary = oht.split(":"); 
					$('#SaleProjectOfficeHourToHour').val(ary[0]);
					$('#SaleProjectOfficeHourToMin').val(ary[1]);
					
					var btf = data.result['SaleProject']['break_time_from'];
					ary = btf.split(":"); 
					$('#SaleProjectBreakTimeFromHour').val(ary[0]);
					$('#SaleProjectBreakTimeFromMin').val(ary[1]);
					
					var btt = data.result['SaleProject']['break_time_to'];
					ary = btt.split(":"); 
					$('#SaleProjectBreakTimeToHour').val(ary[0]);
					$('#SaleProjectBreakTimeToMin').val(ary[1]);
					
				} else {
					// 失敗時
					//alert(data.message);
				}
			}).fail(function(data){
				// アクション側でExceptionが投げられた場合はここに来る。
				// alert('error!!!');
			});
		});
	});		
	function searchProject(){
		var url = '/s/sale_projects/search_project/';
		loadBaseWindow(url);
		return false;
	}
</script>

<?php echo $this->Form->hidden("SaleProject.enable", array("value"=>1))?>
<table class="form">
<tr>
	<th>受注企業*</th>
	<td colspan=3>
		<div style="display:inline-flex">
			<?php echo $this->Form->select('SaleProject.company_id', $companies,array("class"=>"form-control input-sm jpn", 'style'=>SIZE_L));?>
			<!-- <button type="button" onclick="searchCompany()" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-search"></i> 検索</button> -->
		</div>
	</td>
</tr>
<tr>
	<th>案件名*</th>
	<td colspan=3>
		<?php if($search):?>
			<?php echo $this->Form->select('SaleProject.id', $projects, array("class"=>"form-control input-sm jpn", 'style'=>SIZE_L));?>
			<!-- <button type="button" onclick="searchProject()" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-search"></i> 検索</button> -->
		<?php else:?>
			<?php echo $this->Form->hidden("SaleProject.id")?>
		<?php endif;?>
		<?php echo $this->Form->text('SaleProject.name', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_L));?>
	</td>
</tr>
<tr>
	<th>作業内容</th>
	<td colspan=3><?php echo $this->Form->text("SaleProject.content", array("class"=>"form-control input-sm", 'style'=>SIZE_LL));?></td>
</tr>
<tr>
	<th>言語</th>
	<td><?php echo $this->Form->text('SaleProject.language', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
	<th>作業工程</th>
	<td>
		<div style="display:inline-flex">
			<?php echo $this->Form->select('SaleProject.process_s', Configure::read("work_proc"),array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
			<label class="unit">～</label>
			<?php echo $this->Form->select('SaleProject.process_e', Configure::read("work_proc"),array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
		</div>
	</td>
</tr>
<tr>
	<th>期間</th>
	<td colspan="3">
		<div style="display:inline-flex">
			<?php echo $this->Form->select('SaleProject.term_id', Configure::read("work_term"),array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
			<?php echo $this->Form->checkbox('SaleProject.extension', array('options' => Array("0"=>""),'label'=>"false", 'div'=>false)); ?>
			<label class="unit">延長可能性有</label>
			<?php echo $this->Form->text('SaleProject.term_s', array("class"=>"form-control input-sm", 'style'=>SIZE_S));?>
			<label class="unit">～</label>
			<?php echo $this->Form->text('SaleProject.term_e', array("class"=>"form-control input-sm", 'style'=>SIZE_S));?>
		</div>
	</td>
	<th>予算</th>
	<td colspan=3>
		<div style="display:inline-flex">
			<?php echo $this->Form->checkbox('SaleProject.skill_level', array('options' => Array("0"=>""),'label'=>"false", 'div'=>false)); ?>
			<label class="unit">スキル見合い</label>
			<?php echo $this->Form->text('SaleProject.price', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
		</div>
	</td>	
</tr>
<tr>	
	<th>作業場所</th>
	<td colspan=3><?php echo $this->Form->text("SaleProject.place", array("class"=>"form-control input-sm jpn", 'style'=>SIZE_LL));?></td>
</tr>
<tr>
	<th>必須スキル</th>
	<td><?php echo $this->Form->text('SaleProject.required_skill', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
	<th>尚可スキル</th>
	<td><?php echo $this->Form->text('SaleProject.plus_skill', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
	<th>OS</th>
	<td><?php echo $this->Form->text('SaleProject.os', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
	<th>DB</th>
	<td><?php echo $this->Form->text('SaleProject.db', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
	<th>FW</th>
	<td colspan="3"><?php echo $this->Form->text('SaleProject.fw', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
	<th>精算幅</br>（上限）</th>
	<td>
		<div style="display:inline-flex">
			<?php echo $this->Form->select('SaleProject.time_from_id', Configure::read("maxmin_time"),array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
			<?php echo $this->Form->text('SaleProject.time_from', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
			<label class="unit">H</label>
		</div>
	</td>
	<th>精算幅</br>（下限）</th>
	<td>
		<div style="display:inline-flex">
			<?php echo $this->Form->select('SaleProject.time_to_id', Configure::read("maxmin_time"),array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
			<?php echo $this->Form->text('SaleProject.time_to', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
			<label class="unit">H</label>
		</div>
	</td>
	<th>募集人数</th>
	<td>
		<div style="display:inline-flex">
			<?php echo $this->Form->text('SaleProject.num', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
			<label class="unit">人</label>
		</div>
	</td>
	<th>面談回数</th>
	<td>
		<div style="display:inline-flex">
			<?php echo $this->Form->text('SaleProject.interview', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?>
			<label class="unit">回</label>
		</div>
	</td>
</tr>
<tr>
	<th>勤務時間</th>
	<td>
		<div style="display:inline-flex">
			<?php echo $this->Form->input('SaleProject.office_hour_from', array('type'=>'time', 'label'=>false, 'dateFormat'=>'NONE', 'timeFormat'=>'24', 'empty'=>true, 'interval'=>15,));?>
			<label class="unit">～</label>
			<?php echo $this->Form->input('SaleProject.office_hour_to', array('type'=>'time', 'label'=>false, 'dateFormat'=>'NONE', 'timeFormat'=>'24', 'empty'=>true, 'interval'=>15,));?>
		</div>
	</td>
	<th>昼休憩</th>
	<td>
		<div style="display:inline-flex">
			<?php echo $this->Form->input('SaleProject.break_time_from', array('type'=>'time', 'label'=>false, 'dateFormat'=>'NONE', 'timeFormat'=>'24', 'empty'=>true, 'interval'=>15,));?>
			<label class="unit">～</label>
			<?php echo $this->Form->input('SaleProject.break_time_to', array('type'=>'time', 'label'=>false, 'dateFormat'=>'NONE', 'timeFormat'=>'24', 'empty'=>true, 'interval'=>15,));?>
		</div>
	</td>
</tr>
<tr>
	<th>備考</th>
	<td colspan="3"><?php echo $this->Form->textarea("SaleProject.remark", array("empty" => false, "class"=>"form-control input-sm jpn", 'style'=>"width:500px;height:100px"));?></td>
</tr>
<tr>
	<th>ステータス</th>
	<td>
		<?php
			echo  $this->Form->select("SaleProject.status", array(0=>'有効',1=>'終了'), array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M,));
		 ?>
	</td>
</tr>
</table>
