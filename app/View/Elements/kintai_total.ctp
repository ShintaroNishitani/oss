<?php
	$over_time_60h = $over_time > 60 ? $over_time - 60 : 0;
?>
<script type="text/javascript">
	$(function () {
		$('[data-toggle="tooltip"]').tooltip();
	})
</script>

<div class="center">
	<tr>
		<td width=30px></td>
		<td valign="top">
			<table class="table-bordered table-condensed">
				<tr>
			        <th colspan="12" class="center">年間情報</th>
    			</tr>
    			<tr>
	        		<th>有休残日数 [日]</th>
        			<td>
	            		<?php echo $this->Form->text("PaidHoliday",
                			array("class"=>"form-control input-sm number", "style"=>SIZE_XS, "readonly"=>"readonly",
								"value"=>sprintf("%.01f", $paid_remain)))?>
        			</td>
        			<th>振休残日数 [日]</th>
        			<td>
            			<?php echo $this->Form->text("TransferHoliday",
	                		array("class"=>"form-control input-sm number", "style"=>SIZE_XS, "readonly"=>"readonly",
                    			"value"=>sprintf("%.01f", $trans_remain)))?>
        			</td>
        			<th>特休残日数 [日]</th>
        			<td>
	            		<?php echo $this->Form->text("SpecialHoliday",
                			array("class"=>"form-control input-sm number", "style"=>SIZE_XS, "readonly"=>"readonly",
	                    		"value"=>$special_remain))?>
        			</td>
        			<th>半休残数</th>
        			<td>
	            		<?php echo $this->Form->text("HalfHolidayNum",
                			array("class"=>"form-control input-sm number", "style"=>SIZE_XS, "readonly"=>"readonly",
							"value"=>$half_remain))?>
        			</td>
        			<th>年間有休取得日数 [日]</th>
        			<td>
	            		<?php echo $this->Form->text("PaidHolidayDigest",
                			array("class"=>"form-control input-sm number", "style"=>SIZE_XS, "readonly"=>"readonly",
	                    		"value"=>sprintf("%.01f", $paid_digest)))?>
        			</td>
    			</tr>
				<tr>
    			    <th colspan="12" class="center">今月度合計</th>
    			</tr>
    			<tr>
        			<th>営業日 [日]</th>
					<td>
			            <?php echo $this->Form->text("SumBusinessDay",
        			        array("class"=>"form-control input-sm number", "style"=>SIZE_XS, "readonly"=>"readonly",
            			        "value"=>$business_day))?>
        			</td>
					<th>実働時間 [h]</th>
			        <td>
            			<?php echo $this->Form->text("SumWorkTime",
                			array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly",
                   				"value"=>sprintf("%.01f", $work_time)))?>
        			</td>
        			<th>深夜時間 [h]</th>
        			<td>
            			<?php echo $this->Form->text("SumLateTime",
                			array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly",
                    			"value"=>sprintf("%.01f", $late_time)))?>
        			</td>
        			<th>超勤時間 [h]</th>
        			<td>
            			<?php echo $this->Form->text("SumOverTime",
                			array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly",
                    			"value"=>sprintf("%.01f", $over_time)))?>
        			</td>
        			<th>年休取得日数 [日]</th>
        			<td>
            			<?php echo $this->Form->text("SumHoliday",
                			array("class"=>"form-control input-sm number", "style"=>SIZE_XS, "readonly"=>"readonly",
                    			"value"=>sprintf("%.01f", $holiday)))?>
        			</td>
					<?php if ($over_time_60h > 0): ?>
        			<th style="color:red;">超勤60H超分 [h]</th>
        			<td>
            			<?php echo $this->Form->text("SumOverTime",
                			array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly",
                    			"value"=>sprintf("%.01f", $over_time_60h)))?>
        			</td>
					<?php endif; ?>
    			</tr>
    			<tr>
        			<th colspan="12" class="center">36協定関連</th>
    			</tr>
    			<tr>
					<th>36協定<br>超勤時間 [h]
						<a href="#" data-toggle="tooltip" data-original-title="法定休日(土日祝)の実働時間を加えた法廷時間外の実働時間"><?php echo $this->Html->image("info.png")?></a>
					</th>
        			<td>
            			<?php echo $this->Form->text("SumExtraTime",
                			array("class"=>"form-control input-sm number", "style"=>SIZE_XS, "readonly"=>"readonly",
                    			"value"=>sprintf("%.01f", $extra_time)))?>
        			</td>
        			<th>超勤上限<br>超過回数
						<a href="#" data-toggle="tooltip" data-original-title="年間に時間外労働が月45時間を超えた回数、許容は年間で６回まで"><?php echo $this->Html->image("info.png")?></a>
					</th>
        			<td>
            			<?php echo $this->Form->text("SumExtraTime",
                			array("class"=>"form-control input-sm number", "style"=>SIZE_XS, "readonly"=>"readonly",
                    			"value"=>$over_count))?>
        			</td>
        			<th>当月上限<br>超勤時間 [h]
						<a href="#" data-toggle="tooltip" data-original-title="2ヶ月ないし6ヶ月の時間外・休日労働時間の平均を月80時間以内かつ100時間未満とするための当月の上限超勤時間"><?php echo $this->Html->image("info.png")?></a>
					</th>
        			<td>
            			<?php echo $this->Form->text("SumExtraTime",
                			array("class"=>"form-control input-sm number", "style"=>SIZE_XS, "readonly"=>"readonly",
                    			"value"=>$limit_time))?>
        			</td>
    			</tr>
			</table>
		</td>
		<td width=30px></td>
	</tr>
</div>
