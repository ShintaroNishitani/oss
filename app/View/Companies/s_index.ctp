<script>
	$(function(){
		var columns = ['#CompanyScTermS', '#CompanyScTermE'];
		$.each(columns, function(i, value) {
			$(value).datepicker({
				dateFormat: "yy-mm-dd"
			});
		});

	});

	function editCompany(id) {
		if (id == undefined) {
			id = 0;
		}
		var url = '/s/companies/edit/' + id ;
		loadBaseWindow(url);
		return false;
	}


	function updateCompany() {
		var url = "/s/companies/update/";
		updateData(url, "CompanyUpdateForm", null, function(){
			unloadAll();
			location.href = "/s/companies/index/";
		});

		return false;
	}

	function deleteCompany(id) {
		if (id != undefined) {
			if (confirm('削除してもよろしいですか？\nこの操作は元に戻せません。')) {
				var url = "/s/companies/delete/" + id + "/;?>";
				updateData(url, null, null, function(){
					location.href = "/s/companies/index/";
				});
			}
		}
		return false;
	}


</script>


<blockquote><p><?php echo $this->Html->image("job.png")?> <?php echo h('案件管理')?></p></blockquote>

<ul class="nav nav-tabs" role="tablist">
	<li><a href="/s/sale_projects/index/"><?php echo h('案件一覧');?></a></li>
	<li><a href="/s/human_resources/index/"><?php echo h('人材一覧');?></a></li>
	<li><a href="/s/contracts/index/"><?php echo h('契約一覧');?></a></li>
	<li class="active"><a href="/s/companies/index/"><?php echo h('企業一覧');?></a></li>
	<li><a href="/s/contracts/result_index/"><?php echo h('精算一覧');?></a></li>	
</ul>

</br></br>

<TABLE border="0">
<TBODY>
<TR>
<TD valign="top">

<?php echo $this->Form->create("Company", array('url' => array('action' => 'index'), 'type'=>'get'))?>
<table class="table-condensed well">
	<tr>
		<th style="min-width:50px;">企業名</th>
		<td><?php echo $this->Form->text("Company.sc_name", array("class" => "form-control input-sm jpn", 'style'=>SIZE_M, "value"=>$keys['name'][0]));?></td>
	</tr>
	<tr>
		<td class="center" colspan="4">
			<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> 検索</button>
			<button type="submit" class="btn btn-primary" name="csv"><i class="glyphicon glyphicon-download"></i> CSV出力</button>
		</td>
	</tr>
</table>
<?php echo $this->Form->end()?>

</TD>
<TD width=30px>
</TD>
<TD valign="top">

</TD>
</TR>
</TBODY>
</TABLE>

<a href="javascript:void(0);" onclick="editCompany();"><i class="glyphicon glyphicon-plus-sign"></i> 新規追加</a>

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
	<th>No</th>
	<th style="min-width:200px;">企業名</th>
	<th style="min-width:100px;">郵便番号</th>
	<th style="min-width:200px;">住所</th>
	<th style="min-width:50px;">FAX</th>
	<th style="min-width:70px;">TEL</th>
	<th style="min-width:50px;">支社</th>
	<th style="min-width:170px;">顧客</th>
	<th style="min-width:110px;">登録番号</th>
	<th style="min-width:70px;">手数料</th>
	<th style="min-width:70px;">振込先銀行</th>
	<th style="min-width:70px;">支店</th>
	<th style="min-width:70px;">口座番号</th>
	<th style="min-width:70px;">企業名</th>
	<th style="min-width:70px;">口座種別</th>
	<th style="min-width:70px;">少数点計算</th>
	<th style="min-width:70px;">営業担当氏名</th>
	<th style="min-width:70px;">営業担当電話番号</th>
	<th style="min-width:70px;">営業担当メールアドレス</th>
	<th style="min-width:70px;">事務担当氏名</th>
	<th style="min-width:70px;">事務担当電話番号</th>
	<th style="min-width:70px;">事務担当メールアドレス</th>
	<th style="min-width:200px;">備考</th>
	<th>&nbsp;</th>
</tr>
<?php
$num = 1;
foreach ($datas as $data):
	$click = sprintf(' onclick="editCompany(%d);"', $data['Company']['id']);
?>
<tr>
	<td <?php echo $click;?>><?php echo h($num++)?></td>
	<td <?php echo $click;?>><?php echo h($data['Company']['name'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Company']['zip1']."-".$data['Company']['zip2'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Company']['address1'].$data['Company']['address2'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Company']['tel'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Company']['fax'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Company']['office'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Company']['customer'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Company']['regist_no'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Company']['fee'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Company']['bank'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Company']['branch'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Company']['acc_number'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Company']['acc_name'])?></td>
	<td class="center" <?php echo $click;?>>
		<?php
			$items = Configure::read("acc_type");
			echo h($items[$data['Company']['acc_type']]);
		?>
	</td>
	<td class="center" <?php echo $click;?>>
		<?php
			$items = Configure::read("calc");
			echo h($items[$data['Company']['calc']]);
		?>
	</td>
	<td <?php echo $click;?>><?php echo h($data['Company']['sales_name'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Company']['sales_tel'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Company']['sales_mail'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Company']['clerk_name'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Company']['clerk_tel'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Company']['clerk_mail'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Company']['remark'])?></td>
	<td class="center">
		<a href="javascript:void(0);" onclick="deleteCompany(<?php echo $data['Company']["id"];?>);">
		<i class="glyphicon glyphicon-remove-circle"></i></a>
	</td>
</tr>
<?php endforeach; ?>
</table>

<?php echo $this->Element("a_pagination", array('count'=>true));?>

