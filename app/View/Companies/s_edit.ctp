<script>
    $(function(){


    });
</script>
<blockquote><p><?php echo h('企業登録');?></p></blockquote>
<p class="guide"><?php echo '企業情報を入力してください。' ?></p>

<?php echo $this->Form->create('Company', array("action" => "update", "onsubmit" => "return updateCompany();"))?>
<?php echo $this->Form->hidden("Company.id")?>
<?php echo $this->Form->hidden("Company.enable", array("value"=>1))?>
<table class="form">
<tr>
    <th>企業名*</th>
    <td><?php echo $this->Form->text("Company.name", array("class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
    <th>少数点計算</th>
    <td>
        <?php
            echo  $this->Form->select("Company.calc", Configure::read("calc"), array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M,));
         ?>
    </td>
</tr>
<tr>
    <th>郵便番号</th>
    <td colspan="3">
        <div style="display:inline-flex">
        <?php echo $this->Form->text("Company.zip1", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?>
        <label class="unit">-</label>
        <?php echo $this->Form->text("Company.zip2", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?>
        </div>
    </td>
</tr>
<tr>
    <th>住所１</th>
    <td><?php echo $this->Form->text("Company.address1", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_L));?></td>
    <th>住所２</th>
    <td><?php echo $this->Form->text("Company.address2", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_L));?></td>
</tr>
<tr>
    <th>TEL</th>
    <td><?php echo $this->Form->text("Company.tel", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
    <th>FAX</th>
    <td><?php echo $this->Form->text("Company.fax", array("class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>支社</th>
    <td><?php echo $this->Form->text("Company.office", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
    <th>顧客</th>
    <td><?php echo $this->Form->text("Company.customer", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
    <th>登録番号</th>
    <td><?php echo $this->Form->text("Company.regist_no", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>手数料</th>
    <td><?php echo $this->Form->text("Company.fee", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
    <th>振込先銀行</th>
    <td><?php echo $this->Form->text("Company.bank", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
    <th>支店</th>
    <td><?php echo $this->Form->text("Company.branch", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>口座番号</th>
    <td><?php echo $this->Form->text("Company.acc_number", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
    <th>企業名</th>
    <td><?php echo $this->Form->text("Company.acc_name", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
    <th>口座種別</th>
    <td>
        <?php
            echo  $this->Form->select("Company.acc_type", Configure::read("acc_type"), array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M,));
         ?>
    </td>
<tr>
    <th>営業担当氏名</th>
    <td><?php echo $this->Form->text("Company.sales_name", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
    <th>営業担当電話番号</th>
    <td><?php echo $this->Form->text("Company.sales_tel", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
    <th>営業担当メールアドレス</th>
    <td><?php echo $this->Form->text("Company.sales_mail", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>事務担当氏名</th>
    <td><?php echo $this->Form->text("Company.clerk_name", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
    <th>事務担当電話番号</th>
    <td><?php echo $this->Form->text("Company.clerk_tel", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
    <th>事務担当メールアドレス</th>
    <td><?php echo $this->Form->text("Company.clerk_mail", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>234
    <th>備考</th>
    <td colspan="5"><?php echo $this->Form->textarea("Company.remark", array("empty" => false, "class"=>"form-control input-sm jpn", 'style'=>"width:500px;height:100px"));?></td>
</tr>
<tr>
    <td colspan="2" class="center">
    	<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>
</table>
<?php echo $this->Form->end();?>
