
<blockquote><p><?php echo h('案件登録');?></p></blockquote>
<p class="guide"><?php echo '案件情報を入力してください。' ?></p>

<?php echo $this->Form->create('SaleProject', array('url' => array( 'action' => 'update'), "onsubmit" => "return updateSaleProject();"))?>

<?php echo $this->Element("sale_project_edit", array('search'=>false));?>
<div class="center">
	<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
</div>
<?php echo $this->Form->end();?>
