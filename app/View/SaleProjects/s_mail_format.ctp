
<?php if($data['SaleProject']['name']):?>
■案件名
<?php echo h($data['SaleProject']['name'])?>
</br>
<?php endif;?>
<?php if($data['SaleProject']['content']):?>
■作業内容
<?php echo h($data['SaleProject']['content'])?>
</br>
<?php endif;?>
<?php if($data['SaleProject']['process_s'] != "" || $data['SaleProject']['process_e'] != ""):?>
■作業工程
<?php
	$items = Configure::read("work_proc");
	$val = "";
	if ($data['SaleProject']['process_s'] != "") {
		$val .= $items[$data['SaleProject']['process_s']] . "~";
	}
	if ($data['SaleProject']['process_e'] != "") {
		$val .= $items[$data['SaleProject']['process_e']];
	}
	echo h($val);
?>
</br>
<?php endif;?>
<?php if($data['SaleProject']['place']):?>
■作業場所
<?php echo h($data['SaleProject']['place'])?>
</br>
<?php endif;?>
■期間
<?php
	$val = $data['SaleProject']['term_s']."~".$data['SaleProject']['term_e'];
	if ($data['SaleProject']['term_id'] != "") {
		$items = Configure::read("work_term");
		$val .= " ". $items[$data['SaleProject']['term_id']];
	}
	if ($data['SaleProject']['extension']) {
		$val .= " 延長可能性有";
	}
	echo h($val);
?>
</br>
<?php if($data['SaleProject']['required_skill']):?>
■必須スキル
<?php echo h($data['SaleProject']['required_skill'])?>
</br>
<?php endif;?>
<?php if($data['SaleProject']['plus_skill']):?>
■尚可スキル
<?php echo h($data['SaleProject']['plus_skill'])?>
</br>
<?php endif;?>
<?php if($data['SaleProject']['language']):?>
■言語
<?php echo h($data['SaleProject']['language'])?>
</br>
<?php endif;?>
<?php if($data['SaleProject']['os']):?>
■OS
<?php echo h($data['SaleProject']['os'])?>
</br>
<?php endif;?>
<?php if($data['SaleProject']['db']):?>
■DB
<?php echo h($data['SaleProject']['db'])?>
</br>
<?php endif;?>
<?php if($data['SaleProject']['fw']):?>
■FW
<?php echo h($data['SaleProject']['fw'])?>
</br>
<?php endif;?>
<?php if($data['SaleProject']['price'] || $data['SaleProject']['skill_level']):?>
■予算
<?php
	$val = $data['SaleProject']['price'];
	if ($data['SaleProject']['skill_level']) {
		$val = "スキル見合い";
	}
	echo h($val);
?>
</br>
<?php endif;?>
■精算幅
<?php
	$items = Configure::read("maxmin_time");
	$val = "";
	if ($data['SaleProject']['time_from_id'] != "") {
		$val .= $items[$data['SaleProject']['time_from_id']];
	} else {
		$val .= $data['SaleProject']['time_from'];
	}
	$val .= "~";

	if ($data['SaleProject']['time_to_id'] != "") {
		$val .= $items[$data['SaleProject']['time_to_id']];
	} else {
		$val .= $data['SaleProject']['time_to'];
	}
	echo h($val);
?>
</br>
<?php if($data['SaleProject']['num']):?>
■募集人数
<?php echo h($data['SaleProject']['num']."人")?>
</br>
<?php endif;?>
<?php if($data['SaleProject']['interview']):?>
■面接回数
<?php echo h($data['SaleProject']['interview']."回")?>
</br>
<?php endif;?>
<?php if($data['SaleProject']['office_hour_from'] || $data['SaleProject']['office_hour_to']):?>
■勤務時間
<?php
	$val = "";
	if ($data['SaleProject']['office_hour_from']) {
		$val.=date('H:i', strtotime($data['SaleProject']['office_hour_from']));
		$val.="~";
	}
	if ($data['SaleProject']['office_hour_to']) {
		$val.=date('H:i', strtotime($data['SaleProject']['office_hour_to']));
	}
	echo h($val);
?>
</br>
<?php endif;?>
<?php if($data['SaleProject']['break_time_from'] || $data['SaleProject']['break_time_to']):?>
■休憩時間
<?php
	$val = "";
	if ($data['SaleProject']['break_time_from']) {
		$val.=date('H:i', strtotime($data['SaleProject']['break_time_from']));
		$val.="~";
	}
	if ($data['SaleProject']['break_time_to']) {
		$val.=date('H:i', strtotime($data['SaleProject']['break_time_to']));
	}
	echo h($val);
?>
</br>
<?php endif;?>
<?php if($data['SaleProject']['remark']):?>
■備考
<?php echo h($data['SaleProject']['remark'])?>
<?php endif;?>
