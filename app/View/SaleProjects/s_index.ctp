<script>
	$(function(){
		var columns = ['#SaleProjectScTermS', '#SaleProjectScTermE'];
		$.each(columns, function(i, value) {
			$(value).datepicker({
				dateFormat: "yy-mm-dd"
			});
		});

	});

	function editSaleProject(id) {
		if (id == undefined) {
			id = 0;
		}
		var url = '/s/sale_projects/edit/' + id ;
		loadBaseWindow(url);
		return false;
	}


	function updateSaleProject() {
		var url = "/s/sale_projects/update/";
		updateData(url, "SaleProjectUpdateForm", null, function(){
			unloadAll();
			location.href = "/s/sale_projects/index/";
		});

		return false;
	}

	function deleteSaleProject(id) {
		if (id != undefined) {
			if (confirm('削除してもよろしいですか？\nこの操作は元に戻せません。')) {
				var url = "/s/sale_projects/delete/" + id + "/;?>";
				updateData(url, null, null, function(){
					location.href = "/s/sale_projects/index/";
				});
			}
		}
		return false;
	}

	function mail_format(id) {
		var url = '/s/sale_projects/mail_format/' + id ;
		loadBaseWindow(url);
		return false;
	}
</script>


<blockquote><p><?php echo $this->Html->image("job.png")?> <?php echo h('案件管理')?></p></blockquote>

<ul class="nav nav-tabs" role="tablist">
	<li class="active"><a href="/s/sale_projects/index/"><?php echo h('案件一覧');?></a></li>
	<li><a href="/s/human_resources/index/"><?php echo h('人材一覧');?></a></li>
	<li><a href="/s/contracts/index/"><?php echo h('契約一覧');?></a></li>
	<li><a href="/s/companies/index/"><?php echo h('企業一覧');?></a></li>
	<li><a href="/s/contracts/result_index/"><?php echo h('精算一覧');?></a></li>	
</ul>

</br></br>

<p class="guide"><?php echo '終了している案件は赤で色付けされます' ?></p>

<TABLE border="0">
<TBODY>
<TR>
<TD valign="top">

<?php echo $this->Form->create("SaleProject", array('url' => array('action' => 'index'), 'type'=>'get'))?>
<table class="table-condensed well">
	<tr>
		<th style="min-width:50px;">案件名</th>
		<td><?php echo $this->Form->text("SaleProject.sc_name", array("class" => "form-control input-sm jpn", 'style'=>SIZE_M, "value"=>$keys['name'][0]));?></td>
		<th style="min-width:50px;">受注企業</th>
		<td><?php echo $this->Form->text("SaleProject.sc_company", array("class" => "form-control input-sm jpn", 'style'=>SIZE_M, "value"=>$keys['company'][0]));?></td>
	</tr>
	<tr>
		<th style="min-width:50px;">必須スキル</th>
		<td><?php echo $this->Form->text("SaleProject.sc_required_skill", array("class" => "form-control input-sm", 'style'=>SIZE_M, "value"=>$keys['required_skill'][0]));?></td>
		<th style="min-width:70px;">ステータス</th>
		<td><?php echo  $this->Form->select("SaleProject.sc_status", array(0=>'有効',1=>'終了'), array("class" => "form-control input-sm", 'style'=>SIZE_S, "value"=>$keys['status'][0]));?></td>
<!--         <th style="min-width:50px;">期間</th>
		<td>
			<div style="display:inline-flex">
			<?php echo $this->Form->text("SaleProject.sc_term_s", array("class" => "form-control input-sm", 'style'=>SIZE_S, "value"=>$keys['term_s'][0]));?>
			～
			<?php echo $this->Form->text("SaleProject.sc_term_e", array("class" => "form-control input-sm", 'style'=>SIZE_S, "value"=>$keys['term_s'][0]));?>
			</div>
		</td>     -->
	</tr>
	<tr>
		<td class="center" colspan="4">
			<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> 検索</button>
			<!-- <button type="submit" class="btn btn-primary" name="csv"><i class="glyphicon glyphicon-download"></i> CSV出力</button> -->
		</td>
	</tr>
</table>
<?php echo $this->Form->end()?>

</TD>
<TD width=30px>
</TD>
<TD valign="top">
<!-- <div class="well">
<?php
	echo $this->Form->create("SaleProject",
							 array("style"=>"font-size:12px;",
								   'name'=>'input',
								   "action"=>"upload",
								   "type" =>"file",
								   "onsubmit"=>"return beforeFileCheck();"));

?>
<p class="text-info">案件データの自動取込が出来ます。 <a href="/format/project_format.csv" download="案件データフォーマット.csv" target="_blank" style="text-decoration: underline;";><i class="glyphicon glyphicon-download"></i> フォーマットダウンロード</a</p>
<?php echo $this->Form->input('SaleProject.file', array("type"=>"file", "value"=>"", 'label'=>false, 'width'=>40,'height'=>20, "style" => "font-size:12px;"))?>
</br>
<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-upload"></i> アップロード</button>
<?php echo $this->Form->end();?>
</div> -->
</TD>
</TR>
</TBODY>
</TABLE>

<a href="javascript:void(0);" onclick="editSaleProject();"><i class="glyphicon glyphicon-plus-sign"></i> 新規追加</a>

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
	<th>No</th>
	<th>&nbsp;</th>
	<th style="min-width:200px;">案件名</th>
	<th style="min-width:200px;">作業内容</th>
	<th style="min-width:120px;">作業工程</th>
	<th style="min-width:110px;">作業場所</th>
	<th style="min-width:170px;">期間</th>
	<th style="min-width:200px;">必須スキル</th>
	<th style="min-width:200px;">尚可スキル</th>
	<th style="min-width:100px;">言語</th>
	<th style="min-width:100px;">OS</th>
	<th style="min-width:100px;">DB</th>
	<th style="min-width:100px;">FW</th>
	<th style="min-width:50px;">予算</th>
	<th style="min-width:100px;">精算幅</th>
	<th style="min-width:50px;">募集人数</th>
	<th style="min-width:70px;">面接回数</th>
	<th style="min-width:70px;">勤務時間</th>
	<th style="min-width:70px;">休憩時間</th>
	<th style="min-width:100px;">受注企業</th>
	<th style="min-width:200px;">備考</th>
	<th style="min-width:40px;">契約<br/>登録</th>
	<th>&nbsp;</th>
</tr>
<?php
$num = 1;
foreach ($datas as $data):
	$click = sprintf(' onclick="editSaleProject(%d);"', $data['SaleProject']['id']);
	$id = $data['SaleProject']['id'];
	$style="";
	if ($data['SaleProject']['status']) {
		$style = "background-color: #f2dede;";
	}
?>
<tr style="<?php echo $style; ?>">
	<td <?php echo $click;?>><?php echo h($num++)?></td>
	<td class="center">
		<?php echo $this->Form->button("メール",array("class"=>"btn  btn-success btn-sm", 'type'=>'button', 'onclick'=>"mail_format($id)"));?>
	</td>
	<td <?php echo $click;?>><?php echo h($data['SaleProject']['name'])?></td>
	<td <?php echo $click;?>><?php echo h($data['SaleProject']['content'])?></td>
	<td <?php echo $click;?>>
		<?php
			$items = Configure::read("work_proc");
			$val = "";
			if ($data['SaleProject']['process_s'] != "") {
				$val .= $items[$data['SaleProject']['process_s']] . "~";
			}
			if ($data['SaleProject']['process_e'] != "") {
				$val .= $items[$data['SaleProject']['process_e']];
			}
			echo h($val);
		?>
	</td>
	<td <?php echo $click;?>><?php echo h($data['SaleProject']['place'])?></td>
	<td class="center" <?php echo $click;?>>
		<?php
			$val = $data['SaleProject']['term_s']."~".$data['SaleProject']['term_e'];
			if ($data['SaleProject']['term_id'] != "") {
				$items = Configure::read("work_term");
				$val .= " ". $items[$data['SaleProject']['term_id']];
			}
			if ($data['SaleProject']['extension']) {
				$val .= " 延長可能性有";
			}
			echo h($val);
		?>
	</td>
	<td <?php echo $click;?>><?php echo h($data['SaleProject']['required_skill'])?></td>
	<td <?php echo $click;?>><?php echo h($data['SaleProject']['plus_skill'])?></td>
	<td <?php echo $click;?>><?php echo h($data['SaleProject']['language'])?></td>
	<td <?php echo $click;?>><?php echo h($data['SaleProject']['os'])?></td>
	<td <?php echo $click;?>><?php echo h($data['SaleProject']['db'])?></td>
	<td <?php echo $click;?>><?php echo h($data['SaleProject']['fw'])?></td>
	<td class="center" <?php echo $click;?>>
		<?php
			$val = $data['SaleProject']['price'];
			if ($data['SaleProject']['skill_level']) {
				$val = "スキル見合い";
			}
			echo h($val);
		?>
	</td>
	<td class="center" <?php echo $click;?>>
		<?php
			$items = Configure::read("maxmin_time");
			$val = "";
			if ($data['SaleProject']['time_from_id'] != "") {
				$val .= $items[$data['SaleProject']['time_from_id']];
			} else {
				$val .= $data['SaleProject']['time_from'];
			}
			$val .= "~";

			if ($data['SaleProject']['time_to_id'] != "") {
				$val .= $items[$data['SaleProject']['time_to_id']];
			} else {
				$val .= $data['SaleProject']['time_to'];
			}
			echo h($val);
		?>
	</td>
	<td <?php echo $click;?>><?php echo h($data['SaleProject']['num'])?></td>
	<td class="center" <?php echo $click;?>><?php echo h($data['SaleProject']['interview'])?></td>
	<td class="center" <?php echo $click;?>>
		<?php
			echo h($data['SaleProject']['office_hour_from']."~".$data['SaleProject']['office_hour_to'])
		?>
	</td>
	<td class="center" <?php echo $click;?>>
		<?php
			echo h($data['SaleProject']['break_time_from']."~".$data['SaleProject']['break_time_to'])
		?>
	</td>
	<td <?php echo $click;?>><?php echo h($data['Company']['name'])?></td>
	<td <?php echo $click;?>><?php echo h($data['SaleProject']['remark'])?></td>
	<td class="center">
		<a href="/s/contracts/edit/S/<?php echo $data['SaleProject']['id'];?>">
		<i class="glyphicon glyphicon-plus-sign"></i></a>
	</td>
	<td class="center">
		<a href="javascript:void(0);" onclick="deleteSaleProject(<?php echo $data['SaleProject']["id"];?>);">
		<i class="glyphicon glyphicon-remove-circle"></i></a>
	</td>
</tr>
<?php endforeach; ?>
</table>

<?php echo $this->Element("a_pagination", array('count'=>true));?>

