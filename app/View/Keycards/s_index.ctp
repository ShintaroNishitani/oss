<script>
    function editKeycard(id){
        if (id == undefined) {
            id = 0;
        }
        var url = '/s/keycards/edit/' + id ;
        loadBaseWindow(url);
        return false;
    }

    function deleteTransport(id){
        if(window.confirm('削除しますがよろしいですか？')){
            var url = "/s/keycards/delete/" + id;
            location.replace(url);
        }
    }

</script>


<blockquote><p><?php echo $this->Html->image("key.png")?> 鍵カード管理</p></blockquote>
<p class="description"><?php echo '※返却時は削除せずに、所有者、貸与日を空に設定してください' ?></p>


<a href="javascript:void(0);" onclick="editKeycard();"><i class="glyphicon glyphicon-plus-sign"></i> 新規追加</a>

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
    <th>No</th>
    <th>鍵番号</th>
    <th style="min-width:90px;">所有者</th>
    <th style="min-width:100px;">貸与日</th>

    <th></th>
</tr>
<?php
$index = 0;
foreach ($datas as $data):
    $index++;
    $click = sprintf(' onclick="editKeycard(%d);"', $data['Keycard']['id']);
    $no    = $data['Keycard']['no'];
    $name  = '予備';
    if ($data['Staff']['name']) {
        $name  = $data['Staff']['name'];
    }
    $date  = $data['Keycard']['date'];
?>
<tr>
    <td <?php echo $click;?>><?php echo h($index)?></td>
    <td class="center" <?php echo $click;?>><?php echo h($no)?></td>
    <td class="center" <?php echo $click;?>><?php echo h($name)?></td>
    <td class="center" <?php echo $click;?>><?php echo h($date)?></td>
    <td class="center">
        <a href="javascript:void(0);" onclick="deleteTransport(<?php echo $data['Keycard']['id'];?>);">
        <i class="glyphicon glyphicon-remove-circle"></i></a>
    </td>
</tr>
<?php
endforeach;
?>

</table>

