<script>
    $(function(){
        var columns = ['#KeycardDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });   
    });

  
</script>

<blockquote><p>鍵カード編集</p></blockquote>

<?php echo $this->Form->create('Keycard', array("name"=>"Keycard", "action" => "update",
                                                        "onsubmit" => "return updateKeycard();"))?>
<?php echo $this->Form->hidden("Keycard.id")?>
<?php echo $this->Form->hidden("Keycard.enable", array("value"=>1))?>

<table class="form">
<tr>
    <th>No*</th>
    <td><?php echo $this->Form->text('Keycard.no', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>所有者</th>
    <td><?php echo $this->Form->select('Keycard.staff_id',$staffs, array("class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>貸与日</th>
    <td><?php echo $this->Form->text("Keycard.date", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
    <td colspan="6" class="center">
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>        
</table>

<?php echo $this->Form->end();?>