<?php
	require_once('tcpdf/config/lang/eng.php');
	require_once('tcpdf/tcpdf.php');

	$transport_customer = Configure::read("transport_customer");

	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf->AddFont('msmincho');
	$pdf->setFontSubsetting(true);
	
	// ヘッダー、フッターなし
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);

	// 塗りつぶし色指定
	$pdf->SetFillColor(150);

	$border_n = 0;
	$border_y = 1;
	$border_u = 'B';

	$fill_n = 0;
	$fill_y = 1;

	$ln_n = 0;
	$ln_y = 1;

	$reseth_n = false;
	$reseth_y = true;

	$output_y = 5;

	$conditions = "\n裏面に記載される条件に合意し、情報の取り扱いに十分な注意を行います";
	$pdf->SetFont('msmincho');
	$pdf->AddPage();
	$pdf->SetTextColor(128,128,128);
	$pdf->SetFontSize(10.5);
	$loan_info = sprintf("(乙)%s\n", $datas['CustomerName']['full_name']);
	$loan_info = $loan_info . sprintf("\n");//事業部
	$loan_info = $loan_info . sprintf("%s", $datas['AssetLend']['lend_mng_department']);
	if ($datas['AssetLend']['lend_mng_section']) {
		$loan_info = $loan_info . sprintf(" %s\n", $datas['AssetLend']['lend_mng_section']);
	}else{
		$loan_info = $loan_info . sprintf("\n");
	}
	$loan_info = $loan_info . sprintf("%s", $datas['AssetLend']['lend_mng_person']);
	$loan_info = rtrim($loan_info);
	$loan_info = $loan_info . " 殿";
	$pdf->MultiCell(100, 0, $loan_info, $border_n, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	
	$pdf->SetTextColor(0,0,0);
	$pdf->SetFontSize(16);
	$pdf->SetXY(0,40);
	$pdf->MultiCell(0, 0, "資産貸出書　兼　資産返却書", $border_n, 'C', $fill_n, $ln_y, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetFontSize(10.5);
	$loan_info = sprintf("%s ", $datas['CustomerName']['full_name']);
	$loan_info = $loan_info . sprintf("%s", $datas['AssetLend']['lend_mng_department']);
	if ($datas['AssetLend']['lend_mng_section']) {
		$loan_info = $loan_info . sprintf(" %s", $datas['AssetLend']['lend_mng_section']);
	}
	$loan_info = sprintf("株式会社オーシャンソフトウェア（以下、「借用者」といいます）は、%s（以下、「貸与者」といいます）から、借用条件に合意し、以下を借り受けます。", $loan_info);
	$pdf->SetXY(10,55);
	$pdf->MultiCell(0, 0, $loan_info, $border_n, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(0,75);
	$pdf->MultiCell(0, 0, "記", $border_n, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetFontSize(10);
	$pdf->SetXY(30,80);
	$pdf->MultiCell(40, $output_y, "借用期間　　　　", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,80);
	$tmp1 = sprintf("%s", $datas["AssetLend"]["borrowing_period"]);
	$pdf->MultiCell(110, $output_y, $tmp1, $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(30,80 + $output_y);
	$pdf->MultiCell(40, $output_y, "使用目的　　　", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,80 + $output_y);
	$tmp1 = sprintf("%s", $datas['AssetLend']['lend_purpose']);
	$pdf->MultiCell(110, $output_y, $tmp1, $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(30,80 + $output_y * 2);
	$pdf->MultiCell(40, $output_y, "借用物件　　　　", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,80 + $output_y * 2);
	$tmp1 = sprintf("%s", $datas['AssetLend']['lend_object']);
	$pdf->MultiCell(110, $output_y, $tmp1, $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(30,80 + $output_y * 3);
	$pdf->MultiCell(40, $output_y, "資産貸出書番号", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,80 + $output_y * 3);
	$tmp1 = sprintf("%s", $datas['AssetLend']['asset_lend_number']);
	$pdf->MultiCell(110, $output_y, $tmp1, $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(30,80 + $output_y * 4);
	$pdf->MultiCell(150, $output_y, "借用条件", $border_n, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(30,80 + $output_y * 4);
	$pdf->SetFont('msmincho', 'B', 10);
	$pdf->MultiCell(150, $output_y, $conditions, $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$output_y = 5;
	$pdf->SetFont('msmincho', '', 10.);
	$pdf->SetXY(25,115);
	$tmp1 = sprintf("貸出日 %s", $datas['AssetLend']['lend_date']);
	$pdf->MultiCell(80, $output_y, $tmp1, $border_n, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(30,115 + $output_y);
	$pdf->MultiCell(40, $output_y, "<借用者>", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,115 + $output_y);
	$pdf->MultiCell(30, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(100,115 + $output_y);
	$pdf->MultiCell(80, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(30,115 + $output_y * 2);
	$pdf->MultiCell(40, $output_y, "所　在　地", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,115 + $output_y * 2);
	$pdf->MultiCell(30, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(100,115 + $output_y * 2);
	$pdf->MultiCell(80, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(30,115 + $output_y * 3);
	$pdf->MultiCell(40, $output_y, "事　業　者　名", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,115 + $output_y * 3);
	$pdf->MultiCell(30, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(100,115 + $output_y * 3);
	$pdf->MultiCell(80, $output_y, "株式会社　オーシャンソフトウェア", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(30,115 + $output_y * 4);
	$pdf->MultiCell(40, $output_y, "情報管理責任者", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,115 + $output_y * 4);
	$pdf->MultiCell(30, $output_y, "部　署", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(100,115 + $output_y * 4);
	$tmp1 = sprintf("%s", $datas["ManagerName"]["Department"]["name"]);
	$pdf->MultiCell(80, $output_y, $tmp1, $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(30,115 + $output_y * 5);
	$pdf->MultiCell(40, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,115 + $output_y * 5);
	$pdf->MultiCell(30, $output_y, "役　職", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(100,115 + $output_y * 5);
	$tmp1 = sprintf("%s", $datas["ManagerName"]['Authority']['name']);
	$pdf->MultiCell(80, $output_y, $tmp1, $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(30,115 + $output_y * 6);
	$pdf->MultiCell(40, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,115 + $output_y * 6);
	$pdf->MultiCell(30, $output_y, "氏　名", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(100,115 + $output_y * 6);
	$tmp1 = sprintf("%s", $datas["ManagerName"]["name"]);
	$pdf->MultiCell(65, $output_y, $tmp1, $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(165,115 + $output_y * 6);
	if(file_exists($datas["ManagerName"]["mark"]))
	{
		$pdf->MultiCell(15, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
		$pdf->Image($datas["ManagerName"]["mark"], 170, 115 + $output_y * 6, 5, 5, '', '', 'M', false, 300, '', false, false, 0, false, false, false);
	}
	else
	{
		$pdf->MultiCell(15, $output_y, "印", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	}	

	$pdf->SetXY(30,115 + $output_y * 7);
	$pdf->MultiCell(40, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,115 + $output_y * 7);
	$pdf->MultiCell(30, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(100,115 + $output_y * 7);
	$pdf->MultiCell(65, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(165,115 + $output_y * 7);
	$pdf->MultiCell(15, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(30,115 + $output_y * 8);
	$pdf->MultiCell(40, $output_y, "情報管理者", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,115 + $output_y * 8);
	$pdf->MultiCell(30, $output_y, "部　署", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(100,115 + $output_y * 8);
	$tmp1 = sprintf("%s", $datas["RecieverName"]["Department"]["name"]);
	$pdf->MultiCell(65, $output_y, $tmp1, $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(165,115 + $output_y * 8);
	$pdf->MultiCell(15, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(30,115 + $output_y * 9);
	$pdf->MultiCell(40, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,115 + $output_y * 9);
	$pdf->MultiCell(30, $output_y, "役　職", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(100,115 + $output_y * 9);
	$tmp1 = sprintf("%s", $datas['RecieverName']['Authority']['name']);
	$pdf->MultiCell(65, $output_y, $tmp1, $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(165,115 + $output_y * 9);
	$pdf->MultiCell(15, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(30,115 + $output_y * 10);
	$pdf->MultiCell(40, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,115 + $output_y * 10);
	$pdf->MultiCell(30, $output_y, "氏　名", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(100,115 + $output_y * 10);
	$tmp1 = sprintf("%s", $datas["RecieverName"]["name"]);
	$pdf->MultiCell(65, $output_y, $tmp1, $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(165,115 + $output_y * 10);
	if(file_exists($datas["RecieverName"]["mark"]))
	{
		$pdf->MultiCell(15, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
		$pdf->Image($datas["RecieverName"]["mark"], 170, 115 + $output_y * 10, 5, 5, '', '', 'M', false, 300, '', false, false, 0, false, false, false);
	}
	else
	{
		$pdf->MultiCell(15, $output_y, "印", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	}

	$pdf->SetXY(30,180);
	$pdf->MultiCell(40, $output_y, "<貸与者>", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,180);
	$pdf->MultiCell(30, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(100,180);
	$pdf->MultiCell(80, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(30,180 + $output_y);
	$pdf->MultiCell(40, $output_y, "所　在　地", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,180 + $output_y);
	$pdf->MultiCell(30, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(100,180 + $output_y);
	$pdf->MultiCell(80, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(30,180 + $output_y * 2);
	$pdf->MultiCell(40, $output_y, "事　業　者　名", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,180 + $output_y * 2);
	$pdf->MultiCell(30, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(100,180 + $output_y * 2);
	$tmp1 = sprintf("%s", $datas['CustomerName']['full_name']);
	$pdf->MultiCell(80, $output_y, $tmp1, $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(30,180 + $output_y * 3);
	$pdf->MultiCell(40, $output_y, "情報管理責任者", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,180 + $output_y * 3);
	$pdf->MultiCell(30, $output_y, "部　署", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(100,180 + $output_y * 3);
	$tmp1 = sprintf("%s %s", $datas["AssetLend"]["lend_charge_department"],$datas["AssetLend"]["lend_charge_section"]);
	$pdf->MultiCell(80, $output_y, $tmp1, $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(30,180 + $output_y * 4);
	$pdf->MultiCell(40, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,180 + $output_y * 4);
	$pdf->MultiCell(30, $output_y, "役　職", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(100,180 + $output_y * 4);
	$tmp1 = sprintf("%s", $datas["AssetLend"]["lend_charge_position"]);
	$pdf->MultiCell(80, $output_y, $tmp1, $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(30,180 + $output_y * 5);
	$pdf->MultiCell(40, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,180 + $output_y * 5);
	$pdf->MultiCell(30, $output_y, "氏　名", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(100,180 + $output_y * 5);
	$tmp1 = sprintf("%s", $datas["AssetLend"]["lend_charge_person"]);
	$pdf->MultiCell(65, $output_y, $tmp1, $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(165,180 + $output_y * 5);
	$pdf->MultiCell(15, $output_y, "印", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');


	$pdf->SetXY(30,180 + $output_y * 6);
	$pdf->MultiCell(40, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,180 + $output_y * 6);
	$pdf->MultiCell(30, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(100,180 + $output_y * 6);
	$pdf->MultiCell(65, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(165,180 + $output_y * 6);
	$pdf->MultiCell(15, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(30,180 + $output_y * 7);
	$pdf->MultiCell(40, $output_y, "情報管理者", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,180 + $output_y * 7);
	$pdf->MultiCell(30, $output_y, "部　署", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(100,180 + $output_y * 7);
	$tmp1 = sprintf("%s %s", $datas["AssetLend"]["lend_mng_department"],$datas["AssetLend"]["lend_mng_section"]);
	$pdf->MultiCell(65, $output_y, $tmp1, $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(165,180 + $output_y * 7);
	$pdf->MultiCell(15, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(30,180 + $output_y * 8);
	$pdf->MultiCell(40, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,180 + $output_y * 8);
	$pdf->MultiCell(30, $output_y, "役　職", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(100,180 + $output_y * 8);
	$tmp1 = sprintf("%s", $datas["AssetLend"]["lend_mng_position"]);
	$pdf->MultiCell(65, $output_y, $tmp1, $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(165,180 + $output_y * 8);
	$pdf->MultiCell(15, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(30,180 + $output_y * 9);
	$pdf->MultiCell(40, $output_y, "", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(70,180 + $output_y * 9);
	$pdf->MultiCell(30, $output_y, "氏　名", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(100,180 + $output_y * 9);
	$tmp1 = sprintf("%s", $datas["AssetLend"]["lend_mng_person"]);
	$pdf->MultiCell(65, $output_y, $tmp1, $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->SetXY(165,180 + $output_y * 9);
	$pdf->MultiCell(15, $output_y, "印", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetFont('msmincho',"U", 11);
	$pdf->SetXY(20,235);
	$pdf->MultiCell(170, $output_y, "                                                                 　                                                                             　", $border_n, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');


	$pdf->SetFont('msmincho',"", 11);
	$pdf->SetXY(20,250);
	$pdf->MultiCell(70, $output_y, "※以下は、資産返却・廃棄時に記入", 'B', 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetXY(20,260);
	$pdf->MultiCell(70, $output_y, "返却日・廃棄日       年     月     日", $border_n, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetFont('msmincho',"U", 11);
	$pdf->SetXY(50,265);
	$pdf->MultiCell(170, $output_y, "（貸与者） 責任者氏名   　　                      　　　　        印　", $border_n, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->AddPage();

	$pdf->SetXY(10,10);
	$pdf->MultiCell(100, $output_y, "……裏面(必ず両面印刷すること)", $border_n, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetFont('msmincho', '', 8);
	$pdf->SetXY(15,18);
	$pdf->MultiCell(190, $output_y, "借用条件
１． 借用情報は、いかなる理由があっても第三者に引き渡さないものとします。
２． 借用者は借用情報全部または一部を貸与者の書面による事前の承諾がない限り、いかなる形態においても、第三者に対して公開もしくは開示してはならないものとし、且つ漏洩を防止するものとします。
３． 借用情報の使用または取り扱いに起因して借用者が何らかの損害を被ったとしても、借用者は貸与者に対して、一切損害賠償の請求をしないものとします。
４． 借用者は借用期間が満了になったときは、直ちに、貸与者に借用情報を返還するものとします。その際、借用者は借用情報の複製物の全てを廃棄または消去するものとします。
５． 借用者は借用情報を使用目的以外に使用をしたり、または他に配布、貸与、譲渡したりすることはできません。
６． 本貸出書に定めなき事項、または疑義が生じた場合には、借用者・貸与者の両者は信義誠実の原則に従い、協議の上解決するものとします。
                                                                                                                                                                                                             以上

", $border_y, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	// 塗りつぶし色指定
	$pdf->SetTextColor(150);
	$pdf->SetFont('msmincho',"", 11);
	$pdf->SetXY(40,270);
	$pdf->MultiCell(170, $output_y, "株式会社オーシャンソフトウェア　", $border_n, 'R', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	// 幅設定
	$height = 5.5;
	$height2 = 30.0;
	$w_time = 8;
	$w_hour = 8;
	$s1 = 5;
	$s2 = 12;
	$m1 = 20;
	$m2 = 25;
	$l1 = 57;

	$group_xpos = ($s1 * 2) + ($s2 * 1) + ($w_time * 2) + ($w_hour * 5) + 10;
	$pdf->SetX($group_xpos);
	$group_width = $w_hour * 12;

	// 出力
 	ob_end_clean();
	$file_name = sprintf("資産管理台帳", $datas['AssetLend']['id']);
	$pdf->Output("$file_name.pdf", "D");

?>
