<script>
    $(function(){
        var columns = ['#AssetLendLendDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
        var columns = ['#AssetLendReceiveDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
        var columns = ['#AssetLendLendStartDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
        var columns = ['#AssetLendLendEndDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    });

</script>

<blockquote>
<p><?php echo $this->Html->image("asset_lend.png")?> 資産貸出登録</p>
</blockquote>

<?php echo $this->Form->create("AssetLend", array("name"=>"AssetLend", "url"=>array("action" => "update"),
                                                        "onsubmit" => "return s_update();"))?>

<?php echo $this->Form->hidden("AssetLend.id")?>
<?php echo $this->Form->hidden("AssetLend.status")?>
<?php echo $this->Form->hidden("AssetLend.enable", array("value"=>1))?>

<table class="form">
<tr>
	<th>資産貸出書番号*</th>
	<td><?php echo $this->Form->text("AssetLend.asset_lend_number", array("empty"=>false, "class"=>"form-control input-sm", "style"=>SIZE_S));?></td>
</tr>
<tr>
	<th>区分*</th>
	<td><?php echo $this->Form->select("AssetLend.division", Configure::read("loan_kbn"), array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
	<th>借用責任者*</th>
	<td><?php echo $this->Form->select("AssetLend.asset_manager_id", $staffs, array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_M))?></td>
</tr>
<tr>
	<th>借用管理者*</th>
	<td><?php echo $this->Form->select("AssetLend.receive_user_id", $staffs, array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
</tr>
<tr>
	<th>貸借顧客名称*</th>
	<td><?php echo $this->Form->select("AssetLend.customer_id", $customers, array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_L));?></td>
</tr>
<th colspan=2>貸与責任者情報</th>
<tr>
	<th>貸与責任者 部署*</th>
	<td><?php echo $this->Form->text("AssetLend.lend_charge_department", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_L));?></td>
</tr>
<tr>
	<th>貸与責任者 課</th>
	<td><?php echo $this->Form->text("AssetLend.lend_charge_section", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
	<th>貸与責任者 役職</th>
	<td><?php echo $this->Form->text("AssetLend.lend_charge_position", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
	<th>貸与責任者 氏名*</th>
	<td><?php echo $this->Form->text("AssetLend.lend_charge_person", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_L));?></td>
</tr>
<th colspan=2>貸与管理者情報</th>
<tr>
	<th>貸与管理者 部署*</th>
	<td><?php echo $this->Form->text("AssetLend.lend_mng_department", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_L));?></td>
</tr>
<tr>
	<th>貸与管理者 課</th>
	<td><?php echo $this->Form->text("AssetLend.lend_mng_section", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
	<th>貸与管理者 役職</th>
	<td><?php echo $this->Form->text("AssetLend.lend_mng_position", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
	<th>貸与管理者 氏名*</th>
	<td><?php echo $this->Form->text("AssetLend.lend_mng_person", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_L));?></td>
</tr>
<th colspan=2>貸与情報</th>
<tr>
	<th>貸与日*</th>
	<td><?php echo $this->Form->text("AssetLend.lend_date", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
	<th>受領日*</th>
	<td><?php echo $this->Form->text("AssetLend.receive_date", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
	<th>貸借期間開始日*</th>
	<td><?php echo $this->Form->text("AssetLend.lend_start_date", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
	<th>貸借期間終了日*</th>
	<td>
		<div style="display:inline-flex">
			<?php echo $this->Form->text("AssetLend.lend_end_date", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?>
			<?php echo $this->Form->checkbox('AssetLend.auto_extend', array('label'=>'true', 'options' => Array("1"=>""))); ?>自動延長
		</div>
	</td>
</tr>
<tr>
	<th>貸借物件*</th>
	<td><?php echo $this->Form->text("AssetLend.lend_object", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_L));?></td>
</tr>
<tr>
	<th>使用目的*</th>
	<td><?php echo $this->Form->text("AssetLend.lend_purpose", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_L));?></td>
</tr>
<tr>
	<th>貸借方法*</th>
	<td><?php echo $this->Form->select("AssetLend.lend_approach", Configure::read("loan_type"), array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_S,));?></td>
</tr>
<tr>
	<th>貸借移送手段*</th>
	<td><?php echo $this->Form->text("AssetLend.delivery_approach", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
	<th>保管場所*</th>
	<td><?php echo $this->Form->text("AssetLend.storage_area", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_L));?></td>
</tr>
<tr>
	<th>返却要否*</th>
	<td><?php echo $this->Form->select("AssetLend.check_return", Configure::read("return_type"), array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
	<td class="center" colspan=2>
        <button type="button" onclick="history.back()" class="btn btn-info">
        	<i class="glyphicon glyphicon-circle-arrow-left"></i> 戻る
        </button>
 		<button type="submit" class="btn btn-primary">
 			<i class="glyphicon glyphicon-send"></i> 登録
 		</button>
 		<button type="button" class="btn btn-primary" onclick='location.href = "/s/asset_lends/repayment/<?php echo($this->data["AssetLend"]["id"]) ?>";'>
 			<i class="glyphicon glyphicon-save"></i> 返却登録
 		</button>
    </td>
</tr>
</table>

<?php echo $this->Form->end();?>