<script>
    $(function(){
        var columns = ['#AssetLendReturnDeleteDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
        var columns = ['#AssetLendReturnReturnDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    });

</script>

<blockquote>
<p><?php echo $this->Html->image("asset_lend.png")?> 資産返却</p>
</blockquote>

<?php echo $this->Form->create("AssetLend", array('name'=>"AssetLend", 'url'=>array("action" => "repayment")))?>

<?php echo $this->Form->hidden("AssetLendReturn.id");?>
<?php echo $this->Form->hidden("AssetLendReturn.asset_lend_id", array('value'=>$this->data["AssetLend"]["id"]));?>
<?php echo $this->Form->hidden("AssetLend.enable", array("value"=>1));?>

<table class="form">
<tr>
	<th>資産貸出書番号</th>
	<td>
	<?php echo ($this->data["AssetLend"]["asset_lend_number"]);?>
	</td>
</tr>
<tr>
	<th>区分</th>
	<td>
	<?php 
		$divAry = Configure::read("loan_kbn");
		echo($divAry[$this->data["AssetLend"]["division"]]);
	?>
	</td>
</tr>
<tr>
	<th>借用管理者</th>
	<td>
	<?php echo($staffs[$this->data["AssetLend"]["receive_user_id"]]); ?></td>
</tr>
<tr>
	<th>貸借顧客名称</th>
	<td><?php echo($customers[$this->data["AssetLend"]["customer_id"]]); ?></td>
</tr>
<th colspan=2>貸与管理者情報</th>
<tr>
	<th>貸与管理者 部署</th>
	<td><?php echo($this->data["AssetLend"]["lend_mng_department"]);?></td>
</tr>
<tr>
	<th>貸与管理者 課</th>
	<td><?php echo($this->data["AssetLend"]["lend_mng_section"]);?></td>
</tr>
<tr>
	<th>貸与管理者 役職</th>
	<td><?php echo($this->data["AssetLend"]["lend_mng_position"]);?></td>
</tr>
<tr>
	<th>貸与管理者 氏名</th>
	<td><?php echo($this->data["AssetLend"]["lend_mng_person"]);?></td>
</tr>

<th colspan=2>返却情報</th>
<tr>
	<th>返却方法*</th>
	<td><?php echo $this->Form->select("AssetLendReturn.means", Configure::read("return_means"), array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_S,));?></td>

</tr>
<tr>
	<th>返却移送手段</th>
	<td><?php echo $this->Form->text("AssetLendReturn.transfer", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
	<th>削除確認者</th>
	<td><?php echo $this->Form->select("AssetLendReturn.delete_staff_id", $staffs, array("empty"=>true, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
	<th>削除日</th>
	<td><?php echo $this->Form->text("AssetLendReturn.delete_date", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
	<th>返却者</th>
	<td><?php echo $this->Form->select("AssetLendReturn.return_staff_id", $staffs, array("empty"=>true, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
	<th>返却日</th>
	<td><?php echo $this->Form->text("AssetLendReturn.return_date", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
	<th>返却状態*</th>
	<td><?php echo $this->Form->select("AssetLendReturn.status", Configure::read("already_return"), array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
	<td class="center" colspan=2>
        <button type="button" onclick="history.back()" class="btn btn-info">
        	<i class="glyphicon glyphicon-circle-arrow-left"></i> 戻る
        </button>
 		<button type="submit" class="btn btn-primary">
 			<i class="glyphicon glyphicon-send"></i> 登録
 		</button>
    </td>
</tr>

</table>

<?php echo $this->Form->end();?>