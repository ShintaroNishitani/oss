<script>
    function editAssetLends(id){
        if (id == undefined) {
            id = null;
        }
        var url = '/s/asset_lends/edit/' + id ;
        window.location.href = url;
        return false;
    }
    function delAssetLends(id){
        if(window.confirm('削除しますがよろしいですか？')){
	        var url = '/s/asset_lends/delete/' + id ;
            location.replace(url);
        }
        return false;
    }
    /**
     * [printAssetLends 資産管理台帳印刷]
     * @param  {[type]} id 資産管理台帳ID
     * @return {[type]}    [description]
     */
    function printAssetLends(id){
        var url = "/s/asset_lends/print/" + id;
        location.replace(url);
    }

    $(document).ready(function() {
		displayScrap(false);
	});
    $(function() {
		$('#scrap_display').change(function() {
			var check = $(this).is(':checked');
			displayScrap(check);
		});
	});

	function displayScrap (check) {
		var tbody = document.getElementById("mainTbody");

		for (var i = 0; i < tbody.rows.length; i++) {

			var value = tbody.rows[i].cells[15].innerText;
			var display = true;
			console.log(value);
			if (check == false && value == "済") {
					display = false;
			}

			if (display) {
				tbody.rows[i].style.display="";
			} else {
				tbody.rows[i].style.display="none";
			}
		};
	}

</script>
<blockquote><p><?php echo $this->Html->image("asset_lend.png")?> 資産貸出台帳</p></blockquote>
<?= $this->Form->input("scrap_display", array('type' => 'checkbox', 'label'=>' '.__('返却済表示'), 'checked'=>false, 'id'=>'scrap_display'));?>
<div>
	<a href="/s/asset_lends/edit/"><i class="glyphicon glyphicon-plus-sign"></i> 新規追加</a>	
</div>
<div>
	<table class="table-bordered table-condensed table-striped table-hover">
		<tr>
	        <th style="min-width:30px;">No</th>
	        <th style="min-width:100px;">資産貸出番号</th>
	        <th style="min-width:80px;">借用責任者</th>
	        <th style="min-width:150px;">貸借顧客名称</th>
        	<th style="min-width:80px;">区分</th>
	        <th style="min-width:250px;">貸与管理者</th>
	        <th style="min-width:250px;">借用管理者</th>
	        <th style="min-width:180px;">貸借期間</th>
	        <th style="min-width:250px;">貸借物件</th>
	        <th style="min-width:320px;">貸借目的</th>
			<th style="min-width:80px;">貸借方法</th>
			<th style="min-width:80px;">貸借移送手段</th>
			<th style="min-width:200px;">保管場所</th>
			<th style="min-width:60px;">返却要否</th>
			<th style="min-width:60px;">返却状態</th>
			<th style="min-width:20px;"></th>
			<th style="min-width:20px;"></th>
	    </tr>
		<tbody id="mainTbody">
			<?php
				foreach ($datas as $key => $value) :
					$click       = sprintf(' onclick="editAssetLends(%d);"',$value["AssetLend"]["id"]);
					$loan_kbn    = Configure::read("loan_kbn");
					$loan_type   = Configure::read("loan_type");
					$return_type = Configure::read("return_type");
					$already     = Configure::read("already_return");
					$customer    = sprintf("%s %s %s<br> %s<br>%s",
											$value["AssetLend"]["lend_mng_department"],
											$value["AssetLend"]["lend_mng_section"],
											$value["AssetLend"]["lend_mng_position"],
											$value["AssetLend"]["lend_mng_person"],
											$value["AssetLend"]["lend_date"]);
					$receiver     = sprintf("%s<br>%s<br>%s",
											$value["RecieverName"]["Authority"]["name"],
											$value["RecieverName"]["name"],
											$value["AssetLend"]["receive_date"]);
			?>
			<tr>
				<td<?php echo $click;?>><?php echo $value["AssetLend"]["id"];?></td>
				<td<?php echo $click;?>><?php echo $value["AssetLend"]["asset_lend_number"];?></td>
				<td<?php echo $click;?>><?php echo $value["ManagerName"]["name"];?></td>
				<td<?php echo $click;?>><?php echo $value["CustomerName"]["full_name"];?></td>
				<td<?php echo $click;?>><?php echo $loan_kbn[$value["AssetLend"]["division"]];?></td>
				<td<?php echo $click;?>><?php echo $customer;?></td>
				<td<?php echo $click;?>><?php echo $receiver;?></td>
				<td<?php echo $click;?>>
					<?php echo $value["AssetLend"]["lend_start_date"];?> ～ 
					<?php echo $value["AssetLend"]["lend_end_date"];?>
					<?php if(1 == $value["AssetLend"]["auto_extend"]) echo ("<br>(自動延長有)");?>
				</td>
				<td<?php echo $click;?>><?php echo $value["AssetLend"]["lend_object"];?></td>
				<td<?php echo $click;?>><?php echo $value["AssetLend"]["lend_purpose"];?></td>
				<td<?php echo $click;?>><?php echo $loan_type[$value["AssetLend"]["lend_approach"]];?></td>
				<td<?php echo $click;?>><?php echo $value["AssetLend"]["delivery_approach"];?></td>
				<td<?php echo $click;?>><?php echo $value["AssetLend"]["storage_area"];?></td>
				<td<?php echo $click;?>><?php echo $return_type[$value["AssetLend"]["check_return"]];?></td>
				<td<?php echo $click;?>>
					<?php 
						$sts = isSet($already[$value["AssetLendReturn"]["status"]]) ? $value["AssetLendReturn"]["status"] : 0;
						echo $already[$sts];
					?>
				</td>
				<td class="center">
        			<a href="javascript:void(0);" onclick="printAssetLends(<?php echo $value["AssetLend"]["id"];?>);">
			        <i class="glyphicon glyphicon-print"></i></a>
			    </td>
				<td class="center">
        			<a href="javascript:void(0);" onclick="delAssetLends(<?php echo $value["AssetLend"]["id"];?>);">
			        <i class="glyphicon glyphicon-remove-circle"></i></a>
			    </td>
			</tr>
		    <?php
		    	endforeach;
	    	?>
		</tbody>
	</table>
    <div class="paginator">
    <ul class="pagination">
        <?php echo $this->Element("a_pagination", array('count'=>true));?>
    </ul>
    </div>
</div>

