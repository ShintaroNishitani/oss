<script>
    function execConfirm(type, type_id, label, status){
        if(window.confirm(label + 'しますがよろしいですか？')){
            var url = "/s/confirms/update/" + type + "/" + type_id + "/" + status;
            location.replace(url);
        }
    }

    function execReceive(type, type_id){
        if(window.confirm('受領済にしますがよろしいですか？')){
            var url = "/s/confirms/receive/" + type + "/" + type_id;
            location.replace(url);
        }
    }    
</script>

<table class="table-condensed well">
    <tr>
        <th><i class="glyphicon glyphicon-stop"></i> 申請者</th>
        <td class="center" style="min-width:90px;">
            <?php 
                if ($data['Confirm']['request_staff_id']) {
                    echo h($staffs[$data['Confirm']['request_staff_id']]);
                }
            ?>
        </td>          
        <th><i class="glyphicon glyphicon-stop"></i> 申請日時</th>
        <td class="center"  style="min-width:90px;">
            <?php 
                echo h($data['Confirm']['request_date']);
            ?>
        </td>        
        <th><i class="glyphicon glyphicon-stop"></i> 承認者</th>
        <td class="center" style="min-width:90px;">
            <?php 
                if ($data['Confirm']['check_staff_id']) {
                    echo h($staffs[$data['Confirm']['check_staff_id']]);
                }
            ?>
        </td>        
        <th><i class="glyphicon glyphicon-stop"></i> 承認日時</th>
        <td class="center" style="min-width:90px;">
            <?php 
                echo h($data['Confirm']['check_date']);
            ?>
        </td>    
        <th><i class="glyphicon glyphicon-stop"></i> ステータス</th>
        <td class="center">
            <?php 
                $items = Configure::read("confirm_status");
                echo h($items[$data['Confirm']['status']]);
            ?>
        </td>
        <?php if ($data['Confirm']['status'] == CONFIRM_NO):?>
        <th><i class="glyphicon glyphicon-stop"></i> 差戻理由</th>
        <td class="center" style="min-width:90px;">
            <?php 
                echo h($data['Confirm']['remark']);
            ?>
        </td>
        <?php endif;?>
        <?php if ($type != CONFIRM_TYPE_0):?>
        <th><i class="glyphicon glyphicon-stop"></i> 受領日</th>
        <td class="center" style="min-width:90px;">
            <?php 
                echo h($data['Confirm']['receive_date']);
            ?>
        </td>
        <?php endif;?>
        <td class="center">
        <th>
            <?php 
                $status = CONFIRM_IN;
                // 「未申請」「差戻」の場合は[申請]ボタン表示
                if ($data['Confirm']['status'] == CONFIRM_YET || $data['Confirm']['status'] == CONFIRM_NO) {
                    echo $this->Form->button('申請', array("class"=>"btn  btn-success", 'type'=>'button', 'onclick'=>"execConfirm($type, $type_id, '申請', $status)"));
                } 
                // 「承認済」の場合は[受領]ボタン表示
                else if ($data['Confirm']['status'] == CONFIRM_OK && $data['Confirm']['receive'] != RECEIVE_OK && 
                         $type != CONFIRM_TYPE_0) {
                    echo $this->Form->button('受領', array("class"=>"btn  btn-success", 'type'=>'button', 'onclick'=>"execReceive($type, $type_id)"));
                }
            ?>
        </td>
    </tr>    
</table>
</br>