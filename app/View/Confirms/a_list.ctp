<script>
    function execConfirm(type, type_id, label, status){
        debugger;
        if (status == <?= h(CONFIRM_NO)?>) {
            var reason = "";
            reason = window.prompt("差戻理由を入力してください。\r\n※不要な場合もOKボタンをクリックしてください。", "");
            if (reason == null) {
                return false;
            }

            var data = {
                'type':type,
                'type_id':type_id,
                'status':status,
                'reason':reason,
            }

            var url = "/s/confirms/update_ajax";
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function(html){
                    var res = eval('(' + html + ')');
                    if (res["error"] == 0) {
                        // unloadAll();
                        location.reload();
                    }
                    myAlert(res["message"]);
                }
            });

        } else {

            if(window.confirm(label + 'しますがよろしいですか？')){
                var url = "/s/confirms/update/" + type + "/" + type_id + "/" + status;
                location.replace(url);
            }
        }
    }
</script>

<table class="table-condensed well">
    <tr>
        <th><i class="glyphicon glyphicon-stop"></i> 申請者</th>
        <td class="center" style="min-width:90px;">
            <?php
                if ($data['Confirm']['request_staff_id']) {
                    echo h(sprintf("%s（%s）", $data['Staff']['name'], $data['Staff']['no']));
                }
            ?>
        </td>
        <th><i class="glyphicon glyphicon-stop"></i> 申請日時</th>
        <td class="center"  style="min-width:90px;">
            <?php
                echo h($data['Confirm']['request_date']);
            ?>
        </td>
        <th><i class="glyphicon glyphicon-stop"></i> 承認者</th>
        <td class="center" style="min-width:90px;">
            <?php
                if ($data['Confirm']['check_staff_id']) {
                    echo h($staffs[$data['Confirm']['check_staff_id']]);
                }
            ?>
        </td>
        <th><i class="glyphicon glyphicon-stop"></i> 承認日時</th>
        <td class="center" style="min-width:90px;">
            <?php
                echo h($data['Confirm']['check_date']);
            ?>
        </td>
        <th><i class="glyphicon glyphicon-stop"></i> ステータス</th>
        <td class="center">
            <?php
                $items = Configure::read("confirm_status");
                echo h($items[$data['Confirm']['status']]);
            ?>
        </td>
        <?php if ($data['Confirm']['status'] == CONFIRM_NO):?>
        <th><i class="glyphicon glyphicon-stop"></i> 差戻理由</th>
        <td class="center" style="min-width:90px;">
            <?php 
                echo h($data['Confirm']['remark']);
            ?>
        </td>
        <?php endif;?>
        <?php if ($type != CONFIRM_TYPE_0):?>
        <th><i class="glyphicon glyphicon-stop"></i> 受領日</th>
        <td class="center" style="min-width:90px;">
            <?php
                echo h($data['Confirm']['receive_date']);
            ?>
        </td>
        <?php endif;?>
        <td class="center">
        <th>
            <?php
                // ステータスが「未申請」「差戻」の場合はボタンを表示しない
                if (!($data['Confirm']['status'] == CONFIRM_YET || $data['Confirm']['status'] == CONFIRM_NO)) {
                    $status = CONFIRM_OK;
                    // ステータスが「承認」でなければ[承認]ボタン表示
                    if ($data['Confirm']['status'] != $status) {
                        echo $this->Form->button('承認', array("class"=>"btn  btn-success", 'type'=>'button', 'onclick'=>"execConfirm($type, $type_id, '承認', $status)"));
                        echo "&nbsp";
                    }

                    $status = CONFIRM_NO;
                    // [差戻]ボタン
                    echo $this->Form->button('差戻', array("class"=>"btn  btn-success", 'type'=>'button', 'onclick'=>"execConfirm($type, $type_id, '差戻', $status)"));
                }

            ?>
        </td>
    </tr>
</table>
</br>
