<script>
    $(function(){
        var columns = ['#ConfirmTargetDateB', '#ConfirmTargetDateE'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });

    });

    function editConfirm(type, type_id, staff_id, year, month){
        if (!year || !month) {
            date = new Date();
            var year = date.getFullYear();
            var month = date.getMonth() + 1;
        }

        switch (type) {
            case <?= CONFIRM_TYPE_0 ?>:　//　勤怠
                window.open("/a/kintais/index/" + year + "/" + month + "/" + staff_id);
                break;
            case <?= CONFIRM_TYPE_1 ?>:　// 交通費精算
                window.open("/a/transports/index/" + year + "/" + month + "/" + staff_id);
                break;
            case <?= CONFIRM_TYPE_2 ?>: // 経費精算
                window.open("/a/costs/index/" + year + "/" + month + "/" + staff_id);
                break;
            case <?= CONFIRM_TYPE_3 ?>: // 旅費精算
                window.open("/a/businesstriporders/index/" + year + "/" + month + "/" + staff_id);
                break;
            case 4: // 通勤届
                break;
            case 5: // 慶弔届
                break;
            case 6: // 身上届
                break;
            case 7: // 給与振込口座届
                break;
            case <?= CONFIRM_TYPE_8 ?>: // 休暇申請
                window.open("/a/ApplyHolidays/detail_edit/" + type_id);
                break;
            default:
                break;
        }

        loadBaseWindow(url);
        return false;
    }


</script>


<blockquote><p><?php echo $this->Html->image("confirm.png")?> 申請一覧</p></blockquote>
<p class="guide"><?php echo '申請内容を確認し、承認を行ってください' ?></p>

<?php echo $this->Form->create("Confirm", array("action" => "index", 'type'=>'get'))?>
<table class="table-condensed well">
    <tr>
        <th style="min-width:50px;">従業員</th>
        <td>
            <?php echo  $this->Form->select("Confirm.request_staff_id", $staffs, array("class" => "form-control input-sm", "value"=>$keys['request_staff_id'][0]));?>
        </td>
        <th style="min-width:70px;">ステータス</th>
        <td>
            <?php
                $items = Configure::read("confirm_status");
                unset($items[CONFIRM_YET], $items[CONFIRM_NO]);
                echo  $this->Form->select("Confirm.status", $items, array("class" => "form-control input-sm", "value"=>$keys['status'][0]));
            ?>
        </td>
        <th style="min-width:50px;">内容</th>
        <td>
            <?php echo  $this->Form->select("Confirm.type", Configure::read("confirm_type"), array("class" => "form-control input-sm", "value"=>$keys['type'][0]));?>
        </td>
        <th style="min-width:50px;">対象年月</th>
        <td>
            <div style="display:inline-flex">
            <?php echo $this->Form->text("Confirm.target_date_b", array("class" => "form-control input-sm", 'style'=>SIZE_S, "value"=>$keys['target_date_b'][0]));?>
            ～
            <?php echo $this->Form->text("Confirm.target_date_e", array("class" => "form-control input-sm", 'style'=>SIZE_S, "value"=>$keys['target_date_e'][0]));?>
            </div>
        </td>
        <th style="min-width:50px;">承認者</th>
        <td>
            <?php echo  $this->Form->select("Confirm.check_staff_id", $staffs, array("class" => "form-control input-sm", "value"=>$keys['check_staff_id'][0]));?>
        </td>

        <th style="min-width:70px;">受領</th>
        <td>
            <?php
                $items = Configure::read("receive_status");
                echo  $this->Form->select("Confirm.receive", $items, array("class" => "form-control input-sm", "value"=>$keys['receive'][0]));
            ?>
        </td>
        <td class="center" colspan="8">
            <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> 検索</button>
        </td>
    </tr>
</table>
<?php echo $this->Form->end()?>

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
    <th>No</th>
    <th style="min-width:110px;">申請日</th>
    <th style="min-width:110px;">従業員</th>
    <th style="min-width:110px;">内容</th>
    <th style="min-width:110px;">対象年月</th>
    <th style="min-width:110px;">承認日</th>
    <th style="min-width:110px;">承認者</th>
    <th style="min-width:110px;">ステータス</th>
    <th style="min-width:110px;">受領</th>
    <th style="min-width:110px;">受領日</th>
    <th style="min-width:110px;">申請対象日</th>
</tr>
<?php
$num = 1;
foreach ($datas as $data):
    $target_year = null;
    $target_month = null;
    if ($data['Confirm']['target_date']) {
        $date = explode('-', $data['Confirm']['target_date']);
        $target_year = $date[0];
        $target_month = $date[1];
    }
    $click = sprintf(' onclick="editConfirm(%d, %d, %d, %d, %d);"', $data['Confirm']['type'], $data['Confirm']['type_id'], $data['Confirm']['request_staff_id'], $target_year, $target_month);

    // 承認済みの場合は行の色を変更
    $style="";
    if ($data['Confirm']['status'] == CONFIRM_OK) {
        $style = "background-color: #d0e9c6;";
    }
?>
<tr style="<?php echo $style; ?>">
    <td <?php echo $click;?>><?php echo h($num++)?></td>
    <td class="center" <?php echo $click;?>><?php echo h($data['Confirm']['request_date'])?></td>
    <td <?php echo $click;?>><?php echo h($staffs[$data['Confirm']['request_staff_id']])?></td>
    <td <?php echo $click;?>>
        <?php
            $items = Configure::read("confirm_type");
            echo h($items[$data['Confirm']['type']]);
        ?>
    </td>
    <td class="center" <?php echo $click;?>>
        <?php
            if ($target_year && $target_month) {
                echo h($target_year . '年' . $target_month . '月');
            }
        ?>
    </td>
    <td class="center" <?php echo $click;?>><?php echo h($data['Confirm']['check_date'])?></td>
    <td <?php echo $click;?>>
        <?php
            if ($data['Confirm']['check_staff_id']) {
                echo h($staffs[$data['Confirm']['check_staff_id']]);
            }
            if ($data['Confirm']['prev_check_staff_id']) {
                echo h("（".$staffs[$data['Confirm']['prev_check_staff_id']]."）");
            }
        ?>
    </td>
    <td <?php echo $click;?> class="center">
        <?php
            $items = Configure::read("confirm_status");
            echo h($items[$data['Confirm']['status']]);
        ?>
    </td>
    <td <?php echo $click;?> class="center">
        <?php
            if ($data['Confirm']['type'] == CONFIRM_TYPE_1 || $data['Confirm']['type'] == CONFIRM_TYPE_2) {
                $items = Configure::read("receive_status");
                echo h($items[$data['Confirm']['receive']]);
            }
        ?>
    </td>
    <td class="center" <?php echo $click;?>><?php echo h($data['Confirm']['receive_date'])?></td>
    <td class="center" <?php echo $click;?>>
        <?php 
            if (isset($data['Applydays'])) {
                echo implode('<br>',$data['Applydays']);
            }
        ?>
    </td>

</tr>
<?php endforeach; ?>
</table>

<?php echo $this->Element("a_pagination", array('count'=>true));?>

