<script>
    $(function(){
        var columns = ['#OrderScDateB', '#OrderScDateE'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    });

    function editOrder(type, year, id){
        if (id == undefined) {
            id = 0;
        }
        var url = '/s/orders/edit/'+ type + '/' + year + '/' + id;
        loadBaseWindow(url);
        return false;
    }

    function deleteOrder(id){
        if(window.confirm('削除しますがよろしいですか？')){
            var url = "/s/orders/delete/" + id;
            location.replace(url);
        }
    }
</script>


<blockquote><p><?php echo $this->Html->image("order.png")?> <?php echo $label . '管理';?></p></blockquote>

<p class="description"><?php echo '※新しい顧客コードを追加する場合は、マスター管理の顧客管理から追加してください' ?></p>

<div class="center">
<h5>
<a href="/s/orders/index/<?php echo $type;?>/<?php echo ($year - 1);?>"><i class="glyphicon glyphicon-backward"></i> 前年度 </a>
<b><?php echo sprintf("%s年度", $year);?></b>
<a href="/s/orders/index/<?php echo $type;?>/<?php echo ($year + 1);?>"> 次年度 <i class="glyphicon glyphicon-forward"></i></a>
</h5>
</div>

<ul class="nav nav-tabs" role="tablist">
    <?php
        foreach ($types as $key => $name):
            $class = "";
            if ($key == $type) {
                $class = "active";
            }
    ?>
     <li class=<?php echo $class;?>><a href="/s/orders/index/<?php echo $key;?>/<?php echo $year;?>"><?php echo $name;?></a></li>

    <?php
        endforeach;
    ?>
</ul>

</br></br>

<?php echo $this->Form->create("Order", array("action" => "index", 'type'=>'get'))?>
<?php echo $this->Form->hidden("Order.sc_type", array("value"=>$type))?>
<?php echo $this->Form->hidden("Order.sc_year", array("value"=>$year))?>
<table class="table-condensed well">
    <tr>
        <th style="min-width:50px;">見積書番号</th>
        <td><?php echo $this->Form->text("Order.sc_no", array("class" => "form-control input-sm", 'style'=>SIZE_S, "value"=>$keys['no'][0]));?></td>
        <th style="min-width:50px;">使用者</th>
        <td><?php echo  $this->Form->select("Order.sc_staff_id", $staffs, array("class" => "form-control input-sm", "value"=>$keys['staff_id'][0]));?></td>
        <th style="min-width:50px;">取得日</th>
        <td>
            <div style="display:inline-flex">
            <?php echo $this->Form->text("Order.sc_date_b", array("class" => "form-control input-sm", 'style'=>SIZE_S, "value"=>$keys['date_b'][0]));?>
            ～
            <?php echo $this->Form->text("Order.sc_date_e", array("class" => "form-control input-sm", 'style'=>SIZE_S, "value"=>$keys['date_e'][0]));?>
            </div>
        </td>
        <th style="min-width:70px;">顧客コード</th>
        <td>
            <?php
                echo  $this->Form->select("Order.sc_customer_id", $customers, array("class" => "form-control input-sm", "value"=>$keys['customer_id'][0]));
            ?>
        </td>
        <th style="min-width:50px;">内容</th>
        <td><?php echo $this->Form->text("Order.sc_remark", array("class" => "form-control input-sm", 'style'=>SIZE_L, "value"=>$keys['remark'][0]));?></td>
        <td class="center" colspan="8">
            <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> 検索</button>
        </td>
    </tr>
</table>
<?php echo $this->Form->end()?>

<a href="javascript:void(0);" onclick="editOrder(<?php echo $type;?>, <?php echo $year;?>);"><i class="glyphicon glyphicon-plus-sign"></i> 新規追加</a>

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
    <th style="min-width:90px;"><?php echo $label;?></th>
    <th style="min-width:100px;">使用者</th>
    <th style="min-width:90px;">取得日</th>
    <th style="min-width:70px;">顧客コード</th>
<?php if ($type == 1):?>
    <th style="min-width:90px;">主幹部署</th>
    <th style="min-width:90px;">契約区分</th>
    <th style="min-width:90px;">案件種別</th>
    <th style="min-width:90px;">対象見積番号</th>
<?php endif;?>
    <th style="min-width:600px;">内容</th>
    <th></th>
</tr>
<?php
foreach ($datas as $data):
    $click = sprintf(' onclick="editOrder(%d, %d, %d);"', $type, $year, $data['Order']['id']);

?>
<tr>
    <td class="center" <?php echo $click;?>><?php echo h($data['Order']['no'])?></td>
    <td class="center" <?php echo $click;?>><?php echo h($data['Staff']['name'])?></td>
    <td class="center" <?php echo $click;?>><?php echo h($data['Order']['date'])?></td>
    <td class="center" <?php echo $click;?>>
        <?php echo h($customers[$data['Order']['customer_id']])?>
    </td>
<?php if ($type == 1):?>
    <td class="center" <?php echo $click;?>><?php echo h($data['Department']['name'])?></td>
    <td class="center" <?php echo $click;?>><?php echo h(Configure::read("contract_type")[$data['Order']['contract_type']])?></td>
    <td class="center" <?php echo $click;?>><?php echo h(Configure::read("is_mix")[$data['Order']['is_mix']])?></td>
    <td class="center" <?php echo $click;?>><?php echo h($data['Order']['target_no'])?></td>
<?php endif;?>
    <td <?php echo $click;?>><?php echo h($data['Order']['remark'])?></td>
    <td class="center">
        <a href="javascript:void(0);" onclick="deleteOrder(<?php echo $data['Order']["id"];?>);">
        <i class="glyphicon glyphicon-remove-circle"></i></a>
    </td>
</tr>
<?php
endforeach;
?>
</table>

