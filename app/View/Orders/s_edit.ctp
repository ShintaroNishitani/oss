<script>
    $(function(){
        var columns = ['#OrderDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    });

</script>

<blockquote><p><?php echo $label . '登録';?></p></blockquote>
<p class="text-info"><?php echo $label . '情報を入力してください。';?></p>

<?php echo $this->Form->create('Order', array("name"=>"Order", "action" => "update", "onsubmit" => "return updateOrder();"))?>
<?php echo $this->Form->hidden("Order.id")?>
<?php echo $this->Form->hidden("Order.type", array("value"=>$type))?>
<?php echo $this->Form->hidden("Order.year", array("value"=>$year))?>
<?php echo $this->Form->hidden("Order.enable", array("value"=>1))?>
<?php echo $this->Form->hidden("Order.no")?>

<table class="form">
<?php if(!empty($this->data)): ?>
<tr>
    <th><?php echo $label;?></th>
    <td><?php echo  $this->data['Order']['no'];?></td>
</tr>
<?php endif;?>
<tr>
    <th>使用者*</th>
    <td><?php echo  $this->Form->select("Order.staff_id", $staffs, array("class" => "form-control input-sm", 'style'=>SIZE_M, 'default'=>$me));?></td>
</tr>
<tr>
    <th>取得日*</th>
    <td><?php echo $this->Form->text("Order.date", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S, 'default'=>$today));?></td>
</tr>
<tr>
    <th>顧客コード*</th>
    <td><?php echo  $this->Form->select("Order.customer_id", $customers, array("class" => "form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<?php if ($type == 1):?>
<tr>
    <th>主幹部署*</th>
    <td><?php echo  $this->Form->select("Order.department_id", $departments, array("class" => "form-control input-sm", 'style'=>SIZE_M, 'empty'=>false));?></td>
</tr>
<tr>
    <th>契約区分*</th>
    <td><?php echo  $this->Form->select("Order.contract_type", Configure::read("contract_type"), array("class" => "form-control input-sm", 'style'=>SIZE_M, 'empty'=>false));?></td>
</tr>
<tr>
    <th>案件種別*</th>
    <td><?php echo  $this->Form->select("Order.is_mix", Configure::read("is_mix"), array("class" => "form-control input-sm", 'style'=>SIZE_M, 'empty'=>false));?></td>
</tr>
<tr>
    <th>対象見積番号</th>
    <td><?php echo $this->Form->text("Order.target_no", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<?php endif;?>
<tr>
    <th>内容*</th>
    <td><?php echo $this->Form->text('Order.remark', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_LL));?></td>
</tr>
<tr>
    <td colspan="2" class="center">
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>
</table>

<?php echo $this->Form->end();?>
