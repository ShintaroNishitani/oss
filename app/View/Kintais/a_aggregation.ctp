<blockquote><p><?php echo $this->Html->image("graph.png")?><?php echo ' 勤怠集計';?></p></blockquote>
<p class="guide"><?php echo '実働時間の集計期間、および対象社員を選択して下さい。' ?></p>

<?php echo $this->Form->create("Kintai", array("action" => "aggregation", 'type'=>'get'))?>

<table class="table-condensed well">
    <tr>
        <th style="min-width:50px;">開始年月</th>
        <td>
            <?php echo $this->Form->select("Kintai.sc_term_s", $terms, array("class" => "form-control input-sm", 'style'=>SIZE_S, "value"=>$from_id));?>
        </td>
        <th style="min-width:50px;">終了年月</th>
        <td>
            <?php echo $this->Form->select("Kintai.sc_term_e", $terms, array("class" => "form-control input-sm", 'style'=>SIZE_S, "value"=>$to_id));?>
        </td>
        <th style="min-width:50px;">対象社員</th>
        <td>
            <?php echo $this->Form->select("Kintai.sc_staff", $staffs, array("class" => "form-control input-sm", "value"=>$tgt_staff));?>
        </td>
        <th style="min-width:50px;">その他詳細出力</th>
        <td>
            <?php echo $this->Form->checkbox("Kintai.other_detail", array("checked"=>($other_detail == "1") ? true : false)); ?>
        </td>
    </tr>
    <tr>
        <td class="center" colspan="4">
            <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> 表示</button>
            <button type="submit" class="btn btn-primary" name="csv"><i class="glyphicon glyphicon-download"></i> CSV出力</button>
        </td>
    </tr>
</table>
<?php echo $this->Form->end()?>

<?php if ($tgt_datas != null): ?>

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
    <th >No</th>
    <th style="min-width:100px;">年月</th>
    <th style="min-width:150px;">社員名</th>
    <th style="min-width:300px;">プロジェクト名</th>
    <th style="min-width:50px;">形態</th>
    <th style="min-width:100px;">時間(h)</th>
    <th></th>
</tr>
<?php
    $no = 1;
    foreach ($tgt_datas as $data):
?>
<tr>
    <td class = 'center'><?php echo h ($no++)?></td>
    <td class = 'center'><?php echo h (sprintf ("%04d-%02d", $data['year'], $data['month']))?></td>
    <td><?php echo h ($data['staff_name'])?></td>
    <td><?php echo h ($data['project_name'])?></td>
    <td class = 'center'><?php echo h ($kinds[$data['project_kind']])?></td>
    <td class = 'center'><?php echo h ($data['project_time'])?></td>
</tr>
<?php endforeach; ?>

<?php endif; ?>
