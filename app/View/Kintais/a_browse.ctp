<script>
    $(function(){
        var columns = ['#KintaiTargetDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });

        $('#KintaiRequestStaffId').change(function(){
            location.href = "<?php echo $this->Html->url(array("controller" => "kintais"));?>/<?php echo $year;?>/<?php echo $month;?>/" + $('#KintaiRequestStaffId').val();
        });

        $('#KintaiTargetDate').change(function(){
            var data = $('#KintaiTargetDate').val();
            var datas = data.split("-");
            location.href = "<?php echo $this->Html->url(array("controller" => "kintais"));?>/" + datas[0] + "/" + datas[1] + "/" + $('#KintaiRequestStaffId').val();
        });
    });

    /**
     * [browseKintai description]
     * @param  {[type]} year     [年]
     * @param  {[type]} month    [月]
     * @param  {[type]} staff_id [社員ID]
     * @return {[type]}          [description]
     */
    function browseKintai(year, month, staff_id){
        window.open("/a/kintais/index/" + year + "/" + month + "/" + staff_id);
        loadBaseWindow(url);
        return false;
    }

    /**
     * [printKintai 勤怠印刷]
     * @param  {[type]} id 勤怠ID
     * @return {[type]}    [description]
     */
    function printKintai (id)
    {
        var url = "/a/kintais/print/" + id;
        location.replace(url);
    }

    /**
     * [checkKintai 確認ボタンクリック処理]
     * @param  {[type]} id 勤怠ID
     * @return {[type]}    [description]
     */
    function checkKintai(id){
    	var url = "/a/kintais/check_kintai/" + id;
        location.replace(url);
    }
</script>


<blockquote><p><?php echo $this->Html->image("watch.png")?><?php echo ' 勤怠閲覧';?></p></blockquote>
<p class="guide"><?php echo '閲覧を行う社員・年月を選択して下さい。' ?></p>

<?php echo $this->Form->create("Kintai", array("action" => "browse", 'type'=>'get'))?>
<table class="table-condensed well">
    <tr>
        <th style="min-width:50px;">従業員</th>
        <td>
            <?php echo  $this->Form->select("Kintai.request_staff_id", $staffs, array("class" => "form-control input-sm", "value"=>$keys['request_staff_id'][0]));?>
        </td>
        <th style="min-width:50px;">対象年月</th>
        <td>
            <div style="display:inline-flex">
            <?php echo $this->Form->text("Kintai.target_date", array("class" => "form-control input-sm", 'style'=>SIZE_S, "value"=>$keys['target_date'][0]));?>
            </div>
        </td>
<!--         <td class="center" colspan="8">
            <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> 選択</button>
        </td> -->
    </tr>
</table>
<?php echo $this->Form->end()?>

<div class="center">
<h5>
<a href="/a/kintais/browse/<?php echo $b_year;?>/<?php echo $b_month;?>/<?php echo $keys['request_staff_id'][0];?>"><i class="glyphicon glyphicon-backward"></i> 前月 </a>
<b><?php echo sprintf("%s年 %s月", $year, $month);?></b>
<a href="/a/kintais/browse/<?php echo $n_year;?>/<?php echo $n_month;?>/<?php echo $keys['request_staff_id'][0];?>"> 翌月 <i class="glyphicon glyphicon-forward"></i></a>
</h5>
</div>

<?php if ($kintai != null): ?>
<div class="pull-right well form-inline">
	<p class="guide"><i class="glyphicon glyphicon-play"></i> 上長確認</p>
	<div class="form-group">
		<?php
			echo $this->Form->text("Kintai.check_date", array("class"=>"form-control input-sm", "style"=>SIZE_M, "readonly"=>"readonly", 'value'=>$kintai['Kintai']['check_date']));
		?>
	</div>
	<div class="form-group">
		<?php echo $this->Form->text("CheckStaff.name", array("class"=>"form-control input-sm", "style"=>SIZE_S, "readonly"=>"readonly", 'value'=>$kintai['CheckStaff']['name']))?>
	</div>
	<div class="form-group">
		<?php
			if ($kintai['Kintai']['check_date']) {
				echo $this->Form->button('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> '.__('取消'), array("class"=>"btn btn-warning btn-sm", 'type'=>'button', 'onclick'=>"checkKintai($id)"));
			} else {
				echo $this->Form->button('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> '.__('確認'), array("class"=>"btn btn-success btn-sm", 'type'=>'button', 'onclick'=>"checkKintai($id)"));
			}
		?>
	</div>
</div>

<!-- 合計情報 -->
<?php echo $this->element('kintai_total',
    ['business_day'=>$kintai['Kintai']['business_day'],
     'work_time'=>$kintai['Kintai']['work_time'],
     'late_time'=>$kintai['Kintai']['late_time'],
     'over_time'=>$kintai['Kintai']['over_time'],
     'holiday'=>$kintai['Kintai']['holiday'],
     'extra_time'=>$kintai['Kintai']['extra_time']]); ?>

<br>
<a href="javascript:void(0);" onclick="printKintai(<?php echo $id;?>);"><i class="glyphicon glyphicon-print"></i> 印刷</a>
<br>

<table class="table-bordered table-condensed table-striped table-hover">
    <?php
        $total_work_time = 0.0;
        $total_late_time = 0.0;
        $total_pro_time = array("1"=>"0.0", "2"=>"0.0", "3"=>"0.0", "4"=>"0.0", "5"=>"0.0", "6"=>"0.0");
        $total_pro_sum = 0.0;
        $total_group_time = 0.0;
        $total_training_time = 0.0;
        $total_other_time = 0.0;
        $total_other_sum = 0.0;
        $total_total_sum = 0.0;
    ?>
    <tr>
        <th rowspan="2" style="min-width:30px;">日</th>
        <th rowspan="2" style="min-width:30px;">曜日</th>
        <th rowspan="2" style="min-width:50px;">始業時間</th>
        <th rowspan="2" style="min-width:50px;">終業時間</th>
        <th rowspan="2" style="min-width:30px;">実働時間</th>
        <th rowspan="2" style="min-width:40px;">深夜時間</th>
        <th rowspan="2" style="min-width:50px;">適用</th>
        <th colspan="12" style="min-width:500px;">プロジェクト内訳</th>
        <th rowspan="2" style="min-width:30px;">在宅勤務</th>
        <th rowspan="2" style="min-width:100px;">備考</th>
    </tr>
    <tr>
        <?php for ($i = 1; $i <= 6; $i++): ?>
            <th style="min-width:50px;">
                <?php echo h(sprintf("%s", $kintai['Kintai'][sprintf("project%d_name", $i)])); ?>
                <?php if(!empty($kintai['Kintai'][sprintf("project%d_no", $i)])): ?>
                    </br>
                    <?php echo h(sprintf("【%s】", $kintai['Kintai'][sprintf("project%d_no", $i)])); ?>
                <?php endif;?>
                <?php if(!empty($kintai['Kintai'][sprintf("project%d_kind", $i)])): ?>
                    </br>
                    <?php echo h(sprintf("【%s】", $work_kinds[$kintai['Kintai'][sprintf("project%d_kind", $i)]])); ?>
                <?php endif;?>
            </th>
        <?php endfor;?>
        <th style="min-width:40px;">生産活動計</th>
        <th style="min-width:40px;"><?php echo h($project_label); ?></th>
        <th style="min-width:40px;">研修</th>
        <th style="min-width:40px;">その他</th>
        <th style="min-width:40px;">その他計</th>
        <th style="min-width:40px;">合計</th>
    </tr>
    <?php
        foreach ($kintai['KintaiDetail'] as $data):
            $day_color = "#000000";
            if ($data['holiday']) {
                $day_color = "#ff0000";
            };
    ?>
    <tr>
        <td><font color="<?php echo $day_color;?>"><?php echo h($data['date'])?></font></td>
        <td><font color="<?php echo $day_color;?>"><?php echo h($data['day'])?></font></td>
        <td><?php
            /* 開始時間 : [テキストボックス] 未入力の日については空欄 */
            if ($data['start_time'] != NULL) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.start_time",
                    array("class"=>"form-control input-sm", "style"=>SIZE_SB_TIME, "disabled"=>"disabled", "value"=>$s_times[$data['start_time']]));
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.start_time",
                    array("class"=>"form-control input-sm", "style"=>SIZE_SB_TIME, "disabled"=>"disabled", "value"=>$data['start_time']));
            }
            ?>
        </td>
        <td><?php
            /* 終了時間 : [テキストボックス] 未入力の日については空欄 */
            if ($data['end_time'] != NULL) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.end_time",
                    array("class"=>"form-control input-sm", "style"=>SIZE_SB_TIME, "disabled"=>"disabled", "value"=>$e_times[$data['end_time']]));
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.end_time",
                    array("class"=>"form-control input-sm", "style"=>SIZE_SB_TIME, "disabled"=>"disabled", "value"=>$data['end_time']));
            }
            ?>
        </td>
        <td><?php
            /* 実働時間 : [テキストボックス] */
            if ($data['work_time'] != "" && $data['work_time'] != 0) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.work_time",
                array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                    "value"=>sprintf("%.01f", $data['work_time'])));
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.work_time",
                array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                    "value"=>$data['work_time']));
            }
            $total_work_time += $data['work_time'];
            ?>
        </td>
        <td><?php
            /* 深夜時間 : [テキストボックス] */
            if ($data['late_time'] != "" && $data['late_time'] != 0) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.late_time",
                array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                    "value"=>sprintf("%.01f", $data['late_time'])));
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.late_time",
                array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled"));
            }
            $total_late_time += $data['late_time'];
            ?>
        </td>

        <td><?php
            /* 適用 : [テキストボックス] 未入力の日については空欄 */
            if ($data['application'] != NULL) {
                $items = Configure::read("holiday_type");
                echo $this->Form->text("KintaiDetail.{$data['date']}.application",
                    array("class"=>"form-control input-sm", "style"=>SIZE_SB_APPL, "disabled"=>"disabled", "value"=>$items[$data['application']]));
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.application",
                    array("class"=>"form-control input-sm", "style"=>SIZE_SB_APPL, "disabled"=>"disabled", "value"=>$data['application']));
            }
            ?>
        </td>

        <?php for ($i = 1; $i <= 6; $i++): ?>
            <td><?php
                /* プロジェクト内訳 : [テキストボックス] */
                if ($data[sprintf("project%d_time", $i)] != null) {
                    echo $this->Form->text("KintaiDetail.{$data['date']}.project.{$i}._time",
                        array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                            "value"=>sprintf("%.01f", $data[sprintf("project%d_time", $i)])));
                    $total_pro_time[sprintf("%d", $i)] += $data[sprintf("project%d_time", $i)];
                }
                else {
                    echo $this->Form->text("KintaiDetail.{$data['date']}.project.{$i}._time",
                        array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                            "value"=>$data[sprintf("project%d_time", $i)]));
                }
            ?></td>
            <?php echo $this->Form->hidden("Hidden.{$data['date']}.project.{$i}._time", array("value"=>$data[sprintf("project%d_time", $i)]))?>
        <?php endfor;?>

        <td><?php
            /* 生産活動計 : [テキストボックス] */
            if ($data['project_sum'] != null) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.project_sum",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                        "value"=>sprintf("%.01f", $data['project_sum'])));
                $total_pro_sum += $data['project_sum'];
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.project_sum",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                        "value"=>$data['project_sum']));
            }
        ?></td>
        <td><?php
            /* グループ活動 : [テキストボックス] */
            if ($data['group_time'] != null) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.group_time",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                    "value"=>sprintf("%.01f", $data['group_time'])));
                $total_group_time += $data['group_time'];
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.group_time",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                    "value"=>$data['group_time']));
            }
        ?></td>
        <td><?php
            /* 研修 : [テキストボックス] */
            if ($data['training_time'] != null) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.training_time",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                    "value"=>sprintf("%.01f", $data['training_time'])));
                $total_training_time += $data['training_time'];
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.training_time",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                    "value"=>$data['training_time']));
            }
        ?></td>
        <td><?php
            /* その他 : [テキストボックス] */
            if ($data['other_time'] != null) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.other_time",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                    "value"=>sprintf("%.01f", $data['other_time'])));
                $total_other_time += $data['other_time'];
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.other_time",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                    "value"=>$data['other_time']));
            }
        ?></td>
        <td><?php
            /* その他計 : [テキストボックス] */
            if ($data['other_sum'] != null) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.other_sum",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                         "value"=>sprintf("%0.1f", $data['other_sum'])));
                $total_other_sum += $data['other_sum'];
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.other_sum",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                         "value"=>$data['other_sum']));
            }
        ?></td>
        <td><?php
            /* 合計 : [テキストボックス] */
            if ($data['total_sum'] != null) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.total_sum",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                        "value"=>sprintf("%0.1f", $data['total_sum'])));
                $total_total_sum += $data['total_sum'];
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.total_sum",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                        "value"=>$data['total_sum']));
            }
        ?></td>
        <td class="center">
        	<?php echo $this->Form->checkbox("KintaiDetail.{$data['date']}.remote_work",
				array("checked"=>($data['remote_work'] == "1") ? true : false, "disabled"=>"disabled")); ?>
		</td>
        <td><?php
            /* 備考 : [テキストボックス] */
            echo $this->Form->text("KintaiDetail.{$data['date']}.remarks",
                array("class"=>"form-control input-sm", "style"=>SIZE_LL, "disabled"=>"disabled",
                    "value"=>$data['remarks']))
        ?></td>
    </tr>
    <?php endforeach; ?>

    <th colspan="4" class="center">合計</th>
    <td><?php
        echo $this->Form->text("TotalWorkTime",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                "value"=>sprintf("%.01f", $total_work_time)));
    ?></td>
    <td><?php
        echo $this->Form->text("TotalLateTime",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                "value"=>sprintf("%.01f", $total_late_time)));
    ?></td>
    <td></td>
    <?php for ($i = 1; $i <= 6; $i++): ?>
        <td><?php
            echo $this->Form->text("TotalPro.{$i}.Time",
                array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                    "value"=>sprintf("%.01f", $total_pro_time[sprintf("%d", $i)])));
        ?></td>
    <?php endfor;?>
    <td><?php
        echo $this->Form->text("TotalProSum",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                "value"=>sprintf("%.01f", $total_pro_sum)));
    ?></td>
    <td><?php
        echo $this->Form->text("TotalGroupTime",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                "value"=>sprintf("%.01f", $total_group_time)));
    ?></td>
    <td><?php
        echo $this->Form->text("TotalTrainingTime",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                "value"=>sprintf("%.01f", $total_training_time)));
    ?></td>
    <td><?php
        echo $this->Form->text("TotalOtherTime",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                "value"=>sprintf("%.01f", $total_other_time)));
    ?></td>
    <td><?php
        echo $this->Form->text("TotalOtherSum",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                "value"=>sprintf("%.01f", $total_other_sum)));
    ?></td>
    <td><?php
        echo $this->Form->text("TotalTotalSum",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                "value"=>sprintf("%.01f", $total_total_sum)));
    ?></td>
	<td colspan="2">
		&nbsp;&nbsp;
		<label>
			<?php
                if ($kintai['Kintai']['transport'] == "1") echo $this->Html->image("check_on.png");
                else echo $this->Html->image("check_off.png");
            ?>
			定期
		</label>
		&nbsp;&nbsp;&nbsp;
		<label>
			<?php
                if ($kintai['Kintai']['transport'] == "2") echo $this->Html->image("check_on.png");
                else echo $this->Html->image("check_off.png");
            ?>
			小口精算
		</label>
    </td>
</table>
<?php endif; ?>
