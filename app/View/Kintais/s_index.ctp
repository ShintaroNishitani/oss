<script>
	
	const BG_COLOR_GRAY = "#eeeeee";
	const BG_COLOR_WHITE = "#ffffff";
	const BG_COLOR_CAUTION = "#ffff00";
	const BG_COLOR_ERROR = "#ff0000";

	const HOURS = [6.5,  7.0,  7.5,  8.0,  8.5,  9.0,  9.5,  10.0, 10.5, 11.0,
					11.5, 12.0, 14.0, 15.0, 15.5, 16.0, 16.5, 17.0, 17.5, 18.0,
					18.5, 19.0, 19.5, 20.0, 20.5, 21.0, 21.5, 22.5, 23.0, 23.5,
					24.0, 24.5, 25.0, 25.5, 26.0, 26.5, 27.0, 27.5, 28.0, 28.5,
					29.0, 29.5, 30.0];

    $(document).ready(function() {
        loadConfirmList();

		initView();

		switchSubmitDisabled();

        // 登録ボタンClick時の処理
        $('#formSubmitLimit').click(function()
        {
            var parent = $(this).parents("Form");	// thisはクリックされた要素

            // ボタンを無効化
            $('#formSubmitLimit').attr('disabled', true);

            // 当月の日数分ループ
            var day_num = document.getElementById("KintaiDayNum");
            for ($i = 1; $i <= day_num.value; $i++)
            {
                // 入力フォームの内容を取得するリストを作成
                var formList = {
                    id:            "[name=\"data[KintaiDetail][" + $i + "][id]\"]",
                    kintai_id:     "[name=\"data[KintaiDetail][" + $i + "][kintai_id]\"]",
                    staff_id:      "[name=\"data[Kintai][staff_id]\"]",
                    year:          "[name=\"data[KintaiDetail][" + $i + "][year]\"]",
                    month:         "[name=\"data[KintaiDetail][" + $i + "][month]\"]",
                    date:          "[name=\"data[KintaiDetail][" + $i + "][date]\"]",
                    day:           "[name=\"data[KintaiDetail][" + $i + "][day]\"]",
                    holiday:       "[name=\"data[KintaiDetail][" + $i + "][holiday]\"]",
                    start_time:    "[name=\"data[KintaiDetail][" + $i + "][start_time]\"]",
                    end_time:      "[name=\"data[KintaiDetail][" + $i + "][end_time]\"]",
                    work_time:     "[name=\"data[KintaiDetail][" + $i + "][work_time]\"]",
                    late_time:     "[name=\"data[KintaiDetail][" + $i + "][late_time]\"]",
                    application:   "[name=\"data[KintaiDetail][" + $i + "][application]\"]",
                    project1_time: "[name=\"data[KintaiDetail][" + $i + "][project][1][_time]\"]",
                    project2_time: "[name=\"data[KintaiDetail][" + $i + "][project][2][_time]\"]",
                    project3_time: "[name=\"data[KintaiDetail][" + $i + "][project][3][_time]\"]",
                    project4_time: "[name=\"data[KintaiDetail][" + $i + "][project][4][_time]\"]",
                    project5_time: "[name=\"data[KintaiDetail][" + $i + "][project][5][_time]\"]",
                    project6_time: "[name=\"data[KintaiDetail][" + $i + "][project][6][_time]\"]",
                    project_sum:   "[name=\"data[KintaiDetail][" + $i + "][project_sum]\"]",
                    group_time:    "[name=\"data[KintaiDetail][" + $i + "][group_time]\"]",
                    training_time: "[name=\"data[KintaiDetail][" + $i + "][training_time]\"]",
                    other_time:    "[name=\"data[KintaiDetail][" + $i + "][other_time]\"]",
                    other_sum:     "[name=\"data[KintaiDetail][" + $i + "][other_sum]\"]",
                    total_sum:     "[name=\"data[KintaiDetail][" + $i + "][total_sum]\"]",
                    remarks:       "[name=\"data[KintaiDetail][" + $i + "][remarks]\"]"
                }

                // POSTメソッドで送るデータを作成
                var sendList = {
                    id:            parent.find(formList.id).val(),
                    kintai_id:     parent.find(formList.kintai_id).val(),
                    staff_id:      parent.find(formList.staff_id).val(),
                    year:          parent.find(formList.year).val(),
                    month:         parent.find(formList.month).val(),
                    date:          parent.find(formList.date).val(),
                    day:           parent.find(formList.day).val(),
                    holiday:       parent.find(formList.holiday).val(),
                    start_time:    parent.find(formList.start_time).val(),
                    end_time:      parent.find(formList.end_time).val(),
                    work_time:     parent.find(formList.work_time).val(),
                    late_time:     parent.find(formList.late_time).val(),
                    application:   parent.find(formList.application).val(),
                    project1_time: parent.find(formList.project1_time).val(),
                    project2_time: parent.find(formList.project2_time).val(),
                    project3_time: parent.find(formList.project3_time).val(),
                    project4_time: parent.find(formList.project4_time).val(),
                    project5_time: parent.find(formList.project5_time).val(),
                    project6_time: parent.find(formList.project6_time).val(),
                    project_sum:   parent.find(formList.project_sum).val(),
                    group_time:    parent.find(formList.group_time).val(),
                    training_time: parent.find(formList.training_time).val(),
                    other_time:    parent.find(formList.other_time).val(),
                    other_sum:     parent.find(formList.other_sum).val(),
                    total_sum:     parent.find(formList.total_sum).val(),
                    remarks:       parent.find(formList.remarks).val()
                };

                /**
                 * Ajax通信メソッド
                 * @param type  : HTTP通信の種類
                 * @param url   : リクエスト送信先のURL
                 * @param data  : サーバに送信する値
                 */
                $.ajax({
                    type: "POST",
                    async: false,
                    url:  "/s/kintais/ajax_save/",
                    data: sendList,
                    dataType: "json",
                    // Ajax通信が成功した場合に呼び出されるメソッド
                    success: function(result)
                    {
                        if (result.status) {
                            //alert('[処理成功]:' + sendList.date);
                        }
                        else {
                            alert(result.error);
                        }
                    },
                    // Ajax通信が失敗した場合に呼び出されるメソッド
                    error: function()
                    {
                        //alert('[通信失敗]:' + sendList.date);
                    }
                })
            }

            // ボタンを有効化
            $('#formSubmitLimit').attr('disabled', false);
        });
    });

    function loadConfirmList() {
        var url = "/s/confirms/list/" + <?php echo CONFIRM_TYPE_0?> + "/" + <?php echo $id?> + "/" + <?php echo $year?> + "/" + <?php echo $month?>;
        if (<?php echo $comfirm_flag?> == "0") {
            $('#ConfirmList').load(url);
        }
    }

    /**
    * [editProject description]
    * @param  {[type]} year  年
    * @param  {[type]} month 月
    * @param  {[type]} id    勤怠テーブルID
    * @return {[type]}      [description]
    */
    function editProject(year, month, id)
    {
        var url = '/s/kintais/edit_project/' + year + '/' + month + '/' + id;
        loadBaseWindow(url);
        return false;
    }


    /**
    * [initView 初期表示]
    * @return {[type]}      [description]
    */
	function initView()
	{
		var day_num = document.getElementById("KintaiDayNum");
		for (let i = 1; i <= day_num.value; i++) {
			setWorkTimeErrorDate(i);
			setApplicationBgColor(i);
		}
	}

    /**
    * [calcWorkTimeDate 実働時間計算処理]
    * @param  {[type]} date 日付
    * @return {[type]}      [description]
    */
    function calcWorkTimeDate(date)
    {
		calcWorkTime(date);
		
		// 登録ボタンの有効化切替
		switchSubmitDisabled();
    }

    /**
    * [calcWorkTime 実働時間計算処理]
    * @param  {[type]} date 日付
    * @return {[type]}      [description]
    */
    function calcWorkTime(date)
    {
        let result = true;
        
        var elmStartTime = document.getElementById("KintaiDetail" + date + "StartTime");
        var elmEndTime = document.getElementById("KintaiDetail" + date + "EndTime");
        var elmWorkTime = document.getElementById("KintaiDetail" + date + "WorkTime");
        var elmLateTime = document.getElementById("KintaiDetail" + date + "LateTime");
        var elmApplication = document.getElementById("KintaiDetail" + date + "Application");
        var elmProject1Time = document.getElementById("KintaiDetail" + date + "Project1Time");
        var elmProject2Time = document.getElementById("KintaiDetail" + date + "Project2Time");
        var elmProject3Time = document.getElementById("KintaiDetail" + date + "Project3Time");
        var elmProject4Time = document.getElementById("KintaiDetail" + date + "Project4Time");
        var elmProject5Time = document.getElementById("KintaiDetail" + date + "Project5Time");
        var elmProject6Time = document.getElementById("KintaiDetail" + date + "Project6Time");
        var elmProjectSum = document.getElementById("KintaiDetail" + date + "ProjectSum");
        var elmGroupTime = document.getElementById("KintaiDetail" + date + "GroupTime");
        var elmTrainingTime = document.getElementById("KintaiDetail" + date + "TrainingTime");
        var elmOtherTime = document.getElementById("KintaiDetail" + date + "OtherTime");
        var elmTotalSumTime = document.getElementById("KintaiDetail" + date + "TotalSumTime");
        var elmSIvdId = document.getElementById("KintaiSIvdId");
        var elmEIvdId = document.getElementById("KintaiEIvdId");

        // 更新前の実働時間・深夜時間を保持
        var bef_work_time = elmWorkTime.value;
        var bef_late_time = elmLateTime.value;

        // 始業時間・終業時間が未設定の場合
        if (elmStartTime.value == elmSIvdId.value || elmEndTime.value == elmEIvdId.value) {
            // 実働時間・深夜時間を初期化
            elmWorkTime.value = "";
            elmLateTime.value = "";

            // 始業・終業のどちらかのみが未設定の場合、合計欄の背景色を変更
            if ((elmStartTime.value == elmSIvdId.value && elmEndTime.value != elmEIvdId.value) ||
                (elmStartTime.value != elmSIvdId.value && elmEndTime.value == elmEIvdId.value)) {
	        	result = false;
            }
            // 始業・終業の両方が未設定の場合、稼働時間を初期化
            else {
	            elmProject1Time.value = "";
	            elmProject2Time.value = "";
	            elmProject3Time.value = "";
	            elmProject4Time.value = "";
	            elmProject5Time.value = "";
	            elmProject6Time.value = "";
	            elmGroupTime.value = "";
	            elmTrainingTime.value = "";
	            elmOtherTime.value = "";
	            
				// 生産活動計更新
				calcProjectSum(date);

				// 非生産活動計更新
				calcOtherSum(date);

				// 合計更新
				result = calcTotalSum(date)
            }
        }
        else {
	        var start_time    = HOURS[elmStartTime.value-1];
	        var end_time      = HOURS[elmEndTime.value-1];
	        var result_time   = 0.0;
	        var late_time     = 0.0;
	        var noon_break    = 1.0;
	        var night_break   = 0.0;
	        var late_break    = 0.0;

	        // 始業時間 >= 終業時間の場合
	        if (start_time >= end_time) {
	            // 実働時間・深夜時間・プロジェクト時間を初期化
	            elmWorkTime.value = "";
	            elmLateTime.value = "";
	            elmProject1Time.value = "";
	            elmProject2Time.value = "";
	            elmProject3Time.value = "";
	            elmProject4Time.value = "";
	            elmProject5Time.value = "";
	            elmProject6Time.value = "";

				result = false;
			}
			else {
				// 合計欄の背景色を戻す
				setTotalSumBgColor(date, BG_COLOR_GRAY);

				// 昼休憩・・・午前半休 or 午後半休の取得時は無し
				if (elmStartTime.value >= 13 ||
					elmEndTime.value == 12) {
					noon_break = 0.0;
				}
				// 夜休憩・・・終業時間が22時30分以降の場合に発生
				if (elmEndTime.value >= 28) {
					night_break = 0.5;
				}
				// 深夜休憩・・・終業時間が2時30分以降の場合に発生
				if (elmEndTime.value >= 36) {
					late_break = 0.5;
				}
				result_time = end_time - start_time - noon_break - night_break - late_break;

				// 実働時間を設定
				elmWorkTime.value = result_time.toFixed(1);

				// 深夜残業を設定
				if (elmEndTime.value >= 28) {
					if (elmEndTime.value <= 35) {
						late_time = end_time - 22.0;
					}
					else if (elmEndTime.value <= 43) {
						late_time = end_time - 22.0 - 0.5;
					}
					else {
						late_time = end_time - 22.0 - 1.0;
					}
					elmLateTime.value = late_time.toFixed(1);
				}
				else {
					elmLateTime.value = "";
				}

				// プロジェクト１に実働時間を自動設定
				elmProject1Time.value = result_time.toFixed(1);

				// プロジェクト１以外は初期化
				elmProject2Time.value = "";
				elmProject3Time.value = "";
				elmProject4Time.value = "";
				elmProject5Time.value = "";
				elmProject6Time.value = "";

				// 各種合計時間を更新
				calcTargetTotalTime("Work");
				calcTargetTotalTime("Late");
				calcTargetTotalTime("Project1");
				calcTargetTotalTime("Project2");
				calcTargetTotalTime("Project3");
				calcTargetTotalTime("Project4");
				calcTargetTotalTime("Project5");
				calcTargetTotalTime("Project6");

				// 生産活動計を設定 (合計も設定)
				calcProjectSum(date);
        
				result = calcTotalSum(date);
			}
		}
		
		// 始業・終業のセレクトボックス変更時に色変更が必要なので、ここで色変更を行う
		setTotalSumBgColor(date, result);
		
		// 摘要欄の色変更も行う
		setApplicationBgColor(date);
		
  		return result;
    }


    /**
     * [setSumErrorDate 対象日付の合計エラー設定処理]
     * @param  {[type]} date   日付
     * @return {[type]}      [description]
     */
	function setWorkTimeErrorDate(date)
	{
		let result = true;

		var elmStartTime = document.getElementById("KintaiDetail" + date + "StartTime");
		var elmEndTime = document.getElementById("KintaiDetail" + date + "EndTime");
		var elmSIvdId = document.getElementById("KintaiSIvdId");
		var elmEIvdId = document.getElementById("KintaiEIvdId");

		// 始業時間・終業時間が未設定の場合
		if (elmStartTime.value == elmSIvdId.value || elmEndTime.value == elmEIvdId.value) {
			// 始業・終業のどちらかのみが未設定の場合、エラー
			if ((elmStartTime.value == elmSIvdId.value && elmEndTime.value != elmEIvdId.value) ||
				(elmStartTime.value != elmSIvdId.value && elmEndTime.value == elmEIvdId.value)) {
				result = false;
			}
            // 始業・終業の両方が未設定の場合、稼働時間を初期化
            else {
				// 合計更新
				result = calcTotalSum(date)
            }
        }
        else {
	        var start_time    = HOURS[elmStartTime.value-1];
	        var end_time      = HOURS[elmEndTime.value-1];

	        // 始業時間 >= 終業時間の場合
	        if (start_time >= end_time) {
				result = false;
			}
			else {
				result = calcTotalSum(date);
			}
		}

		setTotalSumBgColor(date, result);
		
  		return result;
	}
	

    /**
     * [setTotalSumBgColor 対象日付の合計欄の背景色設定処理]
     * @param  {[type]} date   日付
     * @param  {[type]} isSuccess 処理
     * @return {[type]}      [description]
     */
	function setTotalSumBgColor(date, isSuccess)
	{
		var elmTotalSumTime = document.getElementById("KintaiDetail" + date + "TotalSumTime");
		let sum_error = document.getElementById("KintaiDetail" + date + "SumError");

		if (isSuccess) {
			elmTotalSumTime.style.background = BG_COLOR_GRAY;
			sum_error.value = "0";
		}
		else {
			elmTotalSumTime.style.background = BG_COLOR_ERROR;
			sum_error.value = "1";
		}
	}

    /**
     * [onchangeApplication 適用更新処理]
     * @param  {[type]} date 日付
     * @return {[type]}      [description]
     */
    function onchangeApplication(date)
    {
        var elmApplication = document.getElementById("KintaiDetail" + date + "Application");
        var elmStartTime = document.getElementById("KintaiDetail" + date + "StartTime");
        var elmEndTime = document.getElementById("KintaiDetail" + date + "EndTime");
        var elmSIvdId = document.getElementById("KintaiSIvdId");
        var elmEIvdId = document.getElementById("KintaiEIvdId");
        var elmRemoteWork = document.getElementById("KintaiDetail" + date + "RemoteWork");

        // 半休以外の選択がされている場合
        if (elmApplication.value != "" &&
            elmApplication.value != <?php echo HOLIDAY_MORNING_OFF;?> &&
            elmApplication.value != <?php echo HOLIDAY_AFTERNOON_OFF;?> &&
            elmApplication.value != <?php echo HOLIDAY_OBSERVED_HALF;?>) {
            // 始業時間・終業時間を初期化
            elmStartTime.value = elmSIvdId.value;
            elmEndTime.value = elmEIvdId.value;

            // 在宅勤務を自動チェック
            elmRemoteWork.checked = true;
            
            // 実働時間を再計算
            calcWorkTime(date);
        }
        // 未選択への変更の場合
        else if (elmApplication.value == "") {
            // 在宅勤務は初期化
            elmRemoteWork.checked = false;
        }
        
        // 背景色設定
        setApplicationBgColor(date);
    }


    /**
     * [setApplicationBgColor 適用欄の背景色設定処理]
     * @param  {[type]} date 日付
     * @return {[type]}      [description]
     */
    function setApplicationBgColor(date)
    {
		var elmApplication = document.getElementById("KintaiDetail" + date + "Application");
		var elmStartTime = document.getElementById("KintaiDetail" + date + "StartTime");
		var elmEndTime = document.getElementById("KintaiDetail" + date + "EndTime");
		var elmSIvdId = document.getElementById("KintaiSIvdId");
		var elmEIvdId = document.getElementById("KintaiEIvdId");
		let app_error = document.getElementById("KintaiDetail" + date + "AppError");
  
		// 半休以外の選択がされ、かつ始業・終業時間が選択されている場合、エラー表示
		if (elmApplication.value != "" &&
			elmApplication.value != <?php echo HOLIDAY_MORNING_OFF;?> &&
			elmApplication.value != <?php echo HOLIDAY_AFTERNOON_OFF;?> &&
			elmApplication.value != <?php echo HOLIDAY_OBSERVED_HALF;?> &&
			elmStartTime.value != elmSIvdId.value &&
			elmEndTime.value != elmEIvdId.value) {
			elmApplication.style.background = BG_COLOR_ERROR;

			app_error.value = "1";
		}
		// 有給休暇選択でかつ特別休暇の残日数がある場合、警告表示
		else if (elmApplication.value != '' && elmApplication.value == <?php echo HOLIDAY_PAID_ALL;?> &&
			<?php echo $special_remain?> > 0) {
			elmApplication.style.background = BG_COLOR_CAUTION;

			app_error.value = "0";
		}
		else {
			elmApplication.style.background = BG_COLOR_WHITE;

			app_error.value = "0";
		}
		
		// 登録ボタンの有効化切替
		switchSubmitDisabled();
	}


    /**
    * [onblurProjectTime プロジェクト内訳時間変更処理]
    * @param  {[type]} date   日付
    * @param  {[type]} target 設定対象項目名 (ex. "Project1", "Training", "Other")
    * @return {[type]}      [description]
    */
    function onblurProjectTime(date, target)
    {
        var elm = document.getElementById("KintaiDetail" + date + target + "Time");
        var tmp = Number(elm.value);

        if (tmp > 0) {
            elm.value = tmp.toFixed(1);
        }
        else {
            elm.value = "";
        }

        // 月合計時間を更新
        calcTargetTotalTime(target);

        // 生産活動計算出
        calcProjectSum(date);

        // 非生産活動計算出
        calcOtherSum(date);

        // 合計算出処理
        let ret = calcTotalSum(date);
        
        // 合計欄の背景色設定
		setTotalSumBgColor(date, ret);
		
		// 登録ボタンの有効化切替
		switchSubmitDisabled();
    }
    

    /**
    * [calcProjectSum 生産活動計算出処理]
    * @param  {[type]} date 日付
    * @return {[type]}      [description]
    */
    function calcProjectSum(date)
    {
        var elmProjectSum = document.getElementById("KintaiDetail" + date + "ProjectSumTime");
        var elmProjectTime;
        var sum_time = 0.0;

        for ($i = 1; $i <= 6; $i++) {
            elmProjectTime = document.getElementById("KintaiDetail" + date + "Project" + $i + "Time");
            sum_time += Number(elmProjectTime.value)
        }

        if (sum_time > 0) {
            elmProjectSum.value = sum_time.toFixed(1);
        }
        else {
            elmProjectSum.value = "";
        }

        // 月合計時間を更新
        calcTargetTotalTime("ProjectSum");

        // 合計算出処理
        calcTotalSum(date);
    }

    /**
    * [calcOtherSum その他計算出処理]
    * @param  {[type]} date 日付
    * @return {[type]}      [description]
    */
    function calcOtherSum(date)
    {
        var elmGroupTime    = document.getElementById("KintaiDetail" + date + "GroupTime");
        var elmTrainingTime = document.getElementById("KintaiDetail" + date + "TrainingTime");
        var elmOtherTime    = document.getElementById("KintaiDetail" + date + "OtherTime");
        var elmOtherSum     = document.getElementById("KintaiDetail" + date + "OtherSumTime");
        var sum_time        = 0.0;

        sum_time = Number(elmGroupTime.value)
                 + Number(elmTrainingTime.value)
                 + Number(elmOtherTime.value)
                 ;

        if (sum_time > 0) {
            elmOtherSum.value = sum_time.toFixed(1);
        }
        else {
            elmOtherSum.value = "";
        }

        // 月合計時間を更新
        calcTargetTotalTime("OtherSum");
    }

    /**
    * [calcTotalSum 合計算出処理]
    * @param  {[type]} date 日付
    * @return {[type]}      [description]
    */
    function calcTotalSum(date)
    {
        var elmProjectSum = document.getElementById("KintaiDetail" + date + "ProjectSumTime");
        var elmOtherSum   = document.getElementById("KintaiDetail" + date + "OtherSumTime");
        var elmTotalSum   = document.getElementById("KintaiDetail" + date + "TotalSumTime");
        var elmWorkTime   = document.getElementById("KintaiDetail" + date + "WorkTime");
        var sum_time      = 0.0;

        // 生産活動計 + その他計の時間を合計時間に設定
        sum_time = Number(elmProjectSum.value) + Number(elmOtherSum.value);

        if (sum_time > 0) {
            elmTotalSum.value = sum_time.toFixed(1);
        }
        else {
            elmTotalSum.value = "";
        }

        // 合計時間が実働時間と一致しない場合、エラー
        let ret = true;
        if (elmTotalSum.value != elmWorkTime.value) {
 			ret = false;
        }

        // 月合計時間を更新
        calcTargetTotalTime("TotalSum");

		return ret;
    }

    /**
    * [calcTargetTotalTime 合計時間計算処理]
    * @param  {[type]} target 合計時間設定対象項目名 (ex. "Work", "Late")
    * @return {[type]}        [description]
    */
    function calcTargetTotalTime(target)
    {
        var elmTotalTime = document.getElementById("KintaiTotal" + target + "Time");
        var elmTime;
        var total_time = 0.0;

        // 当月の日数分ループ
        var day_num = document.getElementById("KintaiDayNum");
        for ($i = 1; $i <= day_num.value; $i++) {
            elmTime = document.getElementById("KintaiDetail" + $i + target + "Time");
            total_time += Number(elmTime.value);
        }

        if (total_time > 0) {
            elmTotalTime.value = total_time.toFixed(1);
        }
        else {
            elmTotalTime.value = "0.0";
        }
    }

    /**
	 * [switchSubmitDisabled 登録ボタンの有効化切替
	 * @return {[type]}      [description]
	 */
	function switchSubmitDisabled()
	{
		let day_num = document.getElementById("KintaiDayNum");
		let result = true;
		for (let i = 1; i <= day_num.value; i++) {
			let elmTotalSumTime = document.getElementById("KintaiDetail" + i + "TotalSumTime");
			let sum_error = document.getElementById("KintaiDetail" + i + "SumError");
			let app_error = document.getElementById("KintaiDetail" + i + "AppError");
			if (sum_error.value == "1" || app_error.value == "1") {
				result = false;
				break;
			}
		}
		
		// 全日ともエラー無しの場合、登録ボタンを有効化
		if (result) {
			$('#formSubmit').attr('disabled', false);
		}
		// １日でもエラーありの場合、登録ボタンを無効化
		else {
			$('#formSubmit').attr('disabled', true);
		}
	}
	
    /**
     * [printKintai 勤怠印刷]
     * @param  {[type]} id 勤怠ID
     * @return {[type]}    [description]
     */
    function printKintai(id){
        var url = "/s/kintais/print/" + id;
        location.replace(url);
    }

    /**
     * [setArrival 出社ボタンクリック処理]
     * @param  {[type]} id 勤怠ID
     * @return {[type]}    [description]
     */
    function setArrival(id) {
        var url = "/s/kintais/set_starttime/" + id;
        location.replace(url);
    }

    /**
     * [setQuitting 退社ボタンクリック処理]
     * @param  {[type]} id 勤怠ID
     * @return {[type]}    [description]
     */
    function setQuitting(id){
        var url = "/s/kintais/set_endtime/" + id;
        location.replace(url);
    }

</script>

<blockquote><p><?php echo $this->Html->image("watch.png")?> 勤怠表</p></blockquote>
<p class="guide"><?php echo '勤怠を入力してください。' ?></p>
<p class="guide"><?php echo '[プロジェクト]欄：前月からの継続プロジェクトが設定されます。変更がある場合は、Clickから編集を行って下さい。' ?></p>
<?php if ($comfirm_flag == 0):?>
    <p class="description"><?php echo '※申請後は編集できません。編集を行う場合は、承認者に連絡してください。' ?></p>
<?php else: ?>
    <p class="description"><?php echo '※未入力箇所があるため、申請フォームを表示していません。全日分の勤怠を入力してください。' ?></p>
<?php endif;?>

<div id="ConfirmList"><!-- 申請リスト --></div>

<?php
    /* 承認済みの場合、"disable"属性を設定 */
    if ($status == CONFIRM_IN || $status == CONFIRM_OK) {
        $disabled = 'disabled';
    }
    else {
        $disabled = '';
    }
?>

<div class="center">
<h5>
<a href="/s/kintais/index/<?php echo $b_year;?>/<?php echo $b_month;?>"><i class="glyphicon glyphicon-backward"></i> 前月 </a>
<b><?php echo sprintf("%s年 %s月", $year, $month);?></b>
<a href="/s/kintais/index/<?php echo $n_year;?>/<?php echo $n_month;?>"> 翌月 <i class="glyphicon glyphicon-forward"></i></a>
</h5>
</div>

<div class="pull-right well form-inline">
	<p class="guide"><i class="glyphicon glyphicon-play"></i> 上長確認</p>
	<div class="form-group">
		<?php
			echo $this->Form->text("Kintai.check_date", array("class"=>"form-control input-sm", "style"=>SIZE_M, "readonly"=>"readonly", 'value'=>$datas['Kintai']['check_date']));
		?>
	</div>
	<div class="form-group">
		<?php echo $this->Form->text("CheckStaff.name", array("class"=>"form-control input-sm", "style"=>SIZE_S, "readonly"=>"readonly", 'value'=>$datas['CheckStaff']['name']))?>
	</div>
</div>

<?php
    $trans_remain = 0.0;
    foreach ($staff['TransHoliday'] as $trans_holiday):
?>
<?php
    if ($trans_holiday['digestion_date1'] == "0000-00-00") {
        $trans_remain += 0.5;
    }
    if ($trans_holiday['digestion_date2'] == "0000-00-00") {
        $trans_remain += 0.5;
    }
?>
<?php endforeach; ?>

<!-- 合計情報 -->
<?php echo $this->element('kintai_total',
    ['paid_remain'=>$staff['PaidHoliday'][0]['holiday_remain'],
     'trans_remain'=>$trans_remain,
     'half_remain'=>$staff['PaidHoliday'][0]['half_remain'],
     'business_day'=>$datas['Kintai']['business_day'],
     'work_time'=>$datas['Kintai']['work_time'],
     'late_time'=>$datas['Kintai']['late_time'],
     'over_time'=>$datas['Kintai']['over_time'],
     'holiday'=>$datas['Kintai']['holiday'],
     'extra_time'=>$datas['Kintai']['extra_time']]); ?>

<?php echo $this->Form->create('Kintai', array('name'=>'KintaiDetail', 'action'=>'detail_save'))?>
<?php echo $this->Form->hidden("Kintai.id", array("value"=>$id))?>
<?php echo $this->Form->hidden("Kintai.year", array("value"=>$year))?>
<?php echo $this->Form->hidden("Kintai.month", array("value"=>$month))?>
<?php echo $this->Form->hidden("Kintai.staff_id", array("value"=>$staff['Staff']['id']))?>
<?php echo $this->Form->hidden("Kintai.work_time", array("value"=>$datas['Kintai']['work_time']))?>
<?php echo $this->Form->hidden("Kintai.late_time", array("value"=>$datas['Kintai']['late_time']))?>
<?php echo $this->Form->hidden("Kintai.over_time", array("value"=>$datas['Kintai']['over_time']))?>
<?php echo $this->Form->hidden("Kintai.extra_time", array("value"=>$datas['Kintai']['extra_time']))?>
<?php echo $this->Form->hidden("Kintai.holiday", array("value"=>$datas['Kintai']['holiday']))?>
<?php echo $this->Form->hidden("Kintai.day_num", array("value"=>$day_num))?>
<?php echo $this->Form->hidden("Kintai.s_ivd_id", array("value"=>$s_ivd_id))?>
<?php echo $this->Form->hidden("Kintai.e_ivd_id", array("value"=>$e_ivd_id))?>
<?php echo $this->Form->hidden("Kintai.transport", array("value"=>$datas['Kintai']['transport']))?>

<tr>
    <td class="center">
        <?php if ($status == CONFIRM_YET || $status == CONFIRM_NO):?>
        <button type="submit" class="btn btn-primary" href="javascript:void(0);" id="formSubmit">
            <i class="glyphicon glyphicon-save"></i> 登録
        </button>
        <?php endif;?>
    </td>
</tr>

<a href="javascript:void(0);" onclick="printKintai(<?php echo $id;?>);"><i class="glyphicon glyphicon-print"></i> 出力</a>
<td class="center">
    <?php echo $this->Form->button("出社", array("class"=>"btn btn-success", 'type'=>'button', 'onclick'=>"setArrival($id)"));?>
</td>
<td class="center">
    <?php echo $this->Form->button("退社", array("class"=>"btn btn-success", 'type'=>'button', 'onclick'=>"setQuitting($id)"));?>
</td>

<td class="center">
    <?php if ($status == CONFIRM_YET || $status == CONFIRM_NO):?>
    <button class="btn btn-primary" href="javascript:void(0);" id="formSubmitLimit">
        <i class="glyphicon glyphicon-save"></i> 登録 (通信制限回避用)
    </button>
    <?php endif;?>
</td>

<table class="table-bordered table-condensed table-hover">
    <?php
        $total_work_time = 0.0;
        $total_late_time = 0.0;
        $total_pro_time = array("1"=>"0.0", "2"=>"0.0", "3"=>"0.0", "4"=>"0.0", "5"=>"0.0", "6"=>"0.0");
        $total_pro_sum = 0.0;
        $total_group_time = 0.0;
        $total_training_time = 0.0;
        $total_other_time = 0.0;
        $total_other_sum = 0.0;
        $total_total_sum = 0.0;
        $click = "";
        if ($status == CONFIRM_YET || $status == CONFIRM_NO) {
            $click = sprintf('onclick="editProject(%d, %d, %d);"', $year, $month, $id);
        }
    ?>
    <tr>
        <th rowspan="2" style="min-width:30px;">日</th>
        <th rowspan="2" style="min-width:30px;">曜日</th>
        <th rowspan="2" style="min-width:50px;">始業時間</th>
        <th rowspan="2" style="min-width:50px;">終業時間</th>
        <th rowspan="2" style="min-width:30px;">実働時間</th>
        <th rowspan="2" style="min-width:40px;">深夜時間</th>
        <th rowspan="2" style="min-width:50px;">適用</th>
        <th colspan="12" style="min-width:500px;">プロジェクト内訳</th>
        <th rowspan="2" style="min-width:30px;">在宅勤務</th>
        <th rowspan="2" style="min-width:100px;">備考</th>
    </tr>
    <tr>
        <?php for ($i = 1; $i <= 6; $i++): ?>
            <th style="min-width:50px;" <?php echo $click;?>>
                <?php echo h(sprintf("%s", $datas['Kintai'][sprintf("project%d_name", $i)])); ?>
                <?php if(!empty($datas['Kintai'][sprintf("project%d_no", $i)])): ?>
                    </br>
                    <?php echo h(sprintf("【%s】", $datas['Kintai'][sprintf("project%d_no", $i)])); ?>
                <?php endif;?>
                <?php if(!empty($datas['Kintai'][sprintf("project%d_kind", $i)])): ?>
                    </br>
                    <?php echo h(sprintf("【%s】", $work_kinds[$datas['Kintai'][sprintf("project%d_kind", $i)]])); ?>
                <?php endif;?>
            </th>
        <?php endfor;?>
        <th style="min-width:40px;">生産活動計</th>
        <th style="min-width:40px;"><?php echo h($project_label); ?></th>
        <th style="min-width:40px;">研修</th>
        <th style="min-width:40px;">その他</th>
        <th style="min-width:40px;">その他計</th>
        <th style="min-width:40px;">合計</th>
    </tr>
    <?php
        foreach ($datas['KintaiDetail'] as $data):
            $day_color = "#000000";
            if ($data['holiday']) {
                $day_color = "#ff0000";
            };
            $row_color = "#ffffff";
            if ($data['date'] == $today_d && $year == $today_y && $month == $today_m) {
	            $row_color = "#9999ff";
            }
    ?>
    <tr bgcolor="<?php echo $row_color;?>">
        <?php echo $this->Form->hidden("KintaiDetail.{$data['date']}.id", array("value"=>$data['id']))?>
        <?php echo $this->Form->hidden("KintaiDetail.{$data['date']}.kintai_id", array("value"=>$id))?>
        <?php echo $this->Form->hidden("KintaiDetail.{$data['date']}.year", array("value"=>$year))?>
        <?php echo $this->Form->hidden("KintaiDetail.{$data['date']}.month", array("value"=>$month))?>
        <?php echo $this->Form->hidden("KintaiDetail.{$data['date']}.enable", array("value"=>1))?>
        <?php echo $this->Form->hidden("KintaiDetail.{$data['date']}.date", array("value"=>$data['date']))?>
        <?php echo $this->Form->hidden("KintaiDetail.{$data['date']}.day", array("value"=>$data['day']))?>
        <?php echo $this->Form->hidden("KintaiDetail.{$data['date']}.holiday", array("value"=>$data['holiday']))?>
        <?php echo $this->Form->hidden("KintaiDetail.{$data['date']}.sum_error", array("value"=>"0"))?>
        <?php echo $this->Form->hidden("KintaiDetail.{$data['date']}.app_error", array("value"=>"0"))?>
        <td><font color="<?php echo $day_color;?>"><?php echo h($data['date'])?></font></td>
        <td><font color="<?php echo $day_color;?>"><?php echo h($data['day'])?></font></td>
        <td><?php
            /* 開始時間 : [セレクトボックス] */
            if ($data['holiday']) {
                echo $this->Form->select("KintaiDetail.{$data['date']}.start_time", $s_times_holiday,
                    array("empty" => false, "class"=>"form-control input-sm", "style"=>SIZE_SB_TIME, $disabled, "value"=>$data['start_time'],
                        "onchange"=>sprintf("calcWorkTimeDate(%d);", $data['date'])));
            }
            else {
                echo $this->Form->select("KintaiDetail.{$data['date']}.start_time", $s_times,
                    array("empty" => false, "class"=>"form-control input-sm", "style"=>SIZE_SB_TIME, $disabled, "value"=>$data['start_time'],
                        "onchange"=>sprintf("calcWorkTimeDate(%d);", $data['date'])));
            }
            ?>
        </td>
        <td><?php
            /* 終了時間 : [セレクトボックス] */
            echo $this->Form->select("KintaiDetail.{$data['date']}.end_time", $e_times,
                array("empty" => false, "class"=>"form-control input-sm", "style"=>SIZE_SB_TIME, $disabled, "value"=>$data['end_time'],
                    "onchange"=>sprintf("calcWorkTimeDate(%d);", $data['date'])))
            ?>
        </td>
        <td><?php
            /* 実働時間 : [テキストボックス] */
            if ($data['work_time'] != "" && $data['work_time'] != 0) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.work_time",
                array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly", $disabled,
                    "value"=>sprintf("%.01f", $data['work_time'])));
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.work_time",
                array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly", $disabled,
                    "value"=>$data['work_time']));
            }
            $total_work_time += $data['work_time'];
            ?>
        </td>
        <td><?php
            /* 深夜時間 : [テキストボックス] */
            if ($data['late_time'] != "" && $data['late_time'] != 0) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.late_time",
                array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly", $disabled,
                    "value"=>sprintf("%.01f", $data['late_time'])));
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.late_time",
                array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly", $disabled));
            }
            $total_late_time += $data['late_time'];
            ?>
        </td>

        <td><?php
            /* 適用 : [セレクトボックス] */
            echo $this->Form->select("KintaiDetail.{$data['date']}.application", Configure::read("holiday_type"),
                array("class"=>"form-control input-sm", "style"=>SIZE_SB_APPL, "value"=>$data['application'], $disabled,
                    "onchange"=>sprintf("onchangeApplication(%d);", $data['date'])))
            ?>
        </td>

        <?php for ($i = 1; $i <= 6; $i++): ?>
            <td><?php
                /* プロジェクト内訳 : [テキストボックス] */
                if ($data[sprintf("project%d_time", $i)] != null) {
                    echo $this->Form->text("KintaiDetail.{$data['date']}.project.{$i}._time",
                        array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, $disabled,
                            "value"=>sprintf("%.01f", $data[sprintf("project%d_time", $i)]),
                            "onBlur"=>sprintf("onblurProjectTime(%d, \"Project%d\");", $data['date'], $i)));
                    $total_pro_time[sprintf("%d", $i)] += $data[sprintf("project%d_time", $i)];
                }
                else {
                    echo $this->Form->text("KintaiDetail.{$data['date']}.project.{$i}._time",
                        array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, $disabled,
                            "value"=>$data[sprintf("project%d_time", $i)],
                            "onBlur"=>sprintf("onblurProjectTime(%d, \"Project%d\");", $data['date'], $i)));
                }
            ?></td>
        <?php endfor;?>

        <td><?php
            if ($data['project_sum'] != null) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.project_sum",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly", $disabled,
                        "value"=>sprintf("%.01f", $data['project_sum']), "id"=>sprintf("KintaiDetail%dProjectSumTime", $data['date'])));
                $total_pro_sum += $data['project_sum'];
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.project_sum",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly", $disabled,
                        "value"=>$data['project_sum'], "id"=>sprintf("KintaiDetail%dProjectSumTime", $data['date'])));
            }
        ?></td>
        <td><?php
            if ($data['group_time'] != null) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.group_time",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, $disabled,
                    "value"=>sprintf("%.01f", $data['group_time']),
                    "onBlur"=>sprintf("onblurProjectTime(%d, \"Group\");", $data['date'])));
                $total_group_time += $data['group_time'];
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.group_time",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, $disabled,
                    "value"=>$data['group_time'],
                    "onBlur"=>sprintf("onblurProjectTime(%d, \"Group\");", $data['date'])));
            }
        ?></td>
        <td><?php
            if ($data['training_time'] != null) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.training_time",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, $disabled,
                    "value"=>sprintf("%.01f", $data['training_time']),
                    "onBlur"=>sprintf("onblurProjectTime(%d, \"Training\");", $data['date'])));
                $total_training_time += $data['training_time'];
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.training_time",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, $disabled,
                    "value"=>$data['training_time'],
                    "onBlur"=>sprintf("onblurProjectTime(%d, \"Training\");", $data['date'])));
            }
        ?></td>
        <td><?php
            if ($data['other_time'] != null) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.other_time",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, $disabled,
                    "value"=>sprintf("%.01f", $data['other_time']),
                    "onBlur"=>sprintf("onblurProjectTime(%d, \"Other\");", $data['date'])));
                $total_other_time += $data['other_time'];
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.other_time",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, $disabled,
                    "value"=>$data['other_time'],
                    "onBlur"=>sprintf("onblurProjectTime(%d, \"Other\");", $data['date'])));
            }
        ?></td>
        <td><?php
            if ($data['other_sum'] != null) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.other_sum",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly", $disabled,
                         "value"=>sprintf("%0.1f", $data['other_sum']), "id"=>sprintf("KintaiDetail%dOtherSumTime", $data['date'])));
                $total_other_sum += $data['other_sum'];
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.other_sum",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly", $disabled,
                         "value"=>$data['other_sum'], "id"=>sprintf("KintaiDetail%dOtherSumTime", $data['date'])));
            }
        ?></td>
        <td><?php
            if ($data['total_sum'] != null) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.total_sum",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly", $disabled,
                        "value"=>sprintf("%0.1f", $data['total_sum']), "id"=>sprintf("KintaiDetail%dTotalSumTime", $data['date'])));
                $total_total_sum += $data['total_sum'];
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.total_sum",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly", $disabled,
                        "value"=>$data['total_sum'], "id"=>sprintf("KintaiDetail%dTotalSumTime", $data['date'])));
            }
        ?></td>
        <td class="center">
        	<?php echo $this->Form->checkbox("KintaiDetail.{$data['date']}.remote_work",
				array("checked"=>($data['remote_work'] == "1") ? true : false, $disabled)); ?>
		</td>
        <td>
            <?php echo $this->Form->text("KintaiDetail.{$data['date']}.remarks",
                array("class"=>"form-control input-sm", "style"=>SIZE_LL, $disabled,
                "value"=>$data['remarks']))?>
        </td>
    </tr>
    <?php endforeach; ?>

    <th colspan="4" class="center">合計</th>
    <td><?php
        echo $this->Form->text("TotalWorkTime",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly", $disabled,
                "value"=>sprintf("%.01f", $total_work_time), "id"=>"KintaiTotalWorkTime"));
    ?></td>
    <td><?php
        echo $this->Form->text("TotalLateTime",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly", $disabled,
                "value"=>sprintf("%.01f", $total_late_time), "id"=>"KintaiTotalLateTime"));
    ?></td>
    <td></td>
    <?php for ($i = 1; $i <= 6; $i++): ?>
        <td><?php
            echo $this->Form->text("TotalPro.{$i}.Time",
                array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly", $disabled,
                    "value"=>sprintf("%.01f", $total_pro_time[sprintf("%d", $i)]), "id"=>sprintf("KintaiTotalProject%dTime", $i)));
        ?></td>
    <?php endfor;?>
    <td><?php
        echo $this->Form->text("TotalProSum",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly", $disabled,
                "value"=>sprintf("%.01f", $total_pro_sum), "id"=>"KintaiTotalProjectSumTime"));
    ?></td>
    <td><?php
        echo $this->Form->text("TotalGroupTime",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly", $disabled,
                "value"=>sprintf("%.01f", $total_group_time), "id"=>"KintaiTotalGroupTime"));
    ?></td>
    <td><?php
        echo $this->Form->text("TotalTrainingTime",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly", $disabled,
                "value"=>sprintf("%.01f", $total_training_time), "id"=>"KintaiTotalTrainingTime"));
    ?></td>
    <td><?php
        echo $this->Form->text("TotalOtherTime",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly", $disabled,
                "value"=>sprintf("%.01f", $total_other_time), "id"=>"KintaiTotalOtherTime"));
    ?></td>
    <td><?php
        echo $this->Form->text("TotalOtherSum",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly", $disabled,
                "value"=>sprintf("%.01f", $total_other_sum), "id"=>"KintaiTotalOtherSumTime"));
    ?></td>
    <td><?php
        echo $this->Form->text("TotalTotalSum",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "readonly"=>"readonly", $disabled,
                "value"=>sprintf("%.01f", $total_total_sum), "id"=>"KintaiTotalTotalSumTime"));
    ?></td>
	<td colspan="2">
		&nbsp;&nbsp;
		<label>
            <?php if ($disabled == ''):?>
                <input type="radio" name="data[Kintai][transport]" id="KintaiTransport1" value="1"
				    <?php if ($datas['Kintai']['transport'] == "1") echo 'checked'; ?>
                >
            <?php endif?>
            <?php if ($disabled == 'disabled'):?>
                <?php
                    if ($datas['Kintai']['transport'] == "1") echo $this->Html->image("check_on.png");
                    else echo $this->Html->image("check_off.png");
                ?>
            <?php endif?>
			定期
		</label>
		&nbsp;&nbsp;&nbsp;
		<label>
            <?php if ($disabled == ''):?>
                <input type="radio" name="data[Kintai][transport]" id="KintaiTransport2" value="2"
				    <?php if ($datas['Kintai']['transport'] == "2") echo 'checked'; ?>
                >
            <?php endif?>
            <?php if ($disabled == 'disabled'):?>
                <?php
                    if ($datas['Kintai']['transport'] == "2") echo $this->Html->image("check_on.png");
                    else echo $this->Html->image("check_off.png");
                ?>
            <?php endif?>
			小口精算
		</label>
    </td>
</table>
<?php echo $this->Form->end();?>

<script>
    $(document).ready(function() {
        window.onload　= PressedEnter;//エンターキーで更新しないようにする
    });
</script>
