<script>
    $(function(){
        $('#KintaiProjectName1').autocomplete({
            source: '/a/kintais/auto_item/' + <?php echo CONFIRM_TYPE_0;?> + "/" + <?php echo KINTAI_SUB_0;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#KintaiProjectName1').html(ui.item.details);
            }}
        });
        $('#KintaiProjectName2').autocomplete({
            source: '/a/kintais/auto_item/' + <?php echo CONFIRM_TYPE_0;?> + "/" + <?php echo KINTAI_SUB_0;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#KintaiProjectName2').html(ui.item.details);
            }}
        });
        $('#KintaiProjectName3').autocomplete({
            source: '/a/kintais/auto_item/' + <?php echo CONFIRM_TYPE_0;?> + "/" + <?php echo KINTAI_SUB_0;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#KintaiProjectName3').html(ui.item.details);
            }}
        });
        $('#KintaiProjectName4').autocomplete({
            source: '/a/kintais/auto_item/' + <?php echo CONFIRM_TYPE_0;?> + "/" + <?php echo KINTAI_SUB_0;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#KintaiProjectName4').html(ui.item.details);
            }}
        });
        $('#KintaiProjectName5').autocomplete({
            source: '/a/kintais/auto_item/' + <?php echo CONFIRM_TYPE_0;?> + "/" + <?php echo KINTAI_SUB_0;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#KintaiProjectName5').html(ui.item.details);
            }}
        });
        $('#KintaiProjectName6').autocomplete({
            source: '/a/kintais/auto_item/' + <?php echo CONFIRM_TYPE_0;?> + "/" + <?php echo KINTAI_SUB_0;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#KintaiProjectName6').html(ui.item.details);
            }}
        });
        $('#KintaiProjectNo1').autocomplete({
            source: '/a/kintais/auto_item/' + <?php echo CONFIRM_TYPE_0;?> + "/" + <?php echo KINTAI_SUB_0;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#KintaiProjectNo1').html(ui.item.details);
            }}
        });
        $('#KintaiProjectNo2').autocomplete({
            source: '/a/kintais/auto_item/' + <?php echo CONFIRM_TYPE_0;?> + "/" + <?php echo KINTAI_SUB_0;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#KintaiProjectNo2').html(ui.item.details);
            }}
        });
        $('#KintaiProjectNo3').autocomplete({
            source: '/a/kintais/auto_item/' + <?php echo CONFIRM_TYPE_0;?> + "/" + <?php echo KINTAI_SUB_0;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#KintaiProjectNo3').html(ui.item.details);
            }}
        });
        $('#KintaiProjectNo4').autocomplete({
            source: '/a/kintais/auto_item/' + <?php echo CONFIRM_TYPE_0;?> + "/" + <?php echo KINTAI_SUB_0;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#KintaiProjectNo4').html(ui.item.details);
            }}
        });
        $('#KintaiProjectNo5').autocomplete({
            source: '/a/kintais/auto_item/' + <?php echo CONFIRM_TYPE_0;?> + "/" + <?php echo KINTAI_SUB_0;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#KintaiProjectNo5').html(ui.item.details);
            }}
        });
        $('#KintaiProjectNo6').autocomplete({
            source: '/a/kintais/auto_item/' + <?php echo CONFIRM_TYPE_0;?> + "/" + <?php echo KINTAI_SUB_0;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#KintaiProjectNo6').html(ui.item.details);
            }}
        });
    });

    /**
     * 更新ボタンClick処理
     */
    function updateProject () {
        for ($i = 1; $i <= 6; $i++) {
            var no = document.getElementById("KintaiProject" + $i + "No");
            var kind = document.getElementById("KintaiProject" + $i + "Kind");

            // 実績or請負選択時、見積番号の未登録をチェック
            if ((kind.value == <?php echo WORK_KD_PERFORMANCE;?> ||
                 kind.value == <?php echo WORK_KD_CONTRACTORS;?>) &&
                 no.value == "") {
                alert("プロジェクト" + $i + "の見積番号を登録して下さい。");
                return false;
            }
        }
    }      

</script>

<blockquote><p>プロジェクト名登録</p></blockquote>
<p class="text-info">プロジェクト名を入力してください。</p>

<?php echo $this->Form->create('Kintai', array("name"=>"Kintai", "action" => "update_project", "onsubmit" => "return updateProject();"))?>
<?php echo $this->Form->hidden("Kintai.id", array("value"=>$id))?>
<?php echo $this->Form->hidden("Kintai.year", array("value"=>$year))?>
<?php echo $this->Form->hidden("Kintai.month", array("value"=>$month))?>

<table class="form">
<tr>
    <th>プロジェクト１</th>
    <td><?php echo $this->Form->text("Kintai.project1_name", array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
    <th>見積番号</th>
    <td><?php echo $this->Form->text("Kintai.project1_no", array("class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
    <th>作業形態</th>
    <td><?php echo $this->Form->select("Kintai.project1_kind", $kinds, array('class' => 'form-control input-sm', 'style' => SIZE_S, 'empty' => false, 'value' => $datas['Kintai']['project1_kind']));?></td>
</tr>
<tr>
    <th>プロジェクト２</th>
    <td><?php echo $this->Form->text("Kintai.project2_name", array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
    <th>見積番号</th>
    <td><?php echo $this->Form->text("Kintai.project2_no", array("class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
    <th>作業形態</th>
    <td><?php echo $this->Form->select("Kintai.project2_kind", $kinds, array('class' => 'form-control input-sm', 'style' => SIZE_S, 'empty' => false, 'value' => $datas['Kintai']['project2_kind']));?></td>
</tr>
<tr>
    <th>プロジェクト３</th>
    <td><?php echo $this->Form->text("Kintai.project3_name", array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
    <th>見積番号</th>
    <td><?php echo $this->Form->text("Kintai.project3_no", array("class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
    <th>作業形態</th>
    <td><?php echo $this->Form->select("Kintai.project3_kind", $kinds, array('class' => 'form-control input-sm', 'style' => SIZE_S, 'empty' => false, 'value' => $datas['Kintai']['project3_kind']));?></td>
</tr>
<tr>
    <th>プロジェクト４</th>
    <td><?php echo $this->Form->text("Kintai.project4_name", array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
    <th>見積番号</th>
    <td><?php echo $this->Form->text("Kintai.project4_no", array("class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
    <th>作業形態</th>
    <td><?php echo $this->Form->select("Kintai.project4_kind", $kinds, array('class' => 'form-control input-sm', 'style' => SIZE_S, 'empty' => false, 'value' => $datas['Kintai']['project4_kind']));?></td>
</tr>
<tr>
    <th>プロジェクト５</th>
    <td><?php echo $this->Form->text("Kintai.project5_name", array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
    <th>見積番号</th>
    <td><?php echo $this->Form->text("Kintai.project5_no", array("class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
    <th>作業形態</th>
    <td><?php echo $this->Form->select("Kintai.project5_kind", $kinds, array('class' => 'form-control input-sm', 'style' => SIZE_S, 'empty' => false, 'value' => $datas['Kintai']['project5_kind']));?></td>
</tr>
<tr>
    <th>プロジェクト６</th>
    <td><?php echo $this->Form->text("Kintai.project6_name", array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
    <th>見積番号</th>
    <td><?php echo $this->Form->text("Kintai.project6_no", array("class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
    <th>作業形態</th>
    <td><?php echo $this->Form->select("Kintai.project6_kind", $kinds, array('class' => 'form-control input-sm', 'style' => SIZE_S, 'empty' => false, 'value' => $datas['Kintai']['project6_kind']));?></td>
</tr>
<tr>
    <td colspan="6" class="center">
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>
</table>

<?php echo $this->Form->end();?>