<script>
    $(document).ready(function() {
        loadConfirmList();
    });

    function loadConfirmList() {
        var url = "/a/confirms/list/" + <?php echo CONFIRM_TYPE_0?> + "/" + <?php echo $datas['Kintai']['id'];?>;
        $('#ConfirmList').load(url);
    }

    /**
     * [printKintai description]
     * @param  {[type]} id 勤怠ID
     * @return {[type]}    [description]
     */
    function printKintai(id){
        var url = "/a/kintais/print/" + id;
        location.replace(url);
    }
</script>


<blockquote><p>勤怠表</p></blockquote>

<div id="ConfirmList"><!-- 申請リスト --></div>

<div class="center">
<h5>
<b><?php echo sprintf("%s年 %s月", $year, $month);?></b>
</h5>
</div>

<div class="pull-right well form-inline">
	<p class="guide"><i class="glyphicon glyphicon-play"></i> 上長確認</p>
	<div class="form-group">
		<?php
			echo $this->Form->text("Kintai.check_date", array("class"=>"form-control input-sm", "style"=>SIZE_M, "readonly"=>"readonly", 'value'=>$datas['Kintai']['check_date']));
		?>
	</div>
	<div class="form-group">
		<?php echo $this->Form->text("CheckStaff.name", array("class"=>"form-control input-sm", "style"=>SIZE_S, "readonly"=>"readonly", 'value'=>$datas['CheckStaff']['name']))?>
	</div>
</div>

<!-- 合計情報 -->
<?php echo $this->element('kintai_total',
    ['business_day'=>$datas['Kintai']['business_day'],
     'work_time'=>$datas['Kintai']['work_time'],
     'late_time'=>$datas['Kintai']['late_time'],
     'over_time'=>$datas['Kintai']['over_time'],
     'holiday'=>$datas['Kintai']['holiday'],
     'extra_time'=>$datas['Kintai']['extra_time']]); ?>

<a href="javascript:void(0);" onclick="printKintai(<?php echo $id;?>);"><i class="glyphicon glyphicon-print"></i> 出力</a>

<table class="table-bordered table-condensed table-striped table-hover">
    <?php
        $total_work_time = 0.0;
        $total_late_time = 0.0;
        $total_pro_time = array("1"=>"0.0", "2"=>"0.0", "3"=>"0.0", "4"=>"0.0", "5"=>"0.0", "6"=>"0.0");
        $total_pro_sum = 0.0;
        $total_group_time = 0.0;
        $total_training_time = 0.0;
        $total_other_time = 0.0;
        $total_other_sum = 0.0;
        $total_total_sum = 0.0;
    ?>
    <tr>
        <th rowspan="2" style="min-width:30px;">日</th>
        <th rowspan="2" style="min-width:30px;">曜日</th>
        <th rowspan="2" style="min-width:50px;">始業時間</th>
        <th rowspan="2" style="min-width:50px;">終業時間</th>
        <th rowspan="2" style="min-width:30px;">実働時間</th>
        <th rowspan="2" style="min-width:40px;">深夜時間</th>
        <th rowspan="2" style="min-width:50px;">適用</th>
        <th colspan="12" style="min-width:500px;">プロジェクト内訳</th>
        <th rowspan="2" style="min-width:30px;">在宅勤務</th>
        <th rowspan="2" style="min-width:100px;">備考</th>
    </tr>
    <tr>
        <?php for ($i = 1; $i <= 6; $i++): ?>
            <th style="min-width:50px;">
                <?php echo h(sprintf("%s", $datas['Kintai'][sprintf("project%d_name", $i)])); ?>
                <?php if(!empty($datas['Kintai'][sprintf("project%d_no", $i)])): ?>
                    </br>
                    <?php echo h(sprintf("【%s】", $datas['Kintai'][sprintf("project%d_no", $i)])); ?>
                <?php endif;?>
                <?php if(!empty($datas['Kintai'][sprintf("project%d_kind", $i)])): ?>
                    </br>
                    <?php echo h(sprintf("【%s】", $work_kinds[$datas['Kintai'][sprintf("project%d_kind", $i)]])); ?>
                <?php endif;?>
            </th>
        <?php endfor;?>
        <th style="min-width:40px;">生産活動計</th>
        <th style="min-width:40px;"><?php echo h($project_label); ?></th>
        <th style="min-width:40px;">研修</th>
        <th style="min-width:40px;">その他</th>
        <th style="min-width:40px;">その他計</th>
        <th style="min-width:40px;">合計</th>
    </tr>
    <?php
        foreach ($datas['KintaiDetail'] as $data):
            $day_color = "#000000";
            if ($data['holiday']) {
                $day_color = "#ff0000";
            };
    ?>
    <tr>
        <?php echo $this->Form->hidden("KintaiDetail.{$data['date']}.id", array("value"=>$data['id']))?>
        <?php echo $this->Form->hidden("KintaiDetail.{$data['date']}.kintai_id", array("value"=>$id))?>
        <?php echo $this->Form->hidden("KintaiDetail.{$data['date']}.year", array("value"=>$year))?>
        <?php echo $this->Form->hidden("KintaiDetail.{$data['date']}.month", array("value"=>$month))?>
        <?php echo $this->Form->hidden("KintaiDetail.{$data['date']}.enable", array("value"=>1))?>
        <?php echo $this->Form->hidden("KintaiDetail.{$data['date']}.date", array("value"=>$data['date']))?>
        <?php echo $this->Form->hidden("KintaiDetail.{$data['date']}.day", array("value"=>$data['day']))?>
        <?php echo $this->Form->hidden("KintaiDetail.{$data['date']}.holiday", array("value"=>$data['holiday']))?>
        <td><font color="<?php echo $day_color;?>"><?php echo h($data['date'])?></font></td>
        <td><font color="<?php echo $day_color;?>"><?php echo h($data['day'])?></font></td>
        <td><?php
            /* 開始時間 : [テキストボックス] 未入力の日については空欄 */
            if ($data['start_time'] != NULL) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.start_time",
                    array("class"=>"form-control input-sm", "style"=>SIZE_SB_TIME, "disabled"=>"disabled", "value"=>$s_times[$data['start_time']]));
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.start_time",
                    array("class"=>"form-control input-sm", "style"=>SIZE_SB_TIME, "disabled"=>"disabled", "value"=>$data['start_time']));
            }
            ?>
        </td>
        <td><?php
            /* 終了時間 : [テキストボックス] 未入力の日については空欄 */
            if ($data['end_time'] != NULL) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.end_time",
                    array("class"=>"form-control input-sm", "style"=>SIZE_SB_TIME, "disabled"=>"disabled", "value"=>$e_times[$data['end_time']]));
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.end_time",
                    array("class"=>"form-control input-sm", "style"=>SIZE_SB_TIME, "disabled"=>"disabled", "value"=>$data['end_time']));
            }
            ?>
        </td>
        <td><?php
            /* 実働時間 : [テキストボックス] */
            if ($data['work_time'] != "" && $data['work_time'] != 0) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.work_time",
                array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                    "value"=>sprintf("%.01f", $data['work_time'])));
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.work_time",
                array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                    "value"=>$data['work_time']));
            }
            $total_work_time += $data['work_time'];
            ?>
        </td>
        <td><?php
            /* 深夜時間 : [テキストボックス] */
            if ($data['late_time'] != "" && $data['late_time'] != 0) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.late_time",
                array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                    "value"=>sprintf("%.01f", $data['late_time'])));
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.late_time",
                array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled"));
            }
            $total_late_time += $data['late_time'];
            ?>
        </td>

        <td><?php
            /* 適用 : [テキストボックス] 未入力の日については空欄 */
            if ($data['application'] != NULL) {
                $items = Configure::read("holiday_type");
                echo $this->Form->text("KintaiDetail.{$data['date']}.application",
                    array("class"=>"form-control input-sm", "style"=>SIZE_SB_APPL, "disabled"=>"disabled", "value"=>$items[$data['application']]));
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.application",
                    array("class"=>"form-control input-sm", "style"=>SIZE_SB_APPL, "disabled"=>"disabled", "value"=>$data['application']));
            }
            ?>
        </td>

        <?php for ($i = 1; $i <= 6; $i++): ?>
            <td><?php
                /* プロジェクト内訳 : [テキストボックス] */
                if ($data[sprintf("project%d_time", $i)] != null) {
                    echo $this->Form->text("KintaiDetail.{$data['date']}.project.{$i}._time",
                        array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                            "value"=>sprintf("%.01f", $data[sprintf("project%d_time", $i)])));
                    $total_pro_time[sprintf("%d", $i)] += $data[sprintf("project%d_time", $i)];
                }
                else {
                    echo $this->Form->text("KintaiDetail.{$data['date']}.project.{$i}._time",
                        array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                            "value"=>$data[sprintf("project%d_time", $i)]));
                }
            ?></td>
            <?php echo $this->Form->hidden("Hidden.{$data['date']}.project.{$i}._time", array("value"=>$data[sprintf("project%d_time", $i)]))?>
        <?php endfor;?>

        <td><?php
            /* 生産活動計 : [テキストボックス] */
            if ($data['project_sum'] != null) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.project_sum",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                        "value"=>sprintf("%.01f", $data['project_sum'])));
                $total_pro_sum += $data['project_sum'];
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.project_sum",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                        "value"=>$data['project_sum']));
            }
        ?></td>
        <td><?php
            /* グループ活動 : [テキストボックス] */
            if ($data['group_time'] != null) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.group_time",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                    "value"=>sprintf("%.01f", $data['group_time'])));
                $total_group_time += $data['group_time'];
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.group_time",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                    "value"=>$data['group_time']));
            }
        ?></td>
        <td><?php
            /* 研修 : [テキストボックス] */
            if ($data['training_time'] != null) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.training_time",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                    "value"=>sprintf("%.01f", $data['training_time'])));
                $total_training_time += $data['training_time'];
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.training_time",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                    "value"=>$data['training_time']));
            }
        ?></td>
        <td><?php
            /* その他 : [テキストボックス] */
            if ($data['other_time'] != null) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.other_time",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                    "value"=>sprintf("%.01f", $data['other_time'])));
                $total_other_time += $data['other_time'];
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.other_time",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                    "value"=>$data['other_time']));
            }
        ?></td>
        <td><?php
            /* その他計 : [テキストボックス] */
            if ($data['other_sum'] != null) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.other_sum",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                         "value"=>sprintf("%0.1f", $data['other_sum'])));
                $total_other_sum += $data['other_sum'];
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.other_sum",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                         "value"=>$data['other_sum']));
            }
        ?></td>
        <td><?php
            /* 合計 : [テキストボックス] */
            if ($data['total_sum'] != null) {
                echo $this->Form->text("KintaiDetail.{$data['date']}.total_sum",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                        "value"=>sprintf("%0.1f", $data['total_sum'])));
                $total_total_sum += $data['total_sum'];
            }
            else {
                echo $this->Form->text("KintaiDetail.{$data['date']}.total_sum",
                    array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                        "value"=>$data['total_sum']));
            }
        ?></td>
        <td class="center">
            <?php echo $this->Form->checkbox("KintaiDetail.{$data['date']}.remote_work",
	    		array("checked"=>($data['remote_work'] == "1") ? true : false, "disabled"=>"disabled")); ?>
		</td>
        <td><?php
            /* 備考 : [テキストボックス] */
            echo $this->Form->text("KintaiDetail.{$data['date']}.remarks",
                array("class"=>"form-control input-sm", "style"=>SIZE_LL, "disabled"=>"disabled",
                    "value"=>$data['remarks']))
        ?></td>
    </tr>
    <?php endforeach; ?>

    <th colspan="4" class="center">合計</th>
    <td><?php
        echo $this->Form->text("TotalWorkTime",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                "value"=>sprintf("%.01f", $total_work_time)));
    ?></td>
    <td><?php
        echo $this->Form->text("TotalLateTime",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                "value"=>sprintf("%.01f", $total_late_time)));
    ?></td>
    <td></td>
    <?php for ($i = 1; $i <= 6; $i++): ?>
        <td><?php
            echo $this->Form->text("TotalPro.{$i}.Time",
                array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                    "value"=>sprintf("%.01f", $total_pro_time[sprintf("%d", $i)])));
        ?></td>
    <?php endfor;?>
    <td><?php
        echo $this->Form->text("TotalProSum",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                "value"=>sprintf("%.01f", $total_pro_sum)));
    ?></td>
    <td><?php
        echo $this->Form->text("TotalGroupTime",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                "value"=>sprintf("%.01f", $total_group_time)));
    ?></td>
    <td><?php
        echo $this->Form->text("TotalTrainingTime",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                "value"=>sprintf("%.01f", $total_training_time)));
    ?></td>
    <td><?php
        echo $this->Form->text("TotalOtherTime",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                "value"=>sprintf("%.01f", $total_other_time)));
    ?></td>
    <td><?php
        echo $this->Form->text("TotalOtherSum",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                "value"=>sprintf("%.01f", $total_other_sum)));
    ?></td>
    <td><?php
        echo $this->Form->text("TotalTotalSum",
            array("class"=>"form-control input-sm number", "style"=>SIZE_TX_TIME, "disabled"=>"disabled",
                "value"=>sprintf("%.01f", $total_total_sum)));
    ?></td>
	<td colspan="2">
		&nbsp;&nbsp;
		<label>
			<?php
                if ($datas['Kintai']['transport'] == "1") echo $this->Html->image("check_on.png");
                else echo $this->Html->image("check_off.png");
            ?>
			定期
		</label>
		&nbsp;&nbsp;&nbsp;
		<label>
			<?php
                if ($datas['Kintai']['transport'] == "2") echo $this->Html->image("check_on.png");
                else echo $this->Html->image("check_off.png");
            ?>
			小口精算
		</label>
    </td>
</table>
