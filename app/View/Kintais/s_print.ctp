<?php
	require_once('tcpdf/config/lang/eng.php');
	require_once('tcpdf/tcpdf.php');

	$transport_customer = Configure::read("transport_customer");

	$pdf = new TCPDF('T', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf->AddFont('msmincho');
	$pdf->setFontSubsetting(true);
	
	// ヘッダー、フッターなし
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);

	// 塗りつぶし色指定
	$pdf->SetFillColor(220);

	$border_n = 0;
	$border_y = 1;
	$border_u = 'B';

	$fill_n = 0;
	$fill_y = 1;

	$ln_n = 0;
	$ln_y = 1;

	$reseth_n = false;
	$reseth_y = true;

	$pdf->AddPage();

	$pdf->SetFont('msmincho', '', 12);
	$tmp1 = sprintf("%04d年 %2d月度", $datas['Kintai']['year'], $datas['Kintai']['month']);
	$pdf->MultiCell(50, 0, $tmp1, $border_n, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetFont('msmincho', '', 16);
	$pdf->SetX(80);
	$pdf->MultiCell(50, 0, "勤務状況報告書", $border_n, 'C', $fill_n, $ln_y, "", '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->Ln();

	$pdf->SetFont('msmincho', '', 10);

	$depprtment = is_null($staff['Department']['name']) ? "" : $staff['Department']['name'] . "  ";
	$tmp1 = sprintf("所属・氏名  %s%s", $depprtment, $datas['Staff']['name']);
	$pdf->MultiCell(75, 0, $tmp1, $border_u, 'L', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetFont('msmincho', '', 6);

	$create = "";
	if ($confirm['Confirm']['request_date']) {
		$date = explode(' ', $confirm['Confirm']['request_date']);
		$create = $date[0]."\n".$staffs[$confirm['Confirm']['request_staff_id']];
	}

	$check = "";
	if ($confirm['Confirm']['check_staff_id']) {
		$date = explode(' ', $confirm['Confirm']['check_date']);
		$check = $date[0]."\n".$staffs[$confirm['Confirm']['check_staff_id']];
	}

	$pdf->SetX(154);
	$pdf->MultiCell(15, 3.5, '作成', $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->MultiCell(15, 3.5, '承認', $border_y, 'C', $fill_y, $ln_y, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->SetX(154);
	$pdf->MultiCell(15, 10.0, $create, $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, 0, 'M');
	$pdf->MultiCell(15, 10.0, $check, $border_y, 'C', $fill_n, $ln_y, '', '', $reseth_y, 0, false, true, 0, 'M');

	//$pdf->Ln();

	// 幅設定
	$height = 5.5;
	$height2 = 30.0;
	$w_time = 8;
	$w_hour = 8;
	$s1 = 5;
	$s2 = 12;
	$m1 = 20;
	$m2 = 25;
	$l1 = 57;

	$group_xpos = ($s1 * 2) + ($s2 * 1) + ($w_time * 2) + ($w_hour * 5) + 10;
	$pdf->SetX($group_xpos);
	$group_width = $w_hour * 12;

	$pdf->MultiCell($group_width, $height, "プロジェクト内訳", $border_y, 'C', $fill_y, $ln_y, "", '', $reseth_y, 1, false, true, $height, 'M', true);

	$pdf->MultiCell($s1, $height2, "日", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height2, 'M', true);
	$pdf->MultiCell($s1, $height2, "曜日", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height2, 'M', true);
	$pdf->MultiCell($w_time, $height2, "始業時間", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height2, 'M', true);
	$pdf->MultiCell($w_time, $height2, "終業時間", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height2, 'M', true);
	$pdf->MultiCell($w_hour, $height2, "実働時間", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height2, 'M', true);
	$pdf->MultiCell($w_hour, $height2, "深夜時間", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height2, 'M', true);
	$pdf->MultiCell($w_hour, $height2, "休日出勤", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height2, 'M', true);
	$pdf->MultiCell($s2, $height2, "適用", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height2, 'M', true);
	$pdf->MultiCell($w_hour, $height2, "基礎時間累計", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height2, 'M', true);
	$pdf->MultiCell($w_hour, $height2, "実働時間累計", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height2, 'M', true);
	for ($i = 1; $i <= 6; $i++) {
		if (!empty($datas['Kintai'][sprintf("project%d_no", $i)])) {
			$prj_name = sprintf("%s【%s】", $datas['Kintai'][sprintf("project%d_name", $i)], $datas['Kintai'][sprintf("project%d_no", $i)]);
			$pdf->MultiCell($w_hour, $height2, $prj_name, $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height2, 'M', true);
		}
		else {
			$prj_name = $datas['Kintai'][sprintf("project%d_name", $i)];
			$pdf->MultiCell($w_hour, $height2, $prj_name, $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height2, 'M', true);
		}
	}
	$pdf->MultiCell($w_hour, $height2, "生産活動計", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height2, 'M', true);
	$pdf->MultiCell($w_hour, $height2, $project_label, $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height2, 'M', true);
	$pdf->MultiCell($w_hour, $height2, "研修", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height2, 'M', true);
	$pdf->MultiCell($w_hour, $height2, "その他", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height2, 'M', true);
	$pdf->MultiCell($w_hour, $height2, "その他計", $border_y, 'C', $fill_y, $ln_n, '', '', $reseth_y, 0, false, true, $height2, 'M', true);
	$pdf->MultiCell($w_hour, $height2, "合計", $border_y, 'C', $fill_y, $ln_y, '', '', $reseth_y, 0, false, true, $height2, 'M', true);

	$height = 5.5;

	$num = 0;
	$unit = 0;
	$no = 1;
	$day_cnt = 0;
	$std_time_sum = 0.0;
	$work_time_sum = 0.0;
	$total_sum[0] = 0;
	$total_sum[1] = 0;
	$total_sum[2] = 0;
	$total_sum[3] = 0;
	$total_sum[4] = 0;
	$total_sum[5] = 0;
	$total_sum[6] = 0;
	$total_sum[7] = 0;
	$total_sum[8] = 0;
	$total_sum[9] = 0;
	$total_sum[10] = 0;
	$total_sum[11] = 0;

	foreach ($datas['KintaiDetail'] as $data) {
		$day_cnt++;

		// 平日の場合、基礎時間/実働時間の両累計を更新
		if ($data['holiday'] == 0) {
			// 休暇未取得日 : 8.0 h を計上
			if ($data['application'] == null && $data['work_time'] != null) {
				$std_time_sum += 8.0;
				$work_time_sum += $data['work_time'];
			}
			// 有休・特休・振休取得時 : 基礎時間は計上しない
			else if ($data['application'] == HOLIDAY_PAID_ALL ||
					 $data['application'] == HOLIDAY_SPECIAL ||
					 $data['application'] == HOLIDAY_OBSERVED_ALL) {
				;
			}
			// 午前・午後半休取得時 : 基礎時間は 4.0h を計上
			else if ($data['application'] == HOLIDAY_MORNING_OFF ||
				     $data['application'] == HOLIDAY_AFTERNOON_OFF ||
				     $data['application'] == HOLIDAY_OBSERVED_HALF) {
				$std_time_sum += 4.0;
				$work_time_sum += $data['work_time'];
			}
		}
		else {
			// 休日時は文字色を変更
			$pdf->SetTextColor(255, 0, 0);
		}
		$pdf->MultiCell($s1, $height, $data['date'], $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 1, false, true, $height, 'M', true);
		$pdf->MultiCell($s1, $height, $data['day'], $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 1, false, true, $height, 'M', true);

		// 文字色を元に戻す
		$pdf->SetTextColor(0, 0, 0);

		if ($data['start_time'] != "") {
			$tmp1 = $s_times[$data['start_time']];
		}
		else {
			$tmp1 = "";
		}
		$pdf->MultiCell($w_time, $height, $tmp1, $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 1, false, true, $height, 'M', true);
		if ($data['end_time'] != "") {
			$tmp1 = $e_times[$data['end_time']];
		}
		else {
			$tmp1 = "";
		}
		$pdf->MultiCell($w_time, $height, $tmp1, $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 1, false, true, $height, 'M', true);

		// 実働時間 : 休日の場合は出力対象外
		if ($data['holiday'] == 0 && $data['work_time'] != 0.0) {
			$tmp1 = sprintf("%.01f", $data['work_time']);
		}
		else {
			$tmp1 = "";
		}
		$pdf->MultiCell($w_hour, $height, $tmp1, $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 1, false, true, $height, 'M', true);

		// 深夜時間 : 時間が'0'の場合は出力対象外
		if ($data['late_time'] != "") {
			$tmp1 = sprintf("%.01f", $data['late_time']);
		}
		else {
			$tmp1 = "";
		}
		$pdf->MultiCell($w_hour, $height, $tmp1, $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 1, false, true, $height, 'M', true);

		// 休日出勤 : 平日の場合は出力対象外
		if ($data['holiday'] == 1 && $data['work_time'] != "") {
			$tmp1 = sprintf("%.01f", $data['work_time']);
		}
		else {
			$tmp1 = "";
		}
		$pdf->MultiCell($w_hour, $height, $tmp1, $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 1, false, true, $height, 'M', true);

		// 適用
		if ($data['application'] != null) {
			$items = Configure::read("holiday_type");
			$tmp1 = $items[$data['application']];
		}
		else {
			$tmp1 = "";
		}
		$pdf->MultiCell($s2, $height, $tmp1, $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 1, false, true, $height, 'M', true);


		// 基礎時間累計 : 休日の場合は出力対象外
		if ($data['holiday'] == 0 && $data['work_time'] != null) {
			if ($data['application'] == null ||
				$data['application'] == HOLIDAY_MORNING_OFF ||
				$data['application'] == HOLIDAY_AFTERNOON_OFF ||
				$data['application'] == HOLIDAY_OBSERVED_HALF) {
				$tmp1 = sprintf("%.01f", $std_time_sum);
				$tmp2 = sprintf("%.01f", $work_time_sum);
			}
			else {
				$tmp1 = "";
				$tmp2 = "";
			}
		}
		else {
			$tmp1 = "";
			$tmp2 = "";
		}
		$pdf->MultiCell($w_hour, $height, $tmp1, $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 1, false, true, $height, 'M', true);
		$pdf->MultiCell($w_hour, $height, $tmp2, $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 1, false, true, $height, 'M', true);

		// プロジェクト内訳 : 時間設定箇所のみ出力対象
		for ($i = 1; $i <= 6; $i++) {
			if ($data[sprintf("project%d_time", $i)] != "") {
				$tmp1 = sprintf("%0.1f", $data[sprintf("project%d_time", $i)]);
				$total_sum[$i-1] += $data[sprintf("project%d_time", $i)];
			}
			else {
				$tmp1 = "";
			}
			$pdf->MultiCell($w_hour, $height, $tmp1, $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M', true);
		}
		if ($data['project_sum'] != "") {
			$tmp1 = sprintf("%.01f", $data['project_sum']);
			$total_sum[6] += $data['project_sum'];
		}
		else {
			$tmp1 = "";
		}
		$pdf->MultiCell($w_hour, $height, $tmp1, $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M', true);
		if ($data['group_time'] != "") {
			$tmp1 = sprintf("%.01f", $data['group_time']);
			$total_sum[7] += $data['group_time'];
		}
		else {
			$tmp1 = "";
		}
		$pdf->MultiCell($w_hour, $height, $tmp1, $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M', true);
		if ($data['training_time'] != "") {
			$tmp1 = sprintf("%.01f", $data['training_time']);
			$total_sum[8] += $data['training_time'];
		}
		else {
			$tmp1 = "";
		}
		$pdf->MultiCell($w_hour, $height, $tmp1, $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M', true);
		if ($data['other_time'] != "") {
			$tmp1 = sprintf("%.01f", $data['other_time']);
			$total_sum[9] += $data['other_time'];
		}
		else {
			$tmp1 = "";
		}
		$pdf->MultiCell($w_hour, $height, $tmp1, $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M', true);
		if ($data['other_sum'] != "") {
			$tmp1 = sprintf("%.01f", $data['other_sum']);
			$total_sum[10] += $data['other_sum'];
		}
		else {
			$tmp1 = "";
		}
		$pdf->MultiCell($w_hour, $height, $tmp1, $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M', true);
		if ($data['total_sum'] != "") {
			$tmp1 = sprintf("%.01f", $data['total_sum']);
			$total_sum[11] += $data['total_sum'];
		}
		else {
			$tmp1 = "";
		}
		$pdf->MultiCell($w_hour, $height, $tmp1, $border_y, 'C', $fill_n, $ln_y, '', '', $reseth_y, 0, false, true, $height, 'M', true);
	}

	// 31日まで埋める
	for ($day_cnt = $day_cnt; $day_cnt < 31; $day_cnt++) {
		$pdf->MultiCell($s1, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 1, false, true, $height, 'M', true);
		$pdf->MultiCell($s1, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 2, false, true, $height, 'M', true);
		$pdf->MultiCell($w_time, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 2, false, true, $height, 'M', true);
		$pdf->MultiCell($w_time, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 2, false, true, $height, 'M', true);
		$pdf->MultiCell($w_hour, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M', true);
		$pdf->MultiCell($w_hour, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M', true);
		$pdf->MultiCell($w_hour, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M', true);
		$pdf->MultiCell($s2, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M', true);
		$pdf->MultiCell($w_hour, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M', true);
		$pdf->MultiCell($w_hour, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M', true);
		for ($i = 1; $i <= 6; $i++) {
			$pdf->MultiCell($w_hour, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M', true);
		}
		$pdf->MultiCell($w_hour, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M', true);
		$pdf->MultiCell($w_hour, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M', true);
		$pdf->MultiCell($w_hour, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M', true);
		$pdf->MultiCell($w_hour, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M', true);
		$pdf->MultiCell($w_hour, $height, "", $border_y, 'C', $fill_n, $ln_n, '', '', $reseth_y, 0, false, true, $height, 'M', true);
		$pdf->MultiCell($w_hour, $height, "", $border_y, 'C', $fill_n, $ln_y, '', '', $reseth_y, 0, false, true, $height, 'M', true);
	}

	// 合計欄
	$pdf->SetX($group_xpos);
	for ($i = 0; $i < 12; $i++) {
		if ($total_sum[$i] != 0) {
			$tmp1 = sprintf("%.01f", $total_sum[$i]);
		}
		else {
			$tmp1 = "";
		}
		if ($i != 11) {
			$pdf->MultiCell($w_hour, $height, $tmp1, $border_y, 'C', $fill_n, $ln_n, "", '', $reseth_y, 0, false, true, $height, 'M', true);
		}
		else {
			$pdf->MultiCell($w_hour, $height, $tmp1, $border_y, 'C', $fill_n, $ln_y, "", '', $reseth_y, 0, false, true, $height, 'M', true);
		}
	}

	$pdf->Ln();

	// 超勤欄
	$pdf->SetX(165);
	$tmp1 = sprintf("超勤    %3.01f  H", $datas['Kintai']['over_time']);
	$pdf->MultiCell(20, 0, $tmp1, $border_u, 'L', $fill_n, $ln_y, '', '', $reseth_y, 0, false, true, 0, 'M');

	$pdf->Ln();

	// 年休欄
	$pdf->SetX(165);
	$tmp1 = sprintf("年休    %3.01f  H", $datas['Kintai']['holiday'] * 8.0);
	$pdf->MultiCell(20, 0, $tmp1, $border_u, 'L', $fill_n, $ln_y, '', '', $reseth_y, 0, false, true, 0, 'M');

	// 出力
 	ob_end_clean();
	$file_name = sprintf("勤務状況報告書%04d年%02d月（%s）", $datas['Kintai']['year'], $datas['Kintai']['month'], $datas['Staff']['name']);
	$pdf->Output("$file_name.pdf", "D");

?>
