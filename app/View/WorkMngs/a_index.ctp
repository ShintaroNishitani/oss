<script>
</script>


<blockquote><p><?php echo $this->Html->image("work_mng.png")?> <?php echo '労務管理';?></p></blockquote>

<div class="center">
<h5>
<a href="/a/work_mngs/index/<?php echo ($year - 1);?>"><i class="glyphicon glyphicon-backward"></i> 前年度 </a>
<b><?php echo sprintf("%s年度", $year);?></b>
<a href="/a/work_mngs/index/<?php echo ($year + 1);?>"> 次年度 <i class="glyphicon glyphicon-forward"></i></a>
</h5>
</div>

</br></br>

<table class="table-bordered table-condensed table-striped table-hover">
    <tr>
        <th colspan="10">基本情報</th>
        <th colspan="13">残業時間（36協定残業時間)</th>
    </tr>
    <tr>
        <th style="min-width:60px;">社員番号</th>
        <th style="min-width:100px;">名前</th>
        <th style="min-width:100px;">入社日</th>
        <th style="min-width:100px;">在籍期間</th>
        <th>振休<br/>残日数</th>
        <th>特休<br/>残日数</th>
        <th>有休<br/>残日数</th>
        <th>半休<br/>残数</th>
        <th>有給<br/>取得日数</th>
        <th>超過<br/>回数</th>
        <?php foreach($headers as $header):?>
            <th style="min-width:60px;"><?php echo h($header)?>月</th>
        <?php endforeach;?>
        <th style="min-width:60px;">平均残<br/>業時間</th>
    </tr>
    <?php
        $num = 1;
        foreach ($datas as $data):
            $click = sprintf(' onclick="editStaff(%d);"', $data['Staff']['id']);
            $end = date("Y/m/d");
            if ($data['Staff']['retire_date']) {
                $end = $data['Staff']['retire_date'];
            }

            $from = strtotime($data['Staff']['hire_date']);
            $to = strtotime($end);
            $diff = (date("Y", $to)*12+date("m", $to)) - (date("Y", $from)*12+date("m", $from));
            if ($data['Staff']['retire_date']) {
                $diff++;
            }
            $term = intval(($diff / 12)) .'年' . ($diff % 12) .'ヵ月';

    ?>
    <tr>
        <td class="center"><?php echo h($data['Staff']['no'])?></td>
        <td><?php echo h($data['Staff']['name'])?></td>
        <td class="center"><?php echo h($data['Staff']['hire_date'])?></td>
        <td class="center"><?php echo h($term)?></td>
        <td class="center"><?php echo h($data['PaidHoliday']['trans_remain'])?></td>
        <td class="center"><?php echo h($data['PaidHoliday']['special_remain'])?></td>
        <td class="center"><?php echo h($data['PaidHoliday']['holiday_remain'])?></td>
        <td class="center"><?php echo h($data['PaidHoliday']['half_remain'])?></td>
        <td class="text-right">
            <?php if($data['PaidHoliday']['paid_digest'] < 5):?>
                <i class="glyphicon glyphicon-info-sign" style="color:red;"></i>
            <?php endif;?>
            <?php echo h($data['PaidHoliday']['paid_digest'])?>日
        </td>
        <td class="center">
            <?php if($data['Kintai']['over_extra_time_count'] > 6):?>
                <i class="glyphicon glyphicon-info-sign" style="color:red;"></i>
            <?php endif;?>
            <?php echo h($data['Kintai']['over_extra_time_count']); ?>
        </td>
        <?php foreach($headers as $header):?>
            <td class="text-right">
                <?php if($data['Kintai']['extra_time'][$header] >= MAX_EXTRA_TIME):?>
                    <i class="glyphicon glyphicon-info-sign" style="color:red;"></i>
                <?php endif;?>
                <?php
                $over_time = $data['Kintai']['over_time'][$header];
                $over_time_60h = $over_time > 60 ? $over_time - 60 : 0;
                if($over_time_60h > 0):?>
                    <i class="glyphicon glyphicon-exclamation-sign" style="color:orange;" aria-hidden="true" title="<?= h($over_time_60h)."時間オーバー" ?>"></i>
                <?php endif;?>
                <?php echo h($data['Kintai']['over_time'][$header])?>
                <?php if($data['Kintai']['extra_time'][$header]):?>
                (<?php echo h($data['Kintai']['extra_time'][$header])?>)
                <?php endif;?>
            </td>
        <?php endforeach;?>
        <td class="text-right">
            <?php echo h($data['Kintai']['ave_over_time'])?>
            （<?php echo h($data['Kintai']['ave_extra_time'])?>)
        </td>
    </tr>
    <?php endforeach; ?>
    <tr>
        <th colspan="22">合計</th>
        <th class="text-right">
            <?php echo h($sum_ave_over_time)?>
            （<?php echo h($sum_ave_extra_time)?>)
        </th>
    </tr>
</table>
