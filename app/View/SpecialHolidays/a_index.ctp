<script>
	/**
	 * [addSpecialHoliday 特休情報追加処理]
	 * @return {[type]}    [description]
	 */
	function addSpecialHoliday()
	{
		var url = "/a/special_holidays/add/";
		loadBaseWindow(url);
		return false;
	}

	/**
	 * [editSpecialHoliday 特休情報編集処理]
	 * @param  {[type]} id 編集対象レコードID
	 * @return {[type]}    [description]
	 */
	function editSpecialHoliday(id)
	{
		if (id == undefined) {
			id = 0;
		}
		var url = "/a/special_holidays/edit/" + id;
		loadBaseWindow(url);
		return false;
	}

	/**
	 * [deleteSpecialHoliday 特休情報削除処理]
	 * @param  {[type]} id 削除対象レコードID
	 * @param  {[type]} date 削除対象レコード"使用日"設定値
	 * @return {[type]}    [description]
	 */
	function deleteSpecialHoliday(id, date)
	{
		if (date == 0) {
			if (window.confirm('削除しますがよろしいですか？')) {
				var url = "/a/special_holidays/delete/" + id;
				location.replace(url);
			}
		}
		else {
			if (window.alert('勤怠表との不整合が発生するため、勤怠表を先に更新して下さい。')) {
			}
		}
	}
</script>


<blockquote><p><?php echo $this->Html->image("special_holiday.png")?><?php echo ' 特休管理';?></p></blockquote>

<?php echo $this->Form->create("SpecialHoliday", array("action" => "index", 'type'=>'get'))?>
<table class="table-condensed well">
	<tr>
		<th style="min-width:50px;">社員名</th>
		<td><?php echo  $this->Form->select("SpecialHoliday.select_staff", $staffs, array("class" => "form-control input-sm", "value"=>$select_staff));?></td>
		<th style="min-width:50px;">表示指定</th>
		<td><?php echo  $this->Form->select("SpecialHoliday.select_option",
											$option_list ,
											array("class" => "form-control input-sm",
											      "value" => $select_option));?></td>
		<td class="center" colspan="8">
			<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> 検索</button>
		</td>
	</tr>
</table>
<?php echo $this->Form->end()?>

<a href="javascript:void(0);" onclick="addSpecialHoliday();"><i class="glyphicon glyphicon-plus-sign"></i> 新規追加</a>

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
	<th>No</th>
	<th style="min-width:100px;"><?php echo $this->Paginator->sort('staff_id',   '社員名');?></th>
	<th style="min-width:100px;"><?php echo $this->Paginator->sort('detail',     '内容');?></th>
	<th style="min-width:100px;"><?php echo $this->Paginator->sort('start_date', '期間[開始日]');?></th>
	<th style="min-width:100px;"><?php echo $this->Paginator->sort('end_data',   '期間[終了日]');?></th>
	<th style="min-width:100px;"><?php echo $this->Paginator->sort('use_data',   '使用日');?></th>
	<th></th>
</tr>
<?php
	$no = 1;
	$use_date = "";
	foreach ($holiday_data as $rec):
	$click = sprintf(' onclick="editSpecialHoliday(%d);"', $rec['SpecialHoliday']['id']);
?>
<tr>
	<td <?php echo $click;?>><?php echo h($no++)?></td>
	<td class="center" <?php echo $click;?>><?php echo h($rec['Staff']['name'])?></td>
	<td class="center" <?php echo $click;?>><?php echo h($rec['SpecialHoliday']['detail'])?></td>
	<?php if($rec['SpecialHoliday']['start_date'] == "0000-00-00"): ?>
		<td class="center" <?php echo $click;?>><?php echo h("-------------")?></td>
	<?php else: ?>
		<td class="center" <?php echo $click;?>><?php echo h($rec['SpecialHoliday']['start_date'])?></td>
	<?php endif; ?>
	<?php if($rec['SpecialHoliday']['end_date'] == "0000-00-00"): ?>
		<td class="center" <?php echo $click;?>><?php echo h("-------------")?></td>
	<?php else: ?>
		<td class="center" <?php echo $click;?>><?php echo h($rec['SpecialHoliday']['end_date'])?></td>
	<?php endif;?>
	<td <?php echo $click;?>>
		<?php
			if ($rec['SpecialHoliday']['use_date'] == "0000-00-00" || $rec['SpecialHoliday']['use_date'] == NULL) {
				if ($rec['SpecialHoliday']['expiration'] == 1) {
					echo h("未使用(期限切れ)");
				}
				else {
					echo h("-------------");
				}
			}
			else {
				echo h($rec['SpecialHoliday']['use_date']);
			}
		?>
	</td>
	<td class="center">
		<a href="javascript:void(0);" onclick="deleteSpecialHoliday(<?php echo $rec['SpecialHoliday']['id'];?>, <?php echo $use_date;?>);">
		<i class="glyphicon glyphicon-remove-circle"></i></a>
	</td>
</tr>
<?php 
endforeach; 
?>
</table>

<?php echo $this->Element("a_pagination", array('count'=>true));?>

