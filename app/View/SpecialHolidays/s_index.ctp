<script>
</script>


<blockquote><p><?php echo $this->Html->image("special_holiday.png")?><?php echo ' 特休情報';?></p></blockquote>

<?php echo $this->Form->create("SpecialHoliday", array("action" => "index", 'type'=>'get'))?>
<table class="table-condensed well">
	<tr>
		<th style="min-width:50px;">表示指定</th>
		<td><?php echo  $this->Form->select("SpecialHoliday.select_option",
											$option_list ,
											array("class" => "form-control input-sm",
											      "value" => $select_option));?></td>
		<td class="center" colspan="8">
			<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> 検索</button>
		</td>
	</tr>
</table>
<?php echo $this->Form->end()?>

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
	<th>No</th>
	<th style="min-width:100px;"><?php echo $this->Paginator->sort('detail',     '内容');?></th>
	<th style="min-width:100px;"><?php echo $this->Paginator->sort('start_date', '期間[開始日]');?></th>
	<th style="min-width:100px;"><?php echo $this->Paginator->sort('end_data',   '期間[終了日]');?></th>
	<th style="min-width:100px;"><?php echo $this->Paginator->sort('use_data',   '使用日');?></th>
</tr>
<?php
	$no = 1;
	foreach ($holiday_data as $rec):
?>
<tr>
	<td><?php echo h($no++)?></td>
	<td class="center"><?php echo h($rec['SpecialHoliday']['detail'])?></td>
	<?php if($rec['SpecialHoliday']['start_date'] == "0000-00-00"): ?>
		<td class="center"><?php echo h("-------------")?></td>
	<?php else: ?>
		<td class="center"><?php echo h($rec['SpecialHoliday']['start_date'])?></td>
	<?php endif; ?>
	<?php if($rec['SpecialHoliday']['end_date'] == "0000-00-00"): ?>
		<td class="center"><?php echo h("-------------")?></td>
	<?php else: ?>
		<td class="center"><?php echo h($rec['SpecialHoliday']['end_date'])?></td>
	<?php endif;?>
	<?php if($rec['SpecialHoliday']['use_date'] == "0000-00-00"): ?>
		<td class="center"><?php echo h("-------------")?></td>
	<?php elseif($rec['SpecialHoliday']['use_date'] == ""): ?>
		<td class="center"><?php echo h("-------------")?></td>
	<?php else: ?>
		<td class="center"><?php echo h($rec['SpecialHoliday']['use_date'])?></td>
	<?php endif;?>
</tr>
<?php 
endforeach;
?>
</table>

<?php echo $this->Element("a_pagination", array('count'=>true));?>

