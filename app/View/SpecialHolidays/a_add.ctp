<script>
	$(function(){
		var columns = ['#SpecialHolidayStartDate', '#SpecialHolidayEndDate' , '#SpecialHolidayUseDate'];
		$.each(columns, function(i, value) {
			$(value).datepicker({
				dateFormat: "yy-mm-dd"
			});
		});
	});

</script>

<blockquote><p><?php echo '特休追加';?></p></blockquote>
<p class="text-info"><?php echo '特休情報を入力してください。';?></p>

<?php echo $this->Form->create('SpecialHoliday', array("name"=>"SpecialHoliday", "action" => "commit"))?>
<?php echo $this->Form->hidden("SpecialHoliday.staff_id")?>
<?php echo $this->Form->hidden("SpecialHoliday.id", array("value"=>0))?>
<?php echo $this->Form->hidden("SpecialHoliday.enable", array("value"=>1))?>
<?php echo $this->Form->hidden("SpecialHoliday.expiration", array("value"=>0))?>

<table class="form">
<tr>
	<th>対象社員*</th>
	<td><?php echo  $this->Form->select("SpecialHoliday.staff_id", $staffs, array("class" => "form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
	<th>内容</th>
	<td><?php echo $this->Form->text('SpecialHoliday.detail', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
	<th>有効期間[開始日]*</th>
	<td><?php echo  $this->Form->text("SpecialHoliday.start_date", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
	<th>有効期間[終了日]</th>
	<td><?php echo  $this->Form->text("SpecialHoliday.end_date", array("class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
	<th>追加日数*</th>
    <td><?php echo $this->Form->text("SpecialHoliday.add_num", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
	<td colspan="2" class="center">
		<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
	</td>
</tr>
</table>

<?php echo $this->Form->end();?>
