<?php
App::uses('AppHelper', 'View/Helper');

class CalendarHelper extends AppHelper{
	var $helpers = array('Html');
	//カレンダー開始曜日
	//デフォルトでは日本なので日曜日にする。
	var $cal_start = 0;
	var $youbi = array('日','月','火','水','木','金','土');
	var $youbi_style = array('sun','mon','tue','wed','thu','fri','sat');

	/**
	 * [create カレンダー]
	 * @param  [type] $date          [基準の日付]
	 * @param  [type] $action_detail [日付をクリック時のアクション]
	 * @param  [type] $action_add    [追加用のアクション]
	 * @param  [type] $img           [追加用のアクションの画像]
	 * @param  [type] $objects       [日付欄に出力するオブジェクト]→$obj['year']['month']['day'][]の配列で
	 * @param  [type] $holidays      [休日]
	 * @param  [type] $meeting       [月例会日]
	 * @return [type]                [None]
	 */
	function create($date, $action_detail = null, $action_add = null, $img = null, $objects = null, $holidays = null, $meeting = null){
		$time = strtotime($date);
		$year = date('Y', $time);
		$month = date('n', $time);
		$day = date('j', $time);
		$numOfDay = $this->getDaysOfMonth($year, $month);
		$today = date('Y-n-j');

		$cal_map = array();
		$cal_rev = array();
		$cal = array();
		//最大の広さで6週42日(厳密には37日)
		for($i = 1; $i <= 42; $i++){
			$line = ceil($i / 7);
			$cal_rev[$i] = array('line' => $line, 'week' => ($this->cal_start + $i - 1) % 7);
			$cal_map[$line][($this->cal_start + $i - 1) % 7] = $i;
			$cal[$line][($this->cal_start + $i - 1) % 7] = null;
		}

		//それぞれに割り当てていく
		//月初の曜日から埋めていくポイントを決める
		$first = $cal_map[1][date('w', strtotime("$year-$month-1"))];

		$day = 1;
		$max_line = 6;
		for($i = $first; $i < $first + $numOfDay; $i++){
			$cal[$cal_rev[$i]['line']][$cal_rev[$i]['week']] = $day;
			$max_line = $cal_rev[$i]['line'];
			$day++;
		}

		$th_title = '<th class="title">%s</th>';
		$th = '<th class="%s">%s</th>'."\n";
		$td = '<td class="%s">%s</td>'."\n";

		echo '<table class="calendar">';

		//まず曜日の行
		echo '<tr>';
		for($i = $this->cal_start; $i < $this->cal_start + 7; $i++){
			echo sprintf($th_title, $this->youbi[$i % 7]);
		}

		echo '</tr>';

		for($i = 1; $i <= $max_line; $i++){
			//日付行
			echo '<tr>';
			for($j = $this->cal_start; $j < $this->cal_start + 7; $j++){
				$w = $j % 7;
				$obj = $cal[$i][$w];
				if($obj != null){
					if($action_detail != null){
						$obj = $this->Html->link($cal[$i][$w], array('action' => $action_detail, $year, $month, $cal[$i][$w]));
					}
					if($action_add != null){
						$obj .= ' '.$this->Html->link($this->Html->image($img, array('width' => 15)), array('action' => $action_add, $year, $month, $cal[$i][$w]), null, null, false);
					}
					foreach ($holidays as $holiday) {
						if (strtotime($holiday['Holiday']['date']) == strtotime("$year-$month-$obj")) {
							$obj = $holiday['Holiday']['name'] ." ". $obj;
							break;
						}
					}

					if ($meeting && strtotime($meeting['Meeting']['date']) == strtotime("$year-$month-$obj")) {
						$obj = '月例会' ." ". $obj;
					}
				}
				echo sprintf($th, $this->youbi_style[$w], $obj);
			}
			echo '</tr>';
			//内容行
			echo '<tr>';
			for($j = $this->cal_start; $j < $this->cal_start + 7; $j++){
				$w = $j % 7;
				$day = $cal[$i][$w];
				$obj = '';
				if($day != null && !empty($objects[$year][$month][$day])){
					$obj = '<ul>';
					foreach($objects[$year][$month][$day] as $tmp){
						$obj .= '<li>'.$tmp.'</li>';
					}
					$obj .= '</ul>';
				}
				$style = $this->youbi_style[$w];
				if (strtotime($today) == strtotime("$year-$month-$day")) {
					$style = 'today';
				} else {
					foreach ($holidays as $holiday) {
						if (strtotime($holiday['Holiday']['date']) == strtotime("$year-$month-$day")) {
							$style = $this->youbi_style[0];
							break;
						}
					}
				}

				echo sprintf($td, $style, $obj);
			}
			echo '</tr>';
		}
		echo '</table>';

	}

	/**
	 * [getDaysOfMonth 日数取得]
	 * @param  [type] $year  [年]
	 * @param  [type] $month [月]
	 * @return [type]        [None]
	 */
	function getDaysOfMonth($year, $month){
		$days = array(
			1 => 31,
			2 => 28,
			3 => 31,
			4 => 30,
			5 => 31,
			6 => 30,
			7 => 31,
			8 => 31,
			9 => 30,
			10 => 31,
			11 => 30,
			12 => 31
		);

		// 閏年
		if($month == 2){
			if($year % 4 == 0){
				if($year % 400 == 0){
					return $days[$month] + 1;
				}
				if($year % 100 == 0){
					return $days[$month];
				}
				return $days[$month] + 1;
			}
			return $days[$month];
		}else{
			return $days[$month];
		}

	}
}
?>
