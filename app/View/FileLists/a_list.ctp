<script type="text/javascript" charset="utf-8">
//<![CDATA[
    function deleteFileList(id){
        if (id == undefined) {
            return false;
        }
        if (confirm("削除してもよろしいですか？")) {
            $.ajax({
                type: "get",
                url: "/a/file_lists/delete/" + id,
                cache: false,
                success: function(html){
                    // alert(html);
                    var res = eval('(' + html + ')');
                    if (res["error"] == 0) {
                        $('#FileList').load("/a/file_lists/list/<?php echo $type;?>/<?php echo $id;?>");
                    }
                    myAlert(res["message"]);
                }
            });
            
        }
    }
//]]>
</script>

<div class="well">
<p class="guide"><i class="glyphicon glyphicon-play"></i> <?php echo '添付ファイル' ?></p>
<?php if (count($files)):?>
<table class="table-condensed">
<?php
$num = 1;
foreach ($files as $f):
?>
<tr>
    <td class="left">
        <i class="glyphicon glyphicon-stop"></i>
        <a href="/a/file_lists/download/<?php echo $f["FileList"]["id"];?>">
        <?php
            if (!empty($f["FileList"]["remark"])) {
                echo $f["FileList"]["remark"];
            } else {
                echo $f["FileList"]["name"];
            }
        ?></a>
    </td>
    <td>
    </td>
    <td>
        <a href="javascript:void(0);" onclick="deleteFileList(<?php echo $f["FileList"]["id"];?>);"><i class="glyphicon glyphicon-remove-circle"></i></a>
    </td>
</tr>
<?php
$num++;
endforeach;
?>
<?php endif;?>
</table>
</br>
<?php echo $this->Form->create("FileList", array('url'=>array("action" => "upload_file"), "type" => "file", "target" => "UploadFile"))?>
<?php echo $this->Form->hidden("FileList.".$type."_id", array("value" => $id))?>
<dl>
<dt>ファイル選択</dt>
<dd><?php echo $this->Form->file("FileList.files. ", array( 'type' => 'file', 'multiple' => 'multiple'))?></dd>
<dt>メモ</dt>
<dd><?php echo $this->Form->text("FileList.remark")?></dd>
</dl>
</br>
<button type="submit" class="btn btn-success btn-sm"><i class="glyphicon glyphicon-upload"></i> アップロード</button>
<dd></dd>

<?php echo $this->Form->end();?>
<iframe name="UploadFile" style="display:none"></iframe>
</div>