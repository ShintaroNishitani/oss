<script>

    function editCustomer(id){
        if (id == undefined) {
            id = 0;
        }
        var url = '/a/customers/edit/' + id;
        loadBaseWindow(url);
        return false;
    }

    function deleteCost(id){
        if(window.confirm('削除しますがよろしいですか？')){
            var url = "/a/customers/delete/" + id;
            location.replace(url);
        }
    }

</script>


<blockquote><p><?php echo $this->Html->image("customer.png")?> 顧客管理</p></blockquote>
<p class="guide"><?php echo '顧客を登録してください。' ?></p>


<a href="javascript:void(0);" onclick="editCustomer();"><i class="glyphicon glyphicon-plus-sign"></i> 新規追加</a>
&nbsp;&nbsp;&nbsp;&nbsp;

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
    <th>No</th>
    <th style="min-width:200px;">略称</th>
    <th style="min-width:90px;">顧客コード</th>
    <th style="min-width:200px;">正式名称</th>
    <th style="min-width:50px;">優先</th>    
    <th></th>
</tr>
<?php
$no = 1;
foreach ($datas as $data):
    $click = sprintf(' onclick="editCustomer(%d);"', $data['Customer']['id']);
?>
<tr>
    <td <?php echo $click;?>><?php echo h($no++)?></td>
    <td <?php echo $click;?>><?php echo h($data['Customer']['name'])?></td>
    <td class="center" <?php echo $click;?>><?php echo h($data['Customer']['code'])?></td>
    <td <?php echo $click;?>><?php echo h($data['Customer']['full_name'])?></td>
    <td <?php echo $click;?>><?php if(1 == $data['Customer']['priority'])echo h("〇")?></td>    
    <td class="center">
        <a href="javascript:void(0);" onclick="deleteCost(<?php echo $data['Customer']["id"];?>);">
        <i class="glyphicon glyphicon-remove-circle"></i></a>
    </td>
</tr>
<?php
endforeach;
?>
</table>

<?php echo $this->Element("a_pagination", array('count'=>true));?>

