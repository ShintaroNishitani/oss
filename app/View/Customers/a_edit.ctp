<blockquote><p>顧客登録</p></blockquote>
<p class="text-info">顧客情報を入力してください。</p>

<?php echo $this->Form->create('Customer', array("name"=>"Customer", "action" => "update",
                                                        "onsubmit" => "return updateCustomer();"))?>
<?php echo $this->Form->hidden("Customer.id")?>
<?php echo $this->Form->hidden("Customer.enable", array("value"=>1))?>

<table class="form">
<tr>
    <th>略称</th>
    <td><?php echo $this->Form->text('Customer.name', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>顧客コード*</th>
    <td><?php echo $this->Form->text('Customer.code', array("class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
    <th>正式名称</th>
    <td><?php echo $this->Form->text('Customer.full_name', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_L));?></td>
</tr>
<tr>
    <th>優先表示フラグ</th>
    <td><?php echo $this->Form->checkbox('Customer.priority', array('label'=>'true', 'options' => Array("1"=>""))); ?>優先表示</td>
</tr>
<tr>
    <td colspan="6" class="center">
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>
</table>

<?php echo $this->Form->end();?>
