
<script>
    /**
     * [editBusinessTripOrders 編集]
     * @param  {[type]} id レコードid
     * @return {[type]}    [description]
     */
    function editBusinessTripOrders(id){
        if (id == undefined) {
            id = null;
        }
        var url = '/s/business_trip_order/edit/' + id ;
        window.location.href = url;
        return false;
    }
    /**
     * [delBusinessTripOrders 削除]
     * @param  {[type]} id レコードid
     * @return {[type]}    [description]
     */
    function delBusinessTripOrders(id){
        if(window.confirm('削除しますがよろしいですか？')){
            var url = '/s/business_trip_order/delete/' + id ;
            location.replace(url);
        }
        return false;
    }
    /**
     * [printBusinessTripOrders pdf出力]
     * @param  {[type]} id レコードid
     * @return {[type]}    [description]
     */
    function printBusinessTripOrders(id){
        var url = "/s/business_trip_order/print/" + id;
        location.replace(url);
    }
</script>

<blockquote><p><?php echo $this->Html->image("travel.png")?> 旅費精算</p></blockquote>

<div>
    <table class="table-bordered table-condensed table-striped table-hover">
        <tr>
            <th>No</th>
            <th style="min-width:50px;">出張命令日</th>
            <th style="min-width:50px;">出張先</th>
            <th style="min-width:150px;">出張目的</th>
            <th style="min-width:50px;">精算書提出日</th>
            <th style="min-width:50px;">旅費総額</th>
            <th></th>
            <th></th>
        </tr>
        <tbody>
            <?php
                foreach ($datas as $key => $value) :
                    $click = sprintf(' onclick="editBusinessTripOrders(%d);"',$value["id"]);
            ?>
                <?php
                    $priceSum = 0;
                    foreach ($datas as $key => $value) :
                ?>
                <?php endforeach;?>
            <tr>
                <td<?php echo $click;?>><?php echo h($value["id"])?></td>
                <td<?php echo $click;?>><?php echo h($value["id"])?></td>
                <td<?php echo $click;?>><?php echo h($value["id"])?></td>
                <td<?php echo $click;?>><?php echo h($value["id"])?></td>
                <td<?php echo $click;?>><?php echo h($value["id"])?></td>
                <td<?php echo $click;?>><?php echo h($priceSum)?></td>
                <td class="center">
                    <a href="javascript:void(0);" onclick="printBusinessTripOrders(<?php echo $value["id"];?>);">
                    <i class="glyphicon glyphicon-print"></i></a>
                </td>
                <td class="center">
                    <a href="javascript:void(0);" onclick="delBusinessTripOrders(<?php echo $value["id"];?>);">
                    <i class="glyphicon glyphicon-remove-circle"></i></a>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
<div>