<script>
<?php
 $items = Configure::read("apply_holiday_type"); 
?>

    const applyTypeList = {
    <?php
        foreach ($items as $key => $val):
    ?>
        "<?= $key ?>":"<?= $val ?>",
    <?php
        endforeach;
        ?>
    };

    const applyTypeOptions = $.map(applyTypeList, function (name, value) {
        return $('<option>', { value: value, text: name});
    });

    const template_text = '<?php echo $this->Form->text("ApplyHolidayDetailDate.{0}.{1}", array("empty" => false, "class"=>"form-control input-sm date-picker-ym", 'style'=>SIZE_S, "value"=>"{2}", 'disabled'=>true));?>';
    const tempalte_hidden = '<?php echo $this->Form->hidden("ApplyHolidayDetailDate.{0}.{1}", array("value"=>"{2}"))?>'
    const template_select = '<select id="select_{0}" class="form-control input-sm" style="<?php echo SIZE_M ?>" onchange="selectChange(this, {0})" required="required" disabled="disabled"></select>';
    $(function(){
        init();
        $(".date-picker-ym").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });

    function execConfirmEnd(type, type_id, label, status){
        var reason = "";
        if (status == <?= h(CONFIRM_NO)?>) {
            reason = window.prompt("差戻理由を入力してください。\r\n※不要な場合もOKボタンをクリックしてください。");
            if (reason == null) {
                return false;
            }

        } else {
            if(!window.confirm(label + 'しますがよろしいですか？')){
                return false;
            }
        }
        var check_staff_id = $('#ConfirmCheckStaffId').val();
        var data = {
            'type':type,
            'type_id':type_id,
            'status':status,
            'check_staff_id':check_staff_id,
            'reason':reason,
        }
        // var url = "/s/confirms/update/" + type + "/" + type_id + "/" + status + "/" + check_staff_id + "/" + encodeURI(reason);
        // location.replace(url);
        var url = "/s/confirms/update_ajax";
        $.ajax({
            type: "post",
            url: url,
            data: data,
            success: function(html){
                var res = eval('(' + html + ')');
                if (res["error"] == 0) {
                    // unloadAll();
                    location.reload();
                }
                myAlert(res["message"]);
            }
        });        
    }

    function execConfirm(type, type_id, label, status){
        // 承認者のチェック
        if (!$('#ConfirmCheckStaffId').val()) {
            alert('承認者を選択してください。');
            return false;
        }
        if(window.confirm(label + 'しますがよろしいですか？')){
            var check_staff_id = $('#ConfirmCheckStaffId').val();
            var url = "/s/apply_holidays/approval/" + type + "/" + type_id + "/" + status + "/" + check_staff_id;
            location.replace(url);
        }
    }

    function sprintf(format) {
        // 第2引数以降を順に処理
        for (var i = 1; i < arguments.length; i++) {
            // 正規表現でプレイスホルダと対応する引数の値を置換処理
            var reg = new RegExp('\\{' + (i - 1) + '\\}', 'g');
            format = format.replace(reg, arguments[i]);
        }

        // 最終的な置き換え結果を戻り値として返す
        return format;
    }

    function init() {
        var detaiDateList = [
        <?php
        if (isset($this->data['ApplyHolidayDetailDate'])):
            for ($i = 0; $i < count($this->data['ApplyHolidayDetailDate']);$i++):
                $applyHolidayDetailDate = $this->data['ApplyHolidayDetailDate'][$i];
        ?>
            {
                date:"<?php echo $applyHolidayDetailDate['date'] ?>",
                type:"<?php echo $applyHolidayDetailDate['type'] ?>",
                id:"<?php echo $applyHolidayDetailDate['id'] ?>",
            },
        <?php
            endfor;
        endif;
        ?>
        ];

        if (detaiDateList.length > 0) {
            $.each(detaiDateList,function(index, val){
                createDetail(index, val['date'], val['type'], val['id']);
            });
        } else {
            newDetailDate();
        }
    }

    /**
     * 新しい日付と種別を作成する
     */
    function newDetailDate() {
        // 最終カウントを取得する
        var count = $("div[name=detailDate]").length;
        return createDetail(count, "", "0", "");
    }

    /**
     * 日付と種別を新しく作成する
     */
    function deleteDetailDate() {
        // 最終カウントを取得する
        var count = $("div[name=detailDate]").length;
        $("div[name=detailDate]")[count - 1].remove();
    }
    
    
    /**
     * 日付と種別の入力項目を作成する
     */
    function createDetail(count, date, type, id) {
        var detail_date =  $('<div class="form-inline" name="detailDate" style="padding-bottom: 5px;">').appendTo(".detail_date");

        // 対象日
        var date_elem = sprintf(template_text, count, "date", date);
        var date = $(date_elem).appendTo(detail_date);
        $(date).datepicker({
            dateFormat: "yy-mm-dd"
        });
        $(date).attr('required', true);
        
        // 申請種別
        var select_elem = sprintf(template_select, count);
        var sel = $(select_elem).appendTo(detail_date);
        $(sel).append($.map(applyTypeList, function (name, value) {
            return $('<option>', { value: value, text: name});
        })).val(type);
        
        var type_elem = sprintf(tempalte_hidden, count, "type", type);
        $(type_elem).appendTo(detail_date);
        
        var id_elem = sprintf(tempalte_hidden, count, "id", id);
        $(id_elem).appendTo(detail_date);
        var enable_elem = sprintf(tempalte_hidden, count, "enable", "1");
        $(enable_elem).appendTo(detail_date);
        return detail_date;
    }

    function selectChange(thisItem, count) {
        var val = $(thisItem).val();
        $(sprintf("#ApplyHolidayDetailDate{0}type", count)).val(val);
    }

</script>

<blockquote><p>休出・休暇申請登録</p></blockquote>

<div class="center">
	<tr>
		<td width=30px></td>
		<td valign="top">
			<table class="table-bordered table-condensed">
				<tr>
			        <th colspan="10" class="center">年間情報</th>
    			</tr>
    			<tr>
	        		<th>有休残日数 [日]</th>
        			<td>
	            		<?php echo $this->Form->text("PaidHoliday",
                			array("class"=>"form-control input-sm number", "style"=>SIZE_XS, "readonly"=>"readonly",
								"value"=>sprintf("%.01f", $paid_remain)))?>
        			</td>
        			<th>振休残日数 [日]</th>
        			<td>
            			<?php echo $this->Form->text("TransferHoliday",
	                		array("class"=>"form-control input-sm number", "style"=>SIZE_XS, "readonly"=>"readonly",
                    			"value"=>sprintf("%.01f", $trans_remain)))?>
        			</td>
        			<th>特休残日数 [日]</th>
        			<td>
	            		<?php echo $this->Form->text("SpecialHoliday",
                			array("class"=>"form-control input-sm number", "style"=>SIZE_XS, "readonly"=>"readonly",
	                    		"value"=>$special_remain))?>
        			</td>
        			<th>半休残数</th>
        			<td>
	            		<?php echo $this->Form->text("HalfHolidayNum",
                			array("class"=>"form-control input-sm number", "style"=>SIZE_XS, "readonly"=>"readonly",
							"value"=>$half_remain))?>
        			</td>
        			<th>年間有休取得日数 [日]</th>
        			<td>
	            		<?php echo $this->Form->text("PaidHolidayDigest",
                			array("class"=>"form-control input-sm number", "style"=>SIZE_XS, "readonly"=>"readonly",
	                    		"value"=>sprintf("%.01f", $paid_digest)))?>
        			</td>
    			</tr>
            </table>
        </td>
    </tr>
</div>
<br />
<p class="text-info">申請情報を入力してください。</p>

<?php echo $this->Form->create('ApplyHoliday', array("name"=>"ApplyHoliday", "action" => "detail_update",
                                                        "onsubmit" => "return updateApplyHolidayDetail();"))?>
<?php echo $this->Form->hidden("ApplyHolidayDetail.staff_id", array("value"=>$staff_id))?>
<?php echo $this->Form->hidden("Confirm.id", array("value"=>isset($this->data['Confirm']['id'])?isset($this->data['Confirm']['id']):0))?>

<table class="form">
<tr>
    <th>承認者*</th>
    <td>
        <?php if ($view_apply_button && $data['Confirm']['status'] == CONFIRM_IN && $check_staff_rank != RANK_HEAD_DEPARTMENT): ?>
            <?php echo $this->Form->select('Confirm.check_staff_id', $check_staffs, array("class"=>"form-control input-sm", 'style'=>SIZE_M, 'empty'=>'選択してください'));?>
            ※次の承認者を選択して承認ボタンを押してください。
        <?php else: ?>
            <?php echo $this->Form->select('Confirm.check_staff_id', $check_staffs, array("class"=>"form-control input-sm", 'style'=>SIZE_M, 'empty'=>false, 'disabled'=>true));?>
        <?php endif; ?>
    </td>
</tr>
<?php if ($data['Confirm']['prev_check_staff_id']): ?>
<tr>
    <th>前回承認</th>
    <td>
        <?php echo $this->Form->select('Confirm.prev_check_staff_id', $request_staffs, array("class"=>"form-control input-sm", 'style'=>SIZE_M, 'empty'=>false, 'disabled'=>true));?>
    </td>
</tr>
<?php endif; ?>
<tr>
    <th>申請者</th>
    <td>
        <?php echo $this->Form->select('Confirm.request_staff_id', $request_staffs, array("class"=>"form-control input-sm", 'style'=>SIZE_M, 'empty'=>false, 'disabled'=>true));?>
    </td>
</tr>
<tr>
    <th>申請日</th>
    <td><?php echo $this->Form->text('Confirm.request_date', array("class"=>"form-control input-sm", 'style'=>SIZE_M, 'empty'=>false, 'disabled'=>true));?></td>
</tr>
<tr>
    <th>日付*/種別</th>
    <td>
        <div class="detail_date">
            <!-- 入力ボックスを動的に作成する -->
        </div>
    </td>
</tr>
<tr>
    <th>事由</th>
    <td><?php echo $this->Form->textarea("ApplyHolidayDetail.reason", array("empty" => true, "rows"=>2, "cols"=>50, "class"=>"form-control input-sm", 'disabled'=>true));?></td>
</tr>
<tr>
    <th>備考</th>
    <td><?php echo $this->Form->textarea("ApplyHolidayDetail.memo", array("empty" => true, "rows"=>3, "cols"=>50, "class"=>"form-control input-sm", 'disabled'=>true));?></td>
</tr>
<tr>
    <td colspan="6" class="center">
<?php
    $type = $data['Confirm']['type'];
    $type_id = $data['Confirm']['type_id'];

    // ログインスタッフと承認者が一致する場合のみボタンを表示する
    if ($view_apply_button) {
        // ステータスが「未申請」「差戻」の場合はボタンを表示しない
        if (!($data['Confirm']['status'] == CONFIRM_YET || $data['Confirm']['status'] == CONFIRM_NO)) {
            $status_ok = CONFIRM_OK;
            // ステータスが「承認」でなければ[承認]ボタン表示
            if ($data['Confirm']['status'] != CONFIRM_OK) {
                // 部長と部長以外で承認処理を分ける。部長で終わり。
                if ($staff_rank != RANK_HEAD_DEPARTMENT) {
                    // if ($staff_rank > RANK_HEAD_DEPARTMENT && $staff_rank != RANK_HEAD_DEPARTMENT) {
                    echo $this->Form->button('承認', array("class"=>"btn  btn-success", 'type'=>'button', 'onclick'=>"execConfirm($type, $type_id, '承認', $status_ok)"));
                    
                } else {
                    echo $this->Form->button('承認', array("class"=>"btn  btn-success", 'type'=>'button', 'onclick'=>"execConfirmEnd($type, $type_id, '承認', $status_ok)"));
                    
                }
                echo "&nbsp";
            }
    
            $status_no = CONFIRM_NO;
            // [差戻]ボタン
            echo $this->Form->button('差戻', array("class"=>"btn  btn-success", 'type'=>'button', 'onclick'=>"execConfirmEnd($type, $type_id, '差戻', $status_no)"));
?>
            <br/><br/><p style="color:red">※スマートフォンのLINE WORKSで起動するブラウザでは、差戻コメントが入力できません。<br/>差戻のコメントを入力する場合はGoogleChromeかSafariを使用してください。</p>
<?php
        }
    }


?>
    </td>
</tr>        
</table>

<?php echo $this->Form->end();?>