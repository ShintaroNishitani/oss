<script>
    $(document).ready(function(){
    });

    function execConfirm(type_id, label, status){
        if(window.confirm(label + 'しますがよろしいですか？')){
            var url = "/s/confirms/update/" + "<?= h(CONFIRM_TYPE_8) ?>" + "/" + type_id + "/" + status;
            location.replace(url);
        }
    }

    function editApplyHolidays(year, month, id, detail_id){
        if (detail_id == undefined) {
            detail_id = 0;
        }
        var url = '/s/apply_holidays/detail_edit/'+ year + '/' + month + '/' + id + '/' + detail_id;
        location.replace(url);
    }

    function deleteApplyHolidayDetails(id){
        if(window.confirm('削除しますがよろしいですか？')){
            var url = "/s/apply_holidays/detail_delete/" + id;
            location.replace(url);
        }
    }

    function printTransport(id){
        var url = "/s/apply_holidays/print/" + id;
        location.replace(url);
    }    
</script>


<blockquote><p><?php echo $this->Html->image("paid_holiday.png")?> 休出・休暇申請</p></blockquote>
<p class="guide"><?php echo '休出・休暇届を入力し、申請を行ってください。' ?></p>
<p class="description"><?php echo '※申請後は編集できません。編集を行う場合は、承認者に連絡してください。' ?></p>

<a href="javascript:void(0);" onclick="editApplyHolidays(<?php echo $year;?>, <?php echo $month;?>, <?php echo $id;?>);"><i class="glyphicon glyphicon-plus-sign"></i> 新規追加</a>
&nbsp;&nbsp;&nbsp;&nbsp;

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
    <th>No</th>
    <th style="min-width:150px;">申請日時</th>
    <th style="min-width:350px;">事由</th>
    <th style="min-width:350px;">備考</th>
    <th style="min-width:100px;">承認者</th>
    <th style="min-width:75px;">ステータス</th>
    <th></th>
</tr>
<?php
$items = Configure::read("apply_holiday_type");
$confirm_items = Configure::read("confirm_status");

$no = 1;
if (!empty($datas)):
    foreach ($datas as $data):
        $applyHolidayDetail = $data['ApplyHolidayDetail'];

        $detail_id = $applyHolidayDetail['id'];
        $check_staff_name = $data['Confirm']['CheckStaff']['name'];
        $status = $data['Confirm']['status'];
        $click = sprintf('onclick="editApplyHolidays(%d, %d, %d, %d);"', $year, $month, $data['ApplyHoliday']['id'], $detail_id);
?>
<tr>
    <td <?php echo $click;?>><?php echo h($no++)?></td>
    <td <?php echo $click;?> class="center"><?php echo h($applyHolidayDetail['apply_date'])?></td>
    <td <?php echo $click;?>><?php echo h($applyHolidayDetail['reason'])?></td>
    <td <?php echo $click;?>><?php echo h($applyHolidayDetail['memo'])?></td>
    <td <?php echo $click;?>><?php echo h($check_staff_name)?></td>
    <td <?php echo $click;?>><?php echo h($confirm_items[$status])?></td>
    <td class="center">
        <?php if ($status == CONFIRM_YET || $status == CONFIRM_NO):?>
        <a href="javascript:void(0);" onclick="deleteApplyHolidayDetails(<?php echo $detail_id;?>);">
        <i class="glyphicon glyphicon-remove-circle"></i></a>
        <?php endif;?>
    </td>

</tr>
<?php
    endforeach; 
endif;
?>

</table>

