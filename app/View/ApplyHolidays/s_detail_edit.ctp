<script>
<?php
    $items = Configure::read("apply_holiday_type");
 
    $status = isset($this->data['Confirm']['status'])? $this->data['Confirm']['status'] : 0;
    $hidden = false;
    // 承認済みの場合、"disable"属性、hiddenを設定
    if ($status == CONFIRM_IN || $status == CONFIRM_OK) {
        $disabled = 'disabled';
        $hidden = true;
    }
    else {
        $disabled = '';
        $hidden = false;
    }
?>

    const applyTypeList = {
    <?php
        foreach ($items as $key => $val):
    ?>
        "<?= $key ?>":"<?= $val ?>",
    <?php
        endforeach;
    ?>
    };

    const applyTypeOptions = $.map(applyTypeList, function (name, value) {
        return $('<option>', { value: value, text: name});
    });

    const template_text = '<?php echo $this->Form->text("ApplyHolidayDetailDate.{0}.{1}", array("empty" => false, "class"=>"form-control input-sm date-picker-ym", 'style'=>SIZE_S, "value"=>"{2}", "disabled"=>"$disabled"));?>';
    const tempalte_hidden = '<?php echo $this->Form->hidden("ApplyHolidayDetailDate.{0}.{1}", array("value"=>"{2}", "class"=>"{1}_column"))?>'
    const template_select = '<select id="select_{0}" class="form-control input-sm" style="<?php echo SIZE_M ?>" <?php echo $disabled ?> onchange="selectChange(this, {0})" required="required"></select>';
    $(function(){
        init();
        $(".date-picker-ym").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });

    function back() {
        var url = '/s/apply_holidays';
        location.replace(url);
        
    }

    function updateApplyHolidayDetail(){
        // 承認者のチェック
        if (!$('#ConfirmCheckStaffId').val()) {
            alert('承認者を選択してください。');
            return false;
        }
        if (!confirm('休出・休暇申請しますがよろしいですか？')) {
            return false;
        }
    }

    function sprintf(format) {
        // 第2引数以降を順に処理
        for (var i = 1; i < arguments.length; i++) {
            // 正規表現でプレイスホルダと対応する引数の値を置換処理
            var reg = new RegExp('\\{' + (i - 1) + '\\}', 'g');
            format = format.replace(reg, arguments[i]);
        }

        // 最終的な置き換え結果を戻り値として返す
        return format;
    }

    function init() {
        var detailDateList = [
        <?php
        if (isset($this->data['ApplyHolidayDetailDate'])):
            for ($i = 0; $i < count($this->data['ApplyHolidayDetailDate']);$i++):
                $applyHolidayDetailDate = $this->data['ApplyHolidayDetailDate'][$i];
        ?>
            {
                date:"<?php echo $applyHolidayDetailDate['date'] ?>",
                type:"<?php echo $applyHolidayDetailDate['type'] ?>",
                id:"<?php echo $applyHolidayDetailDate['id'] ?>",
            },
        <?php
            endfor;
        endif;
        ?>
        ];

        if (detailDateList.length > 0) {
            $.each(detailDateList,function(index, val){
                createDetail(index, val['date'], val['type'], val['id']);
            });
        } else {
            newDetailDate();
        }

    }

    /**
     * 新しい日付と種別を作成する
     */
    function newDetailDate() {
        // 最終カウントを取得する
        var last_row = $("div[name=detailDate]:last");
        var idx_column = $(last_row).children('.index_column');
        var idx = 0;
        if (idx_column.length > 0) {
            idx = parseInt($(idx_column).val()) + 1;

        }
        
        return createDetail(idx, "", "0", "");
    }

    /**
     * 日付と種別を削除する
     */
    function deleteDetailDate(idx) {
        // indexを基準に削除する
        var idx_column = $('.index_column[value='+idx+']:last');
        var row = $(idx_column).parent();
        var id_column = $(row).children('.id_column');
        var id = $(id_column).val();
        if (id) {
            var enable_column = $(row).children('.enable_column');
            $(enable_column).val('0');
            $(row).hide();
        } else {
            $(row).remove();
        }
    }
    
    
    /**
     * 日付と種別の入力項目を作成する
     */
    function createDetail(idx, date, type, id) {
        var detail_date =  $('<div class="form-inline" name="detailDate" style="padding-bottom: 5px;">').appendTo(".detail_date");

        // 行数
        var row_elem = sprintf('<input type="hidden" name="detailIndex" class="index_column" value="{0}" />', idx);
        $(row_elem).appendTo(detail_date);

        // 対象日
        var date_elem = sprintf(template_text, idx, "date", date);
        var date = $(date_elem).appendTo(detail_date);
        $(date).datepicker({
            dateFormat: "yy-mm-dd"
        });
        $(date).attr('required', true);
        
        // 申請種別
        var select_elem = sprintf(template_select, idx);
        var sel = $(select_elem).appendTo(detail_date);
        $(sel).append($.map(applyTypeList, function (name, value) {
            return $('<option>', { value: value, text: name});
        })).val(type);
        
        var type_elem = sprintf(tempalte_hidden, idx, "type", type);
        $(type_elem).appendTo(detail_date);
        
        var id_elem = sprintf(tempalte_hidden, idx, "id", id);
        $(id_elem).appendTo(detail_date);
        var enable_elem = sprintf(tempalte_hidden, idx, "enable", "1");
        $(enable_elem).appendTo(detail_date);

        <?php if (!$hidden): ?>
        var del_link =  sprintf('<button type="button" class="btn btn-link btn-sm" onclick="deleteDetailDate({0})">＋削除</button>', idx);
        $(del_link).appendTo(detail_date);
        <?php endif; ?>

        return detail_date;
    }

    function selectChange(thisItem, count) {
        var val = $(thisItem).val();
        $(sprintf("#ApplyHolidayDetailDate{0}type", count)).val(val);
    }

</script>

<blockquote><p>休出・休暇申請登録</p></blockquote>
<p class="text-info">申請情報を入力してください。</p>

<?php echo $this->Form->create('ApplyHoliday', array("name"=>"ApplyHoliday", "action" => "detail_update",
                                                        "onsubmit" => "return updateApplyHolidayDetail();"))?>
<?php echo $this->Form->hidden("ApplyHolidayDetail.id", array("value"=>$detail_id))?>
<?php echo $this->Form->hidden("ApplyHolidayDetail.apply_holiday_id", array("value"=>$id))?>
<?php echo $this->Form->hidden("ApplyHolidayDetail.staff_id", array("value"=>$staff_id))?>
<?php echo $this->Form->hidden("ApplyHoliday.year", array("value"=>$year))?>
<?php echo $this->Form->hidden("ApplyHoliday.month", array("value"=>$month))?>
<?php echo $this->Form->hidden("ApplyHolidayDetail.enable", array("value"=>1))?>
<?php echo $this->Form->hidden("Confirm.id", array("value"=>isset($this->data['Confirm']['id'])?$this->data['Confirm']['id']:0))?>
<?php echo $this->Form->hidden("Confirm.status", array("value"=>isset($this->data['Confirm']['status'])?$this->data['Confirm']['status']:0))?>
<?php echo $this->Form->hidden("Confirm.prev_check_staff_id", array("value"=>""))?>

<table class="form">
<tr>
    <th>承認者*</th>
    <td>
        <?php echo $this->Form->select('Confirm.check_staff_id', $check_staffs, array("class"=>"form-control input-sm", 'style'=>SIZE_M, 'empty'=>'選択してください', "disabled"=>"$disabled"));?>
        ※チーフ以下の方は承認者にリーダー以上の上長を選択してください。<br/>
        ※リーダー以上の方は承認者に所属の部長を選択してください。
    </td>
</tr>
<tr>
    <th>日付*/種別*</th>
    <td>
        <div class="detail_date">
            <!-- 入力ボックスを動的に作成する -->
        </div>
        <?php if (!$hidden): ?>
        <div>
            <button type="button" class="btn btn-link btn-sm" onclick="newDetailDate()">＋追加</button>
            <p class="description" >※申請する日を全て入力してください。</p>
            <!-- <button type="button" class="btn btn-link btn-sm" onclick="deleteDetailDate()">＋削除</button> -->
        </div>
        <?php endif; ?>
    </td>
</tr>
<tr>
    <th>事由*</th>
    <td><?php echo $this->Form->textarea("ApplyHolidayDetail.reason", array("empty" => true, "rows"=>2, "cols"=>50, "class"=>"form-control input-sm", "disabled"=>"$disabled"));?></td>
</tr>
<tr>
    <th>備考</th>
    <td>
        <?php echo $this->Form->textarea("ApplyHolidayDetail.memo", array("empty" => true, "rows"=>3, "cols"=>50, "class"=>"form-control input-sm", "disabled"=>"$disabled"));?>
        ※休日出勤の申請をする場合は、備考に振休予定記入してください。<br/>
        　振休を取得する場合（8h以上）→○月○日に振休取得予定<br/>
        　振休を取得しない場合（8h未満）→○hの作業予定のため振休取得なし<br/>
    </td>
</tr>
<?php if ($this->data['Confirm']['status'] == CONFIRM_NO): ?>
<tr>
    <th>差戻理由</th>
    <td>
        <?php echo $this->Form->text("Confirm.remark", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_LL,'disabled'=>true)); ?>
    </td>
</tr>
<?php endif; ?>
<tr>
    <td colspan="6" class="center">
    <?php if (!$hidden): ?>
        <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-send"></i> 申請</button>
    <?php endif; ?>
    <button type="button" onclick="back()" class="btn btn-info">
        <i class="glyphicon glyphicon-circle-arrow-left"></i> 戻る
    </button>
    </td>
</tr>        
</table>

<?php echo $this->Form->end();?>