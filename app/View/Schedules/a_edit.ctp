<style>
table.form tr td textarea{
    font-size:100%;
    height:300px;
    width:600px;
}
</style>

<script>
    $(function(){
        var columns = ['#ScheduleDate', '#ScheduleEndDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    });

    function updateSchedule() {
        var start = document.getElementById("ScheduleDate").value;
        var end = document.getElementById("ScheduleEndDate").value;

        start = new Date(start);
        end = new Date(end);

        if (start > end) {
        	alert("日付を確認してください");
        	return false;
        }
        return true;
    }
</script>

<blockquote><p>スケジュール登録</p></blockquote>
<p class="text-info">スケジュール情報を入力してください。</p>

<a href="/a/schedules/calendar/<?php echo $year?>/<?php echo $month?>"><i class="glyphicon glyphicon-transfer"></i> スケジュールに戻る</a>

<?php echo $this->Form->create('Schedule', array("action" => "edit", "onsubmit" => "return updateSchedule();", "type" => "file"))?>
<?php echo $this->Form->hidden("Schedule.id")?>
<?php echo $this->Form->hidden("Schedule.staff_id", array("value"=>$staff_id))?>
<?php echo $this->Form->hidden("Schedule.year", array("value"=>$year))?>
<?php echo $this->Form->hidden("Schedule.month", array("value"=>$month))?>
<?php echo $this->Form->hidden("Schedule.enable", array("value"=>1))?>

<table class="form">
<?php if ($id != -1): ?>
<tr>
    <th>登録者</th>
    <td><?php echo $staffs[$this->data['Schedule']['staff_id']];?></td>
</tr>
<?php endif; ?>
<tr>
    <th>開始日時*</th>
    <td>
        <div style="display:inline-flex">
        <?php echo $this->Form->text("Schedule.date", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S, "default"=>$date));?>
        <?php echo $this->Form->datetime('Schedule.start', null, '24', array('class'=>'form-control input-sm', 'interval' => 15, 'empty'=>true));?>
        </div>
    </td>
</tr>
<tr>
    <th>終了日時*</th>
    <td>
        <div style="display:inline-flex">
        <?php echo $this->Form->text("Schedule.end_date", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S, "default"=>$date));?>
        <?php echo $this->Form->datetime('Schedule.end', null, '24', array('class'=>'form-control input-sm', 'interval' => 15, 'empty'=>true));?>
        </div>
    </td>
</tr>
<tr>
    <th>タイトル*</th>
    <td><?php echo $this->Form->text('Schedule.title', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_LL));?></td>
</tr>
<tr>
    <th>メモ</th>
    <td><?php echo $this->Form->textarea("Schedule.memo", array("class"=>"form-control input-sm jpn"));?></td>
</tr>
<tr>
    <th>公開*</th>
    <td><?php echo $this->Form->select('Schedule.public', Configure::read("schedule_public"), array("class"=>"form-control input-sm", 'style'=>SIZE_M, 'empty'=>false));?></td>
</tr>
<tr>
    <td colspan="6" class="center">
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
        <button type="submit" class="btn btn-primary" name="del" value="del"><i class="glyphicon glyphicon-remove"></i> 削除</button>
    </td>
</tr>
</table>

<?php echo $this->Form->end();?>
