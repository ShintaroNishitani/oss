<script>
    $(function(){
	    $('#CalenderDate').datepicker({
	            dateFormat:"yy-mm",
	            showOn:"both",
	            buttonImage: "/img/calendar.png",
	            buttonImageOnly: true,
	            onSelect: function(dateText, inst) {
	                var ymd = dateText.split("-");
	                location.href="<?php echo $this->Html->url(array('action' => 'calendar'));?>/" + ymd[0] + "/" + ymd[1];
	            }
	        });
    });
</script>

<blockquote><p><?php echo $this->Html->image("calendar.png")?> スケジュール</p></blockquote>

<p class="guide">スケジュールを新規登録するには<?php echo $this->Html->image('add2.png', array('width' => 15));?>をクリックしてください。</p>

<div class="center">
<h5>
<a href="/a/schedules/calendar/<?php echo $b_year;?>/<?php echo $b_month;?>"><i class="glyphicon glyphicon-backward"></i> 前月 </a>
<b><?php echo sprintf("%s年 %s月", $year, $month);?></b>
<input type="hidden" id="CalenderDate" value="<?php echo sprintf('%s-%02d', $year, $month);?>">
<a href="/a/schedules/calendar/<?php echo $n_year;?>/<?php echo $n_month;?>"> 翌月 <i class="glyphicon glyphicon-forward"></i></a>
</h5>
</div>

<?php
	$obj = array();
	$month = intval($month);
	for($day = 1; $day <= 31; $day++){
		$obj[$year][$month][$day][0] = $this->Html->image("add2.png", array('url'=>array('action'=>'edit', -1, "$year-$month-$day"),
															  	    		'title'=>"新規登録"));
	}
	foreach($datas as $data){

		$start = strtotime($data['Schedule']['date']);
		if (empty($data['Schedule']['end_date'])){
			$end = $start;
		} else {
			$end = strtotime($data['Schedule']['end_date']);
		}
		while($start <= $end)
		{
			$y = date('Y', $start);
			$m = date('n', $start);
			$d = date('j', $start);

			if ($y != $year || $m != $month) {
				$start = strtotime("+1 day", $start);
				continue;
			}

			$title = "";
			if ($data['Schedule']['title']) {
				$from = "---:---";
				$sep = "～";
				$to = "---:---";

				if ($start == strtotime($data['Schedule']['date'])) {
					if ($data['Schedule']['start']) {
						$tmp = explode(':', $data['Schedule']['start']);
						$from = ($tmp[0].":".$tmp[1]);
					}
				}
				if ($start == strtotime($data['Schedule']['end_date'])) {
					if ($data['Schedule']['end']) {
						$tmp = explode(':', $data['Schedule']['end']);
						$to = ($tmp[0].":".$tmp[1]);
					}
				}
				// if ($from != "&emsp;&emsp;&emsp;" || $to != "&emsp;&emsp;&emsp;") {
				// 	$sep .= "～";
				// }
				$title = $from . $sep . $to;


				$title = '<span style="color:black;font-weight: bold; ">'.$title.'</span>';
				$title .= " </br>";
				$title .= $data['Schedule']['title'];
			}

			$obj[$y][$m][$d][0] = sprintf('%s<br/>%s',
				sprintf('<span style="float: left;text-align:left;margin-bottom:5px"><a href="/a/schedules/edit/%d/%d-%d-%d" title="%s">%s</a></span>',
					$data['Schedule']['id'], $y, $m, $d,
					$data['Schedule']['memo'],
					$title
				),
				$obj[$y][$m][$d][0]
			);
		    $start = strtotime("+1 day", $start);
		}

	}

	$this->Calendar->create("$year-$month-1", null, null, null, $obj, $holidays, $meeting);
?>
