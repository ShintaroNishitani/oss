<script>
    /**
     * [editSchedule スケジュール編集]
     * @param  {[type]} id    [編集対象スケジュールID]
     * @param  {[type]} year  [年]
     * @param  {[type]} month [月]
     * @param  {[type]} dey   [日]
     * @return {[type]}          [description]
     */
    function editSchedule (id, year, month, day)
    {
        var url = "/a/schedules/edit/" + id + "/" + year + "-" + month + "-" + day;
        location.href = url;
    }

</script>

<blockquote><?php echo h($data['Schedule']['title'])?></blockquote>

<hr style="margin-top:5px;margin-botom:10px" width="100%" color="#000000">

<table class="form">
<tr>
    <th>日時</th>
    <td><?php echo h($data['Schedule']['date']); ?></td>
</tr>
</table>
<?php echo nl2br($data['Schedule']['memo']); ?>
<hr style="margin-top:5px;margin-botom:10px" width="100%" color="#000000">
<?php  printf("登録 %s %s", $data['Schedule']['created'], $staffs[$data['Schedule']['staff_id']]); ?>
<hr style="margin-top:5px;margin-botom:10px" width="100%" color="#000000">

<tr>
    <td colspan="2" class="center">
        <?php
            $date = explode('-', $data['Schedule']['date']);
            $target_year = $date[0];
            $target_month = $date[1];
            $target_day = $date[2];
            $click = sprintf ("editSchedule(%d, %d, %d, %d)", $id, $target_year, $target_month, $target_day);
        ?>
        <?php echo $this->Form->button ("編集ページへ",
                                        array("class"=>"btn btn-success", "type"=>"button", "onclick"=>$click));?>
    </td>
</tr>

<?php echo $this->Form->end();?>
