<script>
	$(function(){
		var columns = ['#HumanResourceScTermS', '#HumanResourceScTermE'];
		$.each(columns, function(i, value) {
			$(value).datepicker({
				dateFormat: "yy-mm-dd"
			});
		});

	});

	function editHumanResource(id) {
		if (id == undefined) {
			id = 0;
		}
		var url = '/s/human_resources/edit/' + id ;
		loadBaseWindow(url);
		return false;
	}


	function updateHumanResource() {
		var url = "/s/human_resources/update/";
		updateData(url, "HumanResourceUpdateForm", null, function(){
			unloadAll();
			location.href = "/s/human_resources/index/";
		});

		return false;
	}

	function deleteHumanResource(id) {
		if (id != undefined) {
			if (confirm('削除してもよろしいですか？\nこの操作は元に戻せません。')) {
				var url = "/s/human_resources/delete/" + id + "/;?>";
				updateData(url, null, null, function(){
					location.href = "/s/human_resources/index/";
				});
			}
		}
		return false;
	}

	function mail_format(id) {
		var url = '/s/human_resources/mail_format/' + id ;
		loadBaseWindow(url);
		return false;
	}
</script>


<blockquote><p><?php echo $this->Html->image("job.png")?> <?php echo h('案件管理')?></p></blockquote>

<ul class="nav nav-tabs" role="tablist">
	<li><a href="/s/sale_projects/index/"><?php echo h('案件一覧');?></a></li>
	<li class="active"><a href="/s/human_resources/index/"><?php echo h('人材一覧');?></a></li>	
	<li><a href="/s/contracts/index/"><?php echo h('契約一覧');?></a></li>
	<li><a href="/s/companies/index/"><?php echo h('企業一覧');?></a></li>
	<li><a href="/s/contracts/result_index/"><?php echo h('精算一覧');?></a></li>
</ul>

</br></br>

<p class="guide"><?php echo '終了している人材は赤で色付けされます' ?></p>

<TABLE border="0">
<TBODY>
<TR>
<TD valign="top">

<?php echo $this->Form->create("HumanResource", array('url' => array('action' => 'index'), 'type'=>'get'))?>
<table class="table-condensed well">
	<tr>
		<th style="min-width:50px;">氏名</th>
		<td><?php echo $this->Form->text("HumanResource.sc_name", array("class" => "form-control input-sm jpn", 'style'=>SIZE_M, "value"=>$keys['name'][0]));?></td>
		<th style="min-width:50px;">所属</th>
		<td><?php echo $this->Form->text("HumanResource.sc_company", array("class" => "form-control input-sm jpn", 'style'=>SIZE_M, "value"=>$keys['company'][0]));?></td>
	</tr>
	<tr>
		<th style="min-width:70px;">ステータス</th>
		<td><?php echo  $this->Form->select("HumanResource.sc_status", array(0=>'有効',1=>'終了'), array("class" => "form-control input-sm", 'style'=>SIZE_S, "value"=>$keys['status'][0]));?></td>
		<th style="min-width:50px;">スキル</th>
		<td><?php echo $this->Form->text("HumanResource.sc_skill", array("class" => "form-control input-sm jpn", 'style'=>SIZE_M, "value"=>$keys['skill'][0]));?></td>
<!--         <th style="min-width:50px;">期間</th>
		<td>
			<div style="display:inline-flex">
			<?php echo $this->Form->text("HumanResource.sc_term_s", array("class" => "form-control input-sm", 'style'=>SIZE_S, "value"=>$keys['term_s'][0]));?>
			～
			<?php echo $this->Form->text("HumanResource.sc_term_e", array("class" => "form-control input-sm", 'style'=>SIZE_S, "value"=>$keys['term_s'][0]));?>
			</div>
		</td>
		<th style="min-width:50px;">スキル</th>
		<td><?php echo $this->Form->text("HumanResource.sc_skill", array("class" => "form-control input-sm", 'style'=>SIZE_M, "value"=>$keys['skill'][0]));?></td>

-->
	</tr>
	<tr>
		<td class="center" colspan="4">
			<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> 検索</button>
			<!-- <button type="submit" class="btn btn-primary" name="csv"><i class="glyphicon glyphicon-download"></i> CSV出力</button> -->
		</td>
	</tr>
</table>
<?php echo $this->Form->end()?>

</TD>
<TD width=30px>
</TD>
<TD valign="top">
<!-- <div class="well">
<?php
	echo $this->Form->create("HumanResource",
							 array("style"=>"font-size:12px;",
								   'name'=>'input',
								   "action"=>"upload",
								   "type" =>"file",
								   "onsubmit"=>"return beforeFileCheck();"));

?>
<p class="text-info">人材データの自動取込が出来ます。 <a href="/format/human_format.csv" download="人材データフォーマット.csv" target="_blank" style="text-decoration: underline;";><i class="glyphicon glyphicon-download"></i> フォーマットダウンロード</a></p>
<?php echo $this->Form->input('HumanResource.file', array("type"=>"file", "value"=>"", 'label'=>false, 'width'=>40,'height'=>20, "style" => "font-size:12px;"))?>
</br>
<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-upload"></i> アップロード</button>
<?php echo $this->Form->end();?>
</div> -->
</TD>
</TR>
</TBODY>
</TABLE>

<a href="javascript:void(0);" onclick="editHumanResource();"><i class="glyphicon glyphicon-plus-sign"></i> 新規追加</a>

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
	<th>No</th>
	<th>&nbsp;</th>
	<th style="min-width:80px;">氏名</th>
	<th style="min-width:50px;">生年月日</th>
	<th style="min-width:30px;">年齢</th>
	<th style="min-width:50px;">性別</th>
	<th style="min-width:100px;">商流</th>
	<th style="min-width:50px;">雇用形態</th>
	<th style="min-width:90px;">稼働日</th>
	<th style="min-width:110px;">最寄駅</th>
	<th style="min-width:200px;">提案元企業</th>
	<th style="min-width:200px;">保有スキル</th>	
	<th style="min-width:80px;">希望精算幅</th>
	<th style="min-width:50px;">希望単価</th>
	<th style="min-width:200px;">備考</th>
	<th style="min-width:40px;">契約<br/>登録</th>	
	<th>&nbsp;</th>
</tr>
<?php
$num = 1;
foreach ($datas as $data):
	$click = sprintf(' onclick="editHumanResource(%d);"', $data['HumanResource']['id']);
	$id = $data['HumanResource']['id'];
	$style="";
	if ($data['HumanResource']['status']) {
		$style = "background-color: #f2dede;";
	}
?>
<tr style="<?php echo $style; ?>">
	<td <?php echo $click;?>><?php echo h($num++)?></td>
	<td class="center">
		<?php echo $this->Form->button("メール",array("class"=>"btn  btn-success btn-sm", 'type'=>'button', 'onclick'=>"mail_format($id)"));?>
	</td>
	<td <?php echo $click;?>><?php echo h($data['HumanResource']['name'])?></td>
	<td <?php echo $click;?>><?php echo h($data['HumanResource']['birthday'])?></td>
	<td class="center" <?php echo $click;?>><?php echo h($data['HumanResource']['age']."歳")?></td>
	<td class="center" <?php echo $click;?>>
		<?php
			$items = Configure::read("sex");
			echo h($items[$data['HumanResource']['sex']]);
		?>
	</td>
	<td <?php echo $click;?>><?php echo h($data['HumanResource']['distribute'])?></td>
	<td class="center" <?php echo $click;?>>
		<?php
			$items = Configure::read("employment_type");
			if ($data['HumanResource']['employment_type'] !="") {
				echo h($items[$data['HumanResource']['employment_type']]);
			}
		?>
	</td>
	<td <?php echo $click;?>><?php echo h($data['HumanResource']['term_s'])?></td>
	<td <?php echo $click;?>><?php echo h($data['HumanResource']['station'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Company']['name'])?></td>
	<td <?php echo $click;?>><?php echo h($data['HumanResource']['skill'])?></td>	
	<td class="center" <?php echo $click;?>>
		<?php
			$items = Configure::read("maxmin_time");
			$val = "";
			if ($data['HumanResource']['time_from_id'] != "") {
				$val .= $items[$data['HumanResource']['time_from_id']];
			} else {
				$val .= $data['HumanResource']['time_from'];
			}
			$val .= "~";

			if ($data['HumanResource']['time_to_id'] != "") {
				$val .= $items[$data['HumanResource']['time_to_id']];
			} else {
				$val .= $data['HumanResource']['time_to'];
			}
			echo h($val);
		?>
	</td>
	<td class="center" <?php echo $click;?>><?php echo h($data['HumanResource']['price'])?></td>
	<td <?php echo $click;?>><?php echo h($data['HumanResource']['remark'])?></td>
	<td class="center">	
		<a href="/s/contracts/edit/H/<?php echo $data['HumanResource']['id'];?>">
		<i class="glyphicon glyphicon-plus-sign"></i></a>
	</td>
	<td class="center">
		<a href="javascript:void(0);" onclick="deleteHumanResource(<?php echo $data['HumanResource']["id"];?>);">
		<i class="glyphicon glyphicon-remove-circle"></i></a>
	</td>
</tr>
<?php endforeach; ?>
</table>

<?php echo $this->Element("a_pagination", array('count'=>true));?>

