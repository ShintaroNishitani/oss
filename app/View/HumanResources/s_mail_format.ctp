
■氏名：<?php echo nl2br($data['HumanResource']['name'])?></br>
■年齢：<?php echo nl2br($data['HumanResource']['age']."歳")?></br>
■性別：<?php
        $items = Configure::read("sex");
        echo nl2br($items[$data['HumanResource']['sex']]);
      ?></br>
■稼働日：<?php echo nl2br($data['HumanResource']['term_s']."～")?></br>
■最寄駅：<?php echo nl2br($data['HumanResource']['station'])?></br>
■希望精算幅：
<?php
	$items = Configure::read("maxmin_time");
	$val = "";
	if ($data['HumanResource']['time_from_id'] != "") {
		$val .= $items[$data['HumanResource']['time_from_id']];
	} else {
		$val .= $data['HumanResource']['time_from'];
	}
	$val .= "~";

	if ($data['HumanResource']['time_to_id'] != "") {
		$val .= $items[$data['HumanResource']['time_to_id']];
	} else {
		$val .= $data['HumanResource']['time_to'];
	}
	echo h($val);
?></br>
■希望単価：<?php echo nl2br($data['HumanResource']['price'])?></br>
■備考：<?php echo nl2br($data['HumanResource']['remark'])?></br>

