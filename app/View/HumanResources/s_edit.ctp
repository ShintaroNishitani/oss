<script>
    $(document).ready(function(){
        loadFileList();
    });

    function loadFileList(){
        <?php if ($this->data) : ?>
            url = '/a/file_lists/list/human_resources/'+ <?php echo $this->data['HumanResource']['id']; ?>;
            $('#FileList').load(url);
        <?php endif; ?>
    }
</script>
<blockquote><p><?php echo h('人材登録');?></p></blockquote>
<p class="guide"><?php echo '人材情報を入力してください。' ?></p>

<div style="float:left;width:700px">

<?php echo $this->Form->create('HumanResource', array('url' => array( 'action' => 'update'), "onsubmit" => "return updateHumanResource();"))?>
<?php echo $this->Element("human_resource_edit", array('search'=>false));?>
<div class="center">
	<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
</div>

<?php echo $this->Form->end();?>

</div>

<div style="float:right;width:300px">
<!-- 関連ファイル -->
<div id="HumanResourceFile"><div id="FileList"></div></div>
</div>
