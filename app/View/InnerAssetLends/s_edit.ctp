<script>
    $(function(){
        var columns = ['#InnerAssetLendRentDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
        var columns = ['#InnerAssetLendRentStartDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
        var columns = ['#InnerAssetLendRentEndDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
        var columns = ['#InnerAssetLendReturnDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });        
    });

</script>

<blockquote>
<p><?php echo $this->Html->image("asset_lend.png")?> 社内資産貸出登録</p>
</blockquote>

<?php echo $this->Form->create("InnerAssetLend", array("name"=>"InnerAssetLend", "url"=>array("action" => "update"),
                                                        "onsubmit" => "return s_update();"))?>

<?php echo $this->Form->hidden("InnerAssetLend.id")?>
<?php echo $this->Form->hidden("InnerAssetLend.status")?>
<?php echo $this->Form->hidden("InnerAssetLend.enable", array("value"=>1))?>

<table class="form">
<tr>
	<th>区分*</th>
	<td><?php echo $this->Form->select("InnerAssetLend.division", Configure::read("loan_kbn"), array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
	<th>管理責任者*</th>
	<td><?php echo $this->Form->select("InnerAssetLend.lend_manager_id", $staffs, array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_M))?></td>
</tr>
<tr>
	<th>使用者*</th>
	<td><?php echo $this->Form->select("InnerAssetLend.rent_staff_id", $staffs, array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<th colspan=2>借受情報</th>
<tr>
	<th>借受日*</th>
	<td><?php echo $this->Form->text("InnerAssetLend.rent_date", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
	<th>借受期間開始日*</th>
	<td><?php echo $this->Form->text("InnerAssetLend.rent_start_date", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
	<th>借受期間終了日*</th>
	<td>
		<div style="display:inline-flex">
			<?php echo $this->Form->text("InnerAssetLend.rent_end_date", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?>
			<?php echo $this->Form->checkbox('InnerAssetLend.auto_extend', array('label'=>'true', 'options' => Array("1"=>""))); ?>自動延長
		</div>
	</td>
</tr>
<tr>
	<th>借受物件*</th>
	<td><?php echo $this->Form->text("InnerAssetLend.rent_object", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_L));?></td>
</tr>
<tr>
	<th>使用目的*</th>
	<td><?php echo $this->Form->text("InnerAssetLend.rent_purpose", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_L));?></td>
</tr>
<tr>
	<th>借受方法*</th>
	<td><?php echo $this->Form->select("InnerAssetLend.rent_approach", Configure::read("loan_type"), array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_S,));?></td>
</tr>
<tr>
	<th>借受移送手段*</th>
	<td><?php echo $this->Form->text("InnerAssetLend.delivery_approach", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
	<th>保管場所*</th>
	<td><?php echo $this->Form->text("InnerAssetLend.storage_area", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_L));?></td>
</tr>
<tr>
	<th>返却要否*</th>
	<td><?php echo $this->Form->select("InnerAssetLend.check_return", Configure::read("return_type"), array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
	<th>返却日</th>
	<td><?php echo $this->Form->text("InnerAssetLend.return_date", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
	<th>返却状態*</th>
	<td><?php echo $this->Form->select("InnerAssetLend.status", Configure::read("already_return"), array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
	<td class="center" colspan=2>
        <button type="button" onclick="history.back()" class="btn btn-info">
        	<i class="glyphicon glyphicon-circle-arrow-left"></i> 戻る
        </button>
 		<button type="submit" class="btn btn-primary">
 			<i class="glyphicon glyphicon-send"></i> 登録
 		</button>
    </td>
</tr>
</table>

<?php echo $this->Form->end();?>