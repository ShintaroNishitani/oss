<script>
    function editAssetLends(id){
        if (id == undefined) {
            id = null;
        }
        var url = '/s/inner_asset_lends/edit/' + id ;
        window.location.href = url;
        return false;
    }
    function delAssetLends(id){
        if(window.confirm('削除しますがよろしいですか？')){
	        var url = '/s/inner_asset_lends/delete/' + id ;
            location.replace(url);
        }
        return false;
    }


    $(document).ready(function() {
	});


</script>
<blockquote><p><?php echo $this->Html->image("asset_lend.png")?> 社内資産貸出台帳</p></blockquote>

<?php echo $this->Form->create("InnerAssetLend", array('url' => array('action' => 'index'), 'type'=>'get'))?>
<table class="table-condensed well">
	<tr>
        <th style="min-width:50px;">借用者</th>
        <td>
            <?php echo  $this->Form->select("InnerAssetLend.request_staff_id", $staffs, array("class" => "form-control input-sm", "value"=>$keys['request_staff_id']));?>
        </td>
	</tr>
	<tr>
		<th style="min-width:50px;">返却済み表示</th>
		<td colspan=3> <?php echo $this->Form->checkbox("InnerAssetLend.include_already_return", array( "checked"=>($keys['include_already_return'] == "1") ? true : false )); ?> </td>
	</tr>	
	<tr>
		<th style="min-width:50px;">期間切れのみ表示</th>
		<td colspan=3> <?php echo $this->Form->checkbox("InnerAssetLend.include_out_range", array( "checked"=>($keys['include_out_range'] == "1") ? true : false )); ?> </td>
	</tr>	
	<tr>
		<td class="center" colspan="4">
			<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> 検索</button>
			<!-- <button type="submit" class="btn btn-primary" name="csv"><i class="glyphicon glyphicon-download"></i> CSV出力</button> -->
		</td>
	</tr>
</table>
<?php echo $this->Form->end()?>


<div>
	<a href="/s/inner_asset_lends/edit/"><i class="glyphicon glyphicon-plus-sign"></i> 新規追加</a>	
</div>
<div>
	<table class="table-bordered table-condensed table-striped table-hover">
		<tr>
	        <th style="min-width:30px;">No</th>
	        <th style="min-width:120px;">管理責任者</th>
        	<th style="min-width:60px;">区分</th>
         	<th style="min-width:120px;">借受日</th>	
	        <th style="min-width:120px;">使用者</th>
	        <th style="min-width:180px;">借受期間</th>
	        <th style="min-width:300px;">借受物件</th>
	        <th style="min-width:320px;">借受目的</th>
			<th style="min-width:80px;">借受方法</th>
			<th style="min-width:80px;">借受移送手段</th>
			<th style="min-width:200px;">保管場所</th>
			<th style="min-width:60px;">返却要否</th>
			<th style="min-width:60px;">返却状態</th>
         	<th style="min-width:120px;">返却日</th>
			<th style="min-width:20px;"></th>
	    </tr>
		<tbody id="mainTbody">
			<?php
				foreach ($datas as $key => $value) :
					$click       = "";
					if(($self == $value["InnerAssetLend"]["rent_staff_id"]) || ($self == $value["InnerAssetLend"]["lend_manager_id"]))
					{
						$click   = sprintf(' onclick="editAssetLends(%d);"',$value["InnerAssetLend"]["id"]);
					}
					$loan_kbn    = Configure::read("loan_kbn");
					$loan_type   = Configure::read("loan_type");
					$return_type = Configure::read("return_type");
					$already     = Configure::read("already_return");

			?>
			<tr>
				<td<?php echo $click;?>><?php echo $value["InnerAssetLend"]["id"];?></td>
				<td<?php echo $click;?>><?php echo $value["ManagerName"]["name"];?></td>
				<td<?php echo $click;?>><?php echo $loan_kbn[$value["InnerAssetLend"]["division"]];?></td>
				<td<?php echo $click;?>><?php echo $value["InnerAssetLend"]["rent_date"];?></td>
				<td<?php echo $click;?>><?php echo $value["RecieverName"]["name"];?></td>
				<td<?php echo $click;?>>
					<?php echo $value["InnerAssetLend"]["rent_start_date"];?> ～ 
					<?php echo $value["InnerAssetLend"]["rent_end_date"];?>
					<?php if(1 == $value["InnerAssetLend"]["auto_extend"]) echo ("<br>(自動延長有)");?>
				</td>
				<td<?php echo $click;?>><?php echo $value["InnerAssetLend"]["rent_object"];?></td>
				<td<?php echo $click;?>><?php echo $value["InnerAssetLend"]["rent_purpose"];?></td>
				<td<?php echo $click;?>><?php echo $loan_type[$value["InnerAssetLend"]["rent_approach"]];?></td>
				<td<?php echo $click;?>><?php echo $value["InnerAssetLend"]["delivery_approach"];?></td>
				<td<?php echo $click;?>><?php echo $value["InnerAssetLend"]["storage_area"];?></td>
				<td<?php echo $click;?>><?php echo $return_type[$value["InnerAssetLend"]["check_return"]];?></td>
				<td<?php echo $click;?>>
					<?php 
						$sts = isSet($already[$value["InnerAssetLend"]["status"]]) ? $value["InnerAssetLend"]["status"] : 0;
						echo $already[$sts];
					?>
				</td>
				<td<?php echo $click;?>><?php echo $value["InnerAssetLend"]["return_date"];?></td>
				<td class="center">
					<?php if(($self == $value["InnerAssetLend"]["rent_staff_id"]) || ($self == $value["InnerAssetLend"]["lend_manager_id"])){ ?>
        				<a href="javascript:void(0);" onclick="delAssetLends(<?php echo $value["InnerAssetLend"]["id"];?>);">
			        	<i class="glyphicon glyphicon-remove-circle"></i></a>
			        <?php } ?>
			    </td>
			</tr>
		    <?php
		    	endforeach;
	    	?>
		</tbody>
	</table>
    <div class="paginator">
    <ul class="pagination">
        <?php echo $this->Element("a_pagination", array('count'=>true));?>
    </ul>
    </div>
</div>

