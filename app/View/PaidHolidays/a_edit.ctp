<script>
    $(function(){
        var columns = ['#PaidholidayDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });   
    });

</script>

<blockquote><p><?php echo '有休登録';?></p></blockquote>
<p class="text-info"><?php echo '有休情報を入力してください。';?></p>

<?php echo $this->Form->create('PaidHoliday', array("name"=>"PaidHoliday", "action" => "update", "onsubmit" => "return updatePaidHoliday();"))?>
<?php echo $this->Form->hidden("PaidHoliday.staff_id")?>
<?php echo $this->Form->hidden("PaidHoliday.id")?>
<?php echo $this->Form->hidden("PaidHoliday.enable", array("value"=>1))?>

<table class="form">
<tr>
    <th>社員名*</th>
    <td><?php echo  $this->Form->select("PaidHoliday.staff_id", $staffs, array("class" => "form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>有休残日数*</th>
    <td><?php echo  $this->Form->text("PaidHoliday.holiday_remain", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
    <th>半休残数*</th>
    <td><?php echo  $this->Form->text("PaidHoliday.half_remain", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
    <th>有休更新日</th>
    <td><?php echo h($refresh_date); ?></td>
</tr>
<tr>
    <td colspan="2" class="center">
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>        
</table>

<?php echo $this->Form->end();?>