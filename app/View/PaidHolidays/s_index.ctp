<script>
    $(function(){
        var columns = ['#OrderScDateB', '#OrderScDateE'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });   

        $('#PaidHolidaySelectYear').change(function ()
        {
            var data = $('#PaidHolidaySelectYear').val ();
            var datas = data.split (" ");
            location.href = "<?php echo $this->Html->url (array ("controller" => "paid_holidays"));?>/index/" + datas[0];
        });
    });

    /**
     * [editPaidHoliday description]
     * @param  {[type]} id [description]
     * @return {[type]}    [description]
     */
    function editPaidHoliday(id){
        var url = '/a/paid_holidays/edit/' + id;
        loadBaseWindow(url);
        return false;
    }

    /**
     * [deletePaidHoliday description]
     * @param  {[type]} id [description]
     * @return {[type]}    [description]
     */
    function deletePaidHoliday(id){
        if(window.confirm('削除しますがよろしいですか？')){
            var url = "/a/paid_holidays/delete/" + id;
            location.replace(url);
        }
    }    
</script>


<blockquote><p><?php echo $this->Html->image("paid_holiday.png")?><?php echo ' 有休情報';?></p></blockquote>

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
    <th style="min-width:90px;">社員番号</th>
    <th style="min-width:100px;">社員名</th>
    <th style="min-width:90px;">有休残日数</th>
    <th style="min-width:90px;">半休残数</th>
    <th style="min-width:160px;">次の3月で消滅する有休日数</th>
</tr>
<tr>
    <td class="center" ><?php echo h($datas['Staff']['no'])?></td>
    <td class="center" ><?php echo h($datas['Staff']['name'])?></td>
    <td class="center" ><?php echo h($datas['PaidHoliday']['holiday_remain'])?></td>
    <td class="center" ><?php echo h($datas['PaidHoliday']['half_remain'])?></td>
    <td class="center" ><?php echo h($lost_paid)?></td>
</tr>
</table>

<br><br>
<?php if ($year == 0):?>
    <p class="guide"><?php echo "全年度 有休使用状況" ?></p>
<?php else:?>
    <p class="guide"><?php echo "$year 年度 有休使用状況" ?></p>
<?php endif;?>

<?php echo $this->Form->create ("PaidHoliday", array("action" => "index", 'type'=>'get'))?>
<table class="table-condensed well">
	<tr>
		<th style="min-width:50px;">対象年度</th>
		<td>
			<div style="display:inline-flex">
				<?php echo $this->Form->select ("PaidHoliday.select_year", $years, array ("class" => "form-control input-sm", "value"=>$year));?>
			</div>
		</td>
	</tr>
</table>
<?php echo $this->Form->end()?>

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
    <th style="min-width:30px;">No</th>
    <th style="min-width:100px;">有休使用日</th>
    <th style="min-width:100px;">区分</th>
</tr>
<?php
$num = 1;
foreach ($paid_details as $detail): ?>
<tr>
    <td class="center" ><?php echo h($num++)?></td>
    <td class="center" ><?php echo h($detail['PaidHolidayDetail']['use_date'])?></td>
    <td class="center" ><?php echo h($kind[$detail['PaidHolidayDetail']['kind']])?></td>
</tr>
<?php  endforeach; ?>
</table>
