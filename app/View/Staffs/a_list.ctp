<table class="table-bordered table-condensed table-striped table-hover">
    <tr>
        <th style="min-width:60px;">社員番号</th>
        <th style="min-width:200px;">名前</th>
        <th style="min-width:200px;">部署</th>
        <th style="min-width:150px;">権限</th>
        <th style="min-width:280px;">メール</th>
        <th style="min-width:100px;">入社日</th>
        <th style="min-width:100px;">在籍期間</th>
        <th style="min-width:100px;">退職日</th>
        <th style="min-width:100px;">LineWorks</th>
        <th></th>
    </tr>
    <?php
        $num = 1;
        foreach ($datas as $data):
            $click = sprintf(' onclick="editStaff(%d);"', $data['Staff']['id']);
            $end = date("Y/m/d");
            if ($data['Staff']['retire_date']) {
                $end = $data['Staff']['retire_date'];
            }

            $from = strtotime($data['Staff']['hire_date']);
            $to = strtotime($end);
            $diff = (date("Y", $to)*12+date("m", $to)) - (date("Y", $from)*12+date("m", $from));
            if ($data['Staff']['retire_date']) {
                $diff++;
            }
            $term = intval(($diff / 12)) .'年' . ($diff % 12) .'ヵ月';

    ?>
    <tr>
        <td class="center" <?php echo $click;?>><?php echo h($data['Staff']['no'])?></td>
        <td <?php echo $click;?>><?php echo h($data['Staff']['name'])?></td>
        <td <?php echo $click;?>><?php echo h($data['Staff']['department_id']?$departments[$data['Staff']['department_id']]:"")?></td>
        <td <?php echo $click;?>><?php echo h($auths[$data['Staff']['authority_id']])?></td>
        <td <?php echo $click;?>><?php echo h($data['Staff']['mail'])?></td>
        <td class="center" <?php echo $click;?>><?php echo h($data['Staff']['hire_date'])?></td>
        <td class="center" <?php echo $click;?>><?php echo h($term)?></td>
        <td class="center" <?php echo $click;?>><?php echo h($data['Staff']['retire_date'])?></td>
        <td class="center" <?php echo $click;?>><span class="badge badge-pill badge-success"><?php echo h($data['Staff']['line_account_id']?"連携済":"")?></span></td>
        <td class="center">
            <a href="javascript:void(0);" onclick="deleteStaff(<?php echo $data["Staff"]["id"];?>);">
                <i class="glyphicon glyphicon-remove-circle"></i></a>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
