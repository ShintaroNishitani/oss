<style>
.panel-heading {
    padding: 5px 15px;
}

.panel-footer {
	padding: 1px 15px;
	color: #A0A0A0;
}
</style>

<div class="row">
	<div class="col-sm-6 col-md-4 col-md-offset-4">
		<div class="panel panel-default">
			<div class="panel-body">
				<form role="form" action="" method="POST">
					<fieldset>
						<div class="row">
							<div class="center-block">
								<img src="/img/logo.gif" alt="..." class="img-rounded" style="width:280px; height:35px; margin-left:20px;margin-right:20px;margin-top:20px;margin-bottom:40px;">
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 col-md-10  col-md-offset-1 ">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="glyphicon glyphicon-user"></i>
										</span> 
										<?php echo $this->Form->text('Staff.mail',array("placeholder"=>"ログインID", 'class'=>'form-control'));?>
									</div>
								</div>
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="glyphicon glyphicon-lock"></i>
										</span>
										<?php echo $this->Form->password('Staff.password',array("placeholder"=>"パスワード", 'class'=>'form-control'));?>
									</div>
								</div>
								<?php echo $this->Form->checkbox("Staff.save_mail")?> ログインIDを保存する
								<div class="form-group">
									<button type="submit" class="btn btn-lg btn-primary btn-block"><i class="glyphicon glyphicon-log-in"></i> ログイン</button>
								</div>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
        </div>
	</div>
</div>
