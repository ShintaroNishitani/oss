<script type="text/javascript" charset="utf-8">

    function updatePassword(){
        $.ajax({
            type: "post",
            url: "<?php echo $this->Html->url(array('action' => 'change_password_commit'));?>",
            data: $("#StaffChangePasswordCommitForm").serializeArray(),
            success: function(html){
                var res = eval('(' + html + ')');
                if (res["error"] == 0) {
                    // unloadAll();
                    location.reload();
                }
                myAlert(res["message"]);
            }
        });
        return false;
    }
</script>

<?php echo $this->Form->create("Staff", array("action" => "change_password_commit", "onsubmit" => "return updatePassword()"))?>
<?php echo $this->Form->hidden('change_password_key', array("value" => $change_password_key))?>
<table class="form">
    <tr>
        <th>現在のパスワード</th>
        <td><?php echo $this->Form->password("Staff.password", array('class'=>'form-control input-sm'));?></td>
    </tr>
    <tr>
        <th>新しいパスワード</th>
        <td><?php echo $this->Form->password("Staff.new_password", array('class'=>'form-control input-sm'));?></td>
    </tr>
    <tr>
        <th>新しいパスワード(確認)</th>
        <td><?php echo $this->Form->password("Staff.new_password2", array('class'=>'form-control input-sm'));?></td>
    </tr>
    <tr>
        <td colspan="2" class="center">
            <?php echo $this->Form->submit("パスワードの変更", array("class"=>"btn  btn-primary"))?>
        </td>
    </tr>
</table>

<?php echo $this->Form->end();?>
