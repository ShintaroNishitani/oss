<script>
    $(function(){
        var columns = ['#StaffHireDate', '#StaffRetireDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });

    });

    function sendLineWorks(id){
        $.ajax({
            type: "get",
            url: "<?php echo $this->Html->url(array('action' => 'send_line_works'));?>"+ "/" + id,
            success: function(html){
                var res = eval('(' + html + ')');
                if (res["error"] == 0) {
                }
                myAlert(res["message"]);
            }
        });
        return false;
    }
</script>

<blockquote><p>従業員登録</p></blockquote>
<p class="guide"><?php echo '従業員情報を入力してください。' ?></p>
<?php echo $this->Form->create( 'Staff', array('url'=>array("action" => "upload"), "type" => "file", "enctype" => "multipart/form-data"));?>
<?php echo $this->Form->input( 'image', array( 'type' => 'file'));?>
<?php echo $this->Form->submit( __('Upload'));?>
<?php echo $this->Form->end();?>

<?php echo $this->Form->create('Staff', array('url'=>array("action" => "update"), "onsubmit" => "return updateStaff();"))?>
<?php echo $this->Form->hidden("Staff.id")?>
<?php echo $this->Form->hidden("Staff.enable", array("value"=>1))?>
<table class="form">
<tr>
    <th>社員番号*</th>
    <td><?php echo $this->Form->text("Staff.no", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>所属</th>
    <td><?php echo $this->Form->select('Staff.department_id', $departments, array("class"=>"form-control input-sm", 'style'=>SIZE_M, 'empty'=>""));?></td>
</tr>
<tr>
    <th>名前*</th>
    <td><?php echo $this->Form->text("Staff.name", array("empty" => false, "class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>メールアドレス(ログインID)*</th>
    <td><?php echo $this->Form->text("Staff.mail", array('class'=>'form-control input-sm', 'style'=>SIZE_L))?></td>
</tr>
<tr>
    <th>パスワード</th>
    <td><?php echo $this->Form->password("Staff.password", array('class'=>'form-control input-sm', 'style'=>SIZE_M))?></td>
</tr>
<tr>
    <th>パスワード(確認)</th>
    <td><?php echo $this->Form->password("Staff.password2", array('class'=>'form-control input-sm', 'style'=>SIZE_M))?></td>
</tr>
<tr>
    <th>LineWorksID</th>
    <td>
        <?php echo $this->Form->text("Staff.line_account_id", array('class'=>'form-control input-sm', 'style'=>SIZE_L))?>
        <?php if($id):?>
            <?php echo $this->Form->button('テスト送信', array("class"=>"btn  btn-success", 'type'=>'button', 'onclick'=>"sendLineWorks($id)"));?>
        <?php endif;?>
    </td>
</tr>
<tr>
    <th>入社日*</th>
    <td><?php echo $this->Form->text("Staff.hire_date", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
    <th>退職日</th>
    <td><?php echo $this->Form->text("Staff.retire_date", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
    <th>備考</th>
    <td><?php echo $this->Form->textarea("Staff.remark", array("empty" => false, "class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>権限*</th>
    <td><?php echo $this->Form->select('Staff.authority_id', $auths, array("class"=>"form-control input-sm", 'style'=>SIZE_L, 'empty'=>false));?></td>
</tr>
<tr>
    <th>電子印</th>
    <?php if($images): ?>
            <td><?php echo $this->html->image($images, array('width'=>'30', 'height'=>'30', 'alt' => 'CakePHP'));?></td>
        }
    <?php endif; ?>
</tr>
<tr>
    <td colspan="2" class="center">
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>
</table>
<?php echo $this->Form->end();?>
