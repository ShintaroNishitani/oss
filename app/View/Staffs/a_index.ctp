<script>
	var check = false;
    $(document).ready(function(){
        StaffList();
    });

    function StaffList(retire) {
    	if (retire == undefined || retire == false) {
    		retire = "";
    	}
        $.ajax({
            type: "get",
            url: "/a/staffs/list/" + retire,
            data: $('#StaffListForm').serialize(),
            success: function(html){
                $('#StaffList').html(html);
            }
        });
        return false;
    }

    function editStaff(id) {
        if (id == undefined) {
            id = 0;
        }
        var url = '/a/staffs/edit/' + id;
        loadBaseWindow(url);
        return false;
    }

    function updateStaff() {
        var url = "/a/staffs/update/";
        updateData(url, "StaffUpdateForm", null, function(){unloadAll();StaffList(check);});

        return false;
    }

    function deleteStaff(id) {
        if (id != undefined) {
            if (confirm('削除してもよろしいですか？\nこの操作は元に戻せません。')) {
                var url = "/a/staffs/delete/" + id + "/;?>";
                updateData(url, null, null, function(){StaffList();});
            }
        }
        return false;
    }

	$(function() {
		$('#retire_display').change(function() {
			check = $(this).is(':checked');
			StaffList(check);
		});
	});

</script>

<blockquote><p><?php echo $this->Html->image("staff.png")?> 従業員一覧　<span class="text-muted">（在籍数:<?php echo h($count);?> 名）</span></p></blockquote>

<?= $this->Form->input("retire_display", array('type' => 'checkbox', 'label'=>' '.__('退職者表示'), 'checked'=>false, 'id'=>'retire_display'));?>
<a href="javascript:void(0);" onclick="editStaff();"><i class="glyphicon glyphicon-plus-sign"></i> 新規追加</a>

<div id="StaffList">
<!-- 従業員リスト -->
</div>


