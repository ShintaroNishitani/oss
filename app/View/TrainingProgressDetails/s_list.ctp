<style>
div.child{
    margin-left:4em;
    width: 80%;
    max-width: 80%;
    min-width: 80%;
    border:1px solid #CCC;
    padding:1em;
    background:#FFF;
}
div.right{
    float:right;
}
</style>
<div class="well">
<p class="guide"><i class="glyphicon glyphicon-play"></i> <?php echo '進捗・コメント' ?></p>

<?php
$count = count($datas);
$n = 0;
foreach ($datas as $data) {
?>
<i class="glyphicon glyphicon-stop"></i>
<?php echo 'No.' . ($count - $n);?>
<div class="child">
    <?php echo nl2br($data['TrainingProgressDetail']["content"]);?>
    <div class="right">
        <strong>
        <?php echo $data['Staff']["name"];?>
        |
        <?php echo $data['TrainingProgressDetail']["modified"];?>
    </strong>
    </div>
</div>
</br>
<?php
    $n++;   
}

?>
</div>