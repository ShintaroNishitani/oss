<style>
table.form tr td textarea{
    font-size:100%;
    height:300px;
    width:600px;    
}
</style>

<blockquote><p>進捗・コメント</p></blockquote>
<p class="guide"><?php echo '進捗・コメントを入力してください。' ?></p>

<?php echo $this->Form->create('TrainingProgressDetail', array("action" => "update", "onsubmit" => "return updateTrainingProgressDetail();"))?>
<?php echo $this->Form->hidden("TrainingProgressDetail.id")?>
<?php echo $this->Form->hidden("TrainingProgressDetail.training_progress_id", array("value"=>$id));?>
<?php echo $this->Form->hidden("TrainingProgressDetail.enable", array("value"=>1))?>
<table class="form">
<tr>
    <td><?php echo $this->Form->textarea("TrainingProgressDetail.content", array("class"=>"form-control input-sm jpn"));?></td>
</tr>
<tr>    
    <td colspan="2" class="center">
    	<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>
</table>
<?php echo $this->Form->end();?>
