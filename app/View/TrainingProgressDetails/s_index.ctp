<script>

    $(document).ready(function(){
        loadTrainingProgressDetailList();
        loadFileList();
    });

    function loadFileList(){
        url = '/a/file_lists/list/training_progress/'+ <?php echo $data['TrainingProgress']['id']; ?>;
        $('#FileList').load(url);
    }

    function loadTrainingProgressDetailList(){
        var url = "/s/training_progress_details/list/"+ <?php echo $data['TrainingProgress']['id']; ?>;
        $('#TrainingProgressDetailList').load(url);
    }

    function editDetail(){
        var url = '/s/training_progress_details/edit/'+ <?php echo $data['TrainingProgress']['id']; ?>;
        loadBaseWindow(url);
    }    

    function updateTrainingProgressDetail(){
        var url = "/s/training_progress_details/update/";
        updateData(url, "TrainingProgressDetailUpdateForm", null, function(){unloadAll();loadTrainingProgressDetailList();});

        return false;
    }    
</script>

<blockquote><p>実施課題 進捗一覧</p></blockquote>
<p class="guide"><?php echo '進捗・コメントを追加する場合は[進捗・コメント]ボタンを押してください。ファイルを添付する場合は、ファイルを選択し、[アップロード]ボタンを押してください' ?></p>

<a href="/s/trainings/index/"><i class="glyphicon glyphicon-transfer"></i> 実施課題一覧に戻る</a>
&nbsp;&nbsp;&nbsp;&nbsp;
<a href="javascript:void(0);" onclick="editDetail();"><i class="glyphicon glyphicon-plus-sign"></i> 進捗・コメント追加</a>

</br></br>
<table class="table-condensed well">
    <tr>
        <th><i class="glyphicon glyphicon-stop"></i> 課題番号</th>
        <td><?php echo $data['Training']['no'];?></td>
        <th><i class="glyphicon glyphicon-stop"></i> 課題名</th>
        <td><?php echo $data['Training']['name'];?></td>
        <th><i class="glyphicon glyphicon-stop"></i> 種別</th>
        <td>
            <?php 
                $items =  Configure::read("training_type");
                echo $items[$data['Training']['type']];
            ?>
        </td>     
        <th><i class="glyphicon glyphicon-stop"></i> 対応言語</th>
        <td>
            <?php 
                if ($data['Training']['lang'] != null) {
                    $items =  Configure::read("training_lang");
                    echo $items[$data['Training']['lang']];
                }
            ?>
        </td>           
        <th><i class="glyphicon glyphicon-stop"></i> 難易度</th>
        <td>
            <?php 
                $items =  Configure::read("training_difficulty");
                echo $items[$data['Training']['difficulty']];
            ?>
        </td>               
        <th><i class="glyphicon glyphicon-stop"></i> 実施者</th>
        <td><?php echo $staffs[$data['TrainingProgress']['staff_id']];?></td>
        <th><i class="glyphicon glyphicon-stop"></i> 出題者</th>
        <td><?php echo $staffs[$data['TrainingProgress']['progress_staff_id']];?></td>
        <th><i class="glyphicon glyphicon-stop"></i> 依頼日</th>
        <td><?php echo $data['TrainingProgress']['term_s'];?></td>
        <th><i class="glyphicon glyphicon-stop"></i> 期限</th>
        <td><?php echo $data['TrainingProgress']['term_e'];?></td>                  
    </tr>     
</table>

<div id="TrainingProgressDetailList">
<!-- 実施課題進捗リスト -->
</div>

<!-- 関連ファイル -->
<div id="TrainingProgressDetailFile"><div id="FileList"></div></div>

