<script>

    function editMeeting(id){
        if (id == undefined) {
            id = 0;
        }
        var url = '/a/meetings/edit/' + id;
        loadBaseWindow(url);
        return false;
    }

    function deleteCost(id){
        if(window.confirm('削除しますがよろしいですか？')){
            var url = "/a/meetings/delete/" + id;
            location.replace(url);
        }
    }

    function beforeFileCheck(){
        var element = document.getElementById("MeetingFile");
        if("" == element.value){
            alert("ファイルが選択されていません");
            return false;
        }

        return true;
    }

</script>


<blockquote><p><?php echo $this->Html->image("meeting.png")?> 月例会日設定</p></blockquote>
<p class="guide"><?php echo '月例会日を登録してください。' ?></p>

<div class="col-sm-12 col-md-12">
	<div class="well col-sm-6 col-md-6">
		<?php
		    echo $this->Form->create("Meeting",
		                             array("style"=>"font-size:12px;",
		                                   'name'=>'input',
		                                   "action"=>"upload",
		                                   "type" =>"file",
		                                   "onsubmit"=>"return beforeFileCheck();"));

		?>
		<p class="text-info">月例会データの自動取込が出来ます </p>
		<?php echo $this->Form->input('Meeting.file', array("type"=>"file", "value"=>"", 'label'=>false, 'width'=>40,'height'=>20, "style" => "font-size:12px;"))?>
		</br>
		<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-upload"></i> アップロード</button>
		<div class="pull-right">
		<a href="/format/meeting_format.csv" download="月例会データフォーマット.csv" target="_blank"><span class="glyphicon glyphicon-file" aria-hidden="true"></span>フォーマットダウンロード</a>
		</div>
		<?php echo $this->Form->end();?>
	</div>
</div>

<a href="javascript:void(0);" onclick="editMeeting();"><i class="glyphicon glyphicon-plus-sign"></i> 新規追加</a>
&nbsp;&nbsp;&nbsp;&nbsp;

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
    <th>No</th>
    <th style="min-width:90px;">日付</th>
    <th></th>
</tr>
<?php
$no = 1;
foreach ($datas as $data):
    $click = sprintf(' onclick="editMeeting(%d);"', $data['Meeting']['id']);
?>
<tr>
    <td <?php echo $click;?>><?php echo h($no++)?></td>
    <td class="center" <?php echo $click;?>><?php echo h($data['Meeting']['date'])?></td>
    <td class="center">
        <a href="javascript:void(0);" onclick="deleteCost(<?php echo $data['Meeting']["id"];?>);">
        <i class="glyphicon glyphicon-remove-circle"></i></a>
    </td>
</tr>
<?php
endforeach;
?>
</table>

<?php echo $this->Element("a_pagination", array('count'=>true));?>

