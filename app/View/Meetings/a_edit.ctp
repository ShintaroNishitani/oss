<script>
    $(function(){
        var columns = ['#MeetingDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });   
    });    
</script>

<blockquote><p>月例会日登録</p></blockquote>
<p class="text-info">月例会日を入力してください。</p>

<?php echo $this->Form->create('Meeting', array("name"=>"Meeting", "action" => "update",
                                                        "onsubmit" => "return updateMeeting();"))?>
<?php echo $this->Form->hidden("Meeting.id")?>
<?php echo $this->Form->hidden("Meeting.enable", array("value"=>1))?>

<table class="form">
<tr>
    <th>日付*</th>
    <td><?php echo $this->Form->text("Meeting.date", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
    <td colspan="6" class="center">
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>        
</table>

<?php echo $this->Form->end();?>