<blockquote><p>プロジェクト登録</p></blockquote>
<p class="text-info">プロジェクト情報を入力してください。</p>

<?php echo $this->Form->create('Project', array("name"=>"Project", "action" => "update",
                                                        "onsubmit" => "return updateProject();"))?>
<?php echo $this->Form->hidden("Project.id")?>
<?php echo $this->Form->hidden("Project.enable", array("value"=>1))?>

<table class="form">
<tr>
    <th>名前*</th>
    <td><?php echo $this->Form->text("Project.prj_name", array("class"=>"form-control input-sm jpn", "style"=>SIZE_M, "empty"=>false));?></td>
</tr>
<tr>
    <th>プロジェクトコード*</th>
    <td><?php echo $this->Form->text("Project.prj_code", array("class"=>"form-control input-sm", "style"=>SIZE_S, "empty"=>false));?></td>
</tr>
<tr>
    <th>顧客コード</th>
    <td><?php
        echo $this->Form->select("Project.customer_id", $customers,
            array("class"=>"form-control input-sm", "style"=>SIZE_S));
    ?></td>
</tr>
<tr>
    <th>開始年*</th>
    <td><?php echo $this->Form->text("Project.s_year", array("class"=>"form-control input-sm", "style"=>SIZE_S, "empty"=>false));?></td>
</tr>
<tr>
    <th>開始月*</th>
    <td><?php echo $this->Form->text("Project.s_month", array("class"=>"form-control input-sm", "style"=>SIZE_S, "empty"=>false, ));?></td>
</tr>
<tr>
    <th>終了年</th>
    <td><?php echo $this->Form->text("Project.e_year", array("class"=>"form-control input-sm", "style"=>SIZE_S, "empty"=>false));?></td>
</tr>
<tr>
    <th>終了月</th>
    <td><?php echo $this->Form->text("Project.e_month", array("class"=>"form-control input-sm", "style"=>SIZE_S, "empty"=>false, ));?></td>
</tr>
<tr>
    <td colspan="6" class="center">
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>        
</table>

<?php echo $this->Form->end();?>