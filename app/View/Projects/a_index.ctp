<script>

    /**
     * [editProject プロジェクト編集]
     * @param  {[type]} id [description]
     * @return {[type]}    [description]
     */
    function editProject(id){
        if (id == undefined) {
            id = 0;
        }
        var url = '/a/projects/edit/' + id;
        loadBaseWindow(url);
        return false;
    }

    /**
     * [deleteProject プロジェクト削除]
     * @param  {[type]} id [description]
     * @return {[type]}    [description]
     */
    function deleteProject(id){
        if(window.confirm('削除しますがよろしいですか？')){
            var url = "/a/projects/delete/" + id;
            location.replace(url);
        }
    }
</script>


<blockquote><p><?php echo $this->Html->image("project.png")?> プロジェクト管理</p></blockquote>
<p class="guide"><?php echo 'プロジェクトを登録してください。' ?></p>


<a href="javascript:void(0);" onclick="editProject();"><i class="glyphicon glyphicon-plus-sign"></i> 新規追加</a>
&nbsp;&nbsp;&nbsp;&nbsp;

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
    <th >No</th>
    <th style="min-width:200px;">名前</th>
    <th style="min-width:150px;">プロジェクトコード</th>
    <th style="min-width:90px;">顧客コード</th>
    <th style="min-width:100px;">開始</th>
    <th style="min-width:100px;">終了</th>
    <th></th>
</tr>
<?php
$no = 1;
foreach ($datas as $data):
    $click = sprintf(' onclick="editProject(%d);"', $data['Project']['id']);
?>
<tr>
    <td <?php echo $click;?>><?php echo h($no++)?></td>
    <td <?php echo $click;?>><?php echo h($data['Project']['prj_name'])?></td>
    <td <?php echo $click;?>><?php echo h($data['Project']['prj_code'])?></td>
    <td <?php echo $click;?>><?php echo h($data['Customer']['code'])?></td>
    <td class="center" <?php echo $click;?>><?php echo h($data['Project']['s_year']."年 / ".$data['Project']['s_month']."月")?></td>
    <td class="center" <?php echo $click;?>><?php
        $disp_year = "----";
        $disp_month = "--";
        if ($data['Project']['e_year'] != "") {
            $disp_year = $data['Project']['e_year'];
        }
        if ($data['Project']['e_month'] != "") {
            $disp_month = $data['Project']['e_month'];
        }
        echo h($disp_year."年 / ".$disp_month."月")
    ?></td>
    <td class="center">
        <a href="javascript:void(0);" onclick="deleteProject(<?php echo $data['Project']["id"];?>);">
        <i class="glyphicon glyphicon-remove-circle"></i></a>
    </td>
</tr>
<?php 
endforeach; 
?>
</table>

<?php echo $this->Element("a_pagination", array('count'=>true));?>

