<script>
    $(function(){
        var columns = ['#TrainingDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });

    });
</script>
<blockquote><p>課題登録</p></blockquote>
<p class="guide"><?php echo '課題情報を入力してください。' ?></p>

<?php echo $this->Form->create('Training', array("action" => "update", "onsubmit" => "return updateTraining();"))?>
<?php echo $this->Form->hidden("Training.id")?>
<?php echo $this->Form->hidden("Training.enable", array("value"=>1))?>
<?php echo $this->Form->hidden("Training.no")?>
<table class="form">
<?php
    $disabled = false;
    if($this->data):
        $disabled = true;
    endif;
?>
<tr>
    <th>種別*</th>
    <td>
        <?php
		    echo  $this->Form->select("Training.type", Configure::read("training_type"), array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M, 'disabled'=>$disabled));
		 ?>
    </td>
</tr>
<tr>
    <th>課題名*</th>
    <td><?php echo $this->Form->text("Training.name", array("class"=>"form-control input-sm jpn", 'style'=>SIZE_L));?></td>
</tr>
<tr>
    <th>作成者*</th>
    <td>
        <?php
            echo  $this->Form->select("Training.staff_id", $staffs, array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M, 'default'=>$me));
         ?>
    </td>
</tr>
<tr>
    <th>作成日*</th>
    <td><?php echo $this->Form->text("Training.date", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S, 'default'=>$today));?></td>
</tr>
<tr>
    <th>対応言語</th>
    <td>
        <?php
            echo  $this->Form->select("Training.lang", Configure::read("training_lang"), array("class"=>"form-control input-sm", 'style'=>SIZE_M,));
         ?>
    </td>
</tr>
<tr>
    <th>その他習得技術</th>
    <td><?php echo $this->Form->text("Training.etc", array("class"=>"form-control input-sm jpn", 'style'=>SIZE_L));?></td>
</tr>
<tr>
    <th>難易度*</th>
    <td>
        <?php
            echo  $this->Form->select("Training.difficulty", Configure::read("training_difficulty"), array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M,));
         ?>
    </td>
</tr>
<tr>
    <td colspan="2" class="center">
    	<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>
</table>
<?php echo $this->Form->end();?>
