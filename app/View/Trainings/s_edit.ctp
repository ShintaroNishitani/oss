<style>
table.form tr td textarea{
    font-size:100%;
    height:200px;
    width:600px;
}
</style>
<script>
    $(function(){
        var columns = ['#TrainingProgressTermS', '#TrainingProgressTermE', '#TrainingProgressExeDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });

    });
</script>
<blockquote><p>課題登録</p></blockquote>
<p class="guide"><?php echo '課題情報を入力してください。' ?></p>

<?php echo $this->Form->create('Training', array("action" => "update", "onsubmit" => "return updateTrainingProgress();"))?>
<?php echo $this->Form->hidden("TrainingProgress.id")?>
<?php echo $this->Form->hidden("TrainingProgress.training_id", array("value"=>$id));?>
<?php echo $this->Form->hidden("TrainingProgress.enable", array("value"=>1))?>
<table class="form">
<tr>
    <th>課題番号</th>
    <td><?php echo $training['Training']['no'];?></td>
</tr>
<tr>
    <th>課題名</th>
    <td><?php echo $training['Training']['name'];?></td>
</tr>
<tr>
    <th>出題者*</th>
    <td>
        <?php
            echo  $this->Form->select("TrainingProgress.progress_staff_id", $staffs, array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M, 'default'=>$me));
         ?>
    </td>
</tr>
<tr>
    <th>実施者*</th>
    <td>
        <?php
            echo  $this->Form->select("TrainingProgress.staff_id", $staffs, array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M,));
         ?>
    </td>
</tr>
<tr>
    <th style="min-width:90px;">課題依頼日*</th>
    <td><?php echo $this->Form->text("TrainingProgress.term_s", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S, 'default'=>$today));?></td>
</tr>
<tr>
    <th>期限*</th>
    <td><?php echo $this->Form->text("TrainingProgress.term_e", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
    <th>終了日</th>
    <td><?php echo $this->Form->text("TrainingProgress.exe_date", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
    <th>評価ランク</th>
    <td>
        <?php
		    echo  $this->Form->select("TrainingProgress.rank", Configure::read("training_rank"), array("class"=>"form-control input-sm", 'style'=>SIZE_M,));
		 ?>
    </td>
</tr>
<tr>
    <th>評価</br>コメント</th>
    <td><?php echo $this->Form->textarea("TrainingProgress.remark", array("class"=>"form-control input-sm jpn"));?></td>
</tr>
<tr>
    <td colspan="2" class="center">
    	<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>
</table>
<?php echo $this->Form->end();?>
