<script>

    $(function(){
        $('#TrainingProgressScStaffId').autocomplete({
            source: '/a/staffs/autocomplete_list/Staff',
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#TrainingProgressScStaffId').html(ui.item.details);
            }}            
        });    
    });

    $(document).ready(function(){
        TrainingProgressList();
    });

    function TrainingProgressList() {
        $.ajax({
            type: "get",
            url: "<?php echo $this->Html->url(array('action' => 'list'));?>",
            data: $('#TrainingProgressListForm').serialize(),
            success: function(html){
                $('#TrainingProgressList').html(html);
            }
        });
        return false;
    }

    function editTrainingProgress(id, progress_id) {
        var url = "/s/trainings/edit/" + id + "/" + progress_id;
        loadBaseWindow(url);       
    }

    function deleteTrainingProgress(id) {
        if (id != undefined) {
            if (confirm('削除してもよろしいですか？\nこの操作は元に戻せません。')) {
                var url = "/s/trainings/delete/" + id + "/;?>";
                updateData(url, null, null, function(){TrainingProgressList();});
                location.href = "/s/trainings/index/";
            }
        }
        return false;
    }
</script>

<blockquote><p><?php echo $this->Html->image("study.png")?> 実施課題一覧</p></blockquote>
<p class="guide"><?php echo '実施した課題の一覧が表示されます。評価を行う場合は各行を選択してください。進捗を入力する場合は[進捗]ボタンを押してください' ?></p>

<?php echo $this->Form->create("TrainingProgress", array("action" => "list", "onsubmit" => "return TrainingProgressList()"))?>

<table class="table-condensed well">
    <tr>
        <th>課題番号</th>
        <td><?php echo $this->Form->text("TrainingProgress.sc_no", array("class" => "form-control input-sm", 'style'=>SIZE_S));?></td>
        <th>種別</th>
        <td>
            <?php 
                echo  $this->Form->select("TrainingProgress.sc_type", Configure::read("training_type"), array("class" => "form-control input-sm"));
            ?>
        </td>    
        <th>課題名</th>
        <td><?php echo $this->Form->text("TrainingProgress.sc_name", array("class" => "form-control input-sm jpn", 'style'=>SIZE_M));?></td>   
        <th>実施者</th>
        <td><?php echo $this->Form->text("TrainingProgress.sc_staff_id", array("class" => "form-control input-sm jpn", 'style'=>SIZE_M));?></td>
        <td>
            <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> 検索</button>
        </td>                     
    </tr>     
</table>
<?php echo $this->Form->end()?>

<div id="TrainingProgressList">
<!-- 実施課題リスト -->
</div>


