<script>
    function progressTraining(id){
        var url = "/s/training_progress_details/index/" + id;
        location.href = url;
    }
</script>
<p class="description"><?php echo '※期限を過ぎている実施課題は赤で色付けされます' ?></p>
<table class="table-bordered table-condensed table-striped table-hover">
    <tr>
        <th>No</th>
        <th>進捗</th>
        <th style="min-width:60px;">課題番号</th>
        <th style="min-width:100px;">種別</th>
        <th style="min-width:280px;">課題名</th>
        <th style="min-width:100px;">対応言語</th>
        <th style="min-width:50px;">難易度</th>
        <th style="min-width:100px;">出題者</th>
        <th style="min-width:100px;">実施者</th>
        <th style="min-width:90px;">課題依頼日</th>
        <th style="min-width:90px;">期限</th>
        <th style="min-width:90px;">終了日</th>
        <th style="min-width:50px;">評価</br>ランク</th>
        <th style="min-width:200px;">評価コメント</th>
        <th style="min-width:60px;"></th>
    </tr>
    <?php
        $num = 1;
        foreach ($datas as $data):
            $p_id = $data['TrainingProgress']['id'];
            $click = sprintf(' onclick="editTrainingProgress(%d, %d);"', $data['Training']['id'], $data['TrainingProgress']['id']);

            // 期限を過ぎていて完了していない課題は色をかえる
            $style="";
            if (empty($data['TrainingProgress']['exe_date']) && $data['TrainingProgress']['term_e'] < date('Y-m-d')) {
                $style = "background-color: #f2dede;";
            }
    ?>
    <tr style="<?php echo $style; ?>">
        <td <?php echo $click;?>><?php echo h($num++)?></td>
        <td class="center">
            <?php echo $this->Form->button("進捗",array("class"=>"btn  btn-success", 'type'=>'button', 'onclick'=>"progressTraining($p_id)"));?>
        </td>
        <td class="center" <?php echo $click;?>><?php echo h($data['Training']['no'])?></td>
        <td <?php echo $click;?>>
            <?php
                $items = Configure::read("training_type");
                echo h($items[$data['Training']['type']]);
            ?>
        </td>
        <td <?php echo $click;?>><?php echo h($data['Training']['name'])?></td>
        <td <?php echo $click;?>>
            <?php
                if ($data['Training']['lang'] != null) {
                    $items = Configure::read("training_lang");
                    echo h($items[$data['Training']['lang']]);
                }
            ?>
        </td>
        <td class="center" <?php echo $click;?>>
            <?php
                $items = Configure::read("training_difficulty");
                echo h($items[$data['Training']['difficulty']]);
            ?>
        </td>
        <td <?php echo $click;?>><?php echo h($staffs[$data['TrainingProgress']['progress_staff_id']])?></td>
        <td <?php echo $click;?>><?php echo h($staffs[$data['TrainingProgress']['staff_id']])?></td>
        <td class="center" <?php echo $click;?>><?php echo h($data['TrainingProgress']['term_s'])?>
        <td class="center" <?php echo $click;?>><?php echo h($data['TrainingProgress']['term_e'])?>
        <td class="center" <?php echo $click;?>><?php echo h($data['TrainingProgress']['exe_date'])?>
        <td class="center" <?php echo $click;?>>
            <?php
                if ($data['TrainingProgress']['rank'] != null) {
                    $items = Configure::read("training_rank");
                    echo h($items[$data['TrainingProgress']['rank']]);
                }
            ?>
        </td>
        <td <?php echo $click;?>><?php echo h($data['TrainingProgress']['remark'])?>
        <td class="center">
            <a href="javascript:void(0);" onclick="deleteTrainingProgress(<?php echo $data["TrainingProgress"]["id"];?>);">
                <i class="glyphicon glyphicon-remove-circle"></i></a>
        </td>
    </tr>
    <?php endforeach; ?>
</table>