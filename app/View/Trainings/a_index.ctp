<script>

    $(function(){
        $('#TrainingScName').autocomplete({
            source: '/a/trainings/autocomplete_list/Training',
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#TrainingScName').html(ui.item.details);
            }}            
        });    
    });

    $(document).ready(function(){
        TrainingList();
    });

    function TrainingList() {
        $.ajax({
            type: "get",
            url: "<?php echo $this->Html->url(array('action' => 'list'));?>",
            data: $('#TrainingListForm').serialize(),
            success: function(html){
                $('#TrainingList').html(html);
            }
        });
        return false;
    }

    function editTraining(id) {
        if (id == undefined) {
            id = 0;
        }
        var url = '/a/trainings/edit/' + "/" + id ;
        loadBaseWindow(url);
        return false;
    }

    function updateTraining() {
        var url = "/a/trainings/update/";
        updateData(url, "TrainingUpdateForm", null, function(){unloadAll();TrainingList();});

        return false;
    }

    function deleteTraining(id) {
        if (id != undefined) {
            if (confirm('削除してもよろしいですか？\nこの操作は元に戻せません。')) {
                var url = "/a/trainings/delete/" + id + "/;?>";
                updateData(url, null, null, function(){TrainingList();});
            }
        }
        return false;
    }
</script>

<blockquote><p><?php echo $this->Html->image("question.png")?> 課題一覧</p></blockquote>
<p class="guide"><?php echo '課題を追加する場合は、新規追加ボタンを押してください。課題を実施する場合は、該当の課題の実施ボタンを押してください' ?></p>

<?php echo $this->Form->create("Training", array("action" => "list", "onsubmit" => "return TrainingList()"))?>

<table class="table-condensed well">
    <tr>
        <th>課題番号</th>
        <td><?php echo $this->Form->text("Training.sc_no", array("class" => "form-control input-sm", 'style'=>SIZE_S));?></td>       
        <th>種別</th>
        <td><?php echo  $this->Form->select("Training.sc_type", Configure::read("training_type"), array("class" => "form-control input-sm"));?></td>    
        <th>課題名</th>
        <td><?php echo $this->Form->text("Training.sc_name", array("class" => "form-control input-sm jpn", 'style'=>SIZE_M));?></td>   
        <th>作成者</th>
        <td><?php echo  $this->Form->select("Training.sc_staff_id", $staffs, array("class" => "form-control input-sm"));?></td>     
        <th>対応言語</th>
        <td><?php echo  $this->Form->select("Training.sc_lang", Configure::read("training_lang"), array("class" => "form-control input-sm"));?></td>     
        <th>難易度</th>
        <td><?php echo  $this->Form->select("Training.sc_difficulty", Configure::read("training_difficulty"), array("class" => "form-control input-sm"));?></td>        
        <td>
            <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> 検索</button>
        </td>                        
    </tr>     
</table>
<?php echo $this->Form->end()?>

<a href="javascript:void(0);" onclick="editTraining();"><i class="glyphicon glyphicon-plus-sign"></i> 新規追加</a>

<div id="TrainingList">
<!-- 課題リスト -->
</div>


