<script>
    function progressTraining(id){
        var url = "/s/trainings/edit/" + id;
        loadBaseWindow(url);    
    }
</script>
<table class="table-bordered table-condensed table-striped table-hover">
    <tr>
        <th style="min-width:60px;">課題実施</th>
        <th style="min-width:60px;">課題番号</th>
        <th style="min-width:100px;">種別</th>
        <th style="min-width:280px;">課題名</th>
        <th style="min-width:100px;">作成者</th>
        <th style="min-width:90px;">作成日</th>
        <th style="min-width:100px;">対応言語</th>
        <th style="min-width:100px;">その他習得技術</th>
        <th style="min-width:50px;">難易度</th>
        <th style="min-width:280px;">対応者</th>
        <th></th>
    </tr>
    <?php
        $num = 1;
        foreach ($datas as $data):
            $id = $data['Training']['id'];
            $click = sprintf(' onclick="editTraining(%d);"', $id);
    ?>
    <tr>
        <td class="center">
            <?php echo $this->Form->button("実施",array("class"=>"btn  btn-success", 'type'=>'button', 'onclick'=>"progressTraining($id)"));?>  
        </td>        
        <td class="center" <?php echo $click;?>><?php echo h($data['Training']['no'])?></td>
        <td <?php echo $click;?>>
            <?php 
                $items = Configure::read("training_type");
                echo h($items[$data['Training']['type']]);
            ?>
        </td>
        <td <?php echo $click;?>><?php echo h($data['Training']['name'])?></td>
        <td <?php echo $click;?>><?php echo h($staffs[$data['Training']['staff_id']])?></td>
        <td class="center" <?php echo $click;?>><?php echo h($data['Training']['date'])?></td>
        <td <?php echo $click;?>>
            <?php 
                if ($data['Training']['lang'] != null) {
                    $items = Configure::read("training_lang");
                    echo h($items[$data['Training']['lang']]);
                }
            ?>
        </td>
        <td <?php echo $click;?>><?php echo h($data['Training']['etc'])?></td>
        <td class="center" <?php echo $click;?>>
            <?php 
                $items = Configure::read("training_difficulty");
                echo h($items[$data['Training']['difficulty']]);
            ?>
        </td>
        <td <?php echo $click;?>>
            <?php 
                $val = "";
                foreach ($data['TrainingProgress'] as $progress) {
                    $val .= ($staffs[$progress['staff_id']] . ' / ');
                }
                echo h($val);    
            ?>
        </td>
        <td class="center">
            <a href="javascript:void(0);" onclick="deleteTraining(<?php echo $data["Training"]["id"];?>);">
                <i class="glyphicon glyphicon-remove-circle"></i></a>
        </td>
    </tr>    
    <?php endforeach; ?>
</table>
