<script>
    $(function(){
        var columns = ['#AssetDocumentManageReceiptDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    });
</script>


<p><?php echo $this->Html->image("asset_lend.png")?> 資産貸出番号登録</p>
</blockquote>

<?php echo $this->Form->create("AssetDocumentManage", array("name"=>"AssetDocumentManage",
															"url"=>array("action" => "update"),
                                                        	"onsubmit" => "return s_update();"))?>

<?php echo $this->Form->hidden("AssetDocumentManage.id")?>
<table class="form">
<tr>
	<th>資産貸出番号</th>
	<td><?php echo $this->Form->text("AssetDocumentManage.asset_loan_no", array("empty"=>false, "class"=>"form-control input-sm", "ReadOnly"=>true, "style"=>SIZE_M));?></td>
</tr>
<tr>
	<th>発行日</th>
	<td><?php echo $this->Form->text("AssetDocumentManage.receipt_date", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M, 'default'=>$today));?></td>
</tr>
<tr>
	<th>プロジェクト名</th>
	<td><?php echo $this->Form->text("AssetDocumentManage.project_name", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
	<th>借用者(発行者)</th>
	<td><?php echo $this->Form->text("AssetDocumentManage.borrow_name", array("empty"=>false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>


<tr>
	<td class="center" colspan=2>
 		<button type="submit" class="btn btn-primary">
 			<i class="glyphicon glyphicon-send"></i> 登録
 		</button>

    </td>
</tr>
</table>

<?php echo $this->Form->end();?>