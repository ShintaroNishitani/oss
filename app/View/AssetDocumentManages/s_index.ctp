<script>
	$(function(){
		$('#select_year').change(function ()
		{
			var data = $('#select_year').val ();
			location.href = "<?php echo $this->Html->url (array ("controller" => "asset_document_manages"));?>/index/" + data;
		});

		
	});

    function delAssetLends(id){
        if(window.confirm('削除しますがよろしいですか？')){
	        var url = '/s/AssetDocumentManages/delete/' + id ;
            location.replace(url);
        }
        return false;
    }

	function editAssetLends(id){
			// if (id == undefined) {
			//     id = null;
			// }
			// var url = '/s/asset_lends/edit/' + id ;
			// window.location.href = url;
			// return false;
			var url = '/s/AssetDocumentManages/edit/' + id;
			loadBaseWindow(url);
			return false;
		}

	
	
    /**
     * [printAssetLends 資産管理台帳印刷]
     * @param  {[type]} id 資産管理台帳ID
     * @return {[type]}    [description]
     */
    function printAssetLends(id){
        var url = "/s/asset_lends/print/" + id;
        location.replace(url);
    }

    // $(document).ready(function() {
	// 	displayScrap(false);
	// });
    // $(function() {
	// 	$('#scrap_display').change(function() {
	// 		var check = $(this).is(':checked');
	// 		displayScrap(check);
	// 	});
	// });

	// function displayScrap (check) {
	// 	var tbody = document.getElementById("mainTbody");

	// 	for (var i = 0; i < tbody.rows.length; i++) {

	// 		var value = tbody.rows[i].cells[15].innerText;
	// 		var display = true;
	// 		console.log(value);
	// 		if (check == false && value == "済") {
	// 				display = false;
	// 		}

	// 		if (display) {
	// 			tbody.rows[i].style.display="";
	// 		} else {
	// 			tbody.rows[i].style.display="none";
	// 		}
	// 	};
	// }

</script>
<blockquote><p><?php echo $this->Html->image("asset_lend.png")?> 資産貸出番号発行</p></blockquote>
<div>
	<a href="javascript:void(0);" onclick="editAssetLends();"><i class="glyphicon glyphicon-plus-sign"></i> 新規追加</a>
	<table class="table-condensed well">
		<tr>
			<th style="min-width:50px;">対象年度</th>
			<td>
				<div style="display:inline-flex">
					<?php echo $this->Form->select ("select_year", $years, array ("class" => "form-control input-sm", "value"=>$year));?>
				</div>
			</td>
		</tr>
	</table>
	<table class="table-bordered table-condensed table-striped table-hover">
		<tr>
	        <th style="min-width:30px;">No</th>
	        <th style="min-width:100px;">資産貸出番号</th>
	        <th style="min-width:80px;">発行日</th>
	        <th style="min-width:150px;">プロジェクト名</th>
			<th style="min-width:80px;">借用者(発行者)</th>
			<th style="min-width:20px;"></th>
	    </tr>
		<tbody id="mainTbody">
			<?php
				$num = 1;
				foreach ($datas as $key => $value) :
					$click = sprintf('  onclick="editAssetLends(%d);"',$value["AssetDocumentManage"]["id"]);
			?>
			<tr>
			<td class="center" ><?php echo h($num++)?></td>			
				<td<?php echo $click;?>><?php echo $value["AssetDocumentManage"]["asset_loan_no"];?></td>
				<td<?php echo $click;?>><?php echo $value["AssetDocumentManage"]["receipt_date"];?></td>
				<td<?php echo $click;?>><?php echo $value["AssetDocumentManage"]["project_name"];?></td>
				<td<?php echo $click;?>><?php echo $value["AssetDocumentManage"]["borrow_name"];?></td>
				<td class="center">
        			<a href="javascript:void(0);" onclick="delAssetLends(<?php echo $value["AssetDocumentManage"]["id"];?>);">
			        <i class="glyphicon glyphicon-remove-circle"></i></a>
			    </td>
			</tr>
		    <?php
		    	endforeach;
	    	?>
		</tbody>
	</table>
    <div class="paginator">
    <ul class="pagination">
        <?php echo $this->Element("a_pagination", array('count'=>true));?>
    </ul>
    </div>
</div>