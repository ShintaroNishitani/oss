<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('jquery-ui.min');
		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('style');
		echo $this->Html->css('jquery.jOrgChart');
		echo $this->Html->css('jquery.treegrid');	//jquery.treegrid
		echo $this->Html->css('select2');

		echo $this->Html->script('jquery-1.11.3.min');
		echo $this->Html->script('jquery-ui.min');
		echo $this->Html->script('datepicker-ja');
		echo $this->Html->script('bootstrap.min');
		echo $this->Html->script('common');
        echo $this->Html->script('jquery.jOrgChart');
		echo $this->Html->script('jquery.cookie');
		echo $this->Html->script('jquery.treegrid');
		echo $this->Html->script('jquery.treegrid.bootstrap3');
		echo $this->Html->script('select2');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');

	?>
<style type="text/css" media="screen">
    input,
    textarea{
        ime-mode:inactive;
    }
    input.jpn,
    textarea.jpn{
        ime-mode:active;
    }
</style>
</head>
<body>
	<div id="container">
		<div id="myAlert"></div>
		<div id="header">
			<nav class="navbar navbar-default">
			  <div class="navbar-header">

			    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#gnavi">
			      <span class="sr-only">メニュー</span>
			      <span class="icon-bar"></span>
			      <span class="icon-bar"></span>
			      <span class="icon-bar"></span>
			    </button>
			    <a href="" class="navbar-brand"><img src="/img/logo.gif" alt="..." class="img-rounded"></a>
			  </div>

			  <div id="gnavi" class="collapse navbar-collapse">
			  	<?php if(!$change_pwd):?>
			    <ul class="nav navbar-nav">

					<li><a href="/s/tops/"><?php echo $this->Html->image("home.png")?> トップ</a></li>

					<li class="dropdown">
			          <a href="" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->Html->image("public.png")?> 共有情報<b class="caret"></b></a>
			          <ul class="dropdown-menu">
			            <li><a href="/a/schedules/calendar"><?php echo $this->Html->image("calendar.png")?> スケジュール</a></li>
			            <li><a href="/s/boards/index"><?php echo $this->Html->image("board.png")?> 掲示板</a></li>
			            <li><a href="/a/staffs/coming_soon"><?php echo $this->Html->image("folder.png")?> 共有フォルダ <?php echo $this->Html->image("coming.png")?></a></li>
			          </ul>
			        </li>

					<li class="dropdown">
			          <a href="" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->Html->image("send.png")?> 勤怠情報<b class="caret"></b></a>
			          <ul class="dropdown-menu">
			            <li><a href="/s/kintais/"><?php echo $this->Html->image("watch.png")?> 勤怠</a></li>
			            <li class="divider"></li>
			            <li><a href="/s/paid_Holidays"><?php echo $this->Html->image("paid_holiday.png")?> 有休情報</a></li>
			            <li><a href="/s/trans_Holidays"><?php echo $this->Html->image("trans_holiday.png")?> 振休情報</a></li>
			            <li><a href="/s/special_Holidays"><?php echo $this->Html->image("special_holiday.png")?> 特休情報</a></li>
			            <li class="divider"></li>
			            <li><a href="/a/kintais/browse"><?php echo $this->Html->image("watch.png")?> 勤怠閲覧</a></li>
			            <li><a href="/a/kintais/aggregation"><?php echo $this->Html->image("graph.png")?> 勤怠集計</a></li>
			          </ul>
			        </li>
					<li class="dropdown">
			          <a href="" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->Html->image("send.png")?> 各種届出<b class="caret"></b></a>
			          <ul class="dropdown-menu">
						<li><a href="/s/apply_holidays"><?php echo $this->Html->image("paid_holiday.png")?> 休出・休暇申請</a></li>
			            <li><a href="/s/transports/"><?php echo $this->Html->image("train.png")?> 交通費精算</a></li>
			            <li><a href="/s/costs/"><?php echo $this->Html->image("cost.png")?> 経費精算</a></li>
			            <li><a href="/a/staffs/coming_soon"><?php echo $this->Html->image("travel.png")?> 旅費精算 <?php echo $this->Html->image("coming.png")?></a></li>
			            <li><a href="/s/transports/template"><?php echo $this->Html->image("train.png")?> 交通費テンプレート登録</a></li>
			            <li><a href="/s/asset_document_manages"><?php echo $this->Html->image("order.png")?> 資産管理番号発番</a></li>
			          </ul>
			        </li>
					<li class="dropdown">
			          <a href="" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->Html->image("sale.png")?> 営業関連<b class="caret"></b></a>
			          <ul class="dropdown-menu">
			          	<li><a href="/s/orders/"><?php echo $this->Html->image("order.png")?> 見積請求書番号管理</a></li>
			            <li><a href="/a/staffs/coming_soon"><?php echo $this->Html->image("past.png")?> 業務経歴書 <?php echo $this->Html->image("coming.png")?></a></li>
			            <li><a href="/s/sale_projects/"><?php echo $this->Html->image("job.png")?> 営業管理</a></li>
			          </ul>
			        </li>
					<li class="dropdown">
			          <a href="" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->Html->image("kadai.png")?> 課題管理<b class="caret"></b></a>
			          <ul class="dropdown-menu">
			            <li><a href="/s/trainings/index"><?php echo $this->Html->image("study.png")?> 実施課題一覧</a></li>
			            <li><a href="/a/trainings"><?php echo $this->Html->image("question.png")?> 課題一覧</a></li>
			          </ul>
			        </li>
					<li class="dropdown">
			          <a href="" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->Html->image("admin.png")?> 管理関係<b class="caret"></b></a>
			          <ul class="dropdown-menu">
			            <li><a href="/a/confirms/"><?php echo $this->Html->image("confirm.png")?> 申請一覧</a></li>
			            <li><a href="/a/staffs/"><?php echo $this->Html->image("staff.png")?> 従業員一覧</a></li>
			            <li><a href="/s/pc_manages/"><?php echo $this->Html->image("pc.png")?> PC管理 </a></li>
			            <li><a href="/s/assets/"><?php echo $this->Html->image("asset.png")?> 資産管理 </a></li>
			            <li><a href="/s/keycards/"><?php echo $this->Html->image("key.png")?> 鍵カード管理 </a></li>
			            <li><a href="/s/books/"><?php echo $this->Html->image("question.png")?> 書籍管理 </a></li>
			            <li><a href="/s/asset_lends/"><?php echo $this->Html->image("asset_lend.png")?> 資産貸出台帳 </a></li>
			            <li><a href="/s/inner_asset_lends/"><?php echo $this->Html->image("asset.png")?> 社内資産貸出管理</a></li>
			            <li><a href="/a/work_mngs/"><?php echo $this->Html->image("work_mng.png")?> 労務管理 </a></li>
			          </ul>
			        </li>
					<li class="dropdown">
			          <a href="" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->Html->image("master.png")?> マスター管理<b class="caret"></b></a>
			          <ul class="dropdown-menu">
			            <li><a href="/a/holidays/"><?php echo $this->Html->image("holiday.png")?> 休日設定</a></li>
			            <li><a href="/a/meetings/"><?php echo $this->Html->image("meeting.png")?> 月例会日設定</a></li>
			            <li><a href="/a/customers/"><?php echo $this->Html->image("customer.png")?> 顧客管理</a></li>
			            <li><a href="/a/projects/"><?php echo $this->Html->image("project.png")?> プロジェクト管理</a></li>
			            <li><a href="/a/paid_holidays"><?php echo $this->Html->image("paid.png")?> 有休管理</a></li>
						<li><a href="/a/trans_holidays"><?php echo $this->Html->image("trans_holiday.png")?> 振休管理</a></li>
						<li><a href="/a/special_holidays"><?php echo $this->Html->image("special_holiday.png")?> 特休管理</a></li>
			          </ul>
			        </li>
			        
					<li class="dropdown">
			          <a href="" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->Html->image("kadai.png")?> 規定閲覧<b class="caret"></b></a>
			          <ul class="dropdown-menu">
			            <li><a href="/s/rules/rulelist"><?php echo $this->Html->image("study.png")?> 規定一覧</a></li>
			          </ul>
			        </li>
			    </ul>
				<?php endif;?>
			    <ul class="nav navbar-nav navbar-right">
					<li class="dropdown ">
			          <a href="" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->Html->image("acc.png")?> <?php echo $app_name;?> <b class="caret"></b></a>
			          <ul class="dropdown-menu" >
						<li><a href="javascript:void(0);" onclick="loadBaseWindow('<?php echo $this->Html->url(array('a'=>true, 'controller' => 'staffs', 'action' => 'change_password'));?>');"><?php echo $this->Html->image("pass.png")?> パスワードの変更</a></li>
			            <li class="divider"></li>
			            <li><a href="/a/staffs/logout"><?php echo $this->Html->image("logout.png")?> ログアウト</a></li>
			          </ul>
			        </li>
			    </ul>
			  </div>
			</nav>
		</div>
		<div id="content" style="margin-left: 15px">
			<?php if($change_pwd):?>
			  <script>
					loadBaseWindow('<?php echo $this->Html->url(array('a'=>true, 'controller' => 'staffs', 'action' => 'change_password'));?>');
			  </script>
			  <?php endif;?>
			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">


		</div>
	</div>
	<?php echo $this->element('sql_dump'); ?>
    <div id="Mask"></div>
    <div id="Overlay" onclick="unloadAll();"></div>
    <div id="BaseWindow">
        <p class="close"><img src="<?php echo $this->webroot;?>img/close.gif" onclick="unloadAll();"></p>
        <p class="clear"></p>
        <div id="BaseWindowContent">
        </div>
    </div>
    <div id="SessionContinue"></div>
</body>
</html>
