<script>

    $(function(){
        var columns = ['#AssetPurchaseDate', '#AssetDisposalDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    });


</script>

<blockquote><p>稼働実績</p></blockquote>

<?php echo $this->Form->create("Contract", array("name"=>"ContractResult", "action" => "result_update",
                                                        "onsubmit" => "return updateContractResult();"))?>
<?php
 echo $this->Form->hidden("ContractResult.id");
 echo $this->Form->hidden("ContractResult.enable", array("value"=>1));
 echo $this->Form->hidden("ContractResult.year", array("value"=>$year));
 echo $this->Form->hidden("ContractResult.month", array("value"=>$month));
 echo $this->Form->hidden("ContractResult.contract_detail_id");
 echo $this->Form->hidden("ContractResult.human_resource_id");
?>

<table class="form">
<tr>
    <th>年月</th>
    <td><?php echo h($this->data['ContractResult']['year']."年".$this->data['ContractResult']['month']."月");?></td>
</tr>
<tr>
    <th>技術者名</th>
    <td><?php echo h($this->data['HumanResource']['name']);?></td>
</tr>
<tr>
    <th>稼働時間*</th>
    <td><?php echo $this->Form->text('ContractResult.work_time', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>受注金額*</th>
    <td><?php echo $this->Form->text('ContractResult.receive_price', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>発注金額*</th>
    <td><?php echo $this->Form->text('ContractResult.order_price', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>当月単価*</th>
    <td><?php echo $this->Form->text('ContractResult.unit_price', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>控除額</th>
    <td><?php echo $this->Form->text('ContractResult.down_price', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>超過額</th>
    <td><?php echo $this->Form->text('ContractResult.up_price', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>交通費</th>
    <td><?php echo $this->Form->text('ContractResult.transport', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>経費</th>
    <td><?php echo $this->Form->text('ContractResult.cost', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>請求書</th>
    <td><?php echo $this->Form->select('ContractResult.invoice', Configure::read("invoice"), array("class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>備考</th>
    <td><?php echo $this->Form->text('ContractResult.remark', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <td colspan="2" class="center">
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>
</table>

<?php echo $this->Form->end();?>
