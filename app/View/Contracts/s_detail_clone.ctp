
<blockquote><p><?php echo h('契約複製');?></p></blockquote>
<p class="guide"><?php echo '契約情報を入力してください。' ?></p>

<?php echo $this->Form->create('Contract', array("url" => array('action'=>"clone")))?>
<?php echo $this->Form->hidden("Contract.sale_project_id")?>
<?php echo $this->Form->hidden("Contract.receive_company_id")?>
<?php echo $this->Form->hidden("Contract.order_company_id")?>
<?php echo $this->Form->hidden("ContractDetail.contract_id")?>
<?php echo $this->Form->hidden("ContractDetail.human_resource_id")?>
<?php echo $this->Form->hidden("ContractDetail.	contract_id")?>

<?php echo $this->Element("contract_edit", array('search'=>false));?>
<div class="center">
	<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
</div>
<?php echo $this->Form->end();?>
