<script>
	$(document).ready(function(){
	});


	function editContractResult(id){
		if (id == undefined) {
			id = 0;
		}
		var url = '/s/contracts/result_edit/' + id;
		loadBaseWindow(url);
		return false;
	}

	function deleteContractResult(id){
		if(window.confirm('削除しますがよろしいですか？')){
			var url = "/s/contracts/result_delete/" + id;
			location.replace(url);
		}
	}

	function mail_format(id) {
		var url = '/s/contracts/mail_format/' + id ;
		loadBaseWindow(url);
		return false;
	}

</script>


<blockquote><p><?php echo $this->Html->image("job.png")?> 精算一覧</p></blockquote>

<ul class="nav nav-tabs" role="tablist">
	<li><a href="/s/sale_projects/index/"><?php echo h('案件一覧');?></a></li>
	<li><a href="/s/human_resources/index/"><?php echo h('人材一覧');?></a></li>
	<li><a href="/s/contracts/index/"><?php echo h('契約一覧');?></a></li>
	<li><a href="/s/companies/index/"><?php echo h('企業一覧');?></a></li>
	<li class="active"><a href="/s/contracts/result_index/"><?php echo h('精算一覧');?></a></li>
</ul>

</br></br>

<p class="guide"><?php echo '月毎の契約が確認できます' ?></p>


<div class="center">
<h5>
<a href="/s/contracts/result_index/<?php echo $b_year;?>/<?php echo $b_month;?>"><i class="glyphicon glyphicon-backward"></i> 前月 </a>
<b><?php echo sprintf("%s年 %s月", $year, $month);?></b>
<a href="/s/contracts/result_index/<?php echo $n_year;?>/<?php echo $n_month;?>"> 翌月 <i class="glyphicon glyphicon-forward"></i></a>
</h5>
</div>


<a href="/s/contracts/edit/"><i class="glyphicon glyphicon-plus-sign"></i> 新規契約登録</a>

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
	<th>No</th>
	<th style="min-width:200px;">案件名</th>
	<th style="min-width:200px;">受注企業</th>
	<th style="min-width:200px;">発注企業</th>
	<th style="min-width:150px;">技術者</th>
	<th style="min-width:60px;">稼働時間</th>
	<th style="min-width:60px;">受注金額</th>
	<th style="min-width:60px;">発注金額</th>
	<th style="min-width:60px;">当月単価</th>
	<th style="min-width:60px;">控除額</th>
	<th style="min-width:60px;">超過額</th>
	<th style="min-width:60px;">交通費</th>
	<th style="min-width:60px;">経費</th>
	<th style="min-width:60px;">請求書</th>
<!-- 	<th style="min-width:60px;">入金額</th>
	<th style="min-width:60px;">振込額</th> -->
	<th></th>
</tr>
<?php
$no = 1;
foreach ($datas as $data):
	$click = sprintf(' onclick="editContractResult(%d);"', $data['ContractResult']['id']);
?>
<tr>
	<td <?php echo $click;?>><?php echo h($no++)?></td>
	<td <?php echo $click;?>><?php echo h($data['ContractDetail']['Contract']['SaleProject']['name'])?></td>
	<td <?php echo $click;?>><?php echo h($data['ContractDetail']['Contract']['ReceiveCompany']['name'])?></td>
	<td <?php echo $click;?>><?php echo h($data['ContractDetail']['Contract']['OrderCompany']['name'])?></td>
	<td <?php echo $click;?>><?php echo h($data['HumanResource']['name'])?></td>
	<td class="text-right" <?php echo $click;?>><?php echo h($data['ContractResult']['work_time'])?></td>
	<td class="text-right" <?php echo $click;?>><?php echo h(number_format($data['ContractResult']['receive_price']))?></td>
	<td class="text-right" <?php echo $click;?>><?php echo h(number_format($data['ContractResult']['order_price']))?></td>
	<td class="text-right" <?php echo $click;?>><?php echo h(number_format($data['ContractResult']['unit_price']))?></td>
	<td class="text-right" <?php echo $click;?>><?php echo h(number_format($data['ContractResult']['down_price']))?></td>
	<td class="text-right" <?php echo $click;?>><?php echo h(number_format($data['ContractResult']['up_price']))?></td>
	<td class="text-right" <?php echo $click;?>><?php echo h(number_format($data['ContractResult']['transport']))?></td>
	<td class="text-right" <?php echo $click;?>><?php echo h(number_format($data['ContractResult']['cost']))?></td>
	<td class="center" <?php echo $click;?>>
		<?php
			$items = Configure::read("invoice");
			echo h($items[$data['ContractResult']['invoice']]);
		?>
	</td>
<!-- 	<td <?php echo $click;?>><?php echo h("pending")?></td>
	<td <?php echo $click;?>><?php echo h("pending")?></td> -->
	<td class="center">
		<a href="javascript:void(0);" onclick="deleteContractResult(<?php echo $data['ContractResult']["id"];?>);">
		<i class="glyphicon glyphicon-remove-circle"></i></a>
	</td>
</tr>
<?php
endforeach;
?>
</table>

