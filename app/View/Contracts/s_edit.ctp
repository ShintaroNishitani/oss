<script>

    $(function(){
        var columns = ['#ContractTermS', '#ContractTermE', '#ContractContratDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    });

    // function searchProject(){
    //     var url = '/s/contracts/search_project/';
    //     loadBaseWindow(url);
    //     return false;
    // }
    // function searchCompany(){
    //     var url = '/s/contracts/search_company/';
    //     loadBaseWindow(url);
    //     return false;
    // }
    // function searchHuman(){
    //     var url = '/s/contracts/search_human/';
    //     loadBaseWindow(url);
    //     return false;
    // }

    function updateContract(){
        var element = document.getElementById("ContractTermS");
        if("" == element.value){
            alert("契約期間が入力されていません");
            return false;
        }
        var element = document.getElementById("ContractTermE");
        if("" == element.value){
            alert("契約期間が入力されていません");
            return false;
        }
        var element = document.getElementById("ContractContractDate");
        if("" == element.value){
            alert("契約日が入力されていません");
            return false;
        }        
        return true;
    }

</script>

<blockquote><p>契約登録</p></blockquote>

<?php echo $this->Form->create('Contract', array('url' => array('action' =>'edit'), "onsubmit" => "return updateContract();"))?>
<div class="panel panel-info">
    <div class="panel-heading">
        <h5>案件情報</h5>
    </div>
    <div class="panel-body">
		<?php echo $this->Element("sale_project_edit", array('search'=>true));?>
    </div>
</div>

<div class="panel panel-danger">
    <div class="panel-heading">
        <h5>技術者情報</h5>
    </div>
    <div class="panel-body">
    	<?php echo $this->Element("human_resource_edit", array('search'=>true));?>
    </div>
</div>

<div class="panel panel-warning">
    <div class="panel-heading">
        <h5>契約詳細</h5>
    </div>
    <div class="panel-body">
    	<?php echo $this->Element("contract_edit");?>
    </div>
</div>
<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
<button type="button" onclick="location.href='/s/contracts/result_index'" class="btn btn-success"><i class="glyphicon glyphicon-chevron-left"></i> 戻る</button>
<?php echo $this->Form->end();?>
