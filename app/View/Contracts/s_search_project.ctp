<script>
    $(document).ready(function(){
    });
</script>


<blockquote><p>案件検索</p></blockquote>

<?php echo $this->Form->create("SaleProject", array("action" => "index", 'type'=>'get'))?>
<table class="table-condensed well">
    <tr>
        <th style="min-width:50px;">案件名</th>
        <td><?php echo $this->Form->text("SaleProject.sc_name", array("class" => "form-control input-sm jpn", 'style'=>SIZE_M, "value"=>""));?></td>
        <th style="min-width:50px;">受注企業</th>
        <td><?php echo $this->Form->text("SaleProject.sc_skill", array("class" => "form-control input-sm", 'style'=>SIZE_M, "value"=>""));?></td>
        <td>
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> 検索</button>
    	</td>
    </tr>
</table>
<?php echo $this->Form->end()?>

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
    <th>No</th>
    <th style="min-width:90px;">案件名</th>
    <th style="min-width:200px;">期間</th>
    <th style="min-width:200px;">発注企業</th>
    <th style="min-width:200px;">受注企業</th>
</tr>
<tr>
	<td>1</td>
	<td>XXX</td>
	<td>AAA</td>
	<td>BBB</td>
	<td>CCC</td>
</tr>
</table>

