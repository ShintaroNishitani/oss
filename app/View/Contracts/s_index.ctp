<script>

	function editContractDetail(id) {
		if (id == undefined) {
			id = 0;
		}
		var url = '/s/contracts/detail_edit/' + id ;
		loadBaseWindow(url);
		return false;
	}

	function cloneContractDetail(id) {
		if (id == undefined) {
			id = 0;
		}
		var url = '/s/contracts/detail_clone/' + id ;
		loadBaseWindow(url);
		return false;
	}

	function updateContractDetail() {
		var url = "/s/contracts/update/";
		updateData(url, "ContractDetailUpdateForm", null, function(){
			unloadAll();
			location.href = "/s/contracts/index/";
		});

		return false;
	}

	function deleteContractDetail(id) {
		if (id != undefined) {
			if (confirm('削除してもよろしいですか？\nこの操作は元に戻せません。')) {
				var url = "/s/contracts/delete/" + id + "/;?>";
				updateData(url, null, null, function(){
					location.href = "/s/contracts/index/";
				});
			}
		}
		return false;
	}


</script>


<blockquote><p><?php echo $this->Html->image("job.png")?> <?php echo h('案件管理')?></p></blockquote>

<ul class="nav nav-tabs" role="tablist">
	<li><a href="/s/sale_projects/index/"><?php echo h('案件一覧');?></a></li>
	<li><a href="/s/human_resources/index/"><?php echo h('人材一覧');?></a></li>
	<li class="active"><a href="/s/contracts/index/"><?php echo h('契約一覧');?></a></li>
	<li><a href="/s/companies/index/"><?php echo h('企業一覧');?></a></li>
	<li><a href="/s/contracts/result_index/"><?php echo h('精算一覧');?></a></li>	
</ul>

</br></br>

<TABLE border="0">
<TBODY>
<TR>
<TD valign="top">

<?php echo $this->Form->create("Contract", array('url' => array('action' => 'index'), 'type'=>'get'))?>
<table class="table-condensed well">
	<tr>
		<th style="min-width:50px;">案件名</th>
		<td><?php echo $this->Form->text("Contract.sc_name", array("class" => "form-control input-sm jpn", 'style'=>SIZE_M, "value"=>$keys['name'][0]));?></td>
		<th style="min-width:50px;">技術者</th>
		<td><?php echo $this->Form->text("Contract.sc_human", array("class" => "form-control input-sm jpn", 'style'=>SIZE_M, "value"=>$keys['human'][0]));?></td>
	</tr>
	<tr>
		<th style="min-width:50px;">受注企業</th>
		<td><?php echo $this->Form->text("Contract.sc_receive_company", array("class" => "form-control input-sm jpn", 'style'=>SIZE_M, "value"=>$keys['receive_company'][0]));?></td>
		<th style="min-width:50px;">発注企業</th>
		<td><?php echo $this->Form->text("Contract.sc_order_company", array("class" => "form-control input-sm jpn", 'style'=>SIZE_M, "value"=>$keys['order_company'][0]));?></td>
	</tr>
	<tr>
		<th style="min-width:50px;">期間外契約を含める</th>
		<td colspan=3> <?php echo $this->Form->checkbox("Contract.sc_include_out_range", array( "checked"=>($keys['include_out_range'][0] == "1") ? true : false )); ?> </td>
	</tr>	
	<tr>
		<td class="center" colspan="4">
			<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> 検索</button>
			<!-- <button type="submit" class="btn btn-primary" name="csv"><i class="glyphicon glyphicon-download"></i> CSV出力</button> -->
		</td>
	</tr>
</table>
<?php echo $this->Form->end()?>

</TD>
<TD width=30px>
</TD>
<TD valign="top">

</TD>
</TR>
</TBODY>
</TABLE>

<a href="/s/contracts/edit/"><i class="glyphicon glyphicon-plus-sign"></i> 新規契約登録</a>

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
	<th>No</th>
	<th style="min-width:200px;">案件名</th>
	<th style="min-width:200px;">技術者</th>
	<th style="min-width:100px;">契約日</th>	
	<th style="min-width:100px;">契約形態	</th>
	<th style="min-width:100px;">勤怠時間</br>精算単位</th>
	<th style="min-width:200px;">契約期間</th>
	<th style="min-width:200px;">受注企業</th>
	<th style="min-width:100px;">受注上下限時間</th>
	<th style="min-width:100px;">受注単価</th>
	<th style="min-width:100px;">受注超過単価</th>
	<th style="min-width:100px;">受注控除単価	</th>
	<th style="min-width:100px;">受注入金サイト	</th>
	<th style="min-width:200px;">発注企業</th>
	<th style="min-width:100px;">発注上下限時間</th>
	<th style="min-width:100px;">発注単価</th>
	<th style="min-width:100px;">発注超過単価</th>
	<th style="min-width:100px;">発注控除単価	</th>
	<th style="min-width:100px;">発注支払サイト	</th>
	<th style="min-width:40px;">契約<br/>複製</th>
	<th></th>	
</tr>
<?php
$num = 1;
foreach ($datas as $data):
	$click = sprintf(' onclick="editContractDetail(%d);"', $data['ContractDetail']['id']);
	$id = $data['ContractDetail']['id'];
?>
<tr>
	<td <?php echo $click;?>><?php echo h($num++)?></td>
	<td <?php echo $click;?>><?php echo h($data['SaleProject']['name'])?></td>
	<td <?php echo $click;?>><?php echo h($data['HumanResource']['name'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Contract']['contract_date'])?></td>
	<td <?php echo $click;?>>
		<?php
			$items = Configure::read("contract_type");
			$val = "";
			if ($data['ContractDetail']['contract_type'] != "") {
				$val .= $items[$data['ContractDetail']['contract_type']];
			}
			echo h($val);
		?>
	</td>
	<td <?php echo $click;?>><?php echo h($data['ContractDetail']['kintai_time'])?></td>
	<td class="center" <?php echo $click;?>>
		<?php
			$val = $data['Contract']['term_s']."~".$data['Contract']['term_e'];
			echo h($val);
		?>
	</td>
	<td <?php echo $click;?>><?php echo h($data['ReceiveCompany']['name'])?></td>
	<td class="center" <?php echo $click;?>>
		<?php
			$items = Configure::read("maxmin_time");
			$val = "";
			if ($data['ContractDetail']['receive_time_from_id'] != "") {
				$val .= $items[$data['ContractDetail']['receive_time_from_id']];
			} else {
				$val .= $data['ContractDetail']['receive_time_from'];
			}
			$val .= "~";

			if ($data['ContractDetail']['receive_time_to_id'] != "") {
				$val .= $items[$data['ContractDetail']['receive_time_to_id']];
			} else {
				$val .= $data['ContractDetail']['receive_time_to'];
			}
			echo h($val);
		?>
	</td>
	<td <?php echo $click;?>><?php echo h($data['ContractDetail']['receive_price'])?></td>
	<td <?php echo $click;?>><?php echo h($data['ContractDetail']['receive_up_price'])?></td>
	<td <?php echo $click;?>><?php echo h($data['ContractDetail']['receive_down_price'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Contract']['in_site'])?></td>

	<td <?php echo $click;?>><?php echo h($data['OrderCompany']['name'])?></td>
	<td class="center" <?php echo $click;?>>
		<?php
			$items = Configure::read("maxmin_time");
			$val = "";
			if ($data['ContractDetail']['order_time_from_id'] != "") {
				$val .= $items[$data['ContractDetail']['order_time_from_id']];
			} else {
				$val .= $data['ContractDetail']['order_time_from'];
			}
			$val .= "~";

			if ($data['ContractDetail']['order_time_to_id'] != "") {
				$val .= $items[$data['ContractDetail']['order_time_to_id']];
			} else {
				$val .= $data['ContractDetail']['order_time_to'];
			}
			echo h($val);
		?>
	</td>
	<td <?php echo $click;?>><?php echo h($data['ContractDetail']['order_price'])?></td>
	<td <?php echo $click;?>><?php echo h($data['ContractDetail']['order_up_price'])?></td>
	<td <?php echo $click;?>><?php echo h($data['ContractDetail']['order_down_price'])?></td>
	<td <?php echo $click;?>><?php echo h($data['Contract']['out_site'])?></td>
	<td class="center">
		<a href="javascript:void(0);" onclick="cloneContractDetail(<?php echo $data['ContractDetail']["id"];?>);">
		<i class="glyphicon glyphicon-copy"></i></a>
	</td>
	<td class="center">
		<a href="javascript:void(0);" onclick="deleteContractDetail(<?php echo $data['ContractDetail']["id"];?>);">
		<i class="glyphicon glyphicon-remove-circle"></i></a>
	</td>
</tr>
<?php endforeach; ?>
</table>

<?php echo $this->Element("a_pagination", array('count'=>true));?>

