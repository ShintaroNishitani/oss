<style>
table.form tr td textarea{
    font-size:100%;
    height:200px;
    width:600px;    
}
</style>

<blockquote><p>ノード名称入力</p></blockquote>
<p class="guide"><?php echo 'ノード名称を入力してください。' ?></p>

<?php echo $this->Form->create('Board', array("action" => "updatenode"))?>
<?php echo $this->Form->hidden("BoardTree.level1", array("value"=>$lv1));?>
<?php echo $this->Form->hidden("BoardTree.level2", array("value"=>$lv2));?>
<table class="form">
<tr>
    <th>ノード名称</th>
    <td><?php echo $this->Form->text("BoardTree.title", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>    
    <td colspan="2" class="center">
    	<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>
</table>
<?php echo $this->Form->end();?>
