<script>
	//子ノード追加ボタン押下
    function beforeRegTitle() {
		var element = document.getElementById("BoardTreeTitle");
		if("" == element.value){
            alert("タイトルが入力されていません");
            return false;
		}
		var element = document.getElementById("BoardBodyBody");
		if("" == element.value){
            alert("本文が入力されていません");
            return false;
		}
	return true
    }
</script>

<table>
	<tr>
		<td>
			<?php if(!empty($strLv1Title)){ ?>
				<a href="/s/boards/index/"+ <?php echo($lv1) ?>>
					<?php echo($strLv1Title); ?>
				</a>
			<?php } ?>
			<?php if(!empty($strLv2Title)){ ?>
				<?php echo(" > "); ?>
				<a href="/s/boards/index/"+ <?php echo($lv1) ?> + "/" + <?php echo($lv2) ?>>
				<?php echo($strLv2Title); ?>
				</a>
			<?php } ?>
			<?php if(!empty($strLv3Title)){ ?>
				<?php echo(" > "); ?>
				<a href="/s/boards/index/"+ <?php echo($lv1) ?> + "/" + <?php echo($lv2) ?> + "/" + <?php echo($lv3) ?> >
				<?php echo($strLv3Title); ?>
				</a>
			<?php } ?>
		</td>
	</tr>
</table>

<hr style="margin-top:5px;margin-botom:5px" width="100%" color="#000000">

<blockquote><p>トピック入力</p></blockquote>
<p class="guide"><?php echo 'タイトルと内容を入力してください。' ?></p>

<?php echo $this->Form->create('Board', array("action" => "addtopic", 'type'=>'file', "onsubmit" => "return beforeRegTitle();"))?>

<?php echo $this->Form->hidden("BoardTree.level1", array("value"=>$lv1));?>
<?php echo $this->Form->hidden("BoardTree.level2", array("value"=>$lv2));?>
<?php echo $this->Form->hidden("BoardTree.level3", array("value"=>$lv3));?>
<?php echo $this->Form->hidden("BoardBody.hist_index", array("value"=>0));?>
<?php echo $this->Form->hidden("BoardBody.ref_index", array("value"=>0));?>
<?php echo $this->Form->hidden("BoardBody.disable", array("value"=>0));?>
<table class="form">
<tr>
    <th>タイトル</th>
    <td><?php echo $this->Form->text("BoardTree.title", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_L));?></td>
</tr>
<tr>
    <th>内容</th>
    <td><?php echo $this->Form->textarea("BoardBody.body", array("class"=>"form-control input-sm jpn", 'style'=>'width:600px;height:400px;'));?></td>
</tr>
<tr>
    <th>添付ファイル</th>
    <td>
   		<?php 
		echo $this->Form->input('BoardBodyFile.newfilenames.', 
								 array("type"=>"file", "value"=>"", "multiple", 'label'=>false, 'width'=>"40px",'height'=>"20px", "style" => "font-size:12px;"))
		?>
    </td>
</tr>
<tr>    
    <td colspan="2" class="center">
    	<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>
</table>
<?php echo $this->Form->end();?>
