<script>
    $(document).ready(function() {
        $('.tree').treegrid({
            //expanderExpandedClass: 'icon-minus-sign',
            //expanderCollapsedClass: 'icon-plus-sign',
            expanderExpandedClass: 'treegrid-expander-expanded',
            expanderCollapsedClass: 'treegrid-expander-collapsed',
            initialState: 'collapsed',
            saveState: true,
            //saveStateMethod: 'cookie',
            //saveStateName: 'tree-grid-state',
            expanderTemplate: '<span class="treegrid-expander"></span>',
            indentTemplate: '<span class="treegrid-indent"></span>',

            onChange: function() {
                //alert("Changed: " + $(this).attr("id"));
            },
            onCollapse: function() {
                //alert("Collapsed " + $(this).attr("id"));
            },
            onExpand: function() {
                //alert("Expanded: " + $(this).attr("id"));
            }
        });
        

        GetContent("<?php echo($strDefaultPrm)?>");
    });
    
    
    function GetContent(strParam) {
        $.ajax({
            type: "get",
            url: "<?php echo $this->Html->url(array('action' => 'getcontent'));?>" + strParam,
            data: $('#GetContentForm').serialize(),
            success: function(html){
                $('#BoardContent').html(html);
            }
        });
        return false;
    }

	//子ノード追加ボタン押下
    function addChildNode(lv1, lv2) {
    	var strPrm = "";
    	if(0 != lv1)
    	{
	    	if(0 != lv2)
	    	{
	    		strPrm = lv1 + "/" + lv2;
	    	}else
	    	{
	    		strPrm = lv1;
	    	}
    	}
    	
        var url = "/s/boards/addnode/" + strPrm;
        loadBaseWindow(url);
    }
    
    //トピック追加ボタン押下
    function addBoardBody(lv1, lv2, lv3) {
/*    
        $.ajax({
            type: "get",
            url: "<?php echo $this->Html->url(array('action' => 'addtopic'));?>" + "/" + lv1 + "/" + lv2 + "/" + lv3,
            data: $('#GetContentForm').serialize(),
            success: function(html){
                $('#BoardContent').html(html);
            }
        });
        return false;
*/
		location.href = "/s/boards/addtopic/"+ lv1 + "/" + lv2 + "/" + lv3;
    }
    

	function ReplayComent(hist_idx)
	{
		//引用元を設定
		var element = document.getElementById("BoardBodyRefIndex");
		element.value = hist_idx;
		
		//画面に表示
		var str = "> " + hist_idx + "への返信"
		$('#ReplayHist').html(str);
	}
	
	
	//子ノード追加ボタン押下
    function beforeRegTitle() {
		var element = document.getElementById("BoardBodyBody");
		if("" == element.value){
            alert("本文が入力されていません");
            return false;
		}
		return true;
    }	

</script>
<table width="90%">
	<tr>
	<td align="left" valign="top" width="200px">
		<blockquote>
		掲示板
		</blockquote>
		<hr style="margin-top:5px;margin-botom:5px" width="90%">
		<?php echo $this->Form->create("Board", array("action" => "index", 'type'=>'get', "style" => "font-size:15px;"))?>
			<table class="tree" >
				<tr class="treegrid-0">
				<td>
					<a href="javascript:void(0);"  onclick="GetContent('');">
					Top
				</td>
				</tr>
				
				<?php
					$totalInedx = 1;
					$cntLv1 = count($tree);
					for($i = 0; $i < $cntLv1; $i++)
					{
				    	$click1 = sprintf(' onclick="GetContent(\'/%d\');"', $tree[$i]['id']);
				?>
					<tr id="node-<?php echo($totalInedx);?>" class="treegrid-<?php echo($totalInedx);?>  treegrid-parent-0">
						<?php 
							$keepLv1Idx = $totalInedx;
							$totalInedx++;
						?>
						<td>
							<a href="javascript:void(0);" <?php echo $click1;?>>
								<?php echo($tree[$i]['name']);?>
							</a>
						</td>
					</tr>
				
					<?php
						$cntLv2 = count($tree[$i]['child']);
						for($ii = 0; $ii < $cntLv2; $ii++)
						{
							$treeLv2 = $tree[$i]['child'][$ii];
				    		$click2 = sprintf(' onclick="GetContent(\'/%d/%d/\');"', $tree[$i]['id'], $treeLv2['id']);
					?>
					
						<tr id="node-<?php echo($totalInedx);?>" class="treegrid-<?php echo($totalInedx);?>  treegrid-parent-<?php echo($keepLv1Idx);?>">
							<?php 
								$keepLv2Idx = $totalInedx;
								$totalInedx++;
							?>
							<td>
								<a href="javascript:void(0);" <?php echo $click2;?>>
									<?php echo($treeLv2['name']);?>
								</a>
							</td>
						</tr>
					
						<?php
							$cntLv3 = count($treeLv2['child']);
							for($iii = 0; $iii < $cntLv3; $iii++)
							{
								$treeLv3 = $treeLv2['child'][$iii];
				    			$click3 = sprintf(' onclick="GetContent(\'/%d/%d/%d/\')"', $tree[$i]['id'], $treeLv2['id'],$treeLv3['id']);
						?>

							<tr id="node-<?php echo($totalInedx);?>" class="treegrid-<?php echo($totalInedx);?>  treegrid-parent-<?php echo($keepLv2Idx);?>">
								<?php 
									$keepLv3Idx = $totalInedx;
									$totalInedx++;
								?>
								<td >
									<a href="javascript:void(0);" <?php echo $click3;?>>
										<?php echo($treeLv3['name']);?>
									</a>
								</td>
							</tr>
							
							<?php
								$cntLv4 = count($treeLv3['child']);
								for($iiii = 0; $iiii < $cntLv4; $iiii++)
								{
									$treeLv4 = $treeLv3['child'][$iiii];
					    			$click4 = sprintf(' onclick="GetContent(\'/%d/%d/%d/%d\')"', $tree[$i]['id'], $treeLv2['id'], $treeLv3['id'], $treeLv4['id']);
							?>

								<tr id="node-<?php echo($totalInedx);?>" class="treegrid-<?php echo($totalInedx);?>  treegrid-parent-<?php echo($keepLv3Idx);?>">
									<?php 
										$totalInedx++;
									?>
									<td >
										<a href="javascript:void(0);" <?php echo $click4;?>>
											<?php echo($treeLv4['name']);?>
										</a>
									</td>
								</tr>
				<?php
								}
							}
						}
					}
				?>
			</table>
		<?php echo $this->Form->end()?>
	</td>
	<td align="left" valign="top">
		<div id="BoardContent"></div>
	</td>
</table>

