<script>
    /**
     * [deleteBoardBody 削除]
     * @param  {[type]} id [description]
     */
    function deleteBoardBody(body_id, tree_id){
        if(window.confirm('削除しますがよろしいですか？')){
            var url = "/s/boards/delete/" + body_id + "/" + tree_id;
            location.replace(url);
        }
    }
</script>

<table>
	<tr>
		<td>
			<a href="javascript:void(0);"  <?php printf(' onclick="GetContent(\'\');"'); ?>>
			<?php echo("Top"); ?>
			</a>
			<?php if(!empty($strLv1Title)){ ?>
				<?php echo(" > "); ?>
				<a href="javascript:void(0);"  <?php printf(' onclick="GetContent(\'/%d\');"', $lv1); ?>>
					<?php echo($strLv1Title); ?>
				</a>
			<?php } ?>
			<?php if(!empty($strLv2Title)){ ?>
				<?php echo(" > "); ?>
				<a href="javascript:void(0);"  <?php printf(' onclick="GetContent(\'/%d\/%d\');"', $lv1, $lv2); ?>>
				<?php echo($strLv2Title); ?>
				</a>
			<?php } ?>
			<?php if(!empty($strLv3Title)){ ?>
				<?php echo(" > "); ?>
				<a href="javascript:void(0);"  <?php printf(' onclick="GetContent(\'/%d\/%d/%d\');"', $lv1, $lv2, $lv3); ?>>
				<?php echo($strLv3Title); ?>
				</a>
			<?php } ?>
			<?php echo(" > "); ?>
			<?php echo($boardBody['BoardTree']['title']); ?>
		</td>
	</tr>
</table>

<hr style="margin-top:5px;margin-botom:5px" width="100%" color="#000000">
<blockquote>
    <?php echo $this->Html->image("board.png")?> 
    <label><?php echo h($boardBody['BoardTree']['title']); ?></label>
</blockquote>
<?php if($boardBody['BoardBody']['reg_user_id'] == $staff_id){ ?>
    <div align="right">
        <a href="javascript:void(0);" onclick="deleteBoardBody(<?php echo $boardBody['BoardBody']["id"];?>, <?php echo $boardBody['BoardBody']["tree_id"];?>);">
            <?php echo("【× 削除】") ?>
        </a>
    </div>
<?php } ?>
<hr style="margin-top:5px;margin-botom:5px" width="95%">
<?php echo nl2br($boardBody['BoardBody']['body']);	?>
<hr style="margin-top:5px;margin-botom:5px" width="95%">
<?php 
	printf("登録 %s   %s", $boardBody['BoardBody']['created'], $boardBody['Staff']['name']);
?>
<br>
<?php if(!empty($boardHist)){ ?>
    <?php printf("更新 %s   %s\n", $boardHist[0]['BoardBody']['modified'], $boardHist[0]['Staff']['name']); ?>
    <br>
<?php } ?>
<br>
<?php 
	foreach ($boardBody['BoardTempfile'] as $dataf):
 ?>
<a href="<?php printf("http://127.0.0.1:8181/%s/%s", $dataf['path'], $dataf['filename'])?>", download="<?php echo h($dataf['filename'])?>"> <?php echo h($dataf['filename']) ?></a>
<br>
<?php endforeach; ?>
<hr style="margin-top:2px;margin-botom:5px" width="100%">



<div id="CommentReplay">
<div id="ReplayHist"></div>
<?php echo $this->Form->create("Board", array("onsubmit"=>"return beforeRegTitle();", "action" => "getcontent", 'type'=>'file', "style" => "font-size:12px;"))?>

    <?php echo $this->Form->hidden("BoardBody.ref_index", array("value"=>0));?>
    <?php echo $this->Form->hidden("BoardBody.disable", array("value"=>0));?>
    <?php echo $this->Form->hidden("BoardBody.tree_id", array("value"=>$boardBody['BoardBody']['tree_id']));?>
    <?php echo $this->Form->textarea("BoardBody.body", array("style" => "width:600px;height:200px"));?>
    <?php 
        echo $this->Form->input('BoardBodyFile.newfilenames.', 
            array("type"=>"file", "value"=>"", "multiple", 'label'=>false, 'width'=>"40px",'height'=>"20px", "style" => "font-size:12px;"))
    ?>
    <br>
    <?php echo $this->Form->button("コメントする", array('type'=>'file', 'class'=>"btn btn-primary")); ?>
<?php echo $this->Form->end()?>
</div>
<hr style="margin-top:5px;margin-botom:10px" width="100%" color="#000000">

<?php
    foreach ($boardHist as $data):
?>
    <?php printf("■ %d : %s  %s", $data['BoardBody']['hist_index'], $data['Staff']['name'], $data['BoardBody']['modified']); ?>
    <br>
    <?php if($data['BoardBody']['reg_user_id'] == $staff_id){ ?>
        <div align="right">
            <a href="javascript:void(0);" onclick="deleteBoardBody(<?php echo $data['BoardBody']["id"];?>, 0);">
                <?php echo("【× 削除】") ?>
            </a>
        </div>
    <?php } ?>
    <?php if($data['BoardBody']['ref_index'] != 0) { ?>
        <?php echo h('> ' . $data['BoardBody']['ref_index'] . 'への返信'); ?>
        <br>
    <?php } ?>
    <br>
    <?php 
        echo nl2br($data['BoardBody']['body']);
    ?>
    <br>
    <?php
        foreach ($data['BoardTempfile'] as $dataf):
    ?>
    <a href="<?php printf("/%s/%s", $dataf['path'], $dataf['filename'])?>", download="<?php echo h($dataf['filename'])?>"> <?php echo h($dataf['filename']) ?></a>
    <br>
    <?php endforeach; ?>

    <br>
    <a href="#CommentReplay" title="返信する" onclick="ReplayComent(<?php echo($data['BoardBody']['hist_index']);?>);">
        <?php echo $this->Html->image("confirm.png")?><span>返信する</span>
    </a>

    <hr style="margin-top:2px;margin-botom:5px" width="100%" color="#000000">
<?php endforeach; ?>
