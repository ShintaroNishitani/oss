<table>
	<tr>
		<td>
			<a href="javascript:void(0);"  <?php printf(' onclick="GetContent(\'\');"'); ?>>
			<?php echo("Top"); ?>
			</a>
			<?php if(!empty($strLv1Title)){ ?>
				<?php echo(" > "); ?>
				<a href="javascript:void(0);"  <?php printf(' onclick="GetContent(\'/%d\');"', $lv1); ?>>
				<?php echo($strLv1Title); ?>
				</a>
			<?php } ?>
			<?php if(!empty($strLv2Title)){ ?>
				<?php echo(" > "); ?>
				<a href="javascript:void(0);"  <?php printf(' onclick="GetContent(\'/%d\/%d\');"', $lv1, $lv2); ?>>
				<?php echo($strLv2Title); ?>
				</a>
			<?php } ?>
			<?php if(!empty($strLv3Title)){ ?>
				<?php echo(" > "); ?>
				<a href="javascript:void(0);"  <?php printf(' onclick="GetContent(\'/%d\/%d/%d\');"', $lv1, $lv2, $lv3); ?>>
				<?php echo($strLv3Title); ?>
				</a>
			<?php } ?>
		</td>
	</tr>
</table>

<hr style="margin-top:5px;margin-botom:5px" width="100%" color="#000000">

<?php if(empty($lv3)){ ?>
    <td class="center">
        <button type="button" onclick="addChildNode(<?php printf("%d,%d", $lv1, $lv2) ?>)">ノード追加</button>
    </td>
<?php }else{ ?>
    <button type="button" onclick="addBoardBody(<?php printf("%d,%d,%d", $lv1, $lv2, $lv3) ?>)">トピック追加</button>
<?php } ?>
<br>
<table class="table-bordered table-condensed table-hover">
    <tr>
        <th style="min-width:1500px;text-align:right" >ここにソート用のselectを入れる。</th>
    </tr>
    <?php
        $num = 1;
        foreach ($boardList as $data):
            $click = sprintf(' onclick="GetContent(\'/%d/%d/%d/%d\')"', $data['BoardTree']['level1'],
                                                                        $data['BoardTree']['level2'],
                                                                        $data['BoardTree']['level3'],
                                                                        $data['BoardTree']['level4']
                            );
    ?>
    <tr>
        <td class="left" <?php echo $click;?>>
            <font size="3"><b>
                <?php printf("%s",$data['BoardTree']['title']);?>
            </b></font>
            <?php if($data['BoardTree']['level1'] != 0){ ?>
                <?php echo($data['BoardTree']['lv1_title']); ?>
            <?php } ?>
            <?php if($data['BoardTree']['level2'] != 0){ ?>
                <?php echo(" > "); ?>
                <?php echo($data['BoardTree']['lv2_title']); ?>
            <?php } ?>
            <?php if($data['BoardTree']['level3'] != 0){ ?>
                <?php echo(" > "); ?>
                <?php echo($data['BoardTree']['lv3_title']); ?>
            <?php } ?>
            <br/>
            <div <?php echo $click;?>>
            <?php printf("%s …",mb_substr($data['BoardBody']['body'], 0, 100));?>
            <div align="right">
                <?php  printf("最終更新 %s %s", $data['BoardTree']['modified'], $data['BoardTree']['modified_staff_name']);?>
            </div>
            </div>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
