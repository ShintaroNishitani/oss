<script>
    function editPcManage(id){
        if (id == undefined) {
            id = 0;
        }
        var url = '/s/pc_manages/edit/' + id ;
        loadBaseWindow(url);
        return false;
    }

    function deleteTransport(id){
        if(window.confirm('削除しますがよろしいですか？')){
            var url = "/s/pc_manages/delete/" + id;
            location.replace(url);
        }
    }

	$(document).ready(function() {
		displayScrap(false);
	});

	$(function() {
		$('#scrap_display').change(function() {
			var check = $(this).is(':checked');
			displayScrap(check);
		});
	});

	function displayScrap (check) {
		var tbody = document.getElementById('tbody');

		for (var i = 0; i < tbody.rows.length; i++) {

			var value = tbody.rows[i].cells[7].innerText;
			var display = true;
			console.log(value);
			if (check == false && value != "") {
					display = false;
			}

			if (display) {
				tbody.rows[i].style.display="";
			} else {
				tbody.rows[i].style.display="none";
			}
		};
	}
</script>


<blockquote><p><?php echo $this->Html->image("pc.png")?> PC管理</p></blockquote>

<?= $this->Form->input("scrap_display", array('type' => 'checkbox', 'label'=>' '.__('廃棄表示'), 'checked'=>false, 'id'=>'scrap_display'));?>
<a href="javascript:void(0);" onclick="editPcManage();"><i class="glyphicon glyphicon-plus-sign"></i> 新規追加</a>
<table class="table-bordered table-condensed table-striped table-hover">
<tr>
    <th>No</th>
    <th style="min-width:120px;">管理番号</th>
    <th style="min-width:90px;">資産名称</th>
    <th style="min-width:80px;">メーカー</th>
    <th style="min-width:250px;">型番等</th>
    <th style="min-width:90px;">取得年月日</th>
    <th style="min-width:80px;">管理者</th>
    <th style="min-width:90px;">廃棄年月日</th>
    <th style="min-width:200px;">OS</th>
    <th style="min-width:250px;">Office</th>
    <th style="min-width:120px;">使用場所</th>
    <th style="min-width:120px;">CPU</th>
    <th style="min-width:50px;">メモリ</th>
    <th style="min-width:200px;">備考</th>

    <th></th>
</tr>
<tbody id="tbody">
<?php
$index = 0;
foreach ($datas as $data):
    $index++;
    $click = sprintf(' onclick="editPcManage(%d);"', $data['PcManage']['id']);
    $manage_no  = $data['PcManage']['manage_no'];
    $pc_name    = $data['PcManage']['pc_name'];
    $maker      = $data['PcManage']['maker'];
    $model      = $data['PcManage']['model'];
    $get_date   = $data['PcManage']['get_date'];
    $name  = '社内';
    if ($data['Staff']['name']) {
        $name  = $data['Staff']['name'];
    }
    $scrap_date = $data['PcManage']['scrap_date'];
    $os         = $data['PcManage']['os'];
    $office     = $data['PcManage']['office'];
    $service    = $data['PcManage']['service'];
    $cpu        = $data['PcManage']['cpu'];
    $memory     = $data['PcManage']['memory'];
    $notes      = $data['PcManage']['notes'];
?>
<tr>
    <td <?php echo $click;?>><?php echo h($index)?></td>
    <td <?php echo $click;?>><?php echo h($manage_no)?></td>
    <td <?php echo $click;?>><?php echo h($pc_name)?></td>
    <td <?php echo $click;?>><?php echo h($maker)?></td>
    <td <?php echo $click;?>><?php echo h($model)?></td>
    <td <?php echo $click;?>><?php echo h($get_date)?></td>
    <td <?php echo $click;?>><?php echo h($name)?></td>
    <td <?php echo $click;?>><?php echo h($scrap_date)?></td>
    <td <?php echo $click;?>><?php echo h($os)?></td>
    <td <?php echo $click;?>><?php echo h($office)?></td>
    <td <?php echo $click;?>><?php echo h($service)?></td>
    <td <?php echo $click;?>><?php echo h($cpu)?></td>
    <td <?php echo $click;?>><?php echo h($memory)?></td>
    <td <?php echo $click;?>><?php echo h($notes)?></td>
    <td class="center">
        <a href="javascript:void(0);" onclick="deleteTransport(<?php echo $data['PcManage']['id'];?>);">
        <i class="glyphicon glyphicon-remove-circle"></i></a>
    </td>
</tr>
<?php
endforeach;
?>
</tbody>
</table>

