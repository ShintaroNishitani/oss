<script>
    $(function(){
        var columns = ['#PcManageGetDate','#PcManageScrapDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    });


</script>

<blockquote><p>PC管理編集</p></blockquote>

<?php echo $this->Form->create('PcManage', array("name"=>"PcManage", "action" => "update",
                                                        "onsubmit" => "return updatePcManage();"))?>
<?php echo $this->Form->hidden("PcManage.id")?>
<?php echo $this->Form->hidden("PcManage.enable", array("value"=>1))?>

<table class="form">
<tr>
    <th>管理番号*</th>
    <td><?php echo $this->Form->text('PcManage.manage_no', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>資産名称*</th>
    <td><?php echo $this->Form->text('PcManage.pc_name', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>メーカー*</th>
    <td><?php echo $this->Form->text('PcManage.maker', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>型番等*</th>
    <td><?php echo $this->Form->text('PcManage.model', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_L));?></td>
</tr>
<tr>
    <th>取得年月日*</th>
    <td><?php echo $this->Form->text("PcManage.get_date", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
    <th>管理者</th>
    <td><?php echo $this->Form->select('PcManage.staff_id',$staffs, array("class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>廃棄年月日</th>
    <td><?php echo $this->Form->text("PcManage.scrap_date", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>
    <th>OS*</th>
    <td><?php echo $this->Form->text('PcManage.os', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_L));?></td>
</tr>
<tr>
    <th>Office</th>
    <td><?php echo $this->Form->text('PcManage.office', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_L));?></td>
</tr>
<tr>
    <th>使用場所*</th>
    <td><?php echo $this->Form->text('PcManage.service', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>CPU</th>
    <td><?php echo $this->Form->text('PcManage.cpu', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_LL));?></td>
</tr>
<tr>
    <th>メモリ</th>
    <td><?php echo $this->Form->text('PcManage.memory', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_S));?></td>
</tr>
<tr>
    <th>備考</th>
    <td><?php echo $this->Form->text('PcManage.notes', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_LL));?></td>
</tr>
<tr>
    <td colspan="6" class="center">
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>
</table>

<?php echo $this->Form->end();?>
