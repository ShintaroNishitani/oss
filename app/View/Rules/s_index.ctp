<script type="text/javascript">
jQuery(document).ready(function(){
   
  jQuery(document).on('click','#button',function(e){
     
    jQuery.ajax({
        type: "POST",
        url: "<?php echo $this->webroot; ?>s/rules/CaptchaCheck/",
        data: "captcha="+jQuery("#captcha").val(),
        success: function(xml){
           
          if( jQuery(xml).find("check").text() != 1 ){
            jQuery('#message').html('画像認証コードが一致しません').css({"color":"red"});
          }else{
            /*
             * 実際の運用時はメッセージを出すのではなく、ここでPOST処理させる等
             */
            jQuery('#message').html('認証しました').css({"color":"blue"});
          }
           
        }
    });
 
  });
});
</script>
 
<?php echo $this->Form->create('Rule', array("name"=>"Rule", 'url' => array('action' => 'certificate'))); ?> 
    <?php echo $this->Form->hidden("Rule.rule_kind", array('value'=>$rule_kind))?>
    <p >画像認証</p>
    <p>
      <img src="<?php echo $this->webroot; ?>s/rules/CaptchaRender/?sid=<?php echo md5(uniqid()) ?>" id="siimage">
<!-- ?sid=<?php echo md5(uniqid()) ?> の部分が脆弱性チェックをすると引っかかる事が有ります。無くても動作はするので消してもいいかも -->
      <a href="#" onclick="document.getElementById('siimage').src = '<?php echo $this->webroot; ?>s/rules/CaptchaRender/?sid=' + Math.random(); this.blur(); return false">読み取りにくい場合</a>
      <br />
      <input name="captcha" type="text" id="captcha" size="15">
      <span id="message"></span>
    </p>
 
    <p><button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-log-in"></i>認証</button></p>	
<?php echo $this->Form->end();?>