<style>
.panel-heading {
    padding: 5px 15px;
}

.panel-footer {
	padding: 1px 15px;
	color: #A0A0A0;
}
</style>

<div class="row">
	<div class="col-sm-6 col-md-4 col-md-offset-4">
		<div class="panel panel-default">
			<div class="panel-body">
				<form role="form" action="" method="POST">
					<fieldset>
						<div class="row">
							<div class="center-block">
								<img src="/img/logo.gif" alt="..." class="img-rounded" style="width:280px; height:35px; margin-left:20px;margin-right:20px;margin-top:20px;margin-bottom:40px;">
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 col-md-10  col-md-offset-1 ">
							
								<?php echo $form->input('captcha', Array('label' => false, 'class' => 'captcha')); ?>
								<img src="<?php echo URL; ?>/s/rules/captcha/">	

							</div>
						</div>
					</fieldset>
				</form>
			</div>
        </div>
	</div>
</div>
