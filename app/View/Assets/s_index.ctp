<script>
    function editAsset(id){
        if (id == undefined) {
            id = 0;
        }
        var url = '/s/assets/edit/' + id ;
        loadBaseWindow(url);
        return false;
    }

    function deleteTransport(id){
        if(window.confirm('削除しますがよろしいですか？')){
            var url = "/s/assets/delete/" + id;
            location.replace(url);
        }
    }

	$(document).ready(function() {
		displayScrap(false);
	});

	$(function() {
		$('#scrap_display').change(function() {
			var check = $(this).is(':checked');
			displayScrap(check);
		});
	});

	function displayScrap (check) {
		var tbody = document.getElementById('tbody');

		for (var i = 0; i < tbody.rows.length; i++) {

			var value = tbody.rows[i].cells[9].innerText;
			var display = true;
			console.log(value);
			if (check == false && value != "") {
					display = false;
			}

			if (display) {
				tbody.rows[i].style.display="";
			} else {
				tbody.rows[i].style.display="none";
			}
		};
	}
</script>


<blockquote><p><?php echo $this->Html->image("asset.png")?> 資産管理</p></blockquote>

<?= $this->Form->input("scrap_display", array('type' => 'checkbox', 'label'=>' '.__('廃棄表示'), 'checked'=>false, 'id'=>'scrap_display'));?>
<a href="javascript:void(0);" onclick="editAsset();"><i class="glyphicon glyphicon-plus-sign"></i> 新規追加</a>

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
    <th style="min-width:35px;">No</th>
    <th style="min-width:100px;">名前</th>
    <th style="min-width:90px;">型番</th>
    <th style="min-width:100px;">購入先</th>
    <th style="min-width:90px;">金額</th>
    <th style="min-width:50px;">数量</th>
    <th style="min-width:85px;">購入日</th>
    <th style="min-width:80px;">購入者</th>
    <th style="min-width:80px;">使用者</th>
    <th style="min-width:85px;">廃棄日</th>
    <th style="min-width:150px;">備考</th>
    <th></th>
</tr>
<tbody id="tbody">
<?php
$index = 0;
foreach ($datas as $data):
    $index++;
    $click = sprintf(' onclick="editAsset(%d);"', $data['Asset']['id']);
    $id  = $data['Asset']['id'];
    $name  = $data['Asset']['name'];
    $model = $data['Asset']['model'];
    $purchase = $data['Asset']['purchase'];
    $price = $data['Asset']['price'];
    $number = $data['Asset']['number'];
    $purchase_date = $data['Asset']['purchase_date'];
    $purchaser_id = 'なし';
    if ($data['Staff_P']['name']) {
        $purchaser_id  = $data['Staff_P']['name'];
    }
    $user_id = 'なし';
    if ($data['Staff_U']['name']) {
        $user_id  = $data['Staff_U']['name'];
    }
    $disposal_date = $data['Asset']['disposal_date'];
    $remark = $data['Asset']['remark'];
?>
<tr>
    <td <?php echo $click;?>><?php echo h($index)?></td>
    <td <?php echo $click;?>><?php echo h($name)?></td>
    <td <?php echo $click;?>><?php echo h($model)?></td>
    <td <?php echo $click;?>><?php echo h($purchase)?></td>
    <td class="number" <?php echo $click;?>><?php echo number_format(h($price))?></td>
    <td class="number" style="width:50px" <?php echo $click;?>><?php echo number_format(h($number))?></td>
    <td class="center" <?php echo $click;?>><?php echo h($purchase_date)?></td>
    <td class="center" <?php echo $click;?>><?php echo h($purchaser_id)?></td>
    <td class="center" <?php echo $click;?>><?php echo h($user_id)?></td>
    <td class="center" <?php echo $click;?>><?php echo h($disposal_date)?></td>
    <td <?php echo $click;?>><?php echo h($remark)?></td>
    <td class="center">
        <a href="javascript:void(0);" onclick="deleteTransport(<?php echo $data['Asset']['id'];?>);">
        <i class="glyphicon glyphicon-remove-circle"></i></a>
    </td>
</tr>
<?php
endforeach;
?>
</tbody>
</table>

