<script>

    $(function(){
        var columns = ['#AssetPurchaseDate', '#AssetDisposalDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });   
    });

  
</script>

<blockquote><p>資産管理</p></blockquote>

<?php echo $this->Form->create("Asset", array("name"=>"Asset", "action" => "update",
                                                        "onsubmit" => "return updateAsset();"))?>
<?php
 echo $this->Form->hidden("Asset.id")
 ?>
<?php
 echo $this->Form->hidden("Asset.enable", array("value"=>1))
?>

<table class="form">
<tr>
    <th>名前*</th>
    <td><?php echo $this->Form->text('Asset.name', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>型番</th>
    <td><?php echo $this->Form->text('Asset.model', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>購入先*</th>
    <td><?php echo $this->Form->text('Asset.purchase', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>金額*</th>
    <td><?php echo $this->Form->text('Asset.price', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>数量*</th>
    <td><?php echo $this->Form->text('Asset.number', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>購入日*</th>
    <td><?php echo $this->Form->text('Asset.purchase_date', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>購入者*</th>
    <td><?php echo $this->Form->select('Asset.purchaser_id',$staffs, array("class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>使用者</th>
    <td><?php echo $this->Form->select('Asset.user_id',$staffs, array("class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>廃棄日</th>
    <td><?php echo $this->Form->text('Asset.disposal_date', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>備考</th>
    <td><?php echo $this->Form->text('Asset.remark', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <td colspan="2" class="center">
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>        
</table>

<?php echo $this->Form->end();?>