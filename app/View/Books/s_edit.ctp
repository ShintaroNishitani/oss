<script>

    $(function(){
        var columns = ['#BookPurchaseDate', '#BookLoanDate', '#BookReturnDate', '#BookDisposalDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });
    });

</script>

<blockquote><p>書籍管理</p></blockquote>

<?php echo $this->Form->create("Book", array("name"=>"Book", "action" => "update",
                                                        "onsubmit" => "return updateBook();"))?>
<?php
 echo $this->Form->hidden("Book.id")
 ?>
<?php
 echo $this->Form->hidden("Book.enable", array("value"=>1))
?>

<table class="form">
<tr>
    <th>書籍名*</th>
    <td><?php echo $this->Form->text('Book.name', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>購入日*</th>
    <td><?php echo $this->Form->text('Book.purchase_date', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>購入者*</th>
    <td><?php echo $this->Form->select('Book.purchaser_id',$staffs, array("class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>購入先*</th>
    <td><?php echo $this->Form->text('Book.purchase', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>金額*</th>
    <td><?php echo $this->Form->text('Book.price', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>所有者</th>
    <td><?php echo $this->Form->select('Book.user_id',$staffs, array("class"=>"form-control input-sm", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>貸与日</th>
    <td><?php echo $this->Form->text('Book.loan_date', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>返却予定日</th>
    <td><?php echo $this->Form->text('Book.return_date', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>処分日</th>
    <td><?php echo $this->Form->text('Book.disposal_date', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>備考</th>
    <td><?php echo $this->Form->text('Book.remark', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <td colspan="2" class="center">
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>        
</table>

<?php echo $this->Form->end();?>