<script>
    function editBook(id){
        if (id == undefined) {
            id = 0;
        }
        var url = '/s/books/edit/' + id ;
        loadBaseWindow(url);
        return false;
    }

    function deleteTransport(id){
        if(window.confirm('削除しますがよろしいですか？')){
            var url = "/s/books/delete/" + id;
            location.replace(url);
        }
    }

</script>


<blockquote><p><?php echo $this->Html->image("question.png")?> 書籍管理</p></blockquote>

<p class="guide"><?php echo '社内書籍を対象とした管理を行います。' ?></p>
<?php echo '【管理対象】' ?><br>
<?php echo '  ・社内経費にて購入した書籍' ?><br>
<?php echo '  ・寄贈書' ?><br>
<br>
<?php echo '【備考】' ?><br>
<?php echo '  ・同一書籍を複数購入した場合、個別に管理するために分けて登録を行って下さい。' ?><br>
<br>

<a href="javascript:void(0);" onclick="editBook();"><i class="glyphicon glyphicon-plus-sign"></i> 新規追加</a>

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
    <th style="min-width:35px;">No</th>
    <th style="min-width:100px;">書籍名</th>
    <th style="min-width:85px;">購入日</th>
    <th style="min-width:80px;">購入者</th>
    <th style="min-width:100px;">購入先</th>
    <th style="min-width:90px;">金額</th>
    <th style="min-width:80px;">所有者</th>
    <th style="min-width:85px;">貸与日</th>
    <th style="min-width:85px;">返却予定日</th>
    <th style="min-width:85px;">処分日</th>
    <th style="min-width:150px;">備考</th>
    <th></th>
</tr>
<?php
$index = 0;
foreach ($datas as $data):
    $index++;
    $click = sprintf(' onclick="editBook(%d);"', $data['Book']['id']);
    $id  = $data['Book']['id'];
    $name  = $data['Book']['name'];
    $purchase_date = $data['Book']['purchase_date'];
    $purchaser_id = '―――';
    if ($data['Staff_P']['name']) {
        $purchaser_id  = $data['Staff_P']['name'];
    }
    $purchase = $data['Book']['purchase'];
    $price = $data['Book']['price'];
    $user_id = '本棚';
    if ($data['Staff_U']['name']) {
        $user_id  = $data['Staff_U']['name'];
    }
    $loan_date = $data['Book']['loan_date'];
    $return_date = $data['Book']['return_date'];
    $disposal_date = $data['Book']['disposal_date'];
    $remark = $data['Book']['remark'];
?>
<tr>
    <td <?php echo $click;?>><?php echo h($index)?></td>
    <td class="center" <?php echo $click;?>><?php echo h($name)?></td>
    <td class="center" <?php echo $click;?>><?php echo h($purchase_date)?></td>
    <td class="center" <?php echo $click;?>><?php echo h($purchaser_id)?></td>
    <td class="center" <?php echo $click;?>><?php echo h($purchase)?></td>
    <td class="number" <?php echo $click;?>><?php echo number_format(h($price))?></td>
    <td class="center" <?php echo $click;?>><?php echo h($user_id)?></td>
    <td class="center" <?php echo $click;?>><?php echo h($loan_date)?></td>
    <td class="center" <?php echo $click;?>><?php echo h($return_date)?></td>
    <td class="center" <?php echo $click;?>><?php echo h($disposal_date)?></td>
    <td class="center" <?php echo $click;?>><?php echo h($remark)?></td>
    <td class="center">
        <a href="javascript:void(0);" onclick="deleteTransport(<?php echo $data['Book']['id'];?>);">
        <i class="glyphicon glyphicon-remove-circle"></i></a>
    </td>
</tr>
<?php 
endforeach;
?>

</table>
