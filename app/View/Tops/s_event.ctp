<script>
    /**
     * [jumpSchedulePage 対象ページ移動]
     * @param  {[type]} id [移動先スケジュールID]
     * @return {[type]}          [description]
     */
    function jumpSchedulePage(id)
    {
        var url = '/a/schedules/detail/' + id;
        loadBaseWindow(url);
        return false;
    }

</script>


<div class="panel panel-info">
    <div class="panel-heading">
        <h5>
            <?php echo $this->Html->image("calendar.png")?>
            <?php printf ("%s年%s月%s日(%s)のイベント", $year, $month, $day, $d_week); ?>
        </h5>
    </div>
    <div class="panel-body">
        <?php if (!empty($events)) { ?>
            <?php
                $num = 1;
                foreach ($events as $data):
                    $start_hour = null;
                    $start_min = null;
                    if ($data['Schedule']['start']) {
                        $date = explode(':', $data['Schedule']['start']);
                        $start_hour = $date[0];
                        $start_min = $date[1];
                    }
                    $end_hour = null;
                    $end_min = null;
                    if ($data['Schedule']['end']) {
                        $date = explode(':', $data['Schedule']['end']);
                        $end_hour = $date[0];
                        $end_min = $date[1];
                    }
                    $click = sprintf('onclick="jumpSchedulePage(%d);"', $data['Schedule']['id']);
            ?>
                <tr>
                    <td class="left" <?php echo $click;?>>
                        <div <?php echo $click;?>>
                        <?php echo ("・"); ?>
                        <?php if ($start_hour != null) { ?>
                            <?php printf("%s:%s-%s:%s", $start_hour, $start_min, $end_hour, $end_min);?>
                        <?php } ?>
                        <?php echo h($data['Schedule']['title'])?>
                        </div>
                    </td>
                </tr>
                <hr style="margin-top:5px;margin-botom:10px" width="100%" color="#000000">
            <?php endforeach; ?>
        <?php } else{ ?>
            <p class="guide"><?php echo '本日のイベントはありません。' ?></p>
        <?php } ?>
	</div>
</div>

<div id="StaffList">
<!-- 従業員リスト -->
</div>


