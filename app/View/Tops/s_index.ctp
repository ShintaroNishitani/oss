<script type="text/javascript" charset="utf-8">

    $(document).ready(function(){
        loadEvent();
        loadConfirm();
        loadBoard();
    });

    function loadEvent(){
        var url = "/s/tops/event/";
        $('#Event').load(url);
    }

    function loadConfirm(){
        var url = "/s/tops/confirm/";
        $('#Confirm').load(url);
    }

    function loadBoard(){
        var url = "/s/tops/board/";
        $('#Board').load(url);
    }
</script>

<blockquote><p><?php echo $this->Html->image("home.png")?> トップページ</p></blockquote>

<!-- 本日のイベント -->
<div id="Event">loading...</div>

<!-- 申請・承認・差戻 -->
<div id="Confirm">loading...</div>

<!-- 掲示板新着 -->
<div id="Board">loading...</div>
