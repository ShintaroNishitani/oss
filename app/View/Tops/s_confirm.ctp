<script>
    /**
     * [jumpTargetPage 対象ページ移動]
     * @param  {[type]} type     [移動先ページ種別]
     * @param  {[type]} type_id  [移動先ページID]
     * @param  {[type]} staff_id [社員ID]
     * @param  {[type]} year     [年]
     * @param  {[type]} month    [月]
     * @return {[type]}          [description]
     */
    function jumpTargetPage(type, type_id, staff_id, year, month)
    {
        var url = "";
        switch (type) {
            case 0:     //  勤怠
                //window.open("/s/kintais/index/" + year + "/" + month);
                url = "/s/kintais/index/" + year + "/" + month;
                break;
            case 1:     // 交通費精算
                //window.open("/s/transports/index/" + year + "/" + month);
                url = "/s/transports/index/" + year + "/" + month;
                break;
            case 2:     // 経費精算
                //window.open("/s/costs/index/" + year + "/" + month);
                url = "/s/costs/index/" + year + "/" + month;
                break;
            case 3: // 旅費精算
                break;
            case 4: // 通勤届
                break;
            case 5: // 慶弔届
                break;
            case 6: // 身上届
                break;
            case 7: // 給与振込口座届
                break;
			case <?= CONFIRM_TYPE_8 ?>: // 休暇申請
				url = "/a/ApplyHolidays/detail_edit/" + type_id;
                break;

            default:
                break;
        }
        location.href = url;
    }
    /**
     * [jumpInputPage 対象ページ移動]
     * @param  {[type]} type     [移動先ページ種別]
     * @param  {[type]} type_id  [移動先ページID]
     * @param  {[type]} staff_id [社員ID]
     * @param  {[type]} year     [年]
     * @param  {[type]} month    [月]
     * @return {[type]}          [description]
     */
    function jumpInputPage(type, type_id, id, year, month)
    {
        var url = "";
        switch (type) {
            case 0:     //  勤怠
                break;
            case 1:     // 交通費精算
                break;
            case 2:     // 経費精算
                break;
            case 3: // 旅費精算
                break;
            case 4: // 通勤届
                break;
            case 5: // 慶弔届
                break;
            case 6: // 身上届
                break;
            case 7: // 給与振込口座届
                break;
			case <?= CONFIRM_TYPE_8 ?>: // 休暇申請
				url = "/s/ApplyHolidays/detail_edit/"+ year + "/" + month + "/" + id + "/" + type_id;
                break;

            default:
                break;
        }
        location.href = url;
	}

</script>


<div class="panel panel-info">
	<div class="panel-heading">
		<h5><?php echo $this->Html->image("confirm.png")?> 申請・承認・差戻</h5>
	</div>
	<div class="panel-body">
		<?php if (!empty($confirms)) { ?>
			●申請一覧
		    <table class="table-bordered table-condensed table-striped table-hover">
		    <tr>
		        <th>No</th>
		        <th style="min-width:110px;">申請日</th>
		        <th style="min-width:110px;">内容</th>
		        <th style="min-width:110px;">対象年月</th>
		        <th style="min-width:110px;">承認日</th>
		        <th style="min-width:110px;">承認者</th>
		        <th style="min-width:110px;">ステータス</th>
		    </tr>

		    <?php
		    $num = 1;
		    foreach ($confirms as $data):
		        $target_year = null;
		        $target_month = null;
		        if ($data['Confirm']['target_date']) {
		            $date = explode('-', $data['Confirm']['target_date']);
		            $target_year = $date[0];
		            $target_month = $date[1];
		        }
		        $click = sprintf('onclick="jumpTargetPage(%d, %d, %d, %d, %d);"', $data['Confirm']['type'], $data['Confirm']['type_id'], $data['Confirm']['request_staff_id'], $target_year, $target_month);
		    ?>
		        <tr>
		            <td <?php echo $click;?>><?php echo h($num++)?></td>
		            <td class="center" <?php echo $click;?>><?php echo h($data['Confirm']['request_date'])?></td>
		            <td <?php echo $click;?>>
		                <?php
		                    $items = Configure::read("confirm_type");
		                    echo h($items[$data['Confirm']['type']]);
		                ?>
		            </td>
		            <td class="center" <?php echo $click;?>>
		                <?php
		                    if ($target_year && $target_month) {
		                        echo h($target_year . '年' . $target_month . '月');
		                    }
		                ?>
		            </td>
		            <td class="center" <?php echo $click;?>><?php echo h($data['Confirm']['check_date'])?></td>
		            <td <?php echo $click;?>>
		                <?php
		                    if ($data['Confirm']['check_staff_id']) {
		                        echo h($staffs[$data['Confirm']['check_staff_id']]);
		                    }
		                ?>
		            </td>
		            <td <?php echo $click;?> class="center">
		                <?php
		                    if ($data['Confirm']['type'] == CONFIRM_TYPE_1 || $data['Confirm']['type'] == CONFIRM_TYPE_2) {
		                        $items = Configure::read("receive_status");
		                        echo h($items[$data['Confirm']['receive']]);
		                    }
		                ?>
		            </td>
		        </tr>
		    <?php endforeach; ?>
		    </table>
			<br>
		<?php } ?>
		<?php if (!empty($approvals)) { ?>
			●承認一覧
		    <table class="table-bordered table-condensed table-striped table-hover">
		    <tr>
		        <th>No</th>
		        <th style="min-width:110px;">申請日</th>
		        <th style="min-width:110px;">内容</th>
		        <th style="min-width:110px;">前回承認</th>
		        <th style="min-width:110px;">申請者</th>
		        <th style="min-width:110px;">申請対象日</th>
		    </tr>
		    <?php
		    $num = 1;
		    foreach ($approvals as $data):
		        $target_year = null;
		        $target_month = null;
		        if ($data['Confirm']['target_date']) {
		            $date = explode('-', $data['Confirm']['target_date']);
		            $target_year = $date[0];
		            $target_month = $date[1];
		        }
		        $click = sprintf('onclick="jumpTargetPage(%d, %d, %d, %d, %d);"', $data['Confirm']['type'], $data['Confirm']['type_id'], $data['Confirm']['request_staff_id'], $target_year, $target_month);
		    ?>
		        <tr>
		            <td <?php echo $click;?>><?php echo h($num++)?></td>
		            <td class="center" <?php echo $click;?>><?php echo h($data['Confirm']['request_date'])?></td>
		            <td <?php echo $click;?>>
		                <?php
		                    $items = Configure::read("confirm_type");
		                    echo h($items[$data['Confirm']['type']]);
		                ?>
		            </td>
		            <td <?php echo $click;?>>
		                <?php
		                    if ($data['Confirm']['prev_check_staff_id']) {
		                        echo h($staffs[$data['Confirm']['prev_check_staff_id']]);
		                    }
		                ?>
		            </td>
		            <td <?php echo $click;?>>
		                <?php
		                    if ($data['Confirm']['request_staff_id']) {
		                        echo h($staffs[$data['Confirm']['request_staff_id']]);
		                    }
		                ?>
		            </td>
					<td>
						<?php 
							if (isset($data['Applydays'])) {
								echo implode('<br>',$data['Applydays']);
							}
						?>
					</td>
		        </tr>
		    <?php endforeach; ?>
			</table>
			<br>
		<?php } ?>
		<?php if (!empty($remands)) { ?>
			●差戻一覧
		    <table class="table-bordered table-condensed table-striped table-hover">
		    <tr>
		        <th>No</th>
		        <th style="min-width:110px;">申請日</th>
		        <th style="min-width:110px;">内容</th>
		        <th style="min-width:110px;">確認者</th>
		    </tr>
		    <?php
		    $num = 1;
		    foreach ($remands as $data):
		        $target_year = null;
		        $target_month = null;
		        if ($data['Confirm']['target_date']) {
		            $date = explode('-', $data['Confirm']['target_date']);
		            $target_year = $date[0];
		            $target_month = $date[1];
		        }
		        $click = sprintf('onclick="jumpInputPage(%d, %d, %d, %d, %d);"', $data['Confirm']['type'], $data['Confirm']['type_id'], $data['ApplyHolidayDetail']['apply_holiday_id'], $target_year, $target_month);
		    ?>
		        <tr>
		            <td <?php echo $click;?>><?php echo h($num++)?></td>
		            <td class="center" <?php echo $click;?>><?php echo h($data['Confirm']['request_date'])?></td>
		            <td <?php echo $click;?>>
		                <?php
		                    $items = Configure::read("confirm_type");
		                    echo h($items[$data['Confirm']['type']]);
		                ?>
		            </td>
		            <td <?php echo $click;?>>
		                <?php
		                    if ($data['Confirm']['check_staff_id']) {
		                        echo h($staffs[$data['Confirm']['check_staff_id']]);
		                    }
		                ?>
		            </td>
		        </tr>
		    <?php endforeach; ?>
			</table>
			<br>
		<?php } ?>
		<?php if (empty($confirms) && empty($approvals) && empty($remands)) { ?>
		    <p class="guide"><?php echo '新着情報はありません。' ?></p>
		<?php } ?>
	</div>
</div>

<div id="StaffList">
<!-- 従業員リスト -->
</div>


