<script>
    /**
     * [jumpBoardPage 対象ページ移動]
     * @param  {[type]} type     [移動先ページ種別]
     * @param  {[type]} staff_id [社員ID]
     * @param  {[type]} year     [年]
     * @param  {[type]} month    [月]
     * @return {[type]}          [description]
     */
    function jumpBoardPage(lv1, lv2, lv3, lv4)
    {
        var url = "/s/boards/index/" + lv1 + "/" + lv2 + "/" + lv3 + "/" + lv4;
        location.href = url;
    }

</script>


<div class="panel panel-info">
    <div class="panel-heading">
        <h5><?php echo $this->Html->image("board.png")?> 掲示板</h5>
    </div>
    <div class="panel-body">
        <?php if (!empty($boards)) { ?>
            <?php
                $num = 1;
                foreach ($boards as $data):
                    $click = sprintf ('onclick="jumpBoardPage(%d, %d, %d, %d)"', $data['BoardTree']['level1'],
                                                                                 $data['BoardTree']['level2'],
                                                                                 $data['BoardTree']['level3'],
                                                                                 $data['BoardTree']['level4']
                                     );
            ?>
                <tr>
                    <td class="left" <?php echo $click;?>>
                        <div <?php echo $click;?>>
                        <font size="3"><b>
                            <?php printf("%s",$data['BoardTree']['title']);?>
                        </b></font>
                        <?php if($data['BoardTree']['level1'] != 0){ ?>
                            <?php echo($data['BoardTree']['lv1_title']); ?>
                        <?php } ?>
                        <?php if($data['BoardTree']['level2'] != 0){ ?>
                            <?php echo(" > "); ?>
                            <?php echo($data['BoardTree']['lv2_title']); ?>
                        <?php } ?>
                        <?php if($data['BoardTree']['level3'] != 0){ ?>
                            <?php echo(" > "); ?>
                            <?php echo($data['BoardTree']['lv3_title']); ?>
                        <?php } ?>
                        <br/>
                        <?php printf("%s …",mb_substr($data['BoardTree']['body'], 0, 100));?>
                        <div align="right">
                            <?php  printf("最終更新 %s %s", $data['BoardTree']['modified'], $staffs[$data['BoardTree']['modified_staff_id']]);?>
                        </div>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php } else{ ?>
            <p class="guide"><?php echo '新着情報はありません。' ?></p>
        <?php } ?>
        
    </div>
</div>

