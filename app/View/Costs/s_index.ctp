<script>
    $(document).ready(function(){
        loadConfirmList();
    });

    function loadConfirmList(){
        var url = "/s/confirms/list/" + <?php echo CONFIRM_TYPE_2?> + "/" + <?php echo $id?> + "/" + <?php echo $year?> + "/" + <?php echo $month?>;
        $('#ConfirmList').load(url);
    }


    function editCostDetail(year, month, id, detail_id){
        if (id == undefined) {
            id = 0;
        }
        var url = '/s/costs/detail_edit/'+ year + '/' + month + '/' + id + '/' + detail_id;
        loadBaseWindow(url);
        return false;
    }

    function deleteCost(id){
        if(window.confirm('削除しますがよろしいですか？')){
            var url = "/s/costs/detail_delete/" + id;
            location.replace(url);
        }
    }

    function printCost(id){
        var url = "/s/costs/print/" + id;
        location.replace(url);
    }      
</script>


<blockquote><p><?php echo $this->Html->image("cost.png")?> 経費請求</p></blockquote>
<p class="guide"><?php echo '経費を入力し、申請を行ってください。' ?></p>
<p class="description"><?php echo '※申請後は編集できません。編集を行う場合は、承認者に連絡してください。' ?></p>
<p class="description"><?php echo '※経費を受領後は受領ボタンを押してください。' ?></p>

<?php if (!empty($datas['CostDetail'])) :?>
<div id="ConfirmList"><!-- 申請リスト --></div> 
<?php endif;?>

<div class="center">
<h5>
<a href="/s/costs/index/<?php echo $b_year;?>/<?php echo $b_month;?>"><i class="glyphicon glyphicon-backward"></i> 前月 </a>
<b><?php echo sprintf("%s年 %s月", $year, $month);?></b>
<a href="/s/costs/index/<?php echo $n_year;?>/<?php echo $n_month;?>"> 翌月 <i class="glyphicon glyphicon-forward"></i></a>
</h5>
</div>

<?php if ($status == CONFIRM_YET || $status == CONFIRM_NO):?>
<a href="javascript:void(0);" onclick="editCostDetail(<?php echo $year;?>, <?php echo $month;?>, <?php echo $id;?>);"><i class="glyphicon glyphicon-plus-sign"></i> 新規追加</a>
&nbsp;&nbsp;&nbsp;&nbsp;
<?php endif;?>
<a href="javascript:void(0);" onclick="printCost(<?php echo $id;?>);"><i class="glyphicon glyphicon-print"></i> 出力</a>

<table class="table-bordered table-condensed table-striped table-hover">
<tr>
    <th>No</th>
    <th style="min-width:90px;">日付</th>
    <th style="min-width:400px;">内容</th>
    <th style="min-width:400px;">場所/購入店舗</th>
    <th style="min-width:50px;">単価</th>
    <th style="min-width:50px;">数量</th>
    <th style="min-width:50px;">金額</th>
    <th></th>
</tr>
<?php
$no = 1;
$unit = 0;
$num = 0;
$price = 0;
if (!empty($datas['CostDetail'])) {
foreach ($datas['CostDetail'] as $data):
    $click = "";
    if ($status == CONFIRM_YET || $status == CONFIRM_NO) {
        $click = sprintf(' onclick="editCostDetail(%d, %d, %d, %d);"', $year, $month, $id, $data['id']);
    }   
    $unit += $data['unit'];
    $num += $data['num'];
    $price += $data['price'];
?>
<tr>
    <td <?php echo $click;?>><?php echo h($no++)?></td>
    <td class="center" <?php echo $click;?>><?php echo h($data['date'])?></td>
    <td <?php echo $click;?>><?php echo h($data['purpose'])?></td>
    <td <?php echo $click;?>><?php echo h($data['place'])?></td>
    <td class="number" <?php echo $click;?>><?php echo h(number_format($data['unit']))?></td>
    <td class="number" <?php echo $click;?>><?php echo h(number_format($data['num']))?></td>
    <td class="number" <?php echo $click;?>><?php echo h(number_format($data['price']))?></td>
    <td class="center">
        <?php if ($status == CONFIRM_YET || $status == CONFIRM_NO):?>
        <a href="javascript:void(0);" onclick="deleteCost(<?php echo $data["id"];?>);">
        <i class="glyphicon glyphicon-remove-circle"></i></a>
        <?php endif;?>
    </td>
</tr>
<?php 
endforeach; 
}
?>
<tr>
    <th colspan="4" class="center">合計</th>
    <th class="number"><?php echo h(number_format($unit))?></th>
    <th class="number"><?php echo h(number_format($num))?></th>
    <th class="number"><?php echo h(number_format($price))?></th>
    <th>&nbsp</th>
</tr>
</table>

