<script>
    $(function(){
        var columns = ['#CostDetailDate'];
        $.each(columns, function(i, value) {
            $(value).datepicker({
                dateFormat: "yy-mm-dd"
            });
        });   
        $('#CostDetailPurpose').autocomplete({
            source: '/a/costs/auto_item/' + <?php echo CONFIRM_TYPE_2;?> + "/" + <?php echo COST_SUB_0;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#CostDetailPurpose').html(ui.item.details);
            }}            
        });   
        $('#CostDetailPlace').autocomplete({
            source: '/a/costs/auto_item/' + <?php echo CONFIRM_TYPE_2;?> + "/" + <?php echo COST_SUB_1;?>,
            autoFocus: true,
            select: function(e, ui) {
            if (ui.item) {
                $('#CostDetailPlace').html(ui.item.details);
            }}            
        });  
    });

    function calcPrice() {
        var unit = document.getElementById('CostDetailUnit');
        var num = document.getElementById('CostDetailNum');
        var price  = document.getElementById('CostDetailPrice');

        price.value = Math.round(unit.value * num.value);
    }  

    function updateCostDetail(){
        var element = document.getElementById("CostDetailPrice");
        if(0 == element.value){
            alert("金額を設定してください");
            return false;
        }
    }         
</script>

<blockquote><p>経費登録</p></blockquote>
<p class="text-info">経費情報を入力してください。</p>

<?php echo $this->Form->create('Cost', array("name"=>"CostDetail", "action" => "detail_update",
                                                        "onsubmit" => "return updateCostDetail();"))?>
<?php echo $this->Form->hidden("CostDetail.id")?>
<?php echo $this->Form->hidden("CostDetail.cost_id", array("value"=>$id))?>
<?php echo $this->Form->hidden("CostDetail.staff_id", array("value"=>$staff_id))?>
<?php echo $this->Form->hidden("CostDetail.year", array("value"=>$year))?>
<?php echo $this->Form->hidden("CostDetail.month", array("value"=>$month))?>
<?php echo $this->Form->hidden("CostDetail.enable", array("value"=>1))?>

<table class="form">
<tr>
    <th>日付*</th>
    <td><?php echo $this->Form->text("CostDetail.date", array("empty" => false, "class"=>"form-control input-sm", 'style'=>SIZE_S));?></td>
</tr>
<tr>    
    <th>内容*</th>
    <td><?php echo $this->Form->text('CostDetail.purpose', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_LL));?></td>
</tr>
<tr>
    <th>場所/購入店舗*</th>
    <td><?php echo $this->Form->text('CostDetail.place', array("class"=>"form-control input-sm jpn", 'style'=>SIZE_M));?></td>
</tr>
<tr>
    <th>単価*</th>
    <td><?php echo $this->Form->text('CostDetail.unit', array("class"=>"form-control input-sm number", 'style'=>SIZE_S, 'default'=>0, 'onBlur'=>'calcPrice();', 'type'=>'number'));?></td>
</tr>
<tr>
    <th>数量*</th>
    <td><?php echo $this->Form->text('CostDetail.num', array("class"=>"form-control input-sm number", 'style'=>SIZE_S, 'default'=>1, 'onBlur'=>'calcPrice();', 'type'=>'number'));?></td>
</tr>
<tr>    
    <th>金額*</th>
    <td><?php echo $this->Form->text('CostDetail.price', array("class"=>"form-control input-sm number", 'style'=>SIZE_S, 'default'=>0, 'onBlur'=>'calcPrice();', 'type'=>'number'));?></td>
</tr>
<tr>
    <td colspan="6" class="center">
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-save"></i> 登録</button>
    </td>
</tr>        
</table>

<?php echo $this->Form->end();?>