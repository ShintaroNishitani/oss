<?php
App::uses('AppModel', 'Model');

class Keycard extends AppModel {
	
	var $BeforeFindFlg = true;

	var $validate = array(
		'no'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	  		
	); 	

    public $belongsTo = array(
        'Staff' => array(
            'className'    => 'Staff',
            'foreignKey'   => 'staff_id'
        ),
    );

	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		return $queryData;
	}
}
