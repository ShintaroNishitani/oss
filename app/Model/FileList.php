<?php
/**
* 
*/
class FileList extends AppModel
{
    var $BeforeFindFlg = true;

    // enable = 1のやつだけを取得するためにbeforeFilterを実装す
    function beforeFind($queryData){
    	if ($this->BeforeFindFlg) {
    		if (is_array($queryData['conditions'])) {
    			$queryData['conditions'][$this->alias.'.enable'] = 1;
    		} else if (is_string($queryData['conditions'])) {
    			$queryData['conditions'] = sprintf("(%s) and (%s.enable = 1)", $queryData['conditions'], $this->alias);
    		} else {
    			$queryData['conditions'] = $this->alias.'.enable = 1';
    		}
    	}
    	return $queryData;
    }
}

?>