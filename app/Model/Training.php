<?php
App::uses('AppModel', 'Model');

class Training extends AppModel {
	
	var $BeforeFindFlg = true;

	var $validate = array(
		'name'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	 		
		'staff_id'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	  			
		'date'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	  		
		'difficulty'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	  		    
	); 	

    public $belongsTo = array(
        'Staff' => array(
            'className'    => 'Staff',
            'foreignKey'   => 'staff_id'
        ),
    );

	public $hasMany = array(
        'TrainingProgress' => array(
            'className'    => 'TrainingProgress',
            'foreignKey'   => 'training_id',
            'conditions' => array('enable'=>1),
		),
	);

	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		return $queryData;
	}
}
