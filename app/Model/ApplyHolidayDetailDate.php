<?php
App::uses('AppModel', 'Model');

class ApplyHolidayDetailDate extends AppModel {
	
	var $BeforeFindFlg = true;

	var $validate = array(
		'date'=>array(
			array(		
		      	'rule'=>'notBlank',
			)
	    ),
		'type'=>array(
			array(		
		      	'rule'=>'notBlank',
			)
	    ),
	); 	

	public $belongsTo = array(
		'ApplyHolidayDetail' => array(
			'className' => 'ApplyHolidayDetail',
			'foreignKey' => 'apply_holiday_detail_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);

	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		return $queryData;
	}
}
