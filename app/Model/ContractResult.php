<?php
App::uses('AppModel', 'Model');

class ContractResult extends AppModel {

	var $BeforeFindFlg = true;

	var $validate = array(

	);

	public $belongsTo = array(
		'ContractDetail' => array(
			'className' => 'ContractDetail',
			'foreignKey' => 'contract_detail_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'HumanResource' => array(
			'className' => 'HumanResource',
			'foreignKey' => 'human_resource_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);

	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		return $queryData;
	}
}
