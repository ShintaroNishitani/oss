<?php
App::uses('AppModel', 'Model');

class Confirm extends AppModel {

	public $belongsTo = array(
		'Staff' => array(
			'className' => 'Staff',
			'foreignKey' => 'request_staff_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'CheckStaff' => array(
			'className' => 'Staff',
			'foreignKey' => 'check_staff_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'PrevCheckStaff' => array(
			'className' => 'Staff',
			'foreignKey' => 'prev_check_staff_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
}
