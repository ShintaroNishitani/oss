<?php
App::uses('AppModel', 'Model');

/**
 * Staff
 */
class Staff extends AppModel {
	
	var $BeforeFindFlg = true;

	var $validate = array(
		'no'=>array(
			array(		
				'rule'=>'notBlank',
			)
	    ),		
		'name'=>array(
			array(		
				'rule'=>'notBlank',
			)
	    ),
		'mail'=>array(
			array(		
				'rule'=>'notBlank',
			)
	    ),
		'hire_date'=>array(
			array(		
				'rule'=>'notBlank',
			)
	    ),	    	    
	); 	

	public $belongsTo = array(
		'Authority' => array(
			'className' => 'Authority',
			'foreignKey' => 'authority_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Department' => array(
			'className' => 'Department',
			'foreignKey' => 'department_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);

	public $hasMany = array(
		'PaidHoliday' => array(
			'className'  => 'PaidHoliday',
			'foreignKey' => 'staff_id',
			'conditions' => array('enable'=>1),
		),
		'TransHoliday' => array(
			'className'  => 'TransHoliday',
			'foreignKey' => 'staff_id',
			'conditions' => array('enable'=>1),
		),
		'SpecialHoliday' => array(
			'className'  => 'SpecialHoliday',
			'foreignKey' => 'staff_id',
			'conditions' => array('enable'=>1),
		),
	);

	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		$queryData['order'] = array('Staff.no'=>'asc');
		return $queryData;
	}
}
