<?php
App::uses('AppModel', 'Model');

class TransportDetail extends AppModel {
	
	var $BeforeFindFlg = true;

	var $validate = array(
		'place'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
		'purpose'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
		'transport'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
		'arrival'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
		'departure'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	    	    
		'unit'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
		'num'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
		'price'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	    	    
	); 	

	public $belongsTo = array(
		'Transport' => array(
			'className' => 'Transport',
			'foreignKey' => 'transport_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);

	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		return $queryData;
	}
}
