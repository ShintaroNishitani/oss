<?php
App::uses('AppModel', 'Model');

class ContractDetail extends AppModel {

	var $BeforeFindFlg = true;

	var $validate = array(

	);

	public $belongsTo = array(
		'Contract' => array(
			'className' => 'Contract',
			'foreignKey' => 'contract_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'HumanResource' => array(
			'className' => 'HumanResource',
			'foreignKey' => 'human_resource_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);

	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		return $queryData;
	}
}
