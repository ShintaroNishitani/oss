<?php
App::uses('AppModel', 'Model');

/**
 * Account
 */
class Account extends AppModel {
	
	var $BeforeFindFlg = true;

	var $validate = array(
		'name'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
		'mail'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	  	    
	); 	
	
	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		return $queryData;
	}
}
