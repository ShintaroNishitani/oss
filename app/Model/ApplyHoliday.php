<?php
App::uses('AppModel', 'Model');

class ApplyHoliday extends AppModel {
	
	var $BeforeFindFlg = true;

	var $validate = array(
		'staff_id'=>array(
			array(		
		      	'rule'=>'notBlank',
			)
	    ),	  		
		'year'=>array(
			array(		
		      	'rule'=>'notBlank',
			)
	    ),
		'month'=>array(
			array(		
		      	'rule'=>'notBlank',
			)
	    ),	    
	); 	

    public $belongsTo = array(
        'Staff' => array(
            'className'    => 'Staff',
            'foreignKey'   => 'staff_id'
        ),
    );

	public $hasMany = array(
        'ApplyHolidayDetail' => array(
            'className'    => 'ApplyHolidayDetail',
            'foreignKey'   => 'apply_holiday_id',
            'order' => array('ApplyHolidayDetail.apply_date'),
            'conditions' => array('enable'=>1),
		),
	);

	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		return $queryData;
	}
}
