<?php
App::uses('AppModel', 'Model');

class Order extends AppModel {

	var $BeforeFindFlg = true;

	var $validate = array(
		'date'=>array(
			array(
		      	'rule'=>'notEmpty',
			)
	    ),
		'customer_id'=>array(
			array(
		      	'rule'=>'notEmpty',
			)
	    ),
		'staff_id'=>array(
			array(
		      	'rule'=>'notEmpty',
			)
	    ),
		'remark'=>array(
			array(
		      	'rule'=>'notEmpty',
			)
	    ),
	);

    public $belongsTo = array(
        'Staff' => array(
            'className'    => 'Staff',
            'foreignKey'   => 'staff_id'
        ),
		'Department' => array(
			'className' => 'Department',
			'foreignKey' => 'department_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
    );
}
