<?php
App::uses('AppModel', 'Model');

class Schedule extends AppModel {

	var $BeforeFindFlg = true;

	var $validate = array(
		'date'=>array(
			array(
		      	'rule'=>'notEmpty',
			)
	    ),
		'end_date'=>array(
			array(
		      	'rule'=>'notEmpty',
			)
	    ),
		'title'=>array(
			array(
		      	'rule'=>'notEmpty',
			)
	    ),
	);

    public $belongsTo = array(
        'Staff' => array(
            'className'    => 'Staff',
            'foreignKey'   => 'staff_id'
        ),
    );
}
