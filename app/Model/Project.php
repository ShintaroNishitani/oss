<?php
App::uses('AppModel', 'Model');

class Project extends AppModel {
	
	var $BeforeFindFlg = true;

	var $validate = array(
		'prj_name'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
		'prj_code'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
		's_year'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
		's_month'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	    
	); 	

	public $belongsTo = array(
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'customer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);

	public $hasMany = array(
		'ProjectStaff' => array(
			'className'    => 'ProjectStaff',
			'foreignKey'   => 'prj_id',
			'order' => 'ProjectStaff.id',
			'conditions' => array('enable'=>1),
		),
	);

	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		return $queryData;
	}
}
