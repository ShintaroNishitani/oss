<?php
App::uses('AppModel', 'Model');

class ApplyHolidayDetail extends AppModel {
	
	var $BeforeFindFlg = true;

	var $validate = array(
		'apply_date'=>array(
			array(		
		      	'rule'=>'notBlank',
			)
	    ),
		'reason'=>array(
			array(		
		      	'rule'=>'notBlank',
			)
	    ),
		'status'=>array(
			array(		
		      	'rule'=>'notBlank',
			)
	    ),	    	    
	); 	

	public $belongsTo = array(
		'ApplyHoliday' => array(
			'className' => 'ApplyHoliday',
			'foreignKey' => 'apply_holiday_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Confirm' => array(
			'className' => 'Confirm',
			'foreignKey' => 'confirm_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);

	public $hasMany = array(
        'ApplyHolidayDetailDate' => array(
            'className'    => 'ApplyHolidayDetailDate',
            'foreignKey'   => 'apply_holiday_detail_id',
            'order' => array('ApplyHolidayDetailDate.date'),
            'conditions' => array('enable'=>1),
		),
	);


	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		return $queryData;
	}
}
