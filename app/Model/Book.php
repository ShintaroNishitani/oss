<?php
App::uses('AppModel', 'Model');

class Book extends AppModel {
	
	var $BeforeFindFlg = true;

	var $validate = array(
		'name'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
		'purchase'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
		'price'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
		'number'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
		'purchase_date'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
		'purchaser_id'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
	); 	

    public $belongsTo = array(
        'Staff_U' => array(
            'className'    => 'Staff',
            'foreignKey'   => 'user_id',
        ),
        'Staff_P' => array(
            'className'    => 'Staff',
            'foreignKey'   => 'purchaser_id',
        ),

    );

	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		return $queryData;
	}
}
