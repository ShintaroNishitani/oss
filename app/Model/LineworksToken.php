<?php
App::uses('AppModel', 'Model');

class LineworksToken extends AppModel {

	var $BeforeFindFlg = true;

	var $validate = array(
		'access_token'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	 		
		'access_expired_date'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	  			
		'refresh_token'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	  		
		'refresh_expired_date'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	  		    
	);

	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		return $queryData;
	}
}
