<?php
App::uses('AppModel', 'Model');
/**
 * BoardTree Model
 *
 * @property User $user
 * @property Authority $Authority
 */
class BoardTree extends AppModel {
/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'title';

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
//    public $hasMany = array(
//
//    );


/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */

    var $BeforeFindFlg = false;

    public $belongsTo = array(
        'Staff' => array(
            'className' => 'Staff',
            'foreignKey' => 'modified_staff_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

/**
 * beforeFind Method
 *
 * @var array
 */
    function beforeFind($queryData) 
    {
        //$queryData = AppModel::beforeFind($queryData);
        //$queryData['order'] = array('BoardTree.level1'=>'asc','BoardTree.level2'=>'asc','BoardTree.level3'=>'asc','BoardTree.level4'=>'asc');

        return $queryData;
      }
}
