<?php
App::uses('AppModel', 'Model');

class RuleDetail extends AppModel {
	
	var $BeforeFindFlg = true;

	var $validate = array(
	); 	

    public $belongsTo = array(

    );

	public $hasMany = array(
	);

	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		return $queryData;
	}
}
