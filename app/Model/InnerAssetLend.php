<?php
App::uses('AppModel', 'Model');

class InnerAssetLend extends AppModel {
    public $belongsTo = array(
        'ManagerName' => array(
            'className' => 'Staff',
            'foreignKey' => 'lend_manager_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'RecieverName' => array(
            'className' => 'Staff',
            'foreignKey' => 'rent_staff_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );
    var $BeforeFindFlg = true;

    var $validate = array(
         'lend_manager_id'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
         'division'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
         'rent_date'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),        
        'rent_staff_id'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
        'rent_start_date'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
         'rent_end_date'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
          'rent_object'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
         'rent_approach'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
         'rent_purpose'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
         'delivery_approach'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
         'storage_area'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
         'check_return'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
    );

    /**
     * beforeFind Method
     *
     * @var array
     */
    function beforeFind($queryData) {
        $queryData = AppModel::beforeFind($queryData);
        return $queryData;
    }
}
