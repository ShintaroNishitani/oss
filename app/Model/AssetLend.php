<?php
App::uses('AppModel', 'Model');

class AssetLend extends AppModel {
    public $belongsTo = array(
        'ManagerName' => array(
            'className' => 'Staff',
            'foreignKey' => 'asset_manager_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'RecieverName' => array(
            'className' => 'Staff',
            'foreignKey' => 'receive_user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'CustomerName' => array(
            'className' => 'Customer',
            'foreignKey' => 'customer_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );
    var $BeforeFindFlg = true;

    var $validate = array(
         'asset_lend_number'=>array(
            'number_1' =>array(
                'rule'=>'notEmpty',
            ),
            'number_2' =>array(
                'rule'=>'isUnique',
            )
        ),
         'asset_manager_id'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
         'costomer_id'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
         'division'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
         'info_manager_department'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
        'info_manager_name'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
        'lend_department'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
        'lend_user'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
        'lend_date'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
        'receive_user_id'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
        'receive_date'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
         'receive_date'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
          'lend_start_date'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
         'lend_end_date'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
         'lend_object'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
         'lend_approach'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
         'storage_area'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
         'check_return'=>array(
            array(
                'rule'=>'notEmpty',
            )
        ),
    );

    /**
     * beforeFind Method
     *
     * @var array
     */
    function beforeFind($queryData) {
        $queryData = AppModel::beforeFind($queryData);
        return $queryData;
    }
}
