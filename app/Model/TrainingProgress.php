<?php
App::uses('AppModel', 'Model');

class TrainingProgress extends AppModel {
	
	var $BeforeFindFlg = true;

	var $validate = array(
		'staff_id'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
		'term_s'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
		'term_e'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
	); 	

    public $belongsTo = array(
        'Staff' => array(
            'className'    => 'Staff',
            'foreignKey'   => 'staff_id'
        ),
        'Training' => array(
            'className'    => 'Training',
            'foreignKey'   => 'training_id'
        ),        
    );

	public $hasMany = array(
        'TrainingProgressDetail' => array(
            'className'    => 'TrainingProgressDetail',
            'foreignKey'   => 'training_progress_id',
            'conditions' => array('enable'=>1),
		),
	);

	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		return $queryData;
	}
}
