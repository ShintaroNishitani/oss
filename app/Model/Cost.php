<?php
App::uses('AppModel', 'Model');

class Cost extends AppModel {
	
	var $BeforeFindFlg = true;

	var $validate = array(
		'staff_id'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	  		
		'year'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
		'month'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	    
	); 	

    public $belongsTo = array(
        'Staff' => array(
            'className'    => 'Staff',
            'foreignKey'   => 'staff_id'
        ),
    );

	public $hasMany = array(
        'CostDetail' => array(
            'className'    => 'CostDetail',
            'foreignKey'   => 'cost_id',
            'order' => 'CostDetail.date',
            'conditions' => array('enable'=>1),
		),
	);

	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		return $queryData;
	}
}
