<?php
App::uses('AppModel', 'Model');

class SaleProject extends AppModel {

	var $BeforeFindFlg = true;

	var $validate = array(
		'name'=>array(
			array(
		      	'rule'=>'notBlank',
			)
	    ),
		'company_id'=>array(
			array(
		      	'rule'=>'notBlank',
			)
	    ),
	);

    public $belongsTo = array(
        'Company' => array(
            'className'    => 'Company',
            'foreignKey'   => 'company_id'
        ),
    );

	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		$queryData['order'] = array('SaleProject.created'=>'desc');
		return $queryData;
	}
}
