<?php
App::uses('AppModel', 'Model');

class PaidHoliday extends AppModel {
	
	var $BeforeFindFlg = true;

	var $validate = array( 		
		'id'=>array(
			array(		
		      	'rule'=>'notBlank',
			)
	    ),   
		'staff_id'=>array(
			array(		
		      	'rule'=>'notBlank',
			)
	    ),	 
		'holiday_remain'=>array(
			array(		
		      	'rule'=>'notBlank',
			)
	    ), 
		'half_remain'=>array(
			array(		
		      	'rule'=>'notBlank',
			)
	    ),
        'year'=>array(
			array(		
		      	'rule'=>'notBlank',
			)
	    ),
        'make_day_time'=>array(
			array(		
		      	'rule'=>'notBlank',
			)
	    ),
        'update_day_time'=>array(
			array(		
		      	'rule'=>'notBlank',
			)
	    ),
	); 	

    public $belongsTo = array(
        'Staff' => array(
            'className'    => 'Staff',
            'foreignKey'   => 'staff_id'
        ),
    );
}
