<?php
App::uses('AppModel', 'Model');

class Kintai extends AppModel {

	var $BeforeFindFlg = true;

	var $validate = array(
		'staff_id'=>array(
			array(
		      	'rule'=>'notBlank',
			)
	    ),
		'year'=>array(
			array(
		      	'rule'=>'notBlank',
			)
	    ),
		'month'=>array(
			array(
		      	'rule'=>'notBlank',
			)
	    ),
	);

	public $belongsTo = array(
		'Staff' => array(
			'className'    => 'Staff',
			'foreignKey'   => 'staff_id'
		),
		'CheckStaff' => array(
			'className'    => 'Staff',
			'foreignKey'   => 'check_staff_id'
		),
    );

	public $hasMany = array(
		'KintaiDetail' => array(
			'className'    => 'KintaiDetail',
			'foreignKey'   => 'kintai_id',
			'order' => 'KintaiDetail.date',
			'conditions' => array('enable'=>1),
		),
	);

	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		return $queryData;
	}
}
