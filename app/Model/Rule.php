<?php
App::uses('AppModel', 'Model');

class Rule extends AppModel {
	
	var $BeforeFindFlg = true;

	var $validate = array(
	); 	

    public $belongsTo = array(
        'RuleCategory' => array(
            'className'    => 'RuleCategory',
            'foreignKey'   => 'category'
        ),
    );

	public $hasMany = array(
        'RuleDetail' => array(
            'className'    => 'RuleDetail',
            'foreignKey'   => 'rule_id',
            'order' => 'RuleDetail.index',
            'conditions' => array('enable'=>1),
        ),	
	);

	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		return $queryData;
	}
}
