<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
	function beforeSave($options = array()) {
	  foreach ($this->_schema as $field => $properties) {
	    if ($this->_schema[$field]['type'] === 'integer' && isset($this->data[$this->alias][$field])) {
	    	$this->data[$this->alias][$field] = str_replace(',','',$this->data[$this->alias][$field]);
	    }
	  }
	  return true;
	}

    public function unbindModelAll($keys = null, $reset = true){
        $binds = array();
        $count = 0;
        // もし何も入ってなかったら全部切る
        if ($keys == null) {
            $keys = array(
                "hasMany", "belongsTo", "hasOne", "hasAndBelongsToMany",
            );
        }
        foreach ($keys as $key) {
            $binds[$key] = array();
            foreach ($this->$key as $k => $v) {
                $binds[$key][] = $k;
            }
            $count += count($binds[$key]);
        }
        if ($count) {
            $this->unbindModel($binds, $reset);
        }
    }

	public function begin() {
	    $db = ConnectionManager::getDataSource($this->useDbConfig);
	    $db->begin($this);
	}
	public function commit() {
	    $db = ConnectionManager::getDataSource($this->useDbConfig);
	    $db->commit($this);
	}
	public function rollback() {
	    $db = ConnectionManager::getDataSource($this->useDbConfig);
	    $db->rollback($this);
	}


	var $BeforeFindFlg = false;

	/**
	 * [beforeFind description]
	 * @param  [type] $queryData [description]
	 * @return [type]            [description]
	 */
	function beforeFind($queryData) {
	if ($this->BeforeFindFlg) {
		if (is_array($queryData['conditions'])) {
	        // $queryData['conditions'][$this->name.'.enable'] = 1;
			$queryData['conditions'][$this->alias.'.enable'] = 1;
		} else if (is_string($queryData['conditions'])) {
			$queryData['conditions'] = sprintf("(%s) and (%s.enable = 1)", $queryData['conditions'], $this->alias);
		} else {
			$queryData['conditions'] = $this->alias.'.enable = 1';
		}
	}

	return $queryData;
	}	
}
