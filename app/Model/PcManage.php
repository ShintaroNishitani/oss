<?php
App::uses('AppModel', 'Model');

class PcManage extends AppModel {
	
	var $BeforeFindFlg = true;

	var $validate = array(
		'manage_no'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	  		
		'pc_name'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	  		
		'maker'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	  		
		'model'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	  		
		'get_date'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	  		
		'os'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	  		
		'service'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	  		
	); 	

    public $belongsTo = array(
        'Staff' => array(
            'className'    => 'Staff',
            'foreignKey'   => 'staff_id'
        ),
    );

	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		return $queryData;
	}
}
