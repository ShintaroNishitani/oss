<?php
App::uses('AppModel', 'Model');

class SpecialHoliday extends AppModel {
	
	var $BeforeFindFlg = true;

	var $validate = array(
		'id'=>array(
			array(
		      	'rule'=>'notBlank',
			)
	    ),   
		'staff_id'=>array(
			array(
		      	'rule'=>'notBlank',
			)
	    ),	 
        'start_date'=>array(
			array(
		      	'rule'=>'notBlank',
			)
	    )
	);

    public $belongsTo = array(
        'Staff' => array(
            'className'    => 'Staff',
            'foreignKey'   => 'staff_id'
        ),
    );
}
