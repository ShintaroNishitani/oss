<?php
App::uses('AppModel', 'Model');

class Contract extends AppModel {

	var $BeforeFindFlg = true;

	var $validate = array(
		// 'term_s'=>array(
		// 	array(
		//       	'rule'=>'notEmpty',
		// 	)
	 //    ),
		// 'term_e'=>array(
		// 	array(
		//       	'rule'=>'notEmpty',
		// 	)
	 //    ),
	);

	public $belongsTo = array(
		'SaleProject' => array(
			'className' => 'SaleProject',
			'foreignKey' => 'sale_project_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ReceiveCompany' => array(
			'className' => 'Company',
			'foreignKey' => 'receive_company_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'OrderCompany' => array(
			'className' => 'Company',
			'foreignKey' => 'order_company_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);

	public $hasMany = array(
        'ContractDetail' => array(
            'className'    => 'ContractDetail',
            'foreignKey'   => 'contract_id',
            'conditions' => array('enable'=>1),
		),
	);
}
