<?php
App::uses('AppModel', 'Model');
/**
 * BoardTempfile Model
 * 作業項目モデル
 */
class BoardTempfile extends AppModel {
	public $displayField = 'filename';
	
  
  	public $belongsTo = array(
		'BoardBody' => array(
			'className' => 'BoardBody',
			'foreignKey' => 'body_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
}

