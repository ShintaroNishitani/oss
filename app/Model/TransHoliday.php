<?php
App::uses('AppModel', 'Model');

class TransHoliday extends AppModel {
	
	var $BeforeFindFlg = true;

	var $validate = array( 		
		'id'=>array(
			array(		
		      	'rule'=>'notBlank',
			)
	    ),   
		'staff_id'=>array(
			array(		
		      	'rule'=>'notBlank',
			)
	    ),	 
		'accrual_date'=>array(
			array(		
		      	'rule'=>'notBlank',
			)
	    ), 
		'digestion_date1'=>array(
			array(		
		      	'rule'=>'notBlank',
			)
	    ),
                'created'=>array(
			array(		
		      	'rule'=>'notBlank',
			)
	    ),
                'modified'=>array(
			array(		
		      	'rule'=>'notBlank',
			)
	    ),
	); 	

    public $belongsTo = array(
        'Staff' => array(
            'className'    => 'Staff',
            'foreignKey'   => 'staff_id'
        ),
    );
}
