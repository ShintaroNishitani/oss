<?php
App::uses('AppModel', 'Model');

class Holiday extends AppModel {
	
	var $validate = array(
		'name'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),	  		
		'date'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
	); 	

}
