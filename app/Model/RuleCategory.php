<?php
App::uses('AppModel', 'Model');

class RuleCategory extends AppModel {
	
	var $BeforeFindFlg = true;

	var $validate = array(    
	); 	

	public $belongsTo = array(
	);

	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		return $queryData;
	}
}
