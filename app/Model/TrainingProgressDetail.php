<?php
App::uses('AppModel', 'Model');

class TrainingProgressDetail extends AppModel {
	
	var $BeforeFindFlg = true;

	var $validate = array(
		'content'=>array(
			array(		
		      	'rule'=>'notEmpty',
			)
	    ),
	); 	

    public $belongsTo = array(
        'Staff' => array(
            'className'    => 'Staff',
            'foreignKey'   => 'staff_id'
        ),
        'TrainingProgress' => array(
            'className'    => 'TrainingProgress',
            'foreignKey'   => 'training_progress_id'
        ),        
    );

	/**
	 * beforeFind Method
	 *
	 * @var array
	 */
	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		return $queryData;
	}
}
