<?php
App::uses('AppModel', 'Model');

class Company extends AppModel {

	var $BeforeFindFlg = true;

	function beforeFind($queryData) {
		$queryData = AppModel::beforeFind($queryData);
		$queryData['order'] = array('Company.name'=>'asc');
		return $queryData;
	}
}
