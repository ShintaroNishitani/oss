function flashEffect(id){
    if (id == undefined) {
        id = "#flashMessage";
    }
	var obj = $(id);

	if(obj != null) {
	    var duration = 3000;
	    if (obj.attr("class") == 'error'){
            duration = 5000;
        }
        obj.fadeOut(duration);
	}
}
function myAlert(str){
    $('#myAlert').html(str).css("display","block");
    flashEffect("#myAlert");
}
function loadBaseWindow(url, callback){
    var top = 15;
    var bottom = 15;
    $.ajax({
        type: "GET",
        beforeSend: function(){
            $('#Overlay').css("display", "block");
        },
        complete: function(){
            var left = Math.floor(($(window).width() - $('#BaseWindow').width()) / 2) - 30;
            // var top = Math.floor(($(window).height() - $('#BaseWindow').height()) / 2);
            var top = 15;
            if ($('#BaseWindow').height() > $(window).height() - top - bottom) {
                $('#BaseWindowContent').css("height", $(window).height() - top - bottom - 20);
                $('#BaseWindow').css("width", $('#BaseWindow').width() + 20);
            } else {
                $('#BaseWindowContent').css("height", "auto");
            }
            $('#BaseWindow').css('top', top).css('left', left).css("display", "block");

            if (callback != undefined) {
                callback();
            }
        },
        url: url,
        success: function(html){
            $('#BaseWindowContent').html(html);
        }
    });
}

function unloadBaseWindow(){
    $('#BaseWindow').css("display", "none");
}
function unloadOverlay(){
    $("#Overlay").css("display", "none");
}
function unloadAll(){
    unloadBaseWindow();
    unloadOverlay();
}
function simpleLoad(id, url, callback){
    $.ajax({
        beforeSend: function(){
            var offset = $('#' + id).offset();
            $('#Mask').css('width', $('#' + id).width());
            $('#Mask').css('height', $('#' + id).height());
            $('#Mask').css('top', offset.top).css('left', offset.left);
            $('#Mask').css('display', 'block');
        },
        complete: function(){
            $('#Mask').css('display', 'none');
            if (typeof(callback) == "function") {
                callback();
            }
        },
        url: url,
        success: function(html){
            $('#' + id).html(html);
            $('#Mask').html('Loading...');
        }
    });
}
function updateData(url, id, requires, callback){
    if (requires) {
        var error = 0;
        for (var i = 0; i < requires.length; i++) {
            if ($('#' + requires[i]).val().length == 0) {
                $('#' + requires[i]).css("background-color", "#FCC");
                error++;
            } else {
                $('#' + requires[i]).css("background-color", "#FFF");
            }
        }
        if (error) {
            myAlert("Please input require fields.");
            return false;
        }
    }
    var method = "get";
    var data = null;
    if (id) {
        method = "post";
        data = $("#" + id).serializeArray();
    }
    $.ajax({
        type: method,
        url: url,
        cache: false,
        data: data,
        success: function(html){
            // debugAlert(html);
            var res = eval('(' + html + ')');
            if (res["error"] == 0) {
                if (typeof(callback) == "function") {
                    callback();
                }
            }
            myAlert(res["message"]);
        }
    });
    
}

function submitStop(e){
    if (!e) var e = window.event;
 
    if(e.keyCode == 13)
        return false;
}

/** 
　* Enterキーの禁止 
　*/ 
function PressedEnter(){
        var list = document.getElementsByTagName("input");
        for(var i=0; i<list.length; i++){
        if(list[i].type == 'text' || list[i].type == 'password'){
            list[i].onkeypress = function (event){
                return submitStop(event);
            };
        }
    }
}

/** 
　* BackSpaceキーの禁止 
　* @returns {Boolean} 
　*/ 
function keys(eve){

　// ＩＥ と FireFox の両方のイベントを受け取る為の処理
　var winjudge = false ; 
　if(eve==null){
　　eve = window.event ; 
　　winjudge = true ;
　}

　// ＩＥ と FireFox の両方のキーコードを受け取る為の処理
　var keyCode = eve.which ? eve.which : eve.keyCode ; 
　switch ( keyCode ){

　　　case 8: // BackSpace
　　　　if(eve.srcElement){ 
　　　　　idname = eve.srcElement.id; 
　　　　}else{ 
　　　　　idname = eve.target.id; 
　　　　}
　　　　// ID（id）が定義されている場合
　　　　if(idname != undefined && idname != ""){

　　　　　type = document.getElementById(idname).type ; 
　　　　　actv = document.getElementById(idname).disabled ;

　　　　　// text の場合のみ BackSpace を許可する
　　　　　if(type=="text" || type=="textarea" && actv == false){
　　　　　　return true ;
　　　　　}else{
　　　　　　// win 系の場合 キーコードに 0 を設定
　　　　　　if(winjudge) eve.keyCode = 0; 
　　　　　　return false; 
　　　　　} 
　　　　}else{
　　　　　if(winjudge) eve.keyCode = 0;
　　　　　return false;
　　　　}
    }


}

//数字を3桁区切りにする
function chgNumber() {
    var elements = document.getElementsByClassName("number"); 
    //console.log(elements);
    for (i = 0; i < elements.length; i++) {
        before_val = elements[i].value;
        if(before_val != undefined){
            after_val = before_val.toString().replace( /([0-9]+?)(?=(?:[0-9]{3})+$)/g , '$1,' );
            elements[i].value = after_val;
        }
    }
}