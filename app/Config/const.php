<?php
define('LW_APIID', "jp1OQoKmQiObs");
define('LW_BOTNO', "3909060");
define('LW_TOKEN', "AAABCjM5gVMoFpxR/oAFJamT+pTfRjpKncjivtEWqxyS+JgSY0AoXldEe4DhVIKCdtudAijLHxXVD+UKhlK/y01fqEX0YbbM2S85UHAbiEsSdsHzcwNqTjdEXouJywpnrQxmR9vNfS34Na9s2NROOII8TzPQFzl9ADa4gjucQ54Y7QM6qDxlXyNorg1vhJfFJaflGU3rp93Y20M12crk6oDNCk3D+mtfysrawndcCA/N3JXaO6OvlntWrN/iaMHhiXz2GjCsG1x3If3Xiiy4js/dIPsTHbl/gaxDj1Bi69CkSUCisIql9l+xLBXLrkohmkuo8kGPUlMfdQWMasA8GfZZixAECzTYqKirdOGIL3k3JGqy");
define('LW_CONSUMERKEY', "DukZ2HA1tlEohxpE0oI9");
define('LW_CLIENTID', 'TQvAWGTebdFJ0LxnJCEu');
define('LW_CLIENTSECRET', 'F_IJBFz5MA');
define('LW_SERVICEACCOUNT', '9jfyt.serviceaccount@oceansoftware');
define('LW_BOTID', '5296111');
define('LW_URL_TOKEN', 'https://auth.worksmobile.com/oauth2/v2.0/token');
define('LW_URL_MSG', 'https://www.worksapis.com/v1.0/bots/%s/users/%s/messages');

define('ONE', 1);
define('TWO', 2);

define('SIZE_XS', "width:50px;");
define('SIZE_S',  "width:100px;");
define('SIZE_M',  "width:200px;");
define('SIZE_L',  "width:300px;");
define('SIZE_LL', "width:400px;");
define('NUMBER',  "width:100px;text-align:right;");
define('SIZE_SB_TIME', "width:70px;");                      // セレクトボックス : 時刻表記
define('SIZE_TX_TIME', "width:45px;");                      // テキストボックス : 時間表記
define('SIZE_SB_APPL', "width:80px;");                      // テキストボックス : 摘要欄

// 申請ステータス
define('CONFIRM_YET', 0);    // 未申請
define('CONFIRM_IN', 1);     // 申請済
define('CONFIRM_OK', 2);     // 承認済
define('CONFIRM_NO', 3);     // 差戻

// 届出種別
define('CONFIRM_TYPE_0', 0);    // 勤怠
define('CONFIRM_TYPE_1', 1);    // 交通費精算
define('CONFIRM_TYPE_2', 2);    // 経費精算
define('CONFIRM_TYPE_3', 3);    // 旅費精算
// define('CONFIRM_TYPE_4', 4);    // 通勤届
// define('CONFIRM_TYPE_5', 5);    // 慶弔届
// define('CONFIRM_TYPE_6', 6);    // 身上届
// define('CONFIRM_TYPE_7', 7);    // 給与振込口座届
define('CONFIRM_TYPE_8', 8);    // 休出・休暇申請

// 受領ステータス
define('RECEIVE_YET', 0);    // 未受領
define('RECEIVE_OK', 1);     // 受領済

// 休暇種別
/** 有給休暇 */
define('HOLIDAY_PAID_ALL', '0');
/** 午前休暇 */
define('HOLIDAY_MORNING_OFF', '1');
/** 午後休暇 */
define('HOLIDAY_AFTERNOON_OFF', '2');
/** 振替休暇 */
define('HOLIDAY_OBSERVED_ALL', '3');
/** 振替半休 */
define('HOLIDAY_OBSERVED_HALF', '4');
/** 特別休暇 */
define('HOLIDAY_SPECIAL', '5');
/** 欠勤 */
define('HOLIDAY_ABSENCE', '6');
/** 休職 */
define('HOLIDAY_LEAVE', '7');
/** その他 */
define('HOLIDAY_OTHER', '8');
/** 代休 */
define('HOLIDAY_COMPENSATORY', '9');
/** 午前代休 */
define('HOLIDAY_COMPENSATORY_MORNING', '10');
/** 午後代休 */
define('HOLIDAY_COMPENSATORY_AFTERNOON', '11');
/** 慶弔休暇 */
define('HOLIDAY_CONDOLENCE_LEAVE', '12');
/** 夏季休暇 */
define('HOLIDAY_SUMMER', '13');
/** リフレッシュ休暇 */
define('HOLIDAY_REFRESH', '14');
/** 休日出勤 */
define('HOLIDAY_WORK', '15');



// 勤怠プロジェクト編集サブインデックス
define('KINTAI_SUB_0', 0);   // プロジェクト１

// 交通費精算サブインデックス
define('TRANSPORT_SUB_0', 0);   // 行先
define('TRANSPORT_SUB_1', 1);   // 目的
define('TRANSPORT_SUB_2', 2);   // 利用交通機関
define('TRANSPORT_SUB_3', 3);   // 駅

// 経費精算サブインデックス
define('COST_SUB_0', 0);        // 内容
define('COST_SUB_1', 1);        // 場所/購入店舗

// 承認依頼メール
define('CONFIRM_MAIL', "toshi_ogasawara@oceansoftware.co.jp");
define('CONFIRM_MAIL2', "toyo_akeyama@oceansoftware.co.jp");
define('CONFIRM_TITLE', "【Ocean】承認依頼 %s");
define('CONFIRM_BODY', "%sさんより承認依頼がありました。\n\nシステムにて内容を確認の上、承認をお願いいたします。\n\n https://oss.os-sys.net/");
define('CONFIRM_TYPE8_BODY', "%sさんより休出・休暇申請の承認依頼がありました。\n\nシステムにて内容を確認の上、承認をお願いいたします。\n\n https://oss.os-sys.net/");

define('CONFIRM_TITLE_NO', "【Ocean】差戻 %s");
define('CONFIRM_BODY_NO', "承認依頼の差戻しがありました。\n\n理由：%s\n\nシステムにて内容を確認の上、再度申請をお願いいたします。\n\n https://oss.os-sys.net/");
define('CONFIRM_TYPE8_BODY_NO', "%sさんより休出・休暇申請の差戻しがありました。\n\n理由：%s\n\nシステムにて内容を確認の上、再度申請をお願いいたします。\n\n https://oss.os-sys.net/");

// 交通費請求未申請警告メール
define('TRANSPORT_MAIL_DAY', 3);  // 月例会の何日前に通知するか
define('TRANSPORT_MAIL_TITLE', "【Ocean】交通費請求　未申請のお知らせ");
define('TRANSPORT_MAIL_BODY', "今月の交通費請求が申請されていません。\n\nシステムにて申請をお願いいたします。\n\n https://oss.os-sys.net/");

// 受領ボタン未押下警告メール
define('RECEIVE_MAIL_TITLE', "【Ocean】%s　受領ボタンを押してください");
define('RECEIVE_MAIL_BODY', "今月の%sの受領ボタンが押されていません。\n\nシステムにて受領ボタンを押してください。\n\n https://oss.os-sys.net/");

// セキュリティ資産貸出書
define('SECURITY_ASSET_ALERT', "お疲れ様です。\nセキュリティ担当からのお知らせです。\n客先からの貸与資産（入館カード、在宅用PC）の借用や返却など変更がありましたら、資産貸出書の作成をお願い致します。\n※ご不明な点がございましたら、セキュリティ担当まで連絡ください");

// オーダー種別
define('ORDER_TYPE_0', 0);      // 見積書番号
define('ORDER_TYPE_1', 1);      // 請求書番号
define('ORDER_TYPE_2', 2);      // 納品書番号
define('ORDER_TYPE_3', 3);      // 予備番号
define('ORDER_TYPE_4', 4);      // 注文番号
define('ORDER_TYPE_5', 5);      // 契約書番号

// 作業形態種別
define('WORK_KD_UNKNOWN', 0);       // 不明
define('WORK_KD_PERFORMANCE', 1);   // 実績
define('WORK_KD_CONTRACTORS', 2);   // 請負
define('WORK_KD_TRAINING', 3);      // 研修
define('WORK_KD_OTHER', 4);         // その他

// GoogleApiKey
define('GOOGLE_API_KEY', 'AIzaSyC0a237c6_iVtgrHDrTELRKFL7G3cEpYRs');

// 既読・未読ステータス
define('READ_STATUS_UNREAD', 0);    // 未読
define('READ_STATUS_READ', 1);      // 既読

// 交通費区分
/** 定期 */
define('TRANSPORT_KBN_COMMUTER', 1);
/** 小口精算 */
define('TRANSPORT_KBN_SMALL', 2);

// 36協定最大労働時間
define('MAX_EXTRA_TIME', 45);

// 部長
define('RANK_HEAD_DEPARTMENT', 2);
// マネージャ
define('RANK_MANAGER', 3);
// リーダー
define('RANK_LEADER', 4);
// チーフ
define('RANK_CHIEF', 5);




#===============================================================================
# コード定義
#===============================================================================
// 客先請求
$config['transport_customer'] = array(
    0=>"-",
    1=>"○",
);

// 申請ステータス
$config['confirm_status'] = array(
    CONFIRM_YET=>"未申請",
    CONFIRM_IN=>"申請済",
    CONFIRM_OK=>"承認済",
    CONFIRM_NO=>"差戻",
);

// 申請種別
$config['confirm_type'] = array(
    CONFIRM_TYPE_0=>"勤怠",
    CONFIRM_TYPE_1=>"交通費精算",
    CONFIRM_TYPE_2=>"経費精算",
    CONFIRM_TYPE_3=>"旅費精算",
    // CONFIRM_TYPE_4=>"通勤届",
    // CONFIRM_TYPE_5=>"慶弔届",
    // CONFIRM_TYPE_6=>"身上届",
    // CONFIRM_TYPE_7=>"給与振込口座届",
    CONFIRM_TYPE_8=>"休出・休暇申請",
);

// 受領有無
$config['receive_status'] = array(
    RECEIVE_YET=>"未受領",
    RECEIVE_OK=>"受領済",
);

// 休暇種別
$config['holiday_type'] = array(
    HOLIDAY_PAID_ALL=>"有給休暇",
    HOLIDAY_MORNING_OFF=>"午前半休",
    HOLIDAY_AFTERNOON_OFF=>"午後半休",
    HOLIDAY_OBSERVED_ALL=>"振替休日",
    HOLIDAY_OBSERVED_HALF=>"振替半休",
    HOLIDAY_SPECIAL=>"特別休暇",
    HOLIDAY_ABSENCE=>"欠勤",
    HOLIDAY_LEAVE=>"休職",
    HOLIDAY_OTHER=>"――――",
);

// 課題言語
$config['training_lang'] = array(
    0=>"共通",
    1=>"C",
    2=>"C++",
    3=>"C#",
    4=>"VB.NET",
    5=>"ExcelVBA",
    6=>"PHP",
    7=>"Java",
    8=>"その他",
);

// 課題種別
$config['training_type'] = array(
    0=>"プログラム課題",
    1=>"ロジック課題",
    2=>"フローチャート課題",
    3=>"その他",
);

// 課題難易度
$config['training_difficulty'] = array(
    0=>"低",
    1=>"中",
    2=>"高",
);

// 評価ランク
$config['training_rank'] = array(
    0=>"◎",
    1=>"○",
    2=>"△",
    3=>"×",
);

// スケジュール公開有無
$config['schedule_public'] = array(
    0=>"公開",
    1=>"非公開",
);

// オーダー種別
$config['order_type'] = array(
    ORDER_TYPE_0=>"見積書番号",
    ORDER_TYPE_1=>"請求書番号",
    ORDER_TYPE_2=>"納品書番号",
    ORDER_TYPE_3=>"予備番号",
    ORDER_TYPE_4=>"注文番号",
    ORDER_TYPE_5=>"契約書番号",
);

// 作業形態種別
$config['work_kind'] = array(
    WORK_KD_UNKNOWN=>"不明",
    WORK_KD_PERFORMANCE=>"実績",
    WORK_KD_CONTRACTORS=>"請負",
    WORK_KD_TRAINING=>"研修",
    WORK_KD_OTHER=>"その他",
);

// 外国籍
$config['foreign'] = array(
    0=>"不可",
    1=>"可",
);

// 性別
$config['sex'] = array(
    0=>"男性",
    1=>"女性",
);

// 口座種別
$config['acc_type'] = array(
    0=>"普通",
    1=>"当座",
);

// 小数点計算
$config['calc'] = array(
    0=>"四捨五入",
    1=>"切り捨て",
    2=>"切り上げ",
);

// 上下限時間
$config['maxmin_time'] = array(
    0=>140,
    1=>150,
    2=>160,
    3=>180,
    4=>200,
);

// 工程
$config['work_proc'] = array(
    0=>"要件定義",
    1=>"基本設計",
    2=>"詳細設計",
    3=>"製造",
    4=>"テスト",
    5=>"運用保守",
);

// 期間
$config['work_term'] = array(
    0=>"即日",
    1=>"長期",
);

// 雇用形態
$config['employment_type'] = array(
    0=>"正社員",
    1=>"契約社員",
    2=>"個人事業主",
);

// 契約形態
$config['contract_type'] = array(
    0=>"派遣契約",
    1=>"請負契約",
);

// 請求書
$config['invoice'] = array(
    0=>"未",
    1=>"済",
);

// 区分(情報資産)
$config['loan_kbn'] = array(
    0=>"",
    1=>"借用",
    2=>"貸与",
    3=>"複製",
    4=>"複製貸与",
);

// 貸借方法
$config['loan_type'] = array(
    0=>"",
    1=>"CD",
    2=>"DVD",
    3=>"ストレージ",
    4=>"メール添付",
    5=>"郵送",
    6=>"その他",
);

// 返却要否
$config['return_type'] = array(
    0=>"",
    1=>"要",
    2=>"否",
);

// 返却状態
$config['already_return'] = array(
    0=>"未",
    1=>"済",
);

// 返却方法
$config['return_means'] = array(
    0=>"CD",
    1=>"DVD",
    2=>"サーバ経由",
    3=>"物理的破棄",
    4=>"郵送",
    5=>"削除",
    6=>"手渡し",
);

// 契約種別
$config['contract_type'] = array(
    1=>"派遣",
    2=>"請負",
    3=>"SES",
    4=>"準委任",
    5=>"顧問",
    0=>"その他",
);

// 案件種別
$config['is_mix'] = array(
    0=>"通常案件",
    1=>"MIX案件",
);

// 休出・休暇申請種別
$config['apply_holiday_type'] = array(
    HOLIDAY_PAID_ALL=>"有給休暇",
    HOLIDAY_MORNING_OFF=>"午前半休",
    HOLIDAY_AFTERNOON_OFF=>"午後半休",
    HOLIDAY_OBSERVED_ALL=>"振替休日",
    HOLIDAY_OBSERVED_HALF=>"振替半休",
    HOLIDAY_CONDOLENCE_LEAVE=>"慶弔休暇",
    HOLIDAY_SUMMER=>"夏季休暇",
    HOLIDAY_REFRESH=>"リフレッシュ休暇",
    HOLIDAY_WORK=>"休日出勤",
);
