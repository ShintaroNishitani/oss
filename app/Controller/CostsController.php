<?php
App::uses('AppController', 'Controller');

/**
 * 経費
 */
class CostsController extends AppController {
    var $uses = array('Cost', 'Confirm', 'Staff');

    public $paginate = array(
        'page' => 1,
        'conditions' => array(''),
        );

    /**
     * [s_index 一覧]
     * @param  [type] $year     [年]
     * @param  [type] $month    [月]
     * @param  [type] $staff_id [スタッフID]
     * @return [type]           [None]
     */
    function s_index($year = null, $month = null){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        if ($year == null || $month == null) {
            $year = date('Y');
            $month = date('n');
        }

        //前月、翌月を取得
        $this->Common->getBeforeMonth($year, $month, $b_year, $b_month);
        $this->Common->getNextMonth($year, $month, $n_year, $n_month);

        $staff_id = $this->Session->read('my_staff_id');
        
        // 検索条件作成
        $con = array("Cost.year"=>$year,
                     "Cost.month"=>$month,
                     "Cost.staff_id"=>$staff_id);

        $datas = $this->Cost->find('first', array('conditions'=>$con));
        if (empty($datas)) {
            // 経費保存
            $this->Cost->create();
            $data['Cost'] = array('staff_id'=>$staff_id,
                                       'year'=>$year,
                                       'month'=>$month,
                                       'status'=>0,
                                       'enable'=>1,
                                      );
            $this->Cost->save($data);
            $id = $this->Cost->id;            
        } else {
            $id = $datas['Cost']['id'];
        }

        $status = 0;
        $confirm = $this->Confirm->find('first', array('conditions'=>array('type'=>CONFIRM_TYPE_2, 'type_id'=>$id)));
        if ($confirm) {
            $status = $confirm['Confirm']['status'];
        }

        $this->set(compact('year', 'month', 'b_year', 'b_month', 'n_year', 'n_month', 'datas', 'id', 'status'));

        $this->set('title_for_layout', '経費精算');
    }

    /**
     * [a_index 一覧]
     * @param  [type] $year     [年]
     * @param  [type] $month    [月]
     * @param  [type] $staff_id [スタッフID]
     * @return [type]           [None]
     */
    function a_index($year, $month, $staff_id){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        //前月、翌月を取得
        $this->Common->getBeforeMonth($year, $month, $b_year, $b_month);
        $this->Common->getNextMonth($year, $month, $n_year, $n_month);

        // 検索条件作成
        $con = array("Cost.year"=>$year,
                     "Cost.month"=>$month,
                     "Cost.staff_id"=>$staff_id);

        $datas = $this->Cost->find('first', array('conditions'=>$con));

        $this->set(compact('year', 'month', 'b_year', 'b_month', 'n_year', 'n_month', 'datas', 'staff_id'));

        $this->set('title_for_layout', '経費精算');
    }

    /**
     * [s_detail_edit 詳細更新]
     * @param  [type] $year      [年]
     * @param  [type] $month     [月]
     * @param  [type] $id        [ID]
     * @param  [type] $detail_id [詳細ID]
     * @return [type]            [None]
     */
    function s_detail_edit($year, $month, $id, $detail_id = null){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        $this->layout = "ajax";
        $this->Cost->unbindModelAll();
        $data = $this->Cost->CostDetail->find("first", array("conditions" => array("CostDetail.id"=>$detail_id)));
        if (!empty($data)) {
            $this->data = $data;
        }

        $staff_id = $this->Session->read('my_staff_id');

        $this->set(compact('year', 'month', 'staff_id', 'id'));

        $this->set('title_for_layout', '経費登録・編集');
    }

    /**
     * [s_detail_update 更新]
     * @return [type] [None]
     */
    function s_detail_update(){

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $message = "更新しました";

            if (empty($this->data)) {
                $message = "セッションエラーです";
            } else {

                $this->Cost->CostDetail->create();
                if (!$this->Cost->CostDetail->save($this->request->data)) {
                    $message = "データの更新に失敗しました";
                } else {
                    $this->_saveAutoItem(CONFIRM_TYPE_2, COST_SUB_0, $this->request->data['CostDetail']['purpose']);
                    $this->_saveAutoItem(CONFIRM_TYPE_2, COST_SUB_1, $this->request->data['CostDetail']['place']);
                }
            }
            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
            $this->redirect(array('action' => 'index', $this->data['Cost']['CostDetail']['year'], $this->data['Cost']['CostDetail']['month']));
        }
    }

    /**
     * [s_detail_delete 削除]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function s_detail_delete($id){
        $this->autoRender = false;

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }
        $data = $this->Cost->CostDetail->find('first', array('conditions'=>array('CostDetail.id'=>$id)));
        if(!empty($data)){
            $data['CostDetail']['enable'] = 0;
            $this->Cost->CostDetail->save($data);
            $this->redirect(array('action' => 'index', $data['Cost']['year'], $data['Cost']['month']));
        }
    }

    /**
     * [a_print 印刷]
     * @param  [type] $id    [ID]
     * @return [type]        [None]
     */
    public function a_print($id){  

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        $datas = $this->Cost->find('first', array('conditions'=>array('Cost.id'=>$id)));

        $confirm = $this->Confirm->find('first', array('conditions'=>array('type'=>CONFIRM_TYPE_2, 'type_id'=>$id)));

        $staffs = $this->Staff->find('list');

        $this->set(compact('datas', 'confirm', 'staffs'));

        $this->render('s_print');
    }

    /**
     * [s_print 印刷]
     * @param  [type] $id    [ID]
     * @return [type]        [None]
     */
    public function s_print($id){  

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }
        $staff_id = $this->Session->read('my_staff_id');

        $datas = $this->Cost->find('first', array('conditions'=>array('Cost.id'=>$id)));
        if ($datas['Cost']['staff_id'] != $staff_id) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        $confirm = $this->Confirm->find('first', array('conditions'=>array('type'=>CONFIRM_TYPE_2, 'type_id'=>$id)));

        $staffs = $this->Staff->find('list');

        $this->set(compact('datas', 'confirm', 'staffs'));
    }
}

?>