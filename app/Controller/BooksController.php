<?php
App::uses('AppController', 'Controller');

/**
 * 書籍管理
 */
class BooksController extends AppController {
    var $uses = array('Book', 'Staff');
    
    /**
     * [s_index 一覧]
     * @param  [type] [None]
     * @return [type] [None]
     */
    function s_index() {
        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }
        $datas = $this->Book->find('all', array('order'=>array('purchase_date'=>'desc')));
        $this->set(compact('datas'));
        $this->set('title_for_layout', '書籍管理');
    }

    /**
     * [s_edit 詳細更新]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function s_edit($id){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }
        $this->layout = "ajax";
        $this->Book->unbindModelAll();
        $data = $this->Book->find("first", array("conditions" => array("Book.id"=>$id)));
        if (!empty($data)) {
            $this->data = $data;
        }
        $staffs = $this->Staff->find('list', array("conditions" => array("Staff.no !="=>"9999", "Staff.retire_date"=>null)));
        $this->set(compact('id','staffs'));
    }

    /**
     * [s_update 更新]
     * @param  [type] [None]
     * @return [type] [None]
     */
    function s_update(){

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $message = "更新しました";

            if (empty($this->data)) {
                $message = "セッションエラーです";
            } else {
                $this->Book->create();
                if (!$this->Book->save($this->request->data)) {
                    $message = "データの更新に失敗しました";
                } 
            }
            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
            $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * [s_delete 削除]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function s_delete($id){
        $this->autoRender = false;
        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }
        $data = $this->Book->find('first', array('conditions'=>array('Book.id'=>$id)));
        if(!empty($data)){
            $data['Book']['enable'] = 0;
            $b = $this->Book->save($data);
            $this->redirect(array('action' => 'index'));
        }
    }
}

?>