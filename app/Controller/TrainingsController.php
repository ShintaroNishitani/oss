<?php
App::uses('AppController', 'Controller');

/**
 * 課題管理
 */
class TrainingsController extends AppController {

    /**
     * [a_index 一覧]
     * @return [type]         [None]
     */
    function a_index() {

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        $staffs = $this->Training->Staff->find('list');

        $this->set(compact('staffs'));
        $this->set('title_for_layout', '課題一覧');

    }

    /**
     * [a_list リスト]
     * @return [type] [None]
     */
    public function a_list() {

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        $this->layout = "ajax";

        // 項目名=>array(値,検索条件,モデル名)
        $keys = array('no'=>array('', 'default', ''),
                      'name'=>array('', 'like', ''),
                      'type'=>array('', 'default', ''),
                      'staff_id'=>array('', 'default', ''),
                      'lang'=>array('', 'default', ''),
                      'difficulty'=>array('', 'default', ''),
                     );

        $con = array();

        $model = "Training";
        if(!empty($this->params->query)){
            foreach ($keys as $key => $value) {
                if($this->params->query['data'][$model]["sc_" . $key] != ""){
                    $keys[$key][0] = $this->params->query['data'][$model]["sc_" . $key];
                    if ($value[2]) {
                        $model = $value[2];
                    }
                    // 特殊な検索はswitch文に追加する
                    switch ($keys[$key][1]) {
                        case 'like':
                            array_push($con, array("$model.$key LIKE"=>"%".$keys[$key][0]."%"));
                            break;
                        default:
                            array_push($con, array("$model.$key"=>$keys[$key][0]));
                            break;
                    }
                }
            }
        }

        $datas = $this->Training->find("all", array('conditions'=>$con));

        $staffs = $this->Training->Staff->find('list');

// $this->log($datas);

        $this->set(compact('datas', 'keys', 'staffs'));
    }

    /**
     * [a_edit 登録・編集]
     * @param  integer $genre [ジャンル]
     * @param  [type]  $id    [ID]
     * @return [type]         [None]
     */
    function a_edit($id = null){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            echo '権限がありません';
            exit();
        }

    	$this->layout = "ajax";
        $guide_message = '課題情報を入力してください。';
    	$data = $this->Training->find("first", array("conditions"=>array("Training.id"=>$id)));
        if (!empty($data)) {
            $this->data = $data;
        }

        $today = date("Y-m-d");
        $me = $this->my_staff_id;

        $staffs = $this->Training->Staff->find('list');

        $this->set(compact('datas', 'staffs', 'today', 'me'));
    }

    /**
     * [a_update 更新]
     * @return [type] [None]
     */
    function a_update() {
        $error = 0;
        $message = "更新しました。";

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        if (!empty($this->request->data)) {
            if(empty($this->request->data['Training']['id'])){//新規登録
                $this->Training->create();
                $this->request->data['Training']['no'] = $this->getTrainingNo($this->data['Training']['type']);
            }
            if (!$this->Training->save($this->request->data)) {
                $error++;
                $message = "更新に失敗しました";
            }
        } else {
            $error++;
            $message = "不正なアクセスです。";
        }
        echo sprintf('{"error":"%d","message":"%s"}', $error, $message);
        exit();

	}


    /**
     * [getTrainingNo 番号取得]
     * @param  [type] $type [種別]
     * @param  [type] $year [年度]
     * @return [type]       [None]
     */
    function getTrainingNo($type){

        $no = ($type + 1) + '0001';

        $data = $this->Training->find('first', array('conditions'=>array('type'=>$type),
                                                     'fields'=>array('no'),
                                                     'order'=>array('no'=>'desc')));
        if ($data) {
            $no = $data['Training']['no'] + 1;
        }

        return $no;
    }

    /**
     * [a_delete 削除]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function a_delete($id) {
        $error = 0;
        $message = "削除しました。";

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $error++;
            $message = "権限がありません";
        } else {
            $data = $this->Training->find('first', array('conditions'=>array('Training.id'=>$id)));
            if(!empty($data)){
                $data['Training']['enable'] = 0;
                if (!$this->Training->save($data)) {
                    $error++;
                    $message = "削除に失敗しました";
                }
            }
        }

        echo sprintf('{"error":"%d","message":"%s"}', $error, $message);
        exit();
	}

    /**
     * [s_index 一覧]
     * @return [type]         [None]
     */
    function s_index() {

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        $this->set('title_for_layout', '実施課題一覧');

    }

    /**
     * [a_list リスト]
     * @return [type] [None]
     */
    public function s_list() {

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        $this->layout = "ajax";

        // 項目名=>array(値,検索条件,モデル名)
        $keys = array('no'=>array('', 'default', 'Training'),
                      'type'=>array('', 'default', 'Training'),
                      'name'=>array('', 'default', 'Training'),
                      'staff_id'=>array('', 'staff', 'Staff'),
                     );

        $con = array();

        if(!empty($this->params->query)){
            foreach ($keys as $key => $value) {
                $model = "TrainingProgress";
                if($this->params->query['data'][$model]["sc_" . $key] != ""){
                    $keys[$key][0] = $this->params->query['data'][$model]["sc_" . $key];
                    if ($value[2]) {
                        $model = $value[2];
                    }
                    // 特殊な検索はswitch文に追加する
                    switch ($keys[$key][1]) {
                        case 'like':
                            array_push($con, array("$model.$key LIKE"=>"%".$keys[$key][0]."%"));
                            break;
                        case 'staff':
                            array_push($con, array("Staff.name LIKE"=>"%".$keys['staff_id'][0]."%"));
                            break;
                        default:
                            array_push($con, array("$model.$key"=>$keys[$key][0]));
                            break;
                    }
                }
            }
        }

        $datas = $this->Training->TrainingProgress->find("all", array('conditions'=>$con, 'order'=>array('term_s'=>'desc')));

        $staffs = $this->Training->Staff->find('list');

        $this->set(compact('datas', 'keys', 'staffs'));
    }

    /**
     * [s_edit 実施課題更新]
     * @param  [type] $id          [ID]
     * @param  [type] $progress_id [実施課題ID]
     * @return [type]              [None]
     */
    function s_edit($id, $progress_id = null){

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            echo '権限がありません';
            exit();
        }

        $this->layout = "ajax";
        $data = $this->Training->TrainingProgress->find("first", array("conditions" => array("TrainingProgress.id"=>$progress_id)));
        if (!empty($data)) {
            $this->data = $data;
        }

        $this->Training->unbindModelAll();
        $training = $this->Training->find('first', array('conditions'=>array('id'=>$id)));

        $staffs = $this->Training->Staff->find('list');

        $today = date("Y-m-d");
        $me = $this->my_staff_id;

        $this->set(compact('id', 'training', 'staffs', 'today', 'me'));

        $this->set('title_for_layout', '実施課題登録・編集');
    }

    /**
     * [s_update 更新]
     * @return [type] [None]
     */
    function s_update(){

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $message = "更新しました";

            if (empty($this->data)) {
                $message = "セッションエラーです";
            } else {

                $this->Training->TrainingProgress->create();
                if (!$this->Training->TrainingProgress->save($this->request->data)) {
                    $message = "データの更新に失敗しました";
                }
            }
            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));

            $this->redirect($this->referer());
        }
    }

    /**
     * [s_delete 削除]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function s_delete($id){
        $this->autoRender = false;

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        $data = $this->Training->TrainingProgress->find('first', array('conditions'=>array('TrainingProgress.id'=>$id)));
        if(!empty($data)){
            $data['TrainingProgress']['enable'] = 0;
            $this->Training->TrainingProgress->save($data);
        }
    }

}

?>