<?php
App::uses('AppController', 'Controller');
App::uses('KintaisController', 'Controller');

/**
 * 労務管理
 */
class WorkMngsController extends AppController {
    var $uses = array('PaidHoliday', 'Staff', 'Kintai');

    /**
     * [s_index 一覧]
     * @param  [type] $type     [種別]
     * @param  [type] $year     [年度]
     * @return [type]           [None]
     */
    function a_index($year = null)
    {
        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        if ($year == null) {
            $year = date('Y');
            $month = date('n');
            if ($month < 4) {
                $year--;
            }
        }

        $headers = array(4, 5, 6, 7, 8, 9 , 10, 11, 12, 1, 2, 3,);

		$own = $this->Staff->find('first', array('conditions'=>array("Staff.id" => $this->Session->read('my_staff_id'))));
		$joins = array(
			array(
				"type" => "LEFT",
				"table" => "authorities",
				"alias" => "Authority",
				"conditions" => array("Staff.authority_id = Authority.id"),
			),
		);

        // 最新のデータのみ保持
        $datas = $this->PaidHoliday->find('all', array('conditions'=>array('PaidHoliday.enable'=>"1", "Staff.no !="=>"9999", "Staff.retire_date"=>null, "Staff.authority_id <>"=>1, "Authority.rank >="=>$own['Authority']['rank']),
                                                       'joins'=> $joins,
                                                       'order' => array('Staff.id' =>'asc')));
        // 有給残日数取得
        $controller = new KintaisController();
        $sum_over_time = 0;
        $sum_over_time_count = 0;
        $sum_extra_time = 0;
        $sum_extra_time_count = 0;
        for ($i = 0; $i < count($datas); $i++) {
            $tmp_year = $year;
            $tmp_month = 4;
            // 年間有給消化を数取得
            $datas[$i]['PaidHoliday']['paid_digest'] = $controller->_getPaidHolidayDigest($datas[$i]['Staff']['id']);
            // 振休残日数を取得
            $datas[$i]['PaidHoliday']['trans_remain'] = $controller->_getTrHolidayRemain($datas[$i]['Staff']['id']);
        	// 特休残日数を取得
            $datas[$i]['PaidHoliday']['special_remain'] = $controller->_getSpHolidayRemain($datas[$i]['Staff']['id']);

            // 残業時間を取得
            $datas[$i]['Kintai']['ave_over_time'] = 0;
            $datas[$i]['Kintai']['ave_extra_time'] = 0;
            $datas[$i]['Kintai']['over_extra_time_count'] = 0;
            $staff_over_time = 0;
            $staff_over_time_count = 0;
            $staff_extra_time = 0;
            $staff_extra_time_count = 0;
            foreach ($headers as $header) {
                $datas[$i]['Kintai']['over_time'][$header] = "-----";
                $datas[$i]['Kintai']['extra_time'][$header] = "";
                $this->Kintai->unbindModelAll();
                $k = $this->Kintai->find('first', array('conditions'=>array('staff_id'=>$datas[$i]['Staff']['id'], 'year'=>$tmp_year, 'month'=>$tmp_month), 'fields'=>array('over_time','extra_time', 'check_date')));
                if ($k) {
                    $datas[$i]['Kintai']['over_time'][$header] = number_format($k['Kintai']['over_time'], 1);
                    $datas[$i]['Kintai']['extra_time'][$header] = number_format($k['Kintai']['extra_time'], 1);
                    if ($k['Kintai']['check_date']) { // 確認済みのみカウント
                        $staff_over_time += $k['Kintai']['over_time'];
                        $sum_over_time += $k['Kintai']['over_time'];
                        $staff_over_time_count++;
                        $sum_over_time_count++;

                        $staff_extra_time += $k['Kintai']['extra_time'];
                        $sum_extra_time += $k['Kintai']['extra_time'];
                        $staff_extra_time_count++;
                        $sum_extra_time_count++;
                    }
                }
                // 平均残業時間
                if ($staff_over_time_count){
                    $datas[$i]['Kintai']['ave_over_time'] = number_format(round($staff_over_time / $staff_over_time_count, 1), 1);
                }
                // 36協定平均残業時間
                if ($staff_extra_time_count) {
                    $datas[$i]['Kintai']['ave_extra_time'] = number_format(round($staff_extra_time / $staff_extra_time_count, 1), 1);
                }
                // 超勤上限回数
                if ($datas[$i]['Kintai']['extra_time'][$header] >= MAX_EXTRA_TIME) {
                    $datas[$i]['Kintai']['over_extra_time_count']++;
                }
                $this->Common->getNextMonth($tmp_year, $tmp_month, $tmp_year, $tmp_month);
            }
        }
        // トータル平均残業時間
        $sum_ave_over_time = round($sum_over_time / $sum_over_time_count, 1);
        $sum_ave_extra_time = round($sum_extra_time / $sum_extra_time_count, 1);
        $this->set(compact('datas', 'year', 'headers', 'sum_ave_over_time', 'sum_ave_extra_time'));

        $this->set('title_for_layout', '労務管理');
    }



}

?>
