<?php
class SecurimageComponent extends Component
{
 	var $sesid;
 
//    function startup(&$controller)
//    {
//        $this->controller = $controller;
//    }

    function render()
    {
        //App::import('Vendor', 'Securimage', array('file' => 'Securimage/securimage.php'));
        $securimage = new Securimage();     
        $securimage->ttf_file = ROOT . DS . APP_DIR . '/Vendor/securimage/AHGBold.ttf';
        $securimage->charset = 'ABCDEFGHKLMNPRSTUVWYZ23456789';
        $securimage->session_name = $this->sesid;
        $securimage->image_width = 150;			// 175
		$securimage->image_height = 60; 		// 45
        $securimage->font_size = 55;

		$securimage->code_length = 7;
      	// 文字のゆがみの比率
		$securimage->perturbation  = .65;
		// ラインの数
		$securimage->num_lines     = 7;  
        // ラインの色
		$securimage->line_color      = new Securimage_Color("#ff8888");
		// テキストの色
		$securimage->text_color      = new Securimage_Color("#000000");
		// 背景の色
		$securimage->image_bg_color  = new Securimage_Color("#ffffff");
        
        $securimage->show();
    }
 
    function checkCaptcha($captcha)
    {
        App::import('Vendor','Securimage');
        $securimage = new Securimage();
 
        if ($securimage->check($captcha) === false) {
            return false;
        }
        return true;
    }
}