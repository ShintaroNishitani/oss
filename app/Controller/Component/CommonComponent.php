<?php
App::uses('Component', 'Controller');

class CommonComponent extends Component {

    /**
     * [getAge 年齢取得]
     * @param  [type] $birth [誕生日]
     * @return [type]        [年齢]
     */
    function getAge($birth){
        if (empty($birth)) {
            return "";
        }
        $ty = date("Y");
        $tm = date("m");
        $td = date("d");
        list($by, $bm, $bd) = explode('-', $birth);
        $age = $ty - $by;
        if($tm * 100 + $td < $bm * 100 + $bd) $age--;
        return $age;
    }

    /**
     * [getNextMonth 翌月の年月]
     * @param  [type] $year        [当年]
     * @param  [type] $month       [当月]
     * @param  [type] &$next_year  [翌年]
     * @param  [type] &$next_month [翌月]
     * @return [type]              [None]
     */
    function getNextMonth($year, $month, &$next_year, &$next_month) {
        $next_year = $year;
        $next_month = $month + 1;
        if ($next_month > 12){
            $next_month -= 12;
            $next_year += 1;
        }
    }

    /**
     * [getBeforeMonth 前月の年月]
     * @param  [type] $year          [当年]
     * @param  [type] $month         [当月]
     * @param  [type] &$before_year  [前年]
     * @param  [type] &$before_month [前月]
     * @return [type]                [None]
     */
    function getBeforeMonth($year, $month, &$before_year, &$before_month) {
        $before_year = $year;
        $before_month = $month - 1;
        if ($before_month < 1){
          $before_month += 12;
          $before_year -= 1;
        }
    }

    /**
     * [modify_formatDate modify後の日付をformatしてstringで返す]
     * @param  [str] $date   [description]
     * @param  [str] $modify [+1monthとか-1dayとか]
     * @param  [str] $format ['Y-m-d'とか]
     * @return [str]         [日付]
     */
    function modify_formatDate($date, $modify, $format) {
        $date = new DateTime($date);
        $date = $date->modify($modify);
        $new = $date->format($format);
        return $new;
    }

    /**
     * [_taxRatio 消費税取得]
     * @param  [type] $datetime [日付]
     * @return [type]           [消費税]
     */
    function taxRatio($datetime = null){
        if ($datetime == null) {
            $datetime = time();
        }

        $tax = 0.08;
        // $period_10 = strtotime('2017-04-01 0:00:00');
        // if ($datetime >= $period_10) {
        //     $tax = 0.10;
        // }
        return $tax;
    }

    /**
     * [saveFileList ファイルアップロード処理]
     * @param  [type] $id      [ID]
     * @param  [type] $file_id [ファイルID]
     * @param  [type] $data    [データ]
     * @param  [type] &$result [結果]
     * @return [type]          [結果]
     */
    function saveFileList($id, $file_id, $data, &$result){
        if ($data["error"] != 0) {
            return false;
        }

        $path = "file_lists".DS."%06d".DS."%08d".DS;
        $path = sprintf($path, intval($id / 100), $id);
        if (!is_dir(APP.$path)) {
            if (!mkdir(APP.$path, 0755, true)) {
                $this->log("mkdir error:".$path);
                return false;
            }
        }
        $filename = sprintf('%08d', $file_id);
        if (!move_uploaded_file($data["tmp_name"], APP.$path.$filename)) {
            $this->log("move_uploaded_file error:".$data["tmp_name"]." ".APP.$path.$filename);
            return false;
        }
        chmod(APP.$path.$filename, 0666);
        $result = array(
            "path" => $path,
            "filename" => $filename,
        );
        return true;
    }

    /**
     * [getYear 年度取得]
     * @param  [type] $year          [当年]
     * @param  [type] $month         [当月]
     * @return [type]                [None]
     */
    function getYear($year = null, $month = null) {

        if ($year == null || $month == null) {
            $year = date('Y');
            $month = date('n');
        }

        if ($month == 1) {
            $year--;
        }

        return $year;
    }

    /**
     * [getMonthRange 指定期間の年月取得]
     * @param  [type] $start [開始]
     * @param  [type] $end   [終了]
     * @return [type]        [None]
     */
	function getMonthRange($start, $end = null) {
	    if ($end === null) {
	        $end = time();
	    }
	    $ymList = array();
	    for ($utime = $start; $utime <= $end; $utime = strtotime('+1 month', $utime)) {
	        $ymList[] = array('year'=>date('Y', $utime), 'month'=>date('m', $utime));
	    }
	    return $ymList;
    }

    /**
     * [getProjectLabel 指定日付のプロジェクトラベル取得]
     * @param  [type] $date  [対象日付]
     * @return [type]        [None]
     */
    function getProjectLabel($date = null){
        if (!$date) {
            $date = date('Y-m-1');
        }

        $label = "グループ活動";
        if (strtotime($date) > strtotime("2019-04-01")) {
            $label = "採用";
        }
        return $label;
    }

    /**
     * [createJwt JWT生成]
     * @return [string] JWT
     */
    public function createJwt()
    {
        $private_key = file_get_contents(dirname(__FILE__, 3)."/Config/private_20230414092117.key");
        // header情報をエンコードする
        $header = $this->base64UrlEncode(json_encode([
            'alg' => 'RS256',
            'typ'=> 'JWT'
        ]));
        $now = strtotime("now");
        $payload = $this->base64UrlEncode(json_encode([
            'iss' => LW_CLIENTID,
            'sub' => LW_SERVICEACCOUNT,
            'iat' => $now,
            'exp' => $now + 3600
        ]));

        $signature = null;
        // 署名を生成する
        openssl_sign($header . '.' . $payload, $signature, $private_key, OPENSSL_ALGO_SHA256);
        
        $signature = $this->base64UrlEncode($signature);
        $jwt = $header . '.' . $payload . '.' . $signature;
        return $jwt;
    }
    /**
     * base64でデータをエンコードする
     */
    public function base64UrlEncode($data)
    {
        return str_replace('=', '', strtr(base64_encode($data), '+/', '-_'));
    }

    /**
     * [sendLineWorks LineWorksにメッセージ送信]
     * @param  [type] $line_account_id  [LineWorksID]
     * @param  [type] $message          [メッセージ]
     * @return [type]                   [None]
     */
    function sendLineWorks($line_account_id, $message){
        if(Configure::read('debug') == 0){

            $url = sprintf("https://www.worksapis.com/v1.0/bots/%s/users/%s/messages", LW_BOTID, $line_account_id);
            $body = [
            'json' => [
                "content" => [
                    "type" => "text",
                    "text" => $message
                ]
            ]
            ];

            $token = $this->getLineWorksToken();
            if(!empty($token)){
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body['json']));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Authorization: Bearer '.$token,
                    'Content-Type: application/json;charset=UTF-8'
                ));
                $result = curl_exec($ch);
                curl_close($ch);
            }
        }
    }
    

    /**
     * [getLineWorksToken LineWorksトークン取得]
     * @param  [type]        [None]
     * @return [string] $token [トークン]
     */
    function getLineWorksToken(){
        $now = strtotime('now');
        $lwModel = ClassRegistry::init('LineworksToken');
        $data = $lwModel->find('first');
        // $data = $this->LineworksToken->find('first');
        if(!empty($data)){
            $ref_expire = strtotime($data['LineworksToken']['refresh_expired_date']);
            $ac_expire = strtotime($data['LineworksToken']['access_expired_date']);
            // トークン有効期限チェック
            if ($now < $ac_expire){
                $token = $data['LineworksToken']['access_token'];
                return $token;
            }
            if($now < $ref_expire){
                $body = 'grant_type=refresh_token';
                $body .= '&';
                $body .= 'refresh_token='.urlencode($data['LineworksToken']['refresh_token']);
                $body .= '&';
                $body .= 'client_id='.LW_CLIENTID;
                $body .= '&';
                $body .= 'client_secret='.LW_CLIENTSECRET;
            }
        }
        
        if(empty($body)){
            $jwt = $this->createJwt();
            $body = 'assertion='.$jwt;
            $body .= '&';
            $body .= 'grant_type=urn:ietf:params:oauth:grant-type:jwt-bearer';
            $body .= '&';
            $body .= 'client_id='.LW_CLIENTID;
            $body .= '&';
            $body .= 'client_secret='.LW_CLIENTSECRET;
            $body .= '&';
            $body .= 'scope=bot,bot.read';
        }
        $curl = curl_init(LW_URL_TOKEN);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/x-www-form-urlencoded',
        ]);
        $res = curl_exec($curl);
        curl_close($curl);
        
        $res_decode = json_decode($res, true);
        if (array_key_exists("access_token", $res_decode))
        {
            $token =  $res_decode["access_token"];
            $expire = date("Y-m-d H:i:s", strtotime("+1 day", $now));
            $data['LineworksToken']['access_token'] = $token;
            $data['LineworksToken']['access_expired_date'] = $expire;
            if (array_key_exists("refresh_token", $res_decode))
            {
                $ref_token =  $res_decode["refresh_token"];
                $expire = date("Y-m-d H:i:s", strtotime("+90 day", $now));
                $data['LineworksToken']['refresh_token'] = $ref_token;
                $data['LineworksToken']['refresh_expired_date'] = $expire;
            }
            // $this->LineworksToken->save($data, true);
            $lwModel->save($data, true);
        }
        return $token;
    }
}
?>
