<?php
App::uses('AppController', 'Controller');

/**
 * 顧客管理
 */
class CustomersController extends AppController {
    public $paginate = array(
        'page' => 1,
        'conditions' => array(''),
        );

    /**
     * [a_index 一覧]
     * @return [type]           [None]
     */
    function a_index(){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        $this->paginate['Customer'] = array('limit'=>100, 'order'=>array('date'=>'desc'));
        $datas = $this->paginate('Customer');

        $this->set(compact('datas'));

        $this->set('title_for_layout', '顧客管理');
    }

    /**
     * [a_edit 更新]
     * @param  [type] $id    [ID]
     * @return [type]        [None]
     */
    function a_edit($id = null){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            echo '権限がありません';
            exit();
        }

        $this->layout = "ajax";
        $this->Customer->unbindModelAll();
        $data = $this->Customer->find("first", array("conditions" => array("Customer.id" => $id), 'fields'=>array('*')));
        if (!empty($data)) {
            $this->data = $data;
        }

        $this->set(compact('id', 'data'));
        $this->set('title_for_layout', '顧客登録・編集');
    }

    /**
     * [a_update 更新]
     * @return [type] [None]
     */
    function a_update(){

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $message = "更新しました";

            if (empty($this->data)) {
                $message = "セッションエラーです";
            } else {

                $this->Customer->create();
                if (!$this->Customer->save($this->request->data)) {
                    $message = "データの更新に失敗しました";
                }
            }
            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
            $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * [a_delete 削除]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function a_delete($id){
        $this->autoRender = false;

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        $this->Customer->delete($id);
        $this->redirect(array('action' => 'index'));
    }

    function s_search_company(){
        $this->layout = "ajax";
        $auth = $this->_checkStaffAuthority();
        // if (0 == $auth) {
        //     $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
        //     $this->redirect($this->referer());
        // }

    }
}

?>
