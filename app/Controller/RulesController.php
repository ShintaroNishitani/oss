<?php

App::uses('AppController', 'Controller');
App::import('Vendor', 'Securimage', array('file' => 'securimage/securimage.php'));
/**
 * 従業員
 */
class RulesController extends AppController {


	var $uses = array('Rule', 'RuleCategory', 'RuleDetail');
    public $components = array("Cookie", 'Securimage');


    public function s_index($rule_kind = null) 
    {   
        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }
                   
     	$this->set('rule_kind', $rule_kind);
    }

    function s_certificate()
    {
    
        $error = 0;
        $message = "";
        
		if ($this->request->is('post') || $this->request->is('put')) 
		{
	    	if(!$this->Session->read('securimage_code_disp.default')) return false;
	    	$check = false;
	    	if( $this->data['captcha'] == $this->Session->read('securimage_code_disp.default') )
	    	{
	       		$this->Session->write('rule_session', $this->Session->id());
        		$this->Session->write('rule_auth', 1);  
	      		if(0 == $this->data['Rule']['rule_kind'])
	      		{
	      			$this->redirect("../rules/rulelist");
	      		}
	      		else if(1 == $this->data['Rule']['rule_kind'])
	      		{
	      		
	      		}
	      	    else if(2 == $this->data['Rule']['rule_kind'])
	      		{
	      		
	      		}
	      		else
	      		{
	      			$this->redirect(['action' => 'index', $this->data['Rule']['rule_kind']]);
	      		}
	    	}
	    	else
	    	{
	    		$this->redirect(['action' => 'index',$this->data['Rule']['rule_kind']]);

	    	}
		
		}
	}

	/*
	 * 認証画像の表示
	 */
    public function s_CaptchaRender() {
		$this->Securimage->sesid = $this->Session->id();
    	$this->Securimage->render();
  	}
  	
	/*
	 * 入力された値と検証
	 */
    public function s_CaptchaCheck() {
    	$this->autoRender = false;
     
    	if(!$this->Session->read('securimage_code_disp.default')) return false;
     
    	$check = false;
    	if( $this->data['captcha'] == $this->Session->read('securimage_code_disp.default') ){
      		$check = true;
    	}
     
       	header("Content-type: application/xml");
       	echo '<?xml version="1.0" encoding="UTF-8" ?> ' . "\n";
       	echo '<rss>'."\n"; 
       	echo '      <check>'.$check.'</check>'."\n"; 
       	echo '</rss>'."\n"; 
       	exit;
  	}

    /**
     * [s_rulelist 一覧]
     * @return [type] [None]
     */
    function s_rulelist(){
        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

		$rule_kind = 0;
        $now_ssn = $this->Session->read('rule_session');
        $auth = $this->Session->read('rule_auth');

        //認証不一致
        if(($now_ssn != $this->Session->id()) || (0 == $auth)) 
        {
            $this->redirect(['action' => 'index', $rule_kind]);
            exit();
        }
	
		$fields = array("Rule.id", "Rule.category", "RuleCategory.category_name", "Rule.doc_no", "Rule.index", "Rule.doc_name", "Rule.file_name");

		$con = array();
		
		$rules = $this->Rule->find('all', array("conditions"=>$con, 'fields'=>$fields, 'order'=>array('Rule.doc_no'=>'asc', 'Rule.index'=>'asc')));

		$keep_cat_nm = "";
		$result = array();
		for($i = 0; $i < count($rules); $i++)
		{
			if($keep_cat_nm != $rules[$i]["RuleCategory"]["category_name"])
			{
				$rec = array();
				$rec["category_name"] = $rules[$i]["RuleCategory"]["category_name"];
				//$rules[$i]["Rule"]["file_path"] = $this->webroot . "files/Rules/" . $rules[$i]["Rule"]["file_name"];
				$rules[$i]["Rule"]["Detail"] = $this->Rule->RuleDetail->find('all', array("conditions"=>array('RuleDetail.rule_id'=>$rules[$i]["Rule"]['id']), 'fields'=>array( "RuleDetail.id", "RuleDetail.index", "RuleDetail.doc_name", "RuleDetail.file_name"), 'order'=>array('RuleDetail.index'=>'asc')));
				$rec["Rule"] = array();
				array_push($rec["Rule"], $rules[$i]["Rule"]);
				array_push($result, $rec);
				$keep_cat_nm = $rules[$i]["RuleCategory"]["category_name"];
			}
			else
			{
				//$rules[$i]["Rule"]["file_path"] = $this->webroot . "files/Rules/" . $rules[$i]["Rule"]["file_name"];
				$rules[$i]["Rule"]["Detail"] = $this->Rule->RuleDetail->find('all', array("conditions"=>array('RuleDetail.rule_id'=>$rules[$i]["Rule"]['id']), 'fields'=>array( "RuleDetail.id", "RuleDetail.index", "RuleDetail.doc_name", "RuleDetail.file_name"), 'order'=>array('RuleDetail.index'=>'asc')));				
				array_push($result[count($result)-1]["Rule"], $rules[$i]["Rule"]);
			}
		}
	
		$this->set("result", $result);
        $this->set('title_for_layout', '規約トップページ');
    }


   /**
     * [s_rulelist 一覧]
     * @return [type] [None]
     */
    function s_showpdf($index, $type){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }
        
        
        $now_ssn = $this->Session->read('rule_session');
        $r_auth = $this->Session->read('rule_auth');
        //認証不一致
        if(($now_ssn != $this->Session->id()) || (0 == $r_auth)) 
        {
            $this->Session->setFlash('認証チェックエラー', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }
        
		$fields = array();
		$page = "";
		if(0 == $type)
		{
			$fields = array("Rule.file_name");
			$con = array("Rule.id = " . $index);
		
			$rules = $this->Rule->find('first', array("conditions"=>$con, 'fields'=>$fields));
			$page = "../NotPublic/Files/Rule/" . $rules["Rule"]["file_name"];
		}
		else if(1 == $type)
		{
			$fields = array("RuleDetail.file_name");
			$con = array("RuleDetail.id = " . $index);
		
			$ruledetails = $this->Rule->RuleDetail->find('first', array("conditions"=>$con, 'fields'=>$fields ));
			$page = "../NotPublic/Files/Detail/" . $ruledetails["RuleDetail"]["file_name"];
		}
		
        $pdf = file_get_contents($page);  
        header("Content-Type: application/pdf");  
        echo $pdf;  
        exit(); 
    }

}

?>
