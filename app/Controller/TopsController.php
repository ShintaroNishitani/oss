<?php
App::uses('AppController', 'Controller');

/**
 * トップページ
 */
class TopsController extends AppController {

    var $uses = array('Staff', 'Confirm', 'BoardTree', 'BoardBody', 'BoardRead', 'Schedule', 'ApplyHoliday');

    /**
     * [s_index トップページ]
     * @return [type] [None]
     */
    function s_index()
    {

        $auth = $this->_checkStaffAuthority();
        if ($auth == 0) {
            $this->Session->setFlash ('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect ($this->referer());
        }

        $this->set ('title_for_layout', 'トップページ');
    }

    /**
     * [s_event イベント]
     * @return [type] [None]
     */
    function s_event()
    {

        $auth = $this->_checkStaffAuthority();
        if ($auth == 0) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        $this->layout = "ajax";

        $staffs = $this->Staff->find('list');

        // 本日のスケジュールを取得
        $con = array('Schedule.date <= '=>date('Y/m/d'), 'Schedule.end_date >= '=>date('Y/m/d'), 'Schedule.enable'=>1, 'Schedule.public'=>0);
        $events = $this->Schedule->find('all', array('conditions'=>$con, 'order'=>array('Schedule.start asc')));

        // 日付表示用
        $year = date ('Y');
        $month = date ('n');
        $day = date ('j');
        $week = array ("日", "月", "火", "水", "木", "金", "土");
        $d_week = $week[date ('w')];

        $this->set (compact ('staffs', 'events', 'year', 'month', 'day', 'd_week'));
    }

    /**
     * [s_confirm 申請・承認]
     * @return [type] [None]
     */
    function s_confirm()
    {

        $auth = $this->_checkStaffAuthority();
        if ($auth == 0) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        $this->layout = "ajax";

        $staffs = $this->Staff->find('list');

        // "受領済"になっていない自身の申請一覧を取得
        $staff_id = $this->Session->read('my_staff_id');
        $con = array('request_staff_id'=>$staff_id,
                     'type not in '=>array(CONFIRM_TYPE_0, CONFIRM_TYPE_8),    // 勤怠・休暇申請は受領確認が不要であるため除外
                     'check_staff_id != '=>0,
                     'receive'=>0);
        $confirms = $this->Confirm->find('all', array('conditions'=>$con));

        // 自分自身が対象となっている申請の一覧を取得
        $con = array('request_staff_id != '=>0,
                    //  'type'=>CONFIRM_TYPE_8,
                     'check_staff_id'=>$staff_id,
                     'status'=>CONFIRM_IN,
                     'receive'=>0);
        $approvals = $this->Confirm->find('all', array('conditions'=>$con));
        for ($i = 0; $i < count($approvals); $i++) {
            $approval = $approvals[$i];
            if ($approval['Confirm']['type'] == CONFIRM_TYPE_8) {
                $dates = $this->ApplyHoliday->ApplyHolidayDetail->ApplyHolidayDetailDate->find('list', array(
                    'fields' => array("date"),
                    'conditions'=>array('apply_holiday_detail_id'=>$approval['Confirm']['type_id']),
                    'order'=>array('date'),
                ));
                $approvals[$i]['Applydays'] = $dates;
            }
        }


        // 差戻された申請一覧を取得
        $con = array(
            'conditions'=>array('Confirm.request_staff_id'=>$staff_id,
                'Confirm.type'=>CONFIRM_TYPE_8,
                'Confirm.status'=>CONFIRM_NO,
            ),
            'order' => array('ApplyHolidayDetail.apply_date'=>'desc'),
        );
        $remands = $this->ApplyHoliday->ApplyHolidayDetail->find('all', $con);

        $this->set(compact('staffs', 'confirms', 'approvals', 'remands'));

    }

    /**
     * [s_board 掲示板新着]
     * @return [type] [None]
     */
    function s_board()
    {

        $auth = $this->_checkStaffAuthority();
        if ($auth == 0) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        $this->layout = "ajax";

        $staffs = $this->Staff->find('list');

        // "未読"になっている掲示板一覧を取得
        $staff_id = $this->Session->read('my_staff_id');
        $joins = array (array ("type" => "LEFT",
                               "table" => "board_trees",
                               "alias" => "BoardTree",
                               "conditions" => array("BoardTree.id = BoardRead.board_id"),),);
        $con = array ('BoardRead.staff_id'=>$staff_id,
                      'BoardRead.status'=>READ_STATUS_UNREAD,  // 未読
                      );
        $boards = $this->BoardRead->find ('all', array ('conditions'=>$con, 'joins'=>$joins, 'order'=>array('BoardTree.modified desc'), 'fields'=>('*')));

        // 掲示板階層名・本文を取得
        for ($i = 0; $i < count($boards); $i++) {
            if ($boards[$i]['BoardTree']['level1'] != 0) {
                $name = $this->BoardTree->find('first',
                                               array('conditions'=>array('BoardTree.level1'=>$boards[$i]['BoardTree']['level1'],
                                                                         'BoardTree.level2'=>0),
                                                     'fields'=>'title'));
                $boards[$i]['BoardTree']['lv1_title'] = $name['BoardTree']['title'];
            }
            if ($boards[$i]['BoardTree']['level2'] != 0) {
                $name = $this->BoardTree->find('first',
                                               array('conditions'=>array('BoardTree.level1'=>$boards[$i]['BoardTree']['level1'],
                                                                         'BoardTree.level2'=>$boards[$i]['BoardTree']['level2'],
                                                                         'BoardTree.level3'=>0),
                                                     'fields'=>'title'));
                $boards[$i]['BoardTree']['lv2_title'] = $name['BoardTree']['title'];
            }
            if ($boards[$i]['BoardTree']['level3'] != 0) {
                $name = $this->BoardTree->find('first',
                                               array('conditions'=>array('BoardTree.level1'=>$boards[$i]['BoardTree']['level1'],
                                                                         'BoardTree.level2'=>$boards[$i]['BoardTree']['level2'],
                                                                         'BoardTree.level3'=>$boards[$i]['BoardTree']['level3'],
                                                                         'BoardTree.level4'=>0),
                                                     'fields'=>'title'));
                $boards[$i]['BoardTree']['lv3_title'] = $name['BoardTree']['title'];
            }

            $con = array ('BoardBody.tree_id'=>$boards[$i]['BoardTree']['id'], 'BoardBody.hist_index'=>0);
            $body = $this->BoardBody->find ('first', array ('conditions'=>$con, 'fields'=>'body'));
            $boards[$i]['BoardTree']['body'] = $body['BoardBody']['body'];
        }

        $this->set(compact('staffs', 'boards'));
    }

    /**
     * [s_working 永年勤続表彰]
     * @return [type] [None]
     */
    function s_working()
    {

        // $auth = $this->_checkStaffAuthority();
        // if ($auth == 0) {
        //     $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
        //     $this->redirect($this->referer());
        // }

        $this->layout = "ajax";

        $staffs = $this->Staff->find('all', array('conditions'=>array('Staff.retire_date'=>null)));

        $datas = array();
        foreach ($staffs as $staff) {
            $start = new DateTime($staff['Staff']['hire_date']);
        }
        pr($start);exit();
        $this->set(compact('datas'));

    }
}

?>
