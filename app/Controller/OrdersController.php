<?php
App::uses('AppController', 'Controller');

/**
 * 見積請求書番号管理
 */
class OrdersController extends AppController {
    var $uses = array('Order', 'Staff', 'Customer');

    /**
     * [s_index 一覧]
     * @param  [type] $type     [種別]
     * @param  [type] $year     [年度]
     * @return [type]           [None]
     */
    function s_index($type = 0, $year = null){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        if ($year == null) {
            $year = $this->Common->getYear();   // 年度取得
        }

        $con = array();

        // 項目名=>array(値,検索条件,モデル名)
        $keys = array('no'=>array('', 'default', ''),
                      'staff_id'=>array('', 'default', ''),
                      'date_b'=>array('', 'term_s', ''),
                      'date_e'=>array('', 'term_e', ''),
                      'customer_id'=>array('', 'default', ''),
                      'remark'=>array('', 'like', ''),
                     );

        // 検索条件作成
        $model = "Order";
        if(!empty($this->params->query)){
            foreach ($keys as $key => $value) {
                if($this->params->query["sc_" . $key] != ""){
                    $keys[$key][0] = $this->params->query["sc_" . $key];

                    if ($value[2]) {
                        $model = $value[2];
                    }
                    // 特殊な検索はswitch文に追加する
                    switch ($keys[$key][1]) {
                        case 'like':
                            array_push($con, array("$model.$key LIKE"=>"%".$keys[$key][0]."%"));
                            break;
                        case 'term_s':
                            $item = str_replace('_b', '', $key);
                            array_push($con, array("$model.$item >="=>$keys[$key][0]));
                            break;
                        case 'term_e':
                            $item = str_replace('_e', '', $key);
                            array_push($con, array("$model.$item <="=>$keys[$key][0]));
                            break;
                        default:
                            array_push($con, array("$model.$key"=>$keys[$key][0]));
                            break;
                    }
                }
            }
            $type = $this->params->query['sc_type'];
            $year = $this->params->query['sc_year'];
        }

        array_push($con, array('Order.type'=>$type, 'Order.year'=>$year));

        $datas = $this->Order->find('all', array('conditions'=>$con, 'order'=>array('Order.no'=>'desc')));

        $types = Configure::read("order_type");

        $label = $types[$type];

        $staffs = $this->Staff->find('list', array("conditions" => array("Staff.no !="=>"9999", "Staff.retire_date"=>null)));

        $customers = $this->Customer->find('list', array('fields'=>array('id', 'code'), 'order'=>array('priority'=>'desc', 'code'=>'asc')));

        $this->set(compact('type', 'year', 'datas', 'types', 'label', 'keys', 'staffs', 'customers'));

        $this->set('title_for_layout', $label . '管理');
    }

    /**
     * [s_edit 登録・編集]
     * @param  [type] $type      [種別]
     * @param  [type] $year      [年度]
     * @param  [type] $id        [ID]
     * @return [type]            [None]
     */
    function s_edit($type, $year, $id = null){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        $this->layout = "ajax";
        $this->Order->unbindModelAll();
        $data = $this->Order->find("first", array("conditions" => array("Order.id"=>$id)));
        if (!empty($data)) {
            $this->data = $data;
        }

        $staffs = $this->Staff->find('list', array("conditions" => array("Staff.no !="=>"9999", "Staff.retire_date"=>null)));

        $customers = $this->Customer->find('list', array('fields'=>array('id', 'code'), 'order'=>array('Customer.priority'=>'desc', 'Customer.code')));

        $types = Configure::read("order_type");

        $label = $types[$type];

        $today = date("Y-m-d");
        $me = $this->my_staff_id;

        $departments = $this->Order->Department->find("list");

        $this->set(compact('type', 'year', 'staffs', 'types', 'label', 'customers', 'today', 'me', 'departments'));

        $this->set('title_for_layout', $label . '登録・編集');
    }

    /**
     * [s_update 更新]
     * @return [type] [None]
     */
    function s_update(){

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $message = "更新しました";

            if (empty($this->data)) {
                $message = "セッションエラーです";
            } else {
                // 番号採番
                if (empty($this->request->data['Order']['id'])) {
                    $this->request->data['Order']['no'] = $this->getOrderNo($this->data['Order']['type'], $this->data['Order']['year']);
                }

                // 請求書の場合
                if ($this->data['Order']['type'] == 1) {
                    $order_no = explode('-', $this->request->data['Order']['no']);
                    // 御請求書220038-999
                    // -999 の意味
                    // 100の位 ・・・ 主幹部署(ITS1/ITS2/営業)
                    // 10の位 ・・・ 区分（契約の区分) (顧問/派遣/準委任/請負/SES・・・)
                    // 1の位 ・・・ 複数部署にまたがる案件かどうか(Mix案件かどうか)
                    $departments = $this->Order->Department->find("list", array("fields"=>array("Department.code")));

                    $this->request->data['Order']['no'] = sprintf("%s-%s%s%s", $order_no[0],
                                                                               $departments[$this->data['Order']['department_id']],
                                                                               $this->data['Order']['contract_type'],
                                                                               $this->data['Order']['is_mix']
                                                                            );
                }

                $this->Order->create();
                if (!$this->Order->save($this->request->data)) {
                    $message = "データの更新に失敗しました";
                }
            }
            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
            $this->redirect(array('action' => 'index', $this->data['Order']['type'], $this->data['Order']['year']));
        }
    }

    /**
     * [s_delete 削除]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function s_delete($id){
        $this->autoRender = false;

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }
        $data = $this->Order->find('first', array('conditions'=>array('Order.id'=>$id)));
        if(!empty($data)){
            $data['Order']['enable'] = 0;
            $this->Order->save($data);
            $this->redirect(array('action' => 'index', $data['Order']['type'], $data['Order']['year']));
        }
    }

    /**
     * [getNo 番号取得]
     * @param  [type] $type [種別]
     * @param  [type] $year [年度]
     * @return [type]       [None]
     */
    function getOrderNo($type, $year){

        $no = substr($year, 2, 2) . '0001';

        $data = $this->Order->find('first', array('conditions'=>array('type'=>$type, 'year'=>$year),
                                                  'fields'=>array('no'),
                                                  'order'=>array('no'=>'desc')));
        if ($data) {
            $no = $data['Order']['no'] + 1;
        }

        return $no;
    }

}

?>
