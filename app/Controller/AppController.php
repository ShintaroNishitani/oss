<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    var $components = array('Session', 'Common', );
    var $uses = array('AuthDetail', 'Staff', 'Access', 'AutoItem', 'LineworksToken');
    var $my_staff_id = null;

    public $helpers = array(
        'Session',
        'Html' => array('className' => 'BoostCake.BoostCakeHtml'),
        'Form' => array('className' => 'BoostCake.BoostCakeForm'),
        'Paginator' => array('className' => 'BoostCake.BoostCakePaginator',
        'Calendar')
    );


    /**
     * _checkStaffAuthority method
     *
     * @return (0:Not, 1:ReadOnly, 2:Editable , 3:Own Only)
     */
    function _checkStaffAuthority(){

        $ret = 2;
        $controller = $this->name;
        $action = $this->action;

        $username = $this->Session->read('username');
        if(!$this->Session->check('username') || empty($username)){
            $this->redirect("/");
        }

        $readonly = $this->Session->read('readonly');
        if (!empty($readonly)) {
            $ret = 1;
        }

        $con = array(
            "conditions" => array(
                "AuthDetail.authority_id" => $this->Session->read('authority_id'),
                "AuthDetail.name" => $this->name,
                "AuthDetail.action" => $this->action,
            ),
        );
        $auth = $this->AuthDetail->find("first", $con);
        if (empty($auth)) {
            $ret = 0;
        } else {
            $ret = $auth["AuthDetail"]["auth"];
        }

        return $ret;
    }

    /**
     * [_getStaffAuthority スタッフの権限取得]
     * @param  [type] $staff_id [スタッフID]
     * @return [type]           [None]
     */
    function _getStaffAuthority($staff_id){

        $staff = $this->Staff->find('first', array('conditions'=>array('Staff.id'=>$this->my_staff_id)));

        return $staff['Staff']['authority_id'];
    }

    /**
     * [_genRandomString ランダム文字列作成]
     * @param  [type] $length [description]
     * @return [type]         [description]
     */
    function _genRandomString($length){
        $seeds = 'abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ0123456789';
        $res = '';
        $i = 0;
        while($i < $length){
            $res .= substr($seeds, rand(0, strlen($seeds)), 1);
            $i++;
        }
        return $res;
    }

    /**
     * [a_autocomplete_list オートコンプリート]
     * @param  [type] $model [モデル名]
     * @return [type]        [None]
     */
    public function a_autocomplete_list($model){
        $this->layout = 'ajax';
        $con = array(
           "conditions" => array(
               "$model.name like" => sprintf('%%%s%%', $this->params["url"]["term"]),
           ),
        );

        $datas = $this->$model->find("list", $con);

        echo json_encode($datas);
        exit();
    }

    /**
     * [a_auto_item オートコンプリート]
     * @param  [type] $model [モデル名]
     * @return [type]        [None]
     */
    public function a_auto_item($type, $sub){

        $this->layout = 'ajax';
        $con = array(
           "conditions" => array(
               "AutoItem.name like" => sprintf('%%%s%%', $this->params["url"]["term"]),
               "AutoItem.sub_index" => $sub,
               "AutoItem.staff_id" => $this->my_staff_id,
               "AutoItem.type" => $type,
               "AutoItem.enable" => 1,
           ),
        );

        $datas = $this->AutoItem->find("list", $con);

        echo json_encode($datas);
        exit();
    }

    /**
     * [_sendMail メール送信]
     * @param  [type] $to      [宛先]
     * @param  [type] $subject [件名]
     * @param  [type] $body    [本文]
     * @param  [type] $files   [添付ファイル]　$files[] = array("name" => "ファイル名", "tmp_name" => "フルパス", "type" => "mime_type")
     * @param  [type] $from    [差出人]
     * @return [type]          [None]
     */
    function _sendMail($to, $subject, $body, $files, $from){
        if ($from == null) {
            $from = "oss_system@oceansoftware.co.jp";
        }

        if(Configure::read('debug') == 0){
            $email = new CakeEmail();
            $email->from(array($from => $from));
            $email->to(array($to => $to));
            $email->subject($subject);
            if ($files) {
                $email->attachments($files);
            }
            $email->send($body);
        }
        $this->log($to);
        $this->log($subject);
        $this->log($body);
    }

    /**
     * [_saveAutoItem オートコンプリート項目保存]
     * @param  [type] $type [種別]
     * @param  [type] $sub  [サブインデックス]
     * @param  [type] $name [名称]
     * @return [type]       [None]
     */
    function _saveAutoItem($type, $sub, $name){

        $con = array(
           "name" => $name,
           "type" => $type,
           "sub_index" => $sub,
           "staff_id" => $this->my_staff_id,
           "enable" => 1,
        );
        $this->AutoItem->create();
        $count = $this->AutoItem->find("count", array('conditions'=>$con));
        if (!$count) {
            $this->AutoItem->save($con);
        }
    }

    /**
     * [beforeFilter 共通処理]
     * @return [type] [description]
     */
    function beforeFilter(){
        $this->my_staff_id = $this->Session->read('my_staff_id');

        if(!empty($this->my_staff_id)){
            //アクセスログ
            $access = array(
                'Access' => array(
                    'staff_id' => $this->my_staff_id,
                    'url' => ((getenv('SERVER_PORT')==443)?'https://':'http://').getenv('HTTP_HOST').getenv('REQUEST_URI'),
                    'action' => "$this->action",
                    'controller' => "$this->name",
                    'referrer' => getenv('HTTP_REFERER'),
                    'ip' => getenv('REMOTE_ADDR'),
                    'user_agent' => getenv('HTTP_USER_AGENT'),
                    'post' => print_r($_POST, true),
                    'get' => print_r($_GET, true),
                    'session' => print_r($_SESSION, true),
                )
            );
            $this->Access->create();
            if (!$this->Access->save($access)) {
                $this->log('access log error');
                $this->log($this->Access->validationErrors);
            }
        }

        $change_pwd = false;

        $staff = $this->Staff->find('first', array('conditions'=>array('Staff.id'=>$this->my_staff_id)));
        if ($staff) {
            $this->set('app_name', $staff['Staff']['name']);
            // パスワード有効期限チェック
            if ($staff['Staff']['pwd_limit'] && strtotime($staff['Staff']['pwd_limit']) < time()) {
                $change_pwd = true;
            }
        }

        $this->set("change_pwd", $change_pwd);
    }

    /**
     * [begin begin]
     * @param  [type] $models [モデル]
     * @return [type]         [None]
     */
    public function begin($models) {
        //各モデル毎にトランザクションを開始
        foreach ($models as $model) {
            if ($model->begin()) {
                //トランザクション開始失敗時の処理
                break;
            }
        }
    }

    /**
     * [commit commit]
     * @param  [type] $models [モデル]
     * @return [type]         [None]
     */
    public function commit($models) {
        //各モデル毎にトランザクションを開始
        foreach ($models as $model) {
            if ($model->commit()) {
                //トランザクション開始失敗時の処理
                break;
            }
        }
    }

    /**
     * [rollback rollback]
     * @param  [type] $models [モデル]
     * @return [type]         [None]
     */
    public function rollback($models) {
        //各モデル毎にトランザクションを開始
        foreach ($models as $model) {
            if ($model->rollback()) {
                //トランザクション開始失敗時の処理
                break;
            }
        }
    }

    /**
     * [_sendLineWorks LineWorksにメッセージ送信]
     * @param  [type] $line_account_id  [LineWorksID]
     * @param  [type] $message          [メッセージ]
     * @return [type]                   [None]
     */
    // function _sendLineWorks($line_account_id, $message){

    //     if(Configure::read('debug') == 0){

    //         // $url = sprintf("https://apis.worksmobile.com/r/%s/message/v1/bot/%s/message/push", LW_APIID, LW_BOTNO);
    //         $url = sprintf("https://www.worksapis.com/v1.0/bots/%s/users/%s/messages", LW_BOTID, $line_account_id);
    //         $body = [
    //         'json' => [
    //             "content" => [
    //                 "type" => "text",
    //                 "text" => $message
    //             ]
    //         ]
    //         ];

    //         // $data = $this->LineworksToken->find('first');
    //         $token = $this->__getLineWorksToken();
    //         if(!empty($token)){
    //             $ch = curl_init($url);
    //             curl_setopt($ch, CURLOPT_POST, true);
    //             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    //             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //             curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body['json']));
    //             // curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    //             // 'Content-Type: application/json;charset=UTF-8',
    //             // 'consumerKey: '.LW_CONSUMERKEY,
    //             // 'Authorization: Bearer '.LW_TOKEN
    //             // ));
    //             curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    //                 'Authorization: Bearer '.$token,
    //                 'Content-Type: application/json;charset=UTF-8'
    //             ));
    //             $result = curl_exec($ch);
    //             curl_close($ch);
    //         }
    //     }
    // }

    /**
     * [__getLineWorksToken LineWorksトークン取得]
     * @param  [type]        [None]
     * @return [string] $token [トークン]
     */
    // function __getLineWorksToken(){
    //     $now = strtotime('now');
    //     $data = $this->LineworksToken->find('first');
    //     if(!empty($data)){
    //         $ref_expire = strtotime($data['LineworksToken']['refresh_expired_date']);
    //         $ac_expire = strtotime($data['LineworksToken']['access_expired_date']);
    //         // トークン有効期限チェック
    //         if ($now < $ac_expire){
    //             $token = $data['LineworksToken']['access_token'];
    //             return $token;
    //         }
    //         if($now < $ref_expire){
    //             $body = 'grant_type=refresh_token';
    //             $body .= '&';
    //             $body .= 'refresh_token='.urlencode($data['LineworksToken']['refresh_token']);
    //             $body .= '&';
    //             $body .= 'client_id='.LW_CLIENTID;
    //             $body .= '&';
    //             $body .= 'client_secret='.LW_CLIENTSECRET;
    //         }
    //     }
        
    //     if(empty($body)){
    //         $jwt = $this->Common->createJwt();
    //         $body = 'assertion='.$jwt;
    //         $body .= '&';
    //         $body .= 'grant_type=urn:ietf:params:oauth:grant-type:jwt-bearer';
    //         $body .= '&';
    //         $body .= 'client_id='.LW_CLIENTID;
    //         $body .= '&';
    //         $body .= 'client_secret='.LW_CLIENTSECRET;
    //         $body .= '&';
    //         $body .= 'scope=bot,bot.read';
    //     }
    //     $curl = curl_init(LW_URL_TOKEN);
    //     curl_setopt($curl, CURLOPT_POST, true);
    //     curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    //     curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
    //     curl_setopt($curl, CURLOPT_HTTPHEADER, [
    //         'Content-Type: application/x-www-form-urlencoded',
    //     ]);
    //     $res = curl_exec($curl);
    //     curl_close($curl);
        
    //     $res_decode = json_decode($res, true);
    //     if (array_key_exists("access_token", $res_decode))
    //     {
    //         $token =  $res_decode["access_token"];
    //         $expire = date("Y-m-d H:i:s", strtotime("+1 day", $now));
    //         $data['LineworksToken']['access_token'] = $token;
    //         $data['LineworksToken']['access_expired_date'] = $expire;
    //         if (array_key_exists("refresh_token", $res_decode))
    //         {
    //             $ref_token =  $res_decode["refresh_token"];
    //             $expire = date("Y-m-d H:i:s", strtotime("+90 day", $now));
    //             $data['LineworksToken']['refresh_token'] = $ref_token;
    //             $data['LineworksToken']['refresh_expired_date'] = $expire;
    //         }
    //         $this->LineworksToken->save($data, true);
    //     }
    //     return $token;
    // }
}
