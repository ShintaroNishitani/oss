<?php
/**
* 関連ファイル
*/
class FileListsController extends AppController
{
    var $types = array("training_progress" => "実施課題", "human_resources" => "人材管理");
    
    /**
     * [a_list リスト]
     * @param  [type] $type [種別]
     * @param  [type] $id   [ファイルリストID]
     * @return [type]       [None]
     */
    public function a_list($type = null, $id = null){
        $files = array();
        if ($id) {
            $con = array(
                "conditions" => array(
                    "FileList."."$type"."_id" => $id,
                ),
            );
            $files = $this->FileList->find("all", $con);
        }
        $this->set('files', $files);
        $this->set('id', $id);
        $this->set('type', $type);
        $this->set('types', $this->types);
        $this->layout = 'ajax';
    }
    
    /**
     * [a_upload_file ファイルアップロード]
     * @return [type] [None]
     */
    public function a_upload_file(){
        $this->layout = 'ajax';
        $id = null;
        $type = null;
        $error = 0;
        $message = "ファイルをアップロードしました。";

        if (2 == $this->_checkStaffAuthority()) { // 権限チェックはココに入れる
            if ($this->request->is('post')) {
                // 複数ファイル対応
                $file_datas = $this->request->data["FileList"]["files"];
                foreach ($file_datas as $file_data) {
                    $error = 0;

                    // まずはレコード保存してid取得する
                    // その後ファイルを保存
                    foreach ($this->types as $t => $type_name) {
                        if (isset($this->request->data["FileList"][$t."_id"])) {
                            $id = $this->request->data["FileList"][$t."_id"];
                            $type = $t;
                        }
                    }
                    
                    $save = array(
                        "FileList" => array(
                            $type."_id" => $id,
                            "name" => $file_data["name"],
                            "file_type" => $file_data["type"],
                            "file_size" => $file_data["size"],
                            "remark" => $this->request->data["FileList"]["remark"],
                            "enable" => 1,
                        )
                    );
                    $this->FileList->create();
                    $this->FileList->query("begin");
                    if (!$this->FileList->save($save)) {
                        $error++;
                        $message = "データの保存に失敗しました。";
                    } else {
                        $new_id = $this->FileList->getId();
                        if (!$this->Common->saveFileList($id, $new_id, $file_data, $res)) {
                            $error++;
                            $message = "ファイルのアップロードに失敗しました。";
                        } else {
                            $save = array(
                                "FileList" => array(
                                    "id" => $new_id,
                                ),
                            );
                            foreach ($res as $key => $value) {
                                $save["FileList"][$key] = $value;
                            }
                            if (!$this->FileList->save($save)) {
                                $error++;
                                $message = "データの更新に失敗しました。";
                            }
                        }
                    }
                    if ($error) {
                        $this->FileList->query("rollback");
                    } else {
                        $this->FileList->query("commit");
                    }
                }
            }
        } else {
            $error++;
            $message = "権限がありません。";
        }
        $result = sprintf('{"error":"%d","message":"%s"}', $error, $message);
        $this->set('result', $result);
        $this->set('id', $id);
        $this->set('type', $type);
    }
    
    /**
     * [a_delete 削除]
     * @param  [type] $id [ファイルリストID]
     * @return [type]     [None]
     */
    function a_delete($id = null){
        $error = 0;
        $message = "削除しました。";
        if (2 == $this->_checkStaffAuthority()) { // 権限チェックは必ず入れるように
            $con = array(
                "conditions" => array(
                    "FileList.id" => $id,
                )
            );
            $file = $this->FileList->find("first", $con);
            if (empty($file)) {
                $error++;
                $message = "該当のデータがありません。";
            } else {
                $sql = sprintf('update file_lists set enable=0 where id=%d', $id);
                $this->FileList->query($sql);
            }
        } else {
            $error++;
            $message = "権限がありません。";
        }
        echo sprintf('{"error":"%d","message":"%s"}', $error, $message);
        $this->autoRender = false;
    }
    
    /**
     * [a_download ダウンロード]
     * @param  [type] $id [ファイルリストID]
     * @return [type]     [None]
     */
    function a_download($id = null){
        $auth = $this->_checkStaffAuthority();
        if ($auth) { // 権限チェックは必ず
            $con = array(
                "conditions" => array(
                    "FileList.id" => $id
                )
            );
            $f = $this->FileList->find("first", $con);
            if (empty($f)) {
                echo "該当のファイルがありません。";
                exit();
            }
            $filepath = APP.$f["FileList"]["path"].$f["FileList"]["filename"];
            header(sprintf('Content-Disposition: attachment; filename="%s"', mb_convert_encoding($f["FileList"]["name"], "SJIS", "UTF-8")));
            header('Content-Type: application/octet-stream');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: '.filesize($filepath));
            readfile($filepath);
            
        } else {
            echo "権限がありません。";
            exit();
        }
    }
}

?>