<?php
App::uses('AppController', 'Controller');

/**
 * 従業員
 */
class StaffsController extends AppController {

    var $components = Array("Cookie");

    /**
     * [a_index 一覧]
     * @return [type] [None]
     */
    function a_index(){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        $count = $this->Staff->find("count", array('conditions'=>array('Staff.retire_date'=>null, 'Staff.no <>'=>9999)));

        $this->set(compact('count'));
        $this->set('title_for_layout', '従業員管理');
    }

    /**
     * [a_list リスト]
     * @return [type] [None]
     */
    public function a_list($retire = ""){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        $this->layout = "ajax";

        $con = array();
        if (empty($retire)) {
        	$con = array('Staff.retire_date'=>null);
        }
        $datas = $this->Staff->find("all", array('conditions'=>$con));

        $auths = $this->Staff->Authority->find("list");
        $departments = $this->Staff->Department->find("list");


        $this->set(compact('datas', 'auths', 'departments'));
    }

    /**
     * [a_edit 登録・編集]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function a_edit($id = null){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            echo '権限がありません';
            exit();
        }

        $this->layout = "ajax";

        $data = $this->Staff->find("first", array("conditions"=>array("Staff.id"=>$id)));
        if (!empty($data)) {
            $data["Staff"]["password"] = "";
            $this->data = $data;
        }

        $auths = $this->Staff->Authority->find("list");
        $departments = $this->Staff->Department->find("list");

        // 電子印が存在するならパス情報を送る
        $images = $data["Staff"]["mark"];
        if(file_exists($images)){
            $images = str_replace("../webroot/img/", "", $images);
        }
        else{
            $images = "";
        }
        $this->set(compact('auths','images','departments', 'id'));
    }

    /**
     * [a_update 更新]
     * @return [type] [None]
     */
    function a_update(){
        $error = 0;
        $message = "更新しました。";
        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $message = "登録権限がありません";
            $error++;
        } else {
            if (!empty($this->request->data)) {
                if(!empty($this->request->data["Staff"]["password"])){//パスワード入力があれば
                    if ($this->request->data["Staff"]["password"] != $this->request->data["Staff"]["password2"]) {
                        $error++;
                        $message = "パスワードが一致しません。";
                    }
                } else {//パスワード入力無し
                    unset($this->request->data["Staff"]["password"]);
                }
                if(empty($this->request->data['Staff']['id'])){
                    $this->Staff->create();
                } else {
                    $con = array(
                        "conditions" => array(
                            "Staff.id" => $this->request->data['Staff']['id'],
                        ),
                    );
                    $staff = $this->Staff->find("first", $con);
                    if(empty($staff)){
                        $error++;
                        $message = "不正なアクセスです。";
                    }
                    if(!empty($this->request->data["Staff"]["password"])){ // パスワード入力あり
                        if(strlen($this->request->data["Staff"]["password"]) < 5) {
                            $error++;
                            $message = "新しいパスワードは５文字以上にしてください。";
                        }
                    } else {
                        unset($this->request->data["Staff"]["password"]);
                    }
                }
                if(!$error){
                    if (!$this->Staff->save($this->request->data)) {
                        $error++;
                        $message = "更新に失敗しました";
                    }
                }
            } else {
                $error++;
                $message = "不正なアクセスです。";
            }
        }
        echo sprintf('{"error":"%d","message":"%s"}', $error, $message);
        exit();

    }

    /**
     * [a_delete 削除]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function a_delete($id){
        $error = 0;
        $message = "削除しました。";

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $error++;
            $message = "権限がありません";
        } else {

            $data = $this->Staff->find('first', array('conditions'=>array('Staff.id'=>$id)));
            $this->log($data);
            if(!empty($data)){
                $data['Staff']['enable'] = 0;
                if (!$this->Staff->save($data)) {
                    $error++;
                    $message = "削除に失敗しました";
                }
            }
        }

        echo sprintf('{"error":"%d","message":"%s"}', $error, $message);
        exit();
    }

    /**
     * [a_change_password パスワード変更]
     * @return [type] [description]
     */
    function a_change_password(){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません');
            $this->redirect($this->referer());
        }

        $key = $this->_genRandomString(16);
        $this->set('change_password_key', $key);
        $this->Session->write("change_password_key", $key);

        $this->layout = 'ajax';
    }

    /**
     * [a_change_password_commit パスワード変更コミット]
     * @return [type] [description]
     */
    function a_change_password_commit(){
        $error = 0;
        $message = "パスワードが変更されました。";

        if (!empty($this->data)) {
            $session_key = $this->Session->read("change_password_key");
            if ($this->data["Staff"]["change_password_key"] != $session_key) {
                $error++;
                $message = "セッションエラーです。";
            } else {
                $my_Staff_id = $this->Session->read('my_staff_id');

                $con = array(
                    "conditions" => array(
                        "Staff.id" => $my_Staff_id,
                        "Staff.password" => $this->data["Staff"]["password"],
                    ),
                );
                $staff = $this->Staff->find("first", $con);
                if (empty($staff)) {
                    $error++;
                    $message = "現在のパスワードが正しくありません。";
                } else {
                    if (strlen($this->data["Staff"]["new_password"]) < 5) {
                        $error++;
                        $message = "新しいパスワードは５文字以上にしてください。";
                    } else if ($this->data["Staff"]["new_password"] != $this->data["Staff"]["new_password2"]) {
                        $error++;
                        $message = "新しいパスワードが一致しません。";
                    } else {
                        $pwd_limit = date('Y-m-d H:i:s',strtotime("+3 months"));
                        $sql = sprintf('update staffs set password="%s", modified=now(), pwd_limit="%s" where id="%d"', $this->data["Staff"]["new_password"], $pwd_limit, $my_Staff_id);
                        $this->Staff->query($sql);
                    }
                }
            }
        } else {
            $error++;
            $message = "不正なアクセスです。";
        }
        echo sprintf('{"error":"%d","message":"%s"}', $error, $message);
        $this->autoRender = false;
    }
    function a_upload($id){

        $message = "電子印が登録・変更されました。";

        $directory_path = "../webroot/img/mark/";

        //「$directory_path」で指定されたディレクトリが存在するか確認
        if(!file_exists($directory_path)){
            if(mkdir($directory_path)){
                //作成したディレクトリのパーミッションを確実に変更
                chmod($directory_path);
            }else{
                //作成に失敗した時の処理
                $message = "フォルダの作成に失敗しました。";
            }
        }
        if(move_uploaded_file($this->request->data['Staff']['image']['tmp_name'],'../webroot/img/mark/'.$this->request->data['Staff']['image']['name']))
        {
            // 成功
            $path = $directory_path.$this->request->data['Staff']['image']['name'];
            $data = $this->Staff->find('first', array('conditions'=>array('Staff.id'=>$id)));
            $data['Staff']['mark'] = $path;
            if (!$this->Staff->save($data,false))
            {
                // 社員テーブル更新失敗
                $message = "更新に失敗しました。";
            }
        }
        else
        {
             // 社員テーブル更新失敗
            $message = "電子印が登録・変更に失敗しました。";
        }
        echo sprintf('{message":"%s"}', $message);
        exit();
    }

    /**
     * [login ログイン]
     * @return [type] [None]
     */
    public function login(){

        $error = 0;
        $message = "";
		if ($this->request->is('post') || $this->request->is('put')) {

			if(("" != $this->request->data['Staff']['mail']) && ("" != $this->request->data['Staff']['password'])){
				$username = $this->request->data['Staff']['mail'];
				$password = $this->request->data['Staff']['password'];

				//セッションに情報を保持する
				$this->Session->write('username', $username);
				$this->Session->write('password', $password);

				$staffs = $this->Staff->find('first', array('conditions' => array('Staff.mail'=>$username, 'Staff.password'=>$password)));
				if(!empty($staffs)){
                    if (!$staffs['Staff']['retire_date']) {
                        if (!empty($this->request->data["Staff"]["save_mail"])) {
                            $this->Cookie->write('username', $this->request->data["Staff"]["mail"], false, 90 * 24 * 60 * 60);
                        } else {
                            $this->Cookie->delete('username');
                        }

                        $this->Session->write('authority_id', $staffs["Staff"]["authority_id"]);
                        $this->Session->write('my_staff_id', $staffs["Staff"]["id"]);
                        $this->Session->delete("rule_auth");
                        $this->Session->delete("rule_session");
                        $this->redirect("../s/tops/index");
                    } else {
                        $message = "システムを使用できません。";
                        $error++;
                    }
				}
				else {
					$message = "メールアドレスまたはパスワードが違います";
                    $error++;
				}
			}else{
				$username = "";
				$password = "";
				$message = "メールアドレスとパスワードを入力して下さい";
                $error++;
			}
		}

		$this->set('title_for_layout', 'ログイン');
        if ($error) {
            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
        }
        $this->set(compact('username', 'message'));

		$this->layout = 'non_menu';

	}

    /**
     * [a_logout ログアウト]
     * @return [type] [None]
     */
    public function a_logout(){
        $this->autoRnder = false;

        // セッションを破棄してログイン画面へ
        $this->Session->delete('username');
        $this->Session->delete("readonly");
        $this->Session->delete("rule_auth");
        $this->Session->delete("rule_session");
        $this->redirect("/"); //アドレス
    }

    /**
     * [a_auth_update 権限更新]
     * @return [type] [None]
     */
    public function a_auth_update(){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        if (!empty($this->request->data)) {
            $error = 0;
            $this->Staff->query("begin");
            $this->Staff->query("delete from auth_details");

            $fp = fopen($this->request->data["Staff"]["file"]["tmp_name"], "r");
            $head = fgets($fp);
            $head = str_replace("\n", "", $head);
            $heads = explode(',', $head);

            while ($line = fgets($fp)) {
                $line = str_replace("\n", "", $line);
                $data = explode(',', $line);
                $name = $data[1];
                $action = $data[2];

                if (empty($action) || empty($name)) {
                    continue;
                }

                for ($i=3; $i < count($data); $i++) {
                    $authority_id = $heads[$i];
                    $sql = sprintf('insert into auth_details(name,action,authority_id,auth)values("%s","%s","%d","%d");',
                        $name, $action, $authority_id, $data[$i]
                    );
                    $this->Staff->query($sql);
                }
            }
            if ($error) {
                $this->Staff->query("rollback");
                $this->Session->setFlash('更新に失敗しました。', 'default', array('class'=> 'alert alert-info'));
            } else {
                $this->Staff->query("commit");
                $this->Session->setFlash('更新しました。', 'default', array('class'=> 'alert alert-info'));
            }
        }
    }

    public function a_coming_soon(){

    }

    function a_send_line_works($id){
        $error = 0;
        $message = "送信されました。";

        if (!empty($id)) {
            $data = $this->Staff->find("first", array("conditions"=>array("Staff.id"=>$id)));
            if ($data && $data["Staff"]["line_account_id"]) {
                // $this->_sendLineWorks($data["Staff"]["line_account_id"], $data["Staff"]["name"]."さん\n\nこちらはOSSからの自動送信メッセージとなります。\nこの度、OSSとLineWorksをシステム連携いたしました。\n今後はOSSからの通知がこちらでも確認できます。\n尚、こちらに返信いただいても、返信内容の確認およびご返答ができません。\nあらかじめご了承ください。\n\n本人宛ではない場合、お手数ですが西谷まで連絡をお願いいたします。");
                $this->Common->sendLineWorks($data["Staff"]["line_account_id"], $data["Staff"]["name"]."さん\n\nこちらはOSSからの自動送信メッセージとなります。\nこの度、OSSとLineWorksをシステム連携いたしました。\n今後はOSSからの通知がこちらでも確認できます。\n尚、こちらに返信いただいても、返信内容の確認およびご返答ができません。\nあらかじめご了承ください。\n\n本人宛ではない場合、お手数ですが西谷まで連絡をお願いいたします。");
            }
        } else {
            $error++;
            $message = "不正なアクセスです。";
        }
        echo sprintf('{"error":"%d","message":"%s"}', $error, $message);
        $this->autoRender = false;
    }
}

?>
