<?php
App::uses('AppController', 'Controller');

/**
 * 実施課題進捗
 */
class TrainingProgressDetailsController extends AppController {

    /**
     * [a_index 一覧]
     * @return [type]         [None]
     */
    function s_index($id) {

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        $staffs = $this->TrainingProgressDetail->Staff->find('list');
        $data = $this->TrainingProgressDetail->TrainingProgress->find("first", array("conditions"=>array("TrainingProgress.id"=>$id)));

        $this->set(compact('data', 'staffs'));
        $this->set('title_for_layout', '実施課題 進捗一覧');

    }

    /**
     * [a_list リスト]
     * @return [type] [None]
     */
    public function s_list($id) {

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        $this->layout = "ajax";

        $datas = $this->TrainingProgressDetail->find("all", array('conditions'=>array('TrainingProgressDetail.training_progress_id'=>$id),
                                                                  'order'=>array('TrainingProgressDetail.modified'=>'desc')));

        $this->set(compact('datas'));
    }


    /**
     * [a_edit 登録・編集]
     * @param  integer $genre [ジャンル]
     * @param  [type]  $id    [ID]
     * @return [type]         [None]
     */
    function s_edit($id = null){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

    	$this->layout = "ajax";

        $this->set(compact('id'));
    }


    /**
     * [a_update 更新]
     * @return [type] [None]
     */
    function s_update() {
        $error = 0;
        $message = "更新しました。";

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        if (!empty($this->request->data)) {
            if(empty($this->request->data['TrainingProgressDetail']['id'])){//新規登録
                $this->TrainingProgressDetail->create();
            }
            $data = $this->request->data;
            $data['TrainingProgressDetail']['staff_id'] = $this->my_staff_id;
            if (!$this->TrainingProgressDetail->save($data)) {
                $error++;
                $message = "更新に失敗しました";
            }
        } else {
            $error++;
            $message = "不正なアクセスです。";
        }
        echo sprintf('{"error":"%d","message":"%s"}', $error, $message);
        exit();

	}


}

?>