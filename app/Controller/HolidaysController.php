<?php
App::uses('AppController', 'Controller');

/**
 * 休日設定
 */
class HolidaysController extends AppController {
    public $paginate = array(
        'page' => 1,
        'conditions' => array(''),
        );

    /**
     * [a_index 一覧]
     * @return [type]           [None]
     */
    function a_index(){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        $this->paginate['Holiday'] = array('limit'=>100, 'order'=>array('date'=>'desc'));
        $datas = $this->paginate('Holiday');

        $this->set(compact('datas'));

        $this->set('title_for_layout', '休日設定');
    }

    /**
     * [a_edit 更新]
     * @param  [type] $id    [ID]
     * @return [type]        [None]
     */
    function a_edit($id = null){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            echo '権限がありません';
            exit();
        }

        $this->layout = "ajax";
        $this->Holiday->unbindModelAll();
        $data = $this->Holiday->find("first", array("conditions" => array("Holiday.id" => $id), 'fields'=>array('*')));
        if (!empty($data)) {
            $this->data = $data;
        }

        $this->set(compact('id', 'data'));
        $this->set('title_for_layout', '休日登録・編集');
    }

    /**
     * [a_update 更新]
     * @return [type] [None]
     */
    function a_update(){

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $message = "更新しました";

            if (empty($this->data)) {
                $message = "セッションエラーです";
            } else {

                $this->Holiday->create();
                if (!$this->Holiday->save($this->request->data)) {
                    $message = "データの更新に失敗しました";
                }
            }
            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
            $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * [a_delete 削除]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function a_delete($id){
        $this->autoRender = false;

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        $this->Holiday->delete($id);
        $this->redirect(array('action' => 'index'));
    }

    /**
     * [a_autoset 自動設定]
     * @return [type] [description]
     */
    function a_autoset(){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            echo '権限がありません';
            exit();
        }

    	$this->layout = "ajax";

    	$year = date('Y');

        $this->set(compact('year'));
        $this->set('title_for_layout', '休日自動設定');
    }

    /**
     * [a_update 自動設定更新]
     * @return [type] [None]
     */
    function a_autoset_upload(){

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $message = "更新しました";

            if (empty($this->data)) {
                $message = "セッションエラーです";
            } else {

		        $year = $this->data['Holiday']['year'];
		        if ($year) {
					$start_date = mktime(0, 0, 0, intval(1), 1, intval($year));
					$end_date = mktime(0, 0, 0, intval(12), 31, intval($year));
					$api_key = GOOGLE_API_KEY;
					$holidays_id = 'japanese__ja@holiday.calendar.google.com';  // Google 公式版日本語
					$holidays_url = sprintf(
						'https://www.googleapis.com/calendar/v3/calendars/%s/events?'.
						'key=%s&timeMin=%s&timeMax=%s&maxResults=%d&orderBy=startTime&singleEvents=true',
						$holidays_id,
						$api_key,
						date('Y-m-d', $start_date).'T00:00:00Z' ,  // 取得開始日
						date('Y-m-d', $end_date).'T00:00:00Z' ,   // 取得終了日
						31            // 最大取得数
					);
					if ( $results = file_get_contents($holidays_url) ) {
						$results = json_decode($results);
						$holidays = array();
						foreach ($results->items as $item ) {
						  $date  = strtotime((string) $item->start->date);
						  $title = (string) $item->summary;
						  $holidays[date('Y-m-d', $date)] = $title;
						}
						ksort($holidays);

						foreach ($holidays as $date => $name) {
					        $data = $this->Holiday->find("first", array("conditions" => array("Holiday.date" => $date), 'fields'=>array('*')));
					        if (empty($data)) {
					        	$data['name'] = $name;
					        	$data['date'] = $date;
				                $this->Holiday->create();
				                if (!$this->Holiday->save($data)) {
				                    $message = "データの更新に失敗しました";
				                }
					        }
						}
					} else {
						$message = "データの更新に失敗しました";
					}
				} else {
					$message = "年を入力してください。";
				}
            }
            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
            $this->redirect(array('action' => 'index'));
        }
    }
}

?>
