<?php
App::uses('AppController', 'Controller');

/**
 * 振休閲覧・管理
 */
class TransHolidaysController extends AppController {
    var $uses = array('TransHoliday','Staff');

    public $paginate = array(
        'page' => 1,
        'conditions' => array(''),
        );

    public $not_use = "0000-00-00";

    public $select_trans_option = array('1' => '振休未消化' ,
                                        '2' => '振休消化済' ,
                                        '3' => '振休全表示');

    /**
     * [s_index 一覧]
     * @param  [type] $select_trans [セレクトボックスで選択した振休状況]
     * @param  [type] $trans_from   [選択した日付(始まり)]
     * @param  [type] $trans_to     [選択した日付(終わり)]
     * @return [type]               [None]
     */
    function s_index( $select_trans = "1" , $trans_from = "0000-00-00" , $trans_to = "9999-12-31"){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));            $this->redirect($this->referer());   
        }
        
        $year = $this->Common->getYear();   // 年度取得

        $staff_id = $this->Session->read('my_staff_id');

        // データを取得する
        if( $this->request->query('select_trans') != ""){
            $select_trans = $this->request->query('select_trans');
        }
        if( $this->request->query('trans_from') != ""){
            $trans_from = $this->request->query('trans_from');
        }
        if( $this->request->query('trans_to') != ""){
            $trans_to = $this->request->query('trans_to');
        }

        // 振休未使用
        $select_trans_1 = array( 'TransHoliday.staff_id' => $staff_id ,
                                 'OR' => array( 'TransHoliday.digestion_date1' => $this->not_use ,
                                 'TransHoliday.digestion_date2' => $this->not_use ));
        // 振休使用済
        $select_trans_2 = array( 'TransHoliday.staff_id' => $staff_id ,
                                 "TransHoliday.digestion_date1 !=" => $this->not_use ,
                                 "TransHoliday.digestion_date2 !=" => $this->not_use );

        // 振休指定無し(デフォルト)
        $select_trans_3 = array( 'TransHoliday.staff_id' => $staff_id);

        // 振休未使用
        $cnd_unused = array( 'TransHoliday.staff_id' => $staff_id ,
                             'TransHoliday.digestion_date1' => $this->not_use );
        // 半休使用済
        $cnd_halfused = array( 'TransHoliday.staff_id' => $staff_id ,
                               'TransHoliday.digestion_date1 !=' => $this->not_use,
                               'TransHoliday.digestion_date2 ' => $this->not_use );

        // 引数 $select_transに基づいてconditionを変更する
        switch ($select_trans){
            case 1:
              $con = $select_trans_1;
              break;
            case 2:
              $con = $select_trans_2;
              break;
            case 3:
              $con = $select_trans_3;
              break;
            default:
              $this->Session->setFlash('検索エラー 振休指定が不正です。', 'default', array('class'=> 'alert alert-info'));
              $this->redirect($this->referer());   
        }
        
        // 日付指定を追加
        array_push($con, array("TransHoliday.accrual_date >="=> $trans_from),
                         array("TransHoliday.accrual_date <="=> $trans_to));

        // globalの変数を設定
        $select_trans_option= $this->select_trans_option;

        // 件数を取得
        $trans_count = $this->TransHoliday->find('count', array('conditions'=>array($cnd_unused)));
        $half_count = $this->TransHoliday->find('count', array('conditions'=>array($cnd_halfused)));
        $trans_count += $half_count * 0.5;
        
        // 日付が指定されていない場合は表示を消すために空にする
        if( $trans_from == "0000-00-00"){
            $trans_from = "";
        }
        if( $trans_to == "9999-12-31"){
            $trans_to = "";
        }
        
        // paginate設定
        $this->paginate['TransHoliday'] = array('limit'=>100, "conditions"=>$con, 'fields'=>array('*'));
        $datas = $this->paginate('TransHoliday');

        $id = 0;

        $this->set(compact('datas','id','trans_count', 'select_trans_option' , 'select_trans','trans_from','trans_to'));

        $this->set('holiday_data', $datas);

        $this->set('title_for_layout', '振休閲覧');
    }


    /**
     * [a_index 一覧]
     * @param  [type] $select_staff [セレクトボックスで選択したスタッフのID]
     * @param  [type] $select_trans [セレクトボックスで選択した振休状況]
     * @param  [type] $trans_from   [選択した日付(始まり)]
     * @param  [type] $trans_to     [選択した日付(終わり)]
     * @return [type]               [None]
     */
    function a_index( $select_staff = "" ,$select_trans = "1", $trans_from = "0000-00-00" , $trans_to = "9999-12-31"){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));            $this->redirect($this->referer());   
        }
        
        $year = $this->Common->getYear();   // 年度取得

        $con = array();

        $staffs = $this->Staff->find('list');

        // データを取得する
        if( $this->request->query('select_trans') != ""){
            $select_trans = $this->request->query('select_trans');
        }
        if( $this->request->query('trans_from') != ""){
            $trans_from = $this->request->query('trans_from');
        }
        if( $this->request->query('trans_to') != ""){
            $trans_to = $this->request->query('trans_to');
        }
        $select_staff = $this->request->query('select_staff');

        // 振休未使用
        $select_trans_1 = array( 'OR' => array( 'TransHoliday.digestion_date1' => $this->not_use ,
                                 'TransHoliday.digestion_date2' => $this->not_use ));
        // 振休使用済
        $select_trans_2 = array( "TransHoliday.digestion_date1 !=" => $this->not_use ,
                                 "TransHoliday.digestion_date2 !=" => $this->not_use );
        // 振休指定無し(デフォルト)
        $select_trans_3 = array();

        // globalの変数を設定
        $select_trans_option= $this->select_trans_option;

        // 引数 $select_transに基づいてconditionを変更する
        switch ($select_trans){
            case 1:
              $con = $select_trans_1;
              break;
            case 2:
              $con = $select_trans_2;
              break;
            case 3:
              $con = $select_trans_3;
              break;
            default:
              $this->Session->setFlash('検索エラー 振休指定が不正です。', 'default', array('class'=> 'alert alert-info'));
              $this->redirect($this->referer());   
        }

        // 日付指定を追加
        array_push($con, array("TransHoliday.accrual_date >="=> $trans_from),
                         array("TransHoliday.accrual_date <="=> $trans_to));

        if( $select_staff != ""){
            array_push($con,array( "TransHoliday.staff_id" => $select_staff));
        }

        // 日付が指定されていない場合は表示を消すために空にする
        if( $trans_from == "0000-00-00"){
            $trans_from = "";
        }
        if( $trans_to == "9999-12-31"){
            $trans_to = "";
        }

        // paginate設定
        $this->paginate['TransHoliday'] = array('limit'=>100, "conditions"=>$con, 'fields'=>array('*'));
        $datas = $this->paginate('TransHoliday');

        $id = 0;

        $this->set(compact('datas','id','staffs','select_trans_option' , 'select_trans','select_staff','trans_from','trans_to'));

        $this->set('holiday_data', $datas);

        $this->set('title_for_layout', '振休管理');
    }


    /**
     * [a_edit 登録・編集]
     * @param  [type] $id       [description]
     * @return [type]           [description]
     */
    function a_edit($id){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        $this->layout = "ajax";
        $this->TransHoliday->unbindModelAll();
        $data = $this->TransHoliday->find("first", array("conditions" => array("TransHoliday.id"=>$id)));
        if (!empty($data)) {
            $this->data = $data;
        }        

        $staffs = $this->Staff->find('list');

        $this->set(compact('data', 'staffs'));

        $this->set('title_for_layout',  '振休登録・編集');
    }

    /**
     * [a_update 更新]
     * @return [type] [None]
     */
    function a_update(){

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        //$this->log($this->request->data,LOG_DEBUG);

        if ($this->request->is('post') || $this->request->is('put')) {
            $message = "更新しました";

            if (empty($this->data)) {
                $message = "セッションエラーです";
            } else {
            
                if (empty($this->request->data['TransHoliday']['id'])) {
                    $this->request->data['TransHoliday']['id'] = 0;
                }
                $this->TransHoliday->create();
                $this->data['TransHoliday']['id'];
                //$this->log($this->request->data);
                if (!$this->TransHoliday->save($this->request->data)) {
                    //$this->log($this->request->data,LOG_DEBUG);
                    $message = "データの更新に失敗しました";
                }
                //$this->log($this->request->data,LOG_DEBUG);
            }
            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
            $this->redirect(array('action' => 'index', $this->data['TransHoliday']['id']));
        }
    }
    
    /**
     * [a_delete 削除]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function a_delete($id){
        $this->autoRender = false;

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }
        $data = $this->TransHoliday->find('first', array('conditions'=>array('TransHoliday.id'=>$id)));
        if(!empty($data)){
            $data['TransHoliday']['enable'] = 0;
            $this->TransHoliday->save($data);
            $this->redirect($this->referer());
        }
    }




}

?>