<?php
App::uses('AppController', 'Controller');

/**
 * 交通費
 */
class TransportsController extends AppController {
    var $uses = array('Transport', 'Confirm', 'Staff', 'TransportTemplate');

    public $paginate = array(
        'page' => 1,
        'conditions' => array(''),
        );

    /**
     * [s_index 一覧]
     * @param  [type] $year     [年]
     * @param  [type] $month    [月]
     * @param  [type] $staff_id [スタッフID]
     * @return [type]           [None]
     */
    function s_index($year = null, $month = null){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        if ($year == null || $month == null) {
            $year = date('Y');
            $month = date('n');
        }

        //前月、翌月を取得
        $this->Common->getBeforeMonth($year, $month, $b_year, $b_month);
        $this->Common->getNextMonth($year, $month, $n_year, $n_month);

        $staff_id = $this->Session->read('my_staff_id');

        // 検索条件作成
        $con = array("Transport.year"=>$year,
                     "Transport.month"=>$month,
                     "Transport.staff_id"=>$staff_id);

        $datas = $this->Transport->find('first', array('conditions'=>$con));
        if (empty($datas)) {
            // 交通費保存
            $this->Transport->create();
            $data['Transport'] = array('staff_id'=>$staff_id,
                                       'year'=>$year,
                                       'month'=>$month,
                                       'pay_date'=>null,
                                       'price'=>0,
                                       'status'=>0,
                                       'enable'=>1,
                                      );
            $this->Transport->save($data);
            $id = $this->Transport->id;            
        } else {
            $id = $datas['Transport']['id'];
        }

        $status = 0;
        $confirm = $this->Confirm->find('first', array('conditions'=>array('type'=>CONFIRM_TYPE_1, 'type_id'=>$id)));
        if ($confirm) {
            $status = $confirm['Confirm']['status'];
        }

        $this->set(compact('year', 'month', 'b_year', 'b_month', 'n_year', 'n_month', 'datas', 'id', 'status'));

        $this->set('title_for_layout', '交通費精算');
    }

    /**
     * [a_index 一覧]
     * @param  [type] $year     [年]
     * @param  [type] $month    [月]
     * @param  [type] $staff_id [スタッフID]
     * @return [type]           [None]
     */
    function a_index($year, $month, $staff_id){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        //前月、翌月を取得
        $this->Common->getBeforeMonth($year, $month, $b_year, $b_month);
        $this->Common->getNextMonth($year, $month, $n_year, $n_month);

        // 検索条件作成
        $con = array("Transport.year"=>$year,
                     "Transport.month"=>$month,
                     "Transport.staff_id"=>$staff_id);

        $datas = $this->Transport->find('first', array('conditions'=>$con));

        $this->set(compact('year', 'month', 'b_year', 'b_month', 'n_year', 'n_month', 'datas', 'staff_id'));

        $this->set('title_for_layout', '交通費精算');
    }

    /**
     * [s_detail_edit 詳細更新]
     * @param  [type] $year      [年]
     * @param  [type] $month     [月]
     * @param  [type] $id        [ID]
     * @param  [type] $detail_id [詳細ID]
     * @return [type]            [None]
     */
    function s_detail_edit($year, $month, $id, $detail_id = null){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        $this->layout = "ajax";
        $this->Transport->unbindModelAll();
        $data = $this->Transport->TransportDetail->find("first", array("conditions" => array("TransportDetail.id"=>$detail_id)));
        if (!empty($data)) {
            $this->data = $data;
        }

        $staff_id = $this->Session->read('my_staff_id');
		$templates = $this->TransportTemplate->find('list', array("conditions" => array("TransportTemplate.staff_id"=>$staff_id)));

        $this->set(compact('year', 'month', 'staff_id', 'id', 'templates'));

        $this->set('title_for_layout', '交通費登録・編集');
    }

    /**
     * [s_detail_update 更新]
     * @return [type] [None]
     */
    function s_detail_update(){

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $message = "更新しました";

            if (empty($this->data)) {
                $message = "セッションエラーです";
            } else {

                $this->Transport->TransportDetail->create();
                if (!$this->Transport->TransportDetail->save($this->request->data)) {
                    $message = "データの更新に失敗しました";
                } else {
                    $this->_saveAutoItem(CONFIRM_TYPE_1, TRANSPORT_SUB_0, $this->request->data['TransportDetail']['place']);
                    $this->_saveAutoItem(CONFIRM_TYPE_1, TRANSPORT_SUB_1, $this->request->data['TransportDetail']['purpose']);
                    $this->_saveAutoItem(CONFIRM_TYPE_1, TRANSPORT_SUB_2, $this->request->data['TransportDetail']['transport']);
                    $this->_saveAutoItem(CONFIRM_TYPE_1, TRANSPORT_SUB_3, $this->request->data['TransportDetail']['arrival']);
                    $this->_saveAutoItem(CONFIRM_TYPE_1, TRANSPORT_SUB_3, $this->request->data['TransportDetail']['departure']);
                }
            }
            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
            $this->redirect(array('action' => 'index', $this->data['Transport']['TransportDetail']['year'], $this->data['Transport']['TransportDetail']['month']));
        }
    }

    /**
     * [s_detail_delete 削除]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function s_detail_delete($id){
        $this->autoRender = false;

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        $data = $this->Transport->TransportDetail->find('first', array('conditions'=>array('TransportDetail.id'=>$id)));
        if(!empty($data)){
            $data['TransportDetail']['enable'] = 0;
            $this->Transport->TransportDetail->save($data);
            $this->redirect(array('action' => 'index', $data['Transport']['year'], $data['Transport']['month']));
        }
    }

    /**
     * [a_print 印刷]
     * @param  [type] $id    [ID]
     * @return [type]        [None]
     */
    public function a_print($id){  

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        $datas = $this->Transport->find('first', array('conditions'=>array('Transport.id'=>$id)));

        $confirm = $this->Confirm->find('first', array('conditions'=>array('type'=>CONFIRM_TYPE_1, 'type_id'=>$id)));

        $staffs = $this->Staff->find('list');

        $this->set(compact('datas', 'confirm', 'staffs'));

        $this->render('s_print');
    }

    /**
     * [s_print 印刷]
     * @param  [type] $id    [ID]
     * @return [type]        [None]
     */
    public function s_print($id){  

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }
        $staff_id = $this->Session->read('my_staff_id');

        $datas = $this->Transport->find('first', array('conditions'=>array('Transport.id'=>$id)));
        if ($datas['Transport']['staff_id'] != $staff_id) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        $confirm = $this->Confirm->find('first', array('conditions'=>array('type'=>CONFIRM_TYPE_1, 'type_id'=>$id)));

        $staffs = $this->Staff->find('list');

        $this->set(compact('datas', 'confirm', 'staffs'));
    }    
    
    
    /**
     * [s_detail_edit 詳細更新]
     * @param  [type] $year      [年]
     * @param  [type] $month     [月]
     * @param  [type] $id        [ID]
     * @param  [type] $detail_id [詳細ID]
     * @return [type]            [None]
     */
    function s_template(){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }
        
        
        if ($this->request->is('post') || $this->request->is('put')) 
        {
        
			for($i = 0; $i < count($this->request->data["TransportTemplate"]); $i++)
			{
				$regdat = array();
				$regdat["TransportTemplate"] = $this->request->data["TransportTemplate"][$i];
				if(empty($regdat["TransportTemplate"]["id"]))
				{
					$this->TransportTemplate->create();
				}
				if (!$this->TransportTemplate->save($regdat)) {
					$message = "データの更新に失敗しました";
					$this->Session->setFlash($message, 'default', array('class'=> 'alert alert-danger'));
					$this->TransportTemplate->query("rollback");
					break;
				}
			}
			$this->Session->setFlash('テンプレートが登録されました。', 'default', array('class'=> 'alert alert-info'));
           	$this->redirect(array('action' => 'index'));
        }

        $staff_id = $this->Session->read('my_staff_id');
        $data = $this->TransportTemplate->find("all", array("conditions" => array("TransportTemplate.staff_id"=>$staff_id), 'order'=>array('TransportTemplate.idx'=>'asc')));
        $this->set(compact('staff_id', 'id', 'data'));

        $this->set('title_for_layout', '交通費テンプレート登録・編集');
    }    
    
	/**
	 * [search_human 技術者検索(ajax用)]
	 * @param  [type] [None]
	 * @return [type] [None]
	 */
	public function s_search_template() {

		$auth = $this->_checkStaffAuthority();
		if (0 == $auth) {
			echo '権限がありません';
			exit();
		}
		
		// ----------------------------------------
		// 初期処理
		// ----------------------------------------
		// ビューの使用無を設定
		$this->autoRender = false;

		// ContentTypeをJSONにする
		$this->response->type('json');

//		// Ajax以外の通信の場合
//		if(!$this->request->is('ajax')) {
//			throw new BadRequestException();
//		}

		//-------------------------------------------
		// リクエストパラメータ取得
		//-------------------------------------------
		$id = $this->request->query['id'];

		//-------------------------------------------
		// 検索
		//-------------------------------------------
		$result = $this->TransportTemplate->find("first", array("conditions"=>array("TransportTemplate.id"=>$id)));

		//-------------------------------------------
		// 戻り値の設定
		//-------------------------------------------
		$status = !empty($result);
		if(!$status) {
//			throw new NotFoundException();
			$error = array(
				'message' => 'データがありません',
				'code' => 404
			);
		}

		// json_encodeを使用してJSON形式で返却
		return json_encode(compact('status', 'result', 'error'));
	}
}

?>