<?php
App::uses('AppController', 'Controller');

/**
 * 資産
 */
class AssetsController extends AppController {
    var $uses = array('Asset', 'Staff');
    
    /**
     * [s_index 一覧]
     * @param  [type] [None]
     * @return [type] [None]
     */
    function s_index(){
        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }
        $datas = $this->Asset->find('all', array('order'=>array('purchase_date'=>'desc')));
        $this->set(compact('datas'));
        $this->set('title_for_layout', '資産管理');
    }

    /**
     * [s_edit 詳細更新]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function s_edit($id){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }
        $this->layout = "ajax";
        $this->Asset->unbindModelAll();
        $data = $this->Asset->find("first", array("conditions" => array("Asset.id"=>$id)));
        if (!empty($data)) {
            $this->data = $data;
        }
        $staffs = $this->Staff->find('list', array("conditions" => array("Staff.no !="=>"9999", "Staff.retire_date"=>null)));
        $this->set(compact('id','staffs'));
    }

    /**
     * [s_update 更新]
     * @param  [type] [None]
     * @return [type] [None]
     */
    function s_update(){

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $message = "更新しました";

            if (empty($this->data)) {
                $message = "セッションエラーです";
            } else {
                $this->Asset->create();
                if (!$this->Asset->save($this->request->data)) {
                    $message = "データの更新に失敗しました";
                } 
            }
            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
            $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * [s_delete 削除]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function s_delete($id){
        $this->autoRender = false;
        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }
        $data = $this->Asset->find('first', array('conditions'=>array('Asset.id'=>$id)));
        if(!empty($data)){
            $data['Asset']['enable'] = 0;
            $b = $this->Asset->save($data);
            $this->redirect(array('action' => 'index'));
        }
    }
}

?>