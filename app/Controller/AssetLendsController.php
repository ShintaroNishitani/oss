<?php
App::uses("AppController", "Controller");

/**
 * 資産貸出
 */
class AssetLendsController extends AppController {
    var $uses = array("AssetLend","Staff","Customer","AssetLendReturn","AssetLendInventory");

    /**
     * [s_index 一覧]
     * @param  [type] [None]
     * @return [type] [None]
     */

    //ページネート設定 更新日の降順
    public $paginate = array(
        'page' => 1,
        'conditions' => array(''),
        );

    function s_index(){
  
        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }
            
        $joins = array(
            array(
                "type" => "LEFT",
                "table" => "asset_lend_inventories",
                "alias" => "AssetLendInventory",
                "conditions" => array("AssetLendInventory.asset_lend_id = AssetLend.id"),
            ),
            array(
                "type" => "LEFT",
                "table" => "asset_lend_returns",
                "alias" => "AssetLendReturn",
                "conditions" => array("AssetLendReturn.asset_lend_id = AssetLend.id"),
            ),
        );    

        //設定20件ずつ、降順、enableが1、貸出期間が当日終了以降である。
        $this->paginate['AssetLend'] = array(
            'limit' => 20,
            'order' => array('AssetLend.asset_lend_number' => 'desc'),
            'conditions' => array('AssetLend.enable' => "1",'AssetLendReturn.status' !=0, "enable" == "0"),
            "recursive"=>2,
            'joins'=> $joins,
            'fields'=>array('*')
        );
        $datas = $this->paginate('AssetLend');
        $this->set(compact("datas"));
        $this->set($datas);
        $this->set("title_for_layout", "資産管理台帳");
    }

    /**
     * [s_edit 詳細更新]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function s_edit($id = null){
        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        if($id != null) {
            //編集
            //編集の場合取得したidから該当レコードを取得
            $data = $this->AssetLend->find("first", array("conditions" => array("AssetLend.id" => $id)));
            if (!empty($data)) {
                $this->data = $data;
            }

            //情報管理責任者の値から、社員情報を取得
            $staffs = $this->Staff->find("list",
                array("conditions" =>
                array("Staff.no !="       => "9999",
                      "Staff.retire_date" => null)));

            //顧客管理情報を取得
            $customers = $this->Customer->find('list', array('fields'=>array('id', 'full_name')));

        } else {
            //新規追加
            $data = null;
            $staffs = $this->Staff->find("list",
                array("conditions" =>
                array("Staff.no !="       => "9999",
                      "Staff.retire_date" => null)));

            //顧客管理情報を取得
            $customers = $this->Customer->find('list', array('fields'=>array('id', 'full_name')));
        }
        $this->set(compact("data","staffs","customers"));
        $this->set("title_for_layout", "資産貸出登録");
    }

    /**
    * [s_update ユーザ入力情報で資産情報更新]
    * @param  [type] [None]
    * @return [type] [None]
    */
    function s_update() {
        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }    
    
        if ($this->request->is('post') || $this->request->is('put')) {
            $message = "更新しました";

            if (empty($this->data)) {
                $message = "セッションエラーです";
            }
            else {
                if($this->request->data['AssetLend']['id'] == null) {
                    //新規追加
                    $this->AssetLend->create();
                }
                $this->log($this->request->data, LOG_DEBUG);
                unset($this->request->data['AssetLend']['created']);
                unset($this->request->data['AssetLend']['modified']);
                if (!$this->AssetLend->save($this->request->data, false)) {
                    $message = "同じ番号は登録できません。";
                }
            }
            $this->Session->setFlash($message, 'default', array('class' => 'alert alert-info'));
            $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * [s_delete 詳細更新]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function s_delete($id = null){
        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }
        $data = $this->AssetLend->find('first', array('conditions' => array('AssetLend.id' => $id)));
        if(!empty($data)) {
            $data['AssetLend']['enable'] = 0;
            $this->AssetLend->save($data,false);
            $this->redirect(array('action' => 'index'));
            $this->set('title_for_layout', '資産貸出登録');
        }
    }

    /**
     * [s_repayment 返却登録]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function s_repayment($id = null){

		$auth = $this->_checkStaffAuthority();
		if (0 == $auth) 
		{
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}
    
        if ($this->request->is('post') || $this->request->is('put')) 
        {
        	//AssetLendReturn.idがない場合は作成
			$asset_return_id = $this->request->data['AssetLendReturn']['id'];
			if (empty($asset_return_id)) 
			{
				// 	新規登録
				$this->AssetLendReturn->create();
			}
			if (!$this->AssetLendReturn->save($this->request->data['AssetLendReturn'])) 
			{
				$message = "人材情報の登録に失敗しました";
				$this->redirect($this->referer());
			}
			
        	$this->redirect(array('action' => 'index'));
        }

        $joins = array(
            array(
                "type" => "LEFT",
                "table" => "asset_lends",
                "alias" => "AssetLend",
                "conditions" => array("AssetLendReturn.asset_lend_id = AssetLend.id"),
            ),
        );    

        //編集
        //編集の場合取得したidから該当レコードを取得
        $data = $this->AssetLendReturn->find("first", 
        							   array("conditions" => array("AssetLendReturn.asset_lend_id" => $id),
        							         "recursive"=>2,
            								 'joins'=> $joins,
            								 'fields'=>array('*')
        							   ) );
        if (!empty($data)) 
        {
            $this->data = $data;
        }
        else
        {
        	$data = $this->AssetLend->find('first', array('conditions' => array('AssetLend.id' => $id)));
        	$this->data = $data;
        }

        //情報管理責任者の値から、社員情報を取得
        $staffs = $this->Staff->find("list",
            array("conditions" =>
            array("Staff.no !="       => "9999",
                  "Staff.retire_date" => null)));

        //顧客管理情報を取得
        $customers = $this->Customer->find('list', array('fields'=>array('id', 'full_name')));

        $this->set(compact("data","staffs","customers"));
        $this->set("title_for_layout", "資産返却登録");
    }



    /**
     * [s_print 印刷]
     * @param  [type] $id    [ID]
     * @return [type]        [None]
     */
    public function s_print($id)
    {
        // $auth = $this->_checkStaffAuthority();
        // $this->log($auth,LOG_DEBUG);
        // if (0 == $auth) {
        //     $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
        //     $this->redirect($this->referer());
        // }
        $datas = $this->AssetLend->find('first', array('conditions'=>array('AssetLend.id'=>$id),"recursive"=>2));
        $this->log($datas,LOG_DEBUG);

        // 日付フォーマット変換
        $datas["AssetLend"]["lend_date"] = date("yy年m月d日" ,strtotime($datas["AssetLend"]["lend_date"]));
        $datas["AssetLend"]["lend_start_date"] = date("yy年m月d日" ,strtotime($datas["AssetLend"]["lend_start_date"]));
        $datas["AssetLend"]["lend_end_date"] = date("yy年m月d日" ,strtotime($datas["AssetLend"]["lend_end_date"]));
        $datas["AssetLend"]["borrowing_period"] = $datas["AssetLend"]["lend_start_date"] . mb_convert_encoding(" ～ ", "UTF-8") . $datas["AssetLend"]["lend_end_date"];

        $department = "システム開発部";
        $this->set(compact("datas","department"));
    }
}