<?php
App::uses('AppController', 'Controller');

define('ALL_STAFF', 9998);				// 全社員
define('EXCEPT_OFFICER', 9997);			// 役員以外

/**
 * 特休閲覧・管理
 */
class SpecialHolidaysController extends AppController
{
	var $uses = array('SpecialHoliday','Staff');

	public $paginate = array(
		'page' => 1,
		'conditions' => array(''),
		);

	public $not_use = "0000-00-00";

	public $option_list = array('1' => '有効',
							    '2' => '無効',
							    '3' => '全表示');

	/**
	 * [s_index 特休一覧（ログインユーザ）]
	 * @param  [type] $select_option [表示オプション]
	 * @param  [type] $disp_from     [選択した日付(開始日)]
	 * @param  [type] $disp_to       [選択した日付(終了日)]
	 * @return [type]                [None]
	 */
	function s_index( $select_option = "1" , $disp_from = "0000-00-00" , $disp_to = "9999-12-31")
	{
		$auth = $this->_checkStaffAuthority();
		if ($auth == 0) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}

		$year = $this->Common->getYear();	// 年度取得

		$staff_id = $this->Session->read('my_staff_id');

		// データを取得する
		if ($this->request->query('select_option') != ""){
			$select_option = $this->request->query('select_option');
		}
		if ($this->request->query('disp_from') != ""){
			$disp_from = $this->request->query('disp_from');
		}
		if( $this->request->query('disp_to') != ""){
			$disp_to = $this->request->query('disp_to');
		}

		// globalの変数を設定
		$option_list= $this->option_list;

		// 引数 $select_optionに基づいてconditionを変更する
		switch ($select_option) {
			case 1:
				// 有効
				$con = array('SpecialHoliday.staff_id' => $staff_id ,
							 'SpecialHoliday.use_date' => null,
							 'SpecialHoliday.expiration' => 0);
				break;
			case 2:
				// 無効
				$con = array('SpecialHoliday.staff_id' => $staff_id ,
							 'OR' => array('SpecialHoliday.use_date !=' => null,
										   'SpecialHoliday.expiration' => 1) );
				break;
			case 3:
				// 全表示
				$con = array('SpecialHoliday.staff_id' => $staff_id);
				break;
			default:
				// デフォルト (有効のみ表示)
				$con = array('SpecialHoliday.staff_id' => $staff_id ,
							 'SpecialHoliday.use_date' => null,
							 'SpecialHoliday.expiration' => 0);
		}

		// 日付指定を追加
		array_push($con, array("SpecialHoliday.start_date >=" => $disp_from),
						 array("SpecialHoliday.end_date <=" => $disp_to));

		// 日付が指定されていない場合は表示を消すために空にする
		if ($disp_from == "0000-00-00") {
		    $disp_from = "";
		}
		if ($disp_to == "9999-12-31") {
		    $disp_to = "";
		}

		// paginate設定
		$this->paginate['SpecialHoliday'] = array('limit'=>100, "conditions"=>$con, 'fields'=>array('*'));
		$datas = $this->paginate('SpecialHoliday');

		$id = 0;

		$this->set(compact('datas', 'id', 'staffs', 'option_list', 'select_option', 'select_staff', 'disp_from', 'disp_to'));

		$this->set('holiday_data', $datas);

		$this->set('title_for_layout', '特休閲覧');
    }


	/**
	 * [a_index 特休一覧（管理者）]
	 * @param  [type] $select_staff  [表示対象スタッフ]
	 * @param  [type] $select_option [表示オプション]
	 * @param  [type] $disp_from     [選択した日付(開始日)]
	 * @param  [type] $disp_to       [選択した日付(終了日)]
	 * @return [type]                [None]
	 */
	function a_index($select_staff = "" ,$select_option = "1", $disp_from = "0000-00-00" , $disp_to = "9999-12-31")
	{
		$auth = $this->_checkStaffAuthority();
		if ($auth == 0) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}

		$year = $this->Common->getYear();	// 年度取得

		$con = array();

		$staffs = $this->Staff->find('list');

		// データを取得する
		if ($this->request->query('select_option') != "") {
			$select_option = $this->request->query('select_option');
		}
		if ($this->request->query('disp_from') != "") {
			$disp_from = $this->request->query('disp_from');
		}
		if ($this->request->query('disp_to') != "") {
			$disp_to = $this->request->query('disp_to');
		}
		$select_staff = $this->request->query('select_staff');

		// globalの変数を設定
		$option_list= $this->option_list;

		// 引数 $select_optionに基づいてconditionを変更する
		switch ($select_option) {
			case 1:
				// 有効
				$con = array('SpecialHoliday.use_date' => null,
							 'SpecialHoliday.expiration' => 0);
				break;
			case 2:
				// 無効
				$con = array('OR' => array('SpecialHoliday.use_date !=' => null,
										   'SpecialHoliday.expiration' => 1) );
				break;
			case 3:
				// 全表示
				$con = array();
				break;
			default:
				// デフォルト (有効のみ表示)
				$con = array('SpecialHoliday.use_date' => null,
							 'SpecialHoliday.expiration' => 0);
				break;
		}

		// 日付指定を追加
		array_push($con, array("SpecialHoliday.start_date >=" => $disp_from),
						 array("SpecialHoliday.end_date <=" => $disp_to));

		// 対象スタッフを追加
		if ($select_staff != "") {
			array_push($con,array( "SpecialHoliday.staff_id" => $select_staff));
		}

		// 日付が指定されていない場合は表示を消すために空にする
		if ($disp_from == "0000-00-00") {
		    $disp_from = "";
		}
		if ($disp_to == "9999-12-31") {
		    $disp_to = "";
		}

		// paginate設定
		$this->paginate['SpecialHoliday'] = array('limit'=>100, "conditions"=>$con, 'fields'=>array('*'));
		$datas = $this->paginate('SpecialHoliday');

		$id = 0;

		$this->set(compact('datas', 'id', 'staffs', 'option_list', 'select_option', 'select_staff', 'disp_from', 'disp_to'));

		$this->set('holiday_data', $datas);

		$this->set('title_for_layout', '特休管理');
    }


	/**
	 * [a_edit 登録・編集]
	 * @param  [type] $id       [description]
	 * @return [type]           [description]
	 */
	function a_edit($id = null)
	{
		$auth = $this->_checkStaffAuthority();
		if ($auth == 0) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}

		$this->layout = "ajax";
		$this->SpecialHoliday->unbindModelAll();
		$data = $this->SpecialHoliday->find("first", array("conditions" => array("SpecialHoliday.id"=>$id)));
		if (!empty($data)) {
			$this->data = $data;
		}

		$staffs = $this->Staff->find('list');

		$this->set(compact('data', 'staffs'));

		$this->set('title_for_layout',  '特休登録・編集');
	}

	/**
	 * [a_update 更新]
	 * @return [type] [None]
	 */
	function a_update()
	{
		$auth = $this->_checkStaffAuthority();
		if (2 != $auth) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			$message = "更新しました";

			if (empty($this->data)) {
				$message = "セッションエラーです";
			}
			else {
				if (empty($this->request->data['SpecialHoliday']['id'])) {
					$this->request->data['SpecialHoliday']['id'] = 0;
				}
				if (empty($this->request->data['SpecialHoliday']['expiration'])) {
					$this->request->data['SpecialHoliday']['expiration'] = 0;
				}

				$this->SpecialHoliday->create();
				$this->data['SpecialHoliday']['id'];
				if (!$this->SpecialHoliday->save($this->request->data)) {
					//$this->log($this->request->data,LOG_DEBUG);
					$message = "データの更新に失敗しました";
				}
				//$this->log($this->request->data,LOG_DEBUG);
			}
			$this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
			$this->redirect(array('action' => 'index', $this->data['SpecialHoliday']['id']));
		}
	}

	/**
	 * [a_delete 削除]
	 * @param  [type] $id [ID]
	 * @return [type]     [None]
	 */
	function a_delete($id){
		$this->autoRender = false;

		$auth = $this->_checkStaffAuthority();
		if (2 != $auth) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}
		$data = $this->SpecialHoliday->find('first', array('conditions'=>array('SpecialHoliday.id'=>$id)));
		if (!empty($data)) {
			$data['SpecialHoliday']['enable'] = 0;
			$this->SpecialHoliday->save($data);
			$this->redirect($this->referer());
		}
	}

	/**
	 * [a_add 追加]
	 * @return [type]           [description]
	 */
	function a_add()
	{
		$auth = $this->_checkStaffAuthority();
		if ($auth == 0) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}

		$this->layout = "ajax";
		$this->SpecialHoliday->unbindModelAll();
		$data = $this->SpecialHoliday->find("first", array("conditions" => array("SpecialHoliday.id"=>0)));
		if (!empty($data)) {
			$this->data = $data;
		}

		$staffs[ALL_STAFF] = "＝全社員＝";
		$staffs[EXCEPT_OFFICER] = "＝役員以外＝";
		$staffs += $this->Staff->find('list', array("conditions" => array("Staff.no !="=>"9999", "Staff.retire_date"=>null)));

		$this->set(compact('data', 'staffs'));

		$this->set('title_for_layout',  '特休追加');
	}

	/**
	 * [a_commit 登録]
	 * @return [type] [None]
	 */
	function a_commit()
	{
		$auth = $this->_checkStaffAuthority();
		if (2 != $auth) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			$message = "更新しました";

			if (empty($this->data)) {
				$message = "セッションエラーです";
			}
			else {
				// 全社員指定
				if ($this->request->data['SpecialHoliday']['staff_id'] == ALL_STAFF || $this->request->data['SpecialHoliday']['staff_id'] == EXCEPT_OFFICER) {

					$add_num = 1;

					//$this->Staff->unbindModelAll();
					$staffs = $this->Staff->find('all', array("conditions" => array("Staff.no !="=>"9999", "Staff.retire_date"=>null)));
					foreach ($staffs as $staff) {
						if ($this->request->data['SpecialHoliday']['staff_id'] == EXCEPT_OFFICER &&
							$staff['Authority']['name'] == "役員") {
							continue;
						}
						// 指定日数分のレコードを追加
						for ($lp_num = 0; $lp_num < $this->request->data['SpecialHoliday']['add_num']; $lp_num++) {
							/*
							$add_rec['SpecialHoliday'][$add_num]['id']         = 0;
							$add_rec['SpecialHoliday'][$add_num]['staff_id']   = $staff['Staff']['id'];
							$add_rec['SpecialHoliday'][$add_num]['enable']     = $this->request->data['SpecialHoliday']['enable'];
							$add_rec['SpecialHoliday'][$add_num]['expiration'] = $this->request->data['SpecialHoliday']['expiration'];
							$add_rec['SpecialHoliday'][$add_num]['detail']     = $this->request->data['SpecialHoliday']['detail'];
							$add_rec['SpecialHoliday'][$add_num]['start_date'] = $this->request->data['SpecialHoliday']['start_date'];
							$add_rec['SpecialHoliday'][$add_num]['end_date']   = $this->request->data['SpecialHoliday']['end_date'];
							$add_num ++;
							*/
							$this->SpecialHoliday->create();
							$add_rec['SpecialHoliday'] = array('id'         => 0,
															   'staff_id'   => $staff['Staff']['id'],
															   'enable'     => $this->request->data['SpecialHoliday']['enable'],
															   'expiration' => $this->request->data['SpecialHoliday']['expiration'],
															   'detail'     => $this->request->data['SpecialHoliday']['detail'],
															   'start_date' => $this->request->data['SpecialHoliday']['start_date'],
															   'end_date'   => $this->request->data['SpecialHoliday']['end_date'],
															   );
							if (!$this->SpecialHoliday->save($add_rec)) {
								//$this->log($this->request->data,LOG_DEBUG);
								$message = "データの更新に失敗しました";
							}
						}
					}
				}
				// 特定社員指定
				else {

					$add_num = 1;

					// 指定日数分のレコードを追加
					for ($lp_num = 0; $lp_num < $this->request->data['SpecialHoliday']['add_num']; $lp_num++) {
						/*
						$add_rec['SpecialHoliday'][$add_num]['id']         = 0;
						$add_rec['SpecialHoliday'][$add_num]['staff_id']   = $this->request->data['SpecialHoliday']['staff_id'];
						$add_rec['SpecialHoliday'][$add_num]['enable']     = $this->request->data['SpecialHoliday']['enable'];
						$add_rec['SpecialHoliday'][$add_num]['expiration'] = $this->request->data['SpecialHoliday']['expiration'];
						$add_rec['SpecialHoliday'][$add_num]['detail']     = $this->request->data['SpecialHoliday']['detail'];
						$add_rec['SpecialHoliday'][$add_num]['start_date'] = $this->request->data['SpecialHoliday']['start_date'];
						$add_rec['SpecialHoliday'][$add_num]['end_date']   = $this->request->data['SpecialHoliday']['end_date'];
						$add_num ++;
						*/
						$this->SpecialHoliday->create();
						$add_rec['SpecialHoliday'] = array('id'         => 0,
														   'staff_id'   => $this->request->data['SpecialHoliday']['staff_id'],
														   'enable'     => $this->request->data['SpecialHoliday']['enable'],
														   'expiration' => $this->request->data['SpecialHoliday']['expiration'],
														   'detail'     => $this->request->data['SpecialHoliday']['detail'],
														   'start_date' => $this->request->data['SpecialHoliday']['start_date'],
														   'end_date'   => $this->request->data['SpecialHoliday']['end_date'],
														   );
						if (!$this->SpecialHoliday->save($add_rec)) {
							//$this->log($this->request->data,LOG_DEBUG);
							$message = "データの更新に失敗しました";
						}
					}
				}

			}
			$this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
			$this->redirect(array('action' => 'index'));
		}
	}
}

?>