<?php
App::uses('AppController', 'Controller');

/**
 *     掲示板
 */
class BoardsController extends AppController {
    var $uses = array('BoardTree', 'BoardBody', 'BoardTempfile', 'BoardRead', 'Staff');

    public $paginate = array(
        'page' => 1,
        'limit' => 20, //←ココのlimitでは表示件数を指定
        'conditions' => array(''),
        );


    /**
     * [s_index 一覧]
     * @param  [type] $lv1    [1階層]
     * @param  [type] $lv2    [2階層]
     * @param  [type] $lv3    [3階層]
     * @param  [type] $lv4    [4階層]
     */
    public function s_index($lv1 = null, $lv2 = null, $lv3 = null, $lv4 = null) {
        // 初期化
        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }
        
        
        //デフォルト遷移用
        $strDefaultPrm = "";
        if(!empty($lv1)){
            $strDefaultPrm = '/' . $lv1;
        }
        if(!empty($lv2)){
            $strDefaultPrm .= '/' . $lv2;
        }
        if(!empty($lv3)){
            $strDefaultPrm .= '/' . $lv3;
        }
        if(!empty($lv4)){
            $strDefaultPrm .= '/' . $lv4;
        }

        ///////////////////////////
        // Treeの構造作成
        ///////////////////////////
        $boardtreeLv1 = $this->BoardTree->find('all', 
                                                array('conditions'=>array('BoardTree.enable'=>1,
                                                                          'BoardTree.level2'=>0,
                                                                          'BoardTree.level3'=>0,
                                                                          'BoardTree.level4'=>0
                                                                          ), 
                                                ));
        //メニューのリストを作成する
        $tree = array();
        for($a = 0; $a < count($boardtreeLv1); $a++)
        {            
            $tree[$a]['id'] = $boardtreeLv1[$a]['BoardTree']['level1'];
            $tree[$a]['name'] = $boardtreeLv1[$a]['BoardTree']['title'];
            $tree[$a]['child'] = array();

            $boardtreeLv2 = $this->BoardTree->find('all', 
                                                    array('conditions'=>array('BoardTree.enable'=>1,
                                                                              'BoardTree.level1'=>$tree[$a]['id'],
                                                                              'BoardTree.level2 <>'=>0,
                                                                              'BoardTree.level3'=>0,
                                                                              'BoardTree.level4'=>0
                                                                              ), 
                                                     ));

            $treeLv2 = $tree[$a]['child'];
            for($b = 0; $b < count($boardtreeLv2); $b++)
            {
                $tree[$a]['child'][$b]['id'] = $boardtreeLv2[$b]['BoardTree']['level2'];
                $tree[$a]['child'][$b]['name'] = $boardtreeLv2[$b]['BoardTree']['title'];
                $tree[$a]['child'][$b]['child'] = array();

                $boardtreeLv3 = $this->BoardTree->find('all', 
                                                        array('conditions'=>array('BoardTree.enable'=>1,
                                                                                  'BoardTree.level1'=>$tree[$a]['id'],
                                                                                  'BoardTree.level2'=>$tree[$a]['child'][$b]['id'],
                                                                                  'BoardTree.level3 <>'=>0,
                                                                                  'BoardTree.level4'=>0
                                                                                  ), 
                                                         ));
                $treeLv3 = $tree[$a]['child'][$b]['child'];
                for($c = 0; $c < count($boardtreeLv3); $c++)
                {
                    $tree[$a]['child'][$b]['child'][$c]['id'] = $boardtreeLv3[$c]['BoardTree']['level3'];
                    $tree[$a]['child'][$b]['child'][$c]['name'] = $boardtreeLv3[$c]['BoardTree']['title'];
                    $tree[$a]['child'][$b]['child'][$c]['child'] = array();


                    $boardtreeLv4 = $this->BoardTree->find('all', 
                                                            array('conditions'=>array('BoardTree.enable'=>1,
                                                                                      'BoardTree.level1'=>$tree[$a]['id'],
                                                                                      'BoardTree.level2'=>$tree[$a]['child'][$b]['id'],
                                                                                      'BoardTree.level3'=>$tree[$a]['child'][$b]['child'][$c]['id'],
                                                                                      'BoardTree.level4 <>'=>0
                                                                                      ), 
                                                             ));
                    $treeLv4 = $tree[$a]['child'][$b]['child'][$c]['child'];
                    for($d = 0; $d < count($boardtreeLv4); $d++)
                    {
                        $tree[$a]['child'][$b]['child'][$c]['child'][$d]['id'] = $boardtreeLv4[$d]['BoardTree']['level4'];
                        $tree[$a]['child'][$b]['child'][$c]['child'][$d]['name'] = $boardtreeLv4[$d]['BoardTree']['title'];
                        $tree[$a]['child'][$b]['child'][$c]['child'][$d]['child'] = array();
                    }
                }
            }
        }
        $this->set("tree", $tree);
        $this->set("strDefaultPrm", $strDefaultPrm);
        
//$this->log(119);
//$this->log($tree);
        $this->set('title_for_layout', '掲示板');
    }


    /**
     * [s_index 一覧]
     * @param  [type] $lv1    [1階層]
     * @param  [type] $lv2    [2階層]
     * @param  [type] $lv3    [3階層]
     * @param  [type] $lv4    [4階層]
     */
    public function s_getcontent($lv1 = null, $lv2 = null, $lv3 = null, $lv4 = null)
    {
        // 初期化
        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        $my_staff_id = $this->Session->read('my_staff_id');

        if ($this->request->is('post') || $this->request->is('put')) 
        {
            //書き込み可能のコンテンツ(掲示板登録)が来るはずなので、ここで登録処理
            
            $result = $this->BoardBody->find('first', 
                                             array('conditions'=>array('BoardBody.tree_id'=> $this->request->data['BoardBody']['tree_id']),
                                                   'fields' => "MAX(BoardBody.hist_index) as max_number")
                                             );
            $this->request->data['BoardBody']['hist_index'] = $result[0]["max_number"] + 1;
            $this->request->data['BoardBody']['reg_user_id'] = $my_staff_id;

            $models = array($this->BoardBody, $this->BoardTempfile);
            $this->begin($models);
            if ($this->BoardBody->save($this->request->data)) {
                //ファイルパスを作成する
                $path = sprintf("files/Boards/%08d",$this->BoardBody->getId());
                //フォルダを作成する
                mkdir($path, 0777, true);
                $filecnt = count($this->request->data["BoardBodyFile"]["newfilenames"]);
                $fc_cnt = 0;
                $isError = false;
                for($fc = 0; $fc < $filecnt; $fc++)
                {
                    if(empty($this->request->data["BoardBodyFile"]["newfilenames"][$fc]['name'])){
                        continue;
                    }

                    //ファイルパスを作成する
                    $path = sprintf("files/Boards/%08d/%02d",$this->BoardBody->getId(), $fc+1);
                    //フォルダを作成する
                    mkdir($path, 0777, true);

                    $utfName = mb_convert_encoding($this->request->data["BoardBodyFile"]["newfilenames"][$fc]['name'], "SJIS", "UTF-8");

                    //ファイルパスを作成する
                    $path2 = sprintf("%s/%s",$path, $utfName);

                    //ファイルをコピーする
                    copy($this->request->data["BoardBodyFile"]["newfilenames"][$fc]['tmp_name'], $path2);

                    //テーブルにレコードを新規追加する
                    $this->BoardTempfile->create();
                    
                    $fc_cnt++;
                    //テーブルにレコードを登録する
                    $btf = array();
                    $btf['BoardTempfile'] = array();
                    $btf['BoardTempfile']['body_id'] = $this->BoardBody->getId();
                    $btf['BoardTempfile']['seq_no'] = $fc_cnt;
                    $btf['BoardTempfile']['filename'] = $this->request->data["BoardBodyFile"]["newfilenames"][$fc]['name'];
                    $btf['BoardTempfile']['path'] = $path;
                    $btf['BoardTempfile']['enable'] = 1;
                    
                    if (!$this->BoardTempfile->save($btf)) {
                        $message = "データの更新(ファイル追加)に失敗しました";
                        $isError = true;
                        break;
                    }
                }
                
                if($isError)
                {
                    $this->rollback($models);
                }else
                {
                    $this->commit($models);
                }
            }
            else
            {
                $message = "データの更新に失敗しました";
                $this->rollback($models);
            }

            // 親のTreeレコードに対して、更新者の情報を設定
            $tree = $this->BoardTree->find('first', array('conditions'=>array('BoardTree.id'=> $this->request->data['BoardBody']['tree_id'])));
            $tree['BoardTree']['modified_staff_id'] = $my_staff_id;
            array_splice($tree['BoardTree'], 10, 1);	// "modified"削除
            if (!$this->BoardTree->save($tree['BoardTree'])) {
                $message = "データの更新に失敗しました";
                $this->rollback($models);
            }

            //----- 未読管理テーブル作成 -----//
            $result = $this->_crtBoardReads ($this->request->data['BoardBody']['tree_id'], $my_staff_id);
            if ($result != 0) {
                $message = "データの更新(未読管理レコードの追加)に失敗しました";
                $this->rollback($models);
            }
            
            $treeid = $this->BoardTree->find('first', 
                                            array('conditions'=>array('BoardTree.id'=>$this->request->data['BoardBody']['tree_id'])
                                            ));
            //ページ遷移
            $this->redirect(array('action' => 'index', $treeid['BoardTree']['level1'], $treeid['BoardTree']['level2'], $treeid['BoardTree']['level3'], $treeid['BoardTree']['level4']));
        }

        /////////////////////////////////////////
        //  選択したノードを表示するためのデータ取得
        /////////////////////////////////////////
        $strLv1Title = "";
        $strLv2Title = "";
        $strLv3Title = "";
        $this->_getNodeTitle($this->BoardTree, $lv1, $lv2, $lv3, $strLv1Title, $strLv2Title, $strLv3Title);

        $this->set('strLv1Title', $strLv1Title);
        $this->set('strLv2Title', $strLv2Title);
        $this->set('strLv3Title', $strLv3Title);

        $this->set('lv1', $lv1);
        $this->set('lv2', $lv2);
        $this->set('lv3', $lv3);

        /////////////////////////////////////////
        //  この引数に対応するコンテンツを取得
        /////////////////////////////////////////
        $joins = array(
                    array( "type" => "left",
                        "table" => "board_trees",
                        "alias" => "BoardTree",
                        "conditions" => "BoardTree.id=BoardBody.tree_id",
                        )
                    );

        $treeid = $this->BoardTree->find('first', 
                                            array('fields'=>array('BoardTree.id'),
                                                  'conditions'=>array('BoardTree.enable'=>1,
                                                                      'BoardTree.level1'=>$lv1,
                                                                      'BoardTree.level2'=>$lv2,
                                                                      'BoardTree.level3'=>$lv3,
                                                                      'BoardTree.level4'=>$lv4
                                                                      ), 
                                            ));
        $boardBody = array();
        if(!empty($treeid))
        {
            $boardBody = $this->BoardBody->find('first', 
                                                array('conditions'=>array('BoardBody.enable'=>1,
                                                                          'BoardBody.tree_id'=> $treeid['BoardTree']['id'],
                                                                          'BoardBody.hist_index'=> 0
                                                                          ), 
                                                      'joins' => $joins,
                                                      'fields' => array("*"),
                                                ));

            // 掲示板IDに対応する自身の未読レコードを取得
            $con = array ('BoardRead.staff_id'=>$my_staff_id, 'BoardRead.board_id'=>$treeid['BoardTree']['id']);
            $read = $this->BoardRead->find ('first', array ('conditions'=>$con));
            if (!empty($read)) {
                // 既存レコードが存在する場合、statusを既読に変更
                if ($read['BoardRead']['status'] != READ_STATUS_READ) {
                    $read['BoardRead']['status'] = READ_STATUS_READ;
                    array_splice($read['BoardRead'], 6);    // "modified"削除
                    if (!$this->BoardRead->save ($read)) {
                        $message = "データの更新(ファイル追加)に失敗しました";
                    }
                }
            }
        }
        //ページ切り替え
        if(empty($boardBody))
        {//対応する項目がないのでリストページ
            

            $cond = array();
            if((!empty($lv2)) && (!empty($lv3)))
            {//
                $cond = array('BoardBody.enable'=>1,
                              'BoardBody.hist_index'=> 0,
                              'BoardTree.level1'=>$lv1,
                              'BoardTree.level2'=>$lv2,
                              'BoardTree.level3'=>$lv3
                              );
            }
            else if((!empty($lv2)) && (empty($lv3)))
            {
                $cond = array('BoardBody.enable'=>1,
                              'BoardBody.hist_index'=> 0,
                                  'BoardTree.level1'=>$lv1,
                                'BoardTree.level2'=>$lv2
                                );
              }else if((!empty($lv1)) && (empty($lv2)))
              {
                $cond = array('BoardBody.enable'=>1,
                              'BoardBody.hist_index'=> 0,
                                  'BoardTree.level1'=>$lv1
                                );
              }else 
              {
                $cond = array('BoardBody.enable'=>1,
                              'BoardBody.hist_index'=> 0,
                                );
            }

            $boardList = $this->BoardBody->find('all', 
                                                array('conditions'=>$cond,
                                                      'joins' => $joins,
                                                      'fields' => array("*"),
                                                'order'=>array('BoardTree.modified desc'),
                                               ));

            // 掲示板階層名を取得
            for ($i = 0; $i < count($boardList); $i++) {
                if ($boardList[$i]['BoardTree']['level1'] != 0) {
                    $name = $this->BoardTree->find('first',
                                                   array('conditions'=>array('BoardTree.level1'=>$boardList[$i]['BoardTree']['level1'],
                                                                             'BoardTree.level2'=>0),
                                                         'fields'=>'title'));
                    $boardList[$i]['BoardTree']['lv1_title'] = $name['BoardTree']['title'];
                }
                if ($boardList[$i]['BoardTree']['level2'] != 0) {
                    $name = $this->BoardTree->find('first',
                                                   array('conditions'=>array('BoardTree.level1'=>$boardList[$i]['BoardTree']['level1'],
                                                                             'BoardTree.level2'=>$boardList[$i]['BoardTree']['level2'],
                                                                             'BoardTree.level3'=>0),
                                                         'fields'=>'title'));
                    $boardList[$i]['BoardTree']['lv2_title'] = $name['BoardTree']['title'];
                }
                if ($boardList[$i]['BoardTree']['level3'] != 0) {
                    $name = $this->BoardTree->find('first',
                                                   array('conditions'=>array('BoardTree.level1'=>$boardList[$i]['BoardTree']['level1'],
                                                                             'BoardTree.level2'=>$boardList[$i]['BoardTree']['level2'],
                                                                             'BoardTree.level3'=>$boardList[$i]['BoardTree']['level3'],
                                                                             'BoardTree.level4'=>0),
                                                         'fields'=>'title'));
                    $boardList[$i]['BoardTree']['lv3_title'] = $name['BoardTree']['title'];
                }
            }

            // 最終更新者のスタッフ名を取得
            for ($i = 0; $i < count($boardList); $i++) {
                $staffid = 0;
                if ($boardList[$i]['BoardTree']['modified_staff_id'] != 0) {
                    $staff = $this->Staff->find('first', array('conditions'=>array('Staff.id'=>$boardList[$i]['BoardTree']['modified_staff_id'])));
                    $boardList[$i]['BoardTree']['modified_staff_name'] = $staff['Staff']['name'];
                }
                else {
                    $boardList[$i]['BoardTree']['modified_staff_name'] =  $boardList[$i]['Staff']['name'];
                }
            }

            //データ設定
            $this->set('boardList', $boardList);
//$this->log("■■");
//$this->log($boardList);
            
            $staff_id = $this->Session->read('my_staff_id');
            $this->set(compact('staff_id'));
            
            //レイアウト設定
            $this->layout = 'ajax';
            $this->render('s_list');
        }
        else
        {//掲示板のメインページ
            //データ設定
            $this->set('boardBody',  $boardBody);

            $boardHist = $this->BoardBody->find('all', 
                                                array('conditions'=>array('BoardBody.enable'=>1,
                                                                          'BoardBody.tree_id'=> $treeid['BoardTree']['id'],
                                                                          'BoardBody.hist_index <>'=> 0
                                                                          ), 
                                                      'order'=>array('BoardBody.hist_index desc'),
                                                ));
            $this->set('boardHist',  $boardHist);
            
            $staff_id = $this->Session->read('my_staff_id');
            $this->set(compact('staff_id'));
            
            //レイアウト設定
            $this->layout = 'ajax';
            $this->render('s_main');
        }
    }

    /**
     * [s_addtopic 一覧]
     * @param  [type] $lv1    [1階層]
     * @param  [type] $lv2    [2階層]
     * @param  [type] $lv3    [3階層]
     * @param  [type] $lv4    [4階層]
     */
    public function s_addtopic($lv1 = null, $lv2 = null, $lv3 = null) {
        // 初期化
        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

//$this->log($this->request->data);
        if ($this->request->is('post') || $this->request->is('put')) 
        {
            //書き込み可能のコンテンツ(掲示板登録)が来るはずなので、ここで登録処理
            $message = "更新しました";

            if (empty($this->data)) {
                $message = "セッションエラーです";
            } else {
                ////////////////////////
                // Treeを登録
                ////////////////////////
                //LV4の一番大きい数字
                $result = $this->BoardTree->find('first', 
                                                 array('conditions'=>array('BoardTree.enable'=>1,
                                                                             'BoardTree.level1'=>$this->request->data['BoardTree']['level1'],
                                                                            'BoardTree.level2'=>$this->request->data['BoardTree']['level2'],
                                                                            'BoardTree.level3'=>$this->request->data['BoardTree']['level3']),
                                                         'fields' => "MAX(BoardTree.level4) as max_number")
                                                     );
                $this->request->data['BoardTree']['level4'] = $result[0]["max_number"] + 1;
                $this->request->data['BoardTree']['enable'] = 1;
                $this->request->data['BoardTree']['created_staff_id'] = $this->Session->read('my_staff_id');
                $this->request->data['BoardTree']['modified_staff_id'] = $this->Session->read('my_staff_id');

                $models = array($this->BoardTree, $this->BoardBody, $this->BoardTempfile);
                $this->begin($models);

                $this->BoardTree->create();
                if (!$this->BoardTree->save($this->request->data)) {
                    $message = "データの更新に失敗しました";
                    $this->rollback($models);
                }else{

                    ////////////////////////
                    // Bodyを登録
                    ////////////////////////
                    //
                    $this->request->data['BoardBody']['tree_id'] = $this->BoardTree->getId();
                    $this->request->data['BoardBody']['reg_user_id'] = $this->Session->read('my_staff_id');
                    $this->BoardBody->create();
                    if (!$this->BoardBody->save($this->request->data)) {
                        $message = "データの更新に失敗しました";
                        $this->rollback($models);
                    }
                    else
                    {
                        //ファイルパスを作成する
                        $path = sprintf("files/Boards/%08d",$this->BoardBody->getId());
                        //フォルダを作成する
                        mkdir($path, 0777, true);
                        $filecnt = count($this->request->data["BoardBodyFile"]["newfilenames"]);
                        
                        $fc_cnt = 0;
                        $isError = false;
                        for($fc = 0; $fc < $filecnt; $fc++)
                        {

                            if(empty($this->request->data["BoardBodyFile"]["newfilenames"][$fc]['name'])){
                                continue;
                            }
                                            
                            //ファイルパスを作成する
                            $path = sprintf("files/Boards/%08d/%02d",$this->BoardBody->getId(), $fc+1);
                            //フォルダを作成する
                            mkdir($path, 0777, true);
                            
                            $utfName = mb_convert_encoding($this->request->data["BoardBodyFile"]["newfilenames"][$fc]['name'], "SJIS", "UTF-8");
                            
                            //ファイルパスを作成する
                            $path2 = sprintf("%s/%s",$path, $utfName);
                            
                            //ファイルをコピーする
                            copy($this->request->data["BoardBodyFile"]["newfilenames"][$fc]['tmp_name'], $path2);

                            //テーブルにレコードを新規追加する
                            $this->BoardTempfile->create();
                            
//$this->log($this->request->data);
                            //テーブルにレコードを登録する
                            $btf = array();
                            $btf['BoardTempfile'] = array();
                            $btf['BoardTempfile']['body_id'] = $this->BoardBody->getId();
                            $btf['BoardTempfile']['seq_no'] = $fc + 1;
                            $btf['BoardTempfile']['filename'] = $this->request->data["BoardBodyFile"]["newfilenames"][$fc]['name'];
                            $btf['BoardTempfile']['path'] = $path;
                            $btf['BoardTempfile']['enable'] = 1;
                            
                            if (!$this->BoardTempfile->save($btf)) {
                                $message = "データの更新(ファイル追加)に失敗しました";
                                $isError = true;
                                break;
                            }
                        }
                        
                        //----- 未読管理テーブル作成 -----//
                        $result = $this->_crtBoardReads ($this->request->data['BoardBody']['tree_id'], $this->Session->read('my_staff_id'));
                        if ($result != 0) {
                            $message = "データの更新(未読管理レコードの追加)に失敗しました";
                            $isError = true;
                        }
                        
                        if($isError)
                        {
                            $this->rollback($models);
                        }else
                        {
                            $this->commit($models);
                        }
                    }
                }
            }

            $this->redirect(array('action' => 'index', $this->request->data['BoardTree']['level1'], $this->request->data['BoardTree']['level2'], $this->request->data['BoardTree']['level3'],$this->request->data['BoardTree']['level4']));
        }
        else
        {
            /////////////////////////////////////////
            //  選択したノードを表示するためのデータ取得
            /////////////////////////////////////////
            $strLv1Title = "";
            $strLv2Title = "";
            $strLv3Title = "";
            $this->_getNodeTitle($this->BoardTree, $lv1, $lv2, $lv3, $strLv1Title, $strLv2Title, $strLv3Title);

            $this->set('strLv1Title', $strLv1Title);
            $this->set('strLv2Title', $strLv2Title);
            $this->set('strLv3Title', $strLv3Title);

            $this->set('lv1', $lv1);
            $this->set('lv2', $lv2);
            $this->set('lv3', $lv3);
            
            //レイアウト設定
            //$this->layout = 'ajax';
            $this->render('s_addtopic');
        }

    }


    /**
     * [s_edit 実施課題更新]
     * @param  [type] $id          [ID]
     * @param  [type] $progress_id [実施課題ID]
     * @return [type]              [None]
     */
    function s_addnode($lv1 = null, $lv2 = null){

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            echo '権限がありません';
            exit(); 
        }

        $this->layout = "ajax";
        $this->set('lv1', $lv1);
        $this->set('lv2', $lv2);
        $this->set('title_for_layout', '子ノード名入力');

    }

    /**
     * [s_update 更新]
     * @return [type] [None]
     */
    function s_updatenode(){

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

//$this->log($this->data);
        if ($this->request->is('post') || $this->request->is('put')) {
            $message = "更新しました";

            if (empty($this->data)) {
                $message = "セッションエラーです";
            } else {
                if(!empty($this->request->data['BoardTree']['level2']))
                {
                    //LV3の一番大きい数字
                    $result = $this->BoardTree->find('first', 
                                                     array('conditions'=>array('BoardTree.enable'=>1,
                                                                                'BoardTree.level1'=>$this->request->data['BoardTree']['level1'],
                                                                                'BoardTree.level2'=>$this->request->data['BoardTree']['level2']),
                                                            'fields' => "MAX(BoardTree.level3) as max_number")
                                                     );
                    $this->request->data['BoardTree']['level3'] = $result[0]["max_number"] + 1;
                }else if(!empty($this->request->data['BoardTree']['level1']))
                {
                    //LV2の一番大きい数字
                    $result = $this->BoardTree->find('first', 
                                                     array('conditions'=>array('BoardTree.enable'=>1,
                                                                                'BoardTree.level1'=>$this->request->data['BoardTree']['level1']),
                                                            'fields' => "MAX(BoardTree.level2) as max_number")
                                                     );
                    $this->request->data['BoardTree']['level2'] = $result[0]["max_number"] + 1;
                    $this->request->data['BoardTree']['level3'] = 0;
                }else
                {
                    //LV1の一番大きい数字
                    $result = $this->BoardTree->find('first', 
                                                     array('conditions'=>array('BoardTree.enable'=>1),
                                                            'fields' => "MAX(BoardTree.level1) as max_number")
                                                     );
                    $this->request->data['BoardTree']['level1'] = $result[0]["max_number"] + 1;
                    $this->request->data['BoardTree']['level2'] = 0;
                    $this->request->data['BoardTree']['level3'] = 0;

                }

//$this->log($this->request->data);
                $this->request->data['BoardTree']['enable'] = 1;
// [PEND] カラムを変更し、'enable'に統一したい
//                $this->request->data['BoardTree']['enable'] = 1;
//                $this->request->data['BoardTree']['crt_date'] = date('Y/m/d H:i:s');

                $models = array($this->BoardTree);
                    $this->BoardTree->create();
                if (!$this->BoardTree->save($this->request->data)) {
                    $message = "データの更新に失敗しました";
                    $this->rollback($models);
                }else{
                    $this->commit($models);
                }
                
            }
//            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
            
            $this->redirect(array('action' => 'index', $this->request->data['BoardTree']['level1'], $this->request->data['BoardTree']['level2'], $this->request->data['BoardTree']['level3']));
        }

    }

    /**
     * [s_delete 削除]
     * @param  [type] $body_id [ID]
     * @param  [type] $tree_id [ID]
     * @return [type]     [None]
     */
    function s_delete($body_id, $tree_id){
        $this->autoRender = false;

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        // BoordBodyを削除
        $data = $this->BoardBody->find('first', array('conditions'=>array('BoardBody.id'=>$body_id)));
        if(!empty($data)){
            $data['BoardBody']['enable'] = 0;
            $this->BoardBody->save($data);

            if ($tree_id != 0) {
                // BoardTreeを削除
                $data = $this->BoardTree->find('first', array('conditions'=>array('BoardTree.id'=>$tree_id)));
                if(!empty($data)){
                    $data['BoardTree']['enable'] = 0;
                    $this->BoardTree->save($data);
                }
            }
            $this->redirect($this->referer());            
        }
    }

    //----------------------ここから共通処理-----------------
    function _getNodeTitle($BoardTree, $lv1 = null, $lv2 = null, $lv3 = null, &$strLv1Title, &$strLv2Title, &$strLv3Title)
    {
        /////////////////////////////////////////
        //  選択したノードを表示するためのデータ取得
        /////////////////////////////////////////
        $strLv1Title = "";
        $strLv2Title = "";
        $strLv3Title = "";
        if(!empty($lv1))
        {
            $tmp = $BoardTree->find('first', 
                                            array('fields'=>array('BoardTree.title'),
                                                  'conditions'=>array('BoardTree.enable'=>1,
                                                                      'BoardTree.level1'=>$lv1
                                                                      ), 
                                            ));    
            if(!empty($tmp))
            {
                $strLv1Title = $tmp['BoardTree']['title'];
            }
        }

        if(!empty($lv2))
        {
            $tmp = $BoardTree->find('first', 
                                            array('fields'=>array('BoardTree.title'),
                                                  'conditions'=>array('BoardTree.enable'=>1,
                                                                      'BoardTree.level1'=>$lv1,
                                                                      'BoardTree.level2'=>$lv2
                                                                      ), 
                                            ));            
            if(!empty($tmp))
            {
                $strLv2Title = $tmp['BoardTree']['title'];
            }
        }
        if(!empty($lv3))
        {
            $tmp = $BoardTree->find('first', 
                                            array('fields'=>array('BoardTree.title'),
                                                  'conditions'=>array('BoardTree.enable'=>1,
                                                                      'BoardTree.level1'=>$lv1,
                                                                      'BoardTree.level2'=>$lv2,
                                                                      'BoardTree.level3'=>$lv3
                                                                      ), 
                                            ));            
            if(!empty($tmp))
            {
                $strLv3Title = $tmp['BoardTree']['title'];
            }
        }
    }

    /**
     * [_crtBoardReads 未読管理テーブル作成処理]
     * @param  [type] $tree_id  [掲示板Tree ID]
     * @param  [type] $staff_id [投稿者社員ID]
     * @return [type]     [None]
     */
    function _crtBoardReads ($tree_id, $staff_id)
    {
        // 親のTreeレコード用の全社員の未読レコードを作成する

        $result = 0;

        // 自分以外の全社員を取得
        $con_staffs = array ("Staff.id != " => $staff_id, "Staff.no != " => 9999, "Staff.retire_date" => null);
        $staffs = $this->Staff->find ('all', array ('conditions'=>$con_staffs));

        foreach ($staffs as $staff) {
            // 既存レコードの有無チェック
            $con_reads = array ("BoardRead.staff_id" => $staff['Staff']['id'], "BoardRead.board_id" => $tree_id);
            $read = $this->BoardRead->find ('first', array ('conditions' => $con_reads));
            if (!empty($read)) {
                // 既存レコードが存在する場合、statusを未読に変更
                if ($read['BoardRead']['status'] != READ_STATUS_UNREAD) {
                    $read['BoardRead']['status'] = READ_STATUS_UNREAD;
                    array_splice($read['BoardRead'], 6);    // "modified"削除
                    if (!$this->BoardRead->save ($read)) {
                        $result = 1;
                        break;
                    }
                }
            }
            else {
                // 既存レコードが存在しない場合、新規レコードを作成
                $read = array();
                $read['staff_id'] = $staff['Staff']['id'];
                $read['board_id'] = $tree_id;
                $read['status'] = READ_STATUS_UNREAD;
                $this->BoardRead->create ();
                if (!$this->BoardRead->save ($read)) {
                    $result = 1;
                    break;
                }
            }
        }
        
        return $result;
    }
}

?>
