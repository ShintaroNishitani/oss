<?php
App::uses('AppController', 'Controller');
App::uses('ConfirmsController', 'Controller');
App::uses('KintaisController', 'Controller');

/**
 * 申請
 */
class ApplyHolidaysController extends AppController {
    var $uses = array('ApplyHoliday', 'Confirm', 'Staff');

    /**
     * [s_index 一覧]
     * @param  [type] $year     [年]
     * @param  [type] $month    [月]
     * @param  [type] $staff_id [スタッフID]
     * @return [type]           [None]
     */
    function s_index(){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        // 常に現在月の親テーブルを取得する
        $year = date('Y');
        $month = date('n');

        $staff_id = $this->Session->read('my_staff_id');

        // 検索条件作成
        $applyHoliday = $this->ApplyHoliday->find('first', array(
            'conditions'=> array(
                "ApplyHoliday.year"=>$year,
                "ApplyHoliday.month"=>$month,
                "ApplyHoliday.staff_id"=>$staff_id,
                ),
            // 'recursive' => 3
        ));
        if (empty($applyHoliday)) {
            // 休暇申請保存
            $this->ApplyHoliday->create();
            $applyHoliday['ApplyHoliday'] = array('staff_id'=>$staff_id,
                                       'year'=>$year,
                                       'month'=>$month,
                                       'enable'=>1,
                                      );
            $this->ApplyHoliday->save($applyHoliday);
            $id = $this->ApplyHoliday->id;            
        } else {
            $id = $applyHoliday['ApplyHoliday']['id'];
        }

        // Detailを全て取得する
        $con = array(
            'conditions'=> array(
                "ApplyHoliday.staff_id"=>$staff_id,
            ),
            'order' => array('ApplyHolidayDetail.apply_date'=>'desc'),
            'recursive' => 2
        );
        $datas = $this->ApplyHoliday->ApplyHolidayDetail->find('all', $con);


        $this->set(compact('year', 'month', 'datas', 'id'));

        $this->set('title_for_layout', '休暇申請');
    }

    /**
     * [s_detail_edit 詳細]
     * @param  [type] $year      [年]
     * @param  [type] $month     [月]
     * @param  [type] $id        [ID]
     * @return [type]            [None]
     */
    function s_detail_edit($year, $month, $id, $detail_id = null){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        // $this->layout = "ajax";
        $data = $this->ApplyHoliday->ApplyHolidayDetail->find("first", array("conditions" => array("ApplyHolidayDetail.id"=>$detail_id)));

        if (!empty($data)) {
            // ステータスが差戻の場合、承認者と前回承認者をクリアする
            if ($data['Confirm']['status'] == CONFIRM_NO) {
                $data['Confirm']['check_staff_id'] = null;
            }
            $this->data = $data;
        }

        $staff_id = $this->Session->read('my_staff_id');

        $check_staffs = $this->get_check_staffs($staff_id);
 
        $this->set(compact('year', 'month', 'staff_id', 'id', 'detail_id', 'check_staffs'));

        $this->set('title_for_layout', '休暇申請登録・編集');
    }

    /**
     * [s_detail_update 更新]
     * @return [type] [None]
     */
    function s_detail_update(){

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $message = "更新しました";

            if (empty($this->data)) {
                $message = "セッションエラーです";
            } else {
                $year = $this->data['ApplyHoliday']['year'];
                $month = $this->data['ApplyHoliday']['month'];
                
                $applyHolidayDetail = array('ApplyHolidayDetail' => $this->data['ApplyHolidayDetail'],
                                    'ApplyHolidayDetailDate' => $this->data['ApplyHolidayDetailDate'],
                                    );


                // 申請日時の設定
                $applyHolidayDetail['ApplyHolidayDetail']['apply_date'] = date('Y-m-d H:i:s');
                // 新規の場合
                if (isset($this->data['ApplyHolidayDetail']['id']) && $this->data['ApplyHolidayDetail']['id'] > 0) {
                    // 更新の時のみ申請オブジェクトを設定する
                    $applyHolidayDetail['Confirm'] = $this->data['Confirm'];
                }
                // $this->ApplyHoliday->ApplyHolidayDetail->create(false);
                if (!$this->ApplyHoliday->ApplyHolidayDetail->saveAll($applyHolidayDetail)) {
                    $message = "データの更新に失敗しました";
                } else {
                    $detail_id = 0;
                    if ($applyHolidayDetail['ApplyHolidayDetail']['id'] > 0) {
                        $detail_id = $applyHolidayDetail['ApplyHolidayDetail']['id'];
                    } else {
                        $detail_id = $this->ApplyHoliday->ApplyHolidayDetail->getLastInsertID();
                        //  申請データを作成する
                        $check_staff_id = $this->data['Confirm']['check_staff_id'];
                        $data = array('Confirm'=>array('id'=>0,
                            'type'=>CONFIRM_TYPE_8,
                            'type_id'=>$detail_id,
                            'target_date'=>date('Y-m-d'),
                            'request_date'=>null,
                            'request_staff_id'=>$this->my_staff_id,
                            'check_date'=>null,
                            'check_staff_id'=>$check_staff_id,
                            'status'=>CONFIRM_YET,
                            'receive'=>RECEIVE_YET,
                            'receive_date'=>null,
                            'remark'=>"",
                            'enable'=>1,
                            )
                        );
                        if (!$this->Confirm->save($data)) {
                            $message = "申請データの更新に失敗しました";
                        } else {
                            $confirm_id = $this->Confirm->getLastInsertID();
                            $this->ApplyHoliday->ApplyHolidayDetail->save(array('id' => $detail_id, 'confirm_id'=> $confirm_id));
                        }
                    }

                    // 登録時に申請する
                    $message = "申請しました。";
                    
                    // 申請処理を実行する
                    // NOTE:新規の場合に呼び出し先のrefererが期待通りに動かないので、メソッド呼び出しにする
                    $confirms_controller = new ConfirmsController();
                    $collection = new ComponentCollection();
                    $confirms_controller->Common = new CommonComponent($collection);
                    $confirms_controller->update(CONFIRM_TYPE_8, $detail_id, CONFIRM_IN);
                    // $this->requestAction(
                    //     array(
                    //         'controller' => 'confirms',
                    //         'action' => 'update',
                    //     ),
                    //     array(
                    //         'pass' => array(CONFIRM_TYPE_8, $detail_id, CONFIRM_IN)
                    //     )
                    // );

                }
            }
            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
            $this->redirect(array('action' => 'index', $this->data['ApplyHoliday']['year'], $this->data['ApplyHoliday']['month']));
        }
    }

    /**
     * [a_detail_edit 詳細]
     * @param  [type] $detail_id [詳細ID]
     */
    function a_detail_edit($detail_id = null){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        $data = $this->ApplyHoliday->ApplyHolidayDetail->find("first", array("conditions" => array("ApplyHolidayDetail.id"=>$detail_id)));

        $staff_id = $this->Session->read('my_staff_id');
        // // 承認者とログインIDが同じかチェックする
        // if ($data['Confirm']['check_staff_id'] != $staff_id) {
        //     $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
        //     $this->redirect($this->referer());   
        // }

        $check_staff_rank = 0;
        $check_staff_id = 0;
        $request_staff_id = $data['Confirm']['request_staff_id'];
        $year = $data['ApplyHoliday']['year'];
        $month = $data['ApplyHoliday']['month'];
        if (!empty($data)) {
            $this->data = $data;

            // 承認者のランクを取得する
            $check_staff_id = $data['Confirm']['check_staff_id'];
            $check_staff = $this->Staff->read(array('Staff.id', 'Authority.rank'), $check_staff_id);

            $check_staff_rank = $check_staff['Authority']['rank'];
        }

        $staff = $this->Staff->read(null, $staff_id);
        $view_apply_button = false;
        if ($check_staff_id == $staff_id) {
            $view_apply_button = true;
        }
        $staff_rank = $staff['Authority']['rank'];
        $check_staffs = array();
        if ($view_apply_button && !($data['Confirm']['status'] == CONFIRM_YET || $data['Confirm']['status'] == CONFIRM_NO)) {
            $check_staffs = $this->get_check_staffs($staff_id);
        } else {
            $check_staffs = $this->get_check_staffs($request_staff_id);
        }
        $request_staffs =  $this->get_check_staffs();
        
        // 勤怠の年間情報を取得する
        $kintais_controller = new KintaisController();
        // 有給休暇残日数取得
        $paid_remain = $kintais_controller->_getPaidHolidayRemain($request_staff_id);
        // 振替休暇残日数取得
        $trans_remain = $kintais_controller->_getTrHolidayRemain($request_staff_id);
        // 特別休暇残日数取得
        $special_remain = $kintais_controller->_getSpHolidayRemain ($request_staff_id);
        // 半日休暇残数取得
		$half_remain = $kintais_controller->_getHalfHolidayRemain ($request_staff_id, $year, $month);
        // 年間有休消化日数取得
        $paid_digest = $kintais_controller->_getPaidHolidayDigest ($request_staff_id, $year, $month);

        $this->set(compact('data', 'staff_id', 'detail_id', 'staff_rank', 'check_staffs', 'request_staffs', 'check_staff_rank', 'view_apply_button'));
        $this->set(compact('paid_remain', 'trans_remain', 'special_remain', 'half_remain', 'paid_digest'));

        $this->set('title_for_layout', '休暇申請登録・編集');
    }

    /**
     * [s_approval 承認]
     * @return [type] [None]
     */
    function s_approval($type, $type_id, $status, $check_staff_id){

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        $staff_id = $this->Session->read('my_staff_id');
        $message = "更新しました";


        // Note:リーダー or マネージャの承認の履歴は残さない
        // 承認者を更新する
        $apply_detail = $this->ApplyHoliday->ApplyHolidayDetail->read(null, $type_id);
        $confirm = $apply_detail['Confirm'];
        $confirm['prev_check_staff_id'] = $confirm['check_staff_id'];
        $confirm['check_staff_id'] = $check_staff_id;
        if (!$this->Confirm->save($confirm)) {
            $$message = "承認に失敗しました。";
            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());            
        } else {
            // 申請処理を実行する
            $this->requestAction(
                array(
                    'controller' => 'confirms',
                    'action' => 'update',
                ),
                array(
                    'pass' => array($type, $type_id, CONFIRM_IN)
                )
            );
        }

        $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
        $this->redirect($this->referer());

    }

    /**
     * [s_detail_delete 削除]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function s_detail_delete($id){
        $this->autoRender = false;
        $message = "削除しました。";

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        $data = $this->ApplyHoliday->ApplyHolidayDetail->find('first', array('conditions'=>array('ApplyHolidayDetail.id'=>$id)));
        if(!empty($data)){
            $data['ApplyHolidayDetail']['enable'] = 0;
            $this->ApplyHoliday->ApplyHolidayDetail->save($data);
            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
            $this->redirect(array('action' => 'index', $data['ApplyHoliday']['year'], $data['ApplyHoliday']['month']));
        }
    }

    /**
     * [get_check_staffs スタッフを取得]
     * @param  [type] $staff_id  [スタッフID]
     * @return [type] object     [スタッフ一覧]
     */
    private function get_check_staffs($staff_id = null) {
        $con = array(
            "conditions" => array(
                'Staff.retire_date'=>null,
                'Staff.no <>'=>9999,
            ),
            "fields" => array("Staff.id", "Staff.name")
        );

        if ($staff_id) {
            $staff = $this->Staff->read(null, $staff_id);
            $rank = $staff['Authority']['rank'];
            if ($rank > RANK_LEADER) {
                // リーダー未満はリーダ以上を表示
                $con["conditions"]["Authority.rank <="] = RANK_LEADER;
            } elseif ($rank == RANK_LEADER || $rank == RANK_MANAGER) {
                // リーダー、マネージャは、自分より上のランクを表示
                $con["conditions"]["Authority.rank <"] = $rank;
            } elseif($rank < RANK_MANAGER) {
                // 部長以上は部長以上を表示
                $con["conditions"]["Authority.rank <="] = RANK_HEAD_DEPARTMENT;
            }
        }
        $staffs = $this->Staff->find('all', $con);
        $check_staffs = array();
        foreach ($staffs as $staff) {
            $check_staffs[$staff["Staff"]["id"]] = $staff["Staff"]["name"];
        }
        
        return $check_staffs;
    }
}
?>