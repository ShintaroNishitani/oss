<?php
App::uses('AppController', 'Controller');
/**
 * AssetDocumentManages Controller
 */
class AssetDocumentManagesController extends AppController {
	var $uses = array("AssetDocumentManage", "AssetLend","Staff","Customer","AssetLendReturn","AssetLendInventory");

    /**
     * [s_index 一覧]
     * @param  [type] [None]
     * @return [type] [None]
     */

    //ページネート設定 更新日の降順
    public $paginate = array(
        'page' => 1,
        'conditions' => array(''),
        );

    function s_index($year = null){
  
        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        // 今年度取得
        $now_year = $this->Common->getYear();
        // 選択用年度リスト
        $years = array ();
        for ($i = $now_year + 1; $i >= 2021; $i--) {
            $years += array ($i => sprintf ("%d 年度", $i));
        }

        // 年度未指定時は今年度を指定
        if ($year == null) {
            $year = $now_year;
        }
        
        // コンディション設定
        $con = array();
        array_push ($con, array ('limit' => 20, 'order' => array('AssetDocumentManage.asset_loan_no' => 'desc'),
                                 'fields'=>array('id', 'asset_loan_no', 'receipt_date', 'project_name', 'borrow_name')));


        if ($year != 0 && $year != null) {
            $disp_from = sprintf ("%04d-02-01", $year);
            $disp_to = sprintf ("%04d-01-31", $year + 1);
        }

        $this->paginate['AssetDocumentManage'] = array(
            'limit' => 20,
            'order' => array('AssetDocumentManage.asset_loan_no' => 'desc'),
            //"recursive"=>2,
            'fields'=>array('id', 'asset_loan_no', 'receipt_date', 'project_name', 'borrow_name'),
            'conditions'=>array('receipt_date >='=>$disp_from, 'receipt_date <='=>$disp_to, 'enable'=>1)
        );

        $datas = $this->paginate('AssetDocumentManage');
        $this->set(compact("datas", 'years', 'year'));
        $this->set($datas);
        $this->set("title_for_layout", "資産貸出書管理");
    }

    function s_edit($id = null){
        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }
        $this->layout = "ajax";
        $this->AssetDocumentManage->unbindModelAll();
        
        
        if($id != null) {
            //編集
            //編集の場合取得したidから該当レコードを取得
            $data = $this->AssetDocumentManage->find("first", array("conditions" => array("AssetDocumentManage.id"=>$id)));
            if (!empty($data)) {
                $this->data = $data;
            }

            //情報管理責任者の値から、社員情報を取得
            $staffs = $this->Staff->find("list",
                array("conditions" =>
                array("Staff.no !="       => "9999",
                      "Staff.retire_date" => null)));

        } else {
            //新規追加
            $data = null;
            $staffs = $this->Staff->find("list",
                array("conditions" =>
                array("Staff.no !="       => "9999",
                      "Staff.retire_date" => null)));

            //顧客管理情報を取得
            $customers = $this->Customer->find('list', array('fields'=>array('id', 'full_name')));
        }

        $today = date("Y-m-d");      

        if (!empty($data)) {
            $this->data = $data;
            $this->log($data,LOG_DEBUG);
        }
        //$staffs = $this->Staff->find('list', array("conditions" => array("Staff.no !="=>"9999", "Staff.retire_date"=>null)));
        $this->set(compact('id','staffs', 'today'));
    }

     /**
     * [s_update 更新]
     * @param  [type] [None]
     * @return [type] [None]
     */
    function s_update(){
        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }
        
        if ($this->request->is('post') || $this->request->is('put')) {
            
            $message = "更新しました";

            if (empty($this->data)) {
                $message = "セッションエラーです";
            } else {
                if(empty($this->request->data['AssetDocumentManage']['asset_loan_no'])){
                    $receipt_date = date($this->request->data['AssetDocumentManage']['receipt_date']);
                    $receipt_dates = explode("-", $receipt_date);
                
                    $get_year = $this->Common->getYear($receipt_dates[0],$receipt_dates[1]); //年取得
                    $disp_from = sprintf ("%04d-02-01", $get_year);
                    $disp_to = sprintf ("%04d-01-31", $get_year + 1);
                
                    $sarchdate = $receipt_dates[0].'-'.$receipt_dates[1].$receipt_dates[2];

                    //発行日の管理番号の最大値を取得
                    $max_data = $this->AssetDocumentManage->find("first", array(
                                    'fields'=>array('id', 'asset_loan_no', 'receipt_date', 'project_name', 'borrow_name'),
                                    'conditions'=>array('receipt_date >='=>$disp_from, 'receipt_date <='=>$disp_to, 'enable'=>1, 'asset_loan_no like' => $sarchdate.'%'),
                                    'order' => array('receipt_date' => 'desc', 'asset_loan_no' => 'desc')));
                    
                    $setcnt = 1;
                    if (!empty($max_data)) {
                        $max_asset_loan_no = $max_data['AssetDocumentManage']['asset_loan_no'];
                        $getdatas = explode("-", $max_asset_loan_no);
                        $setcnt = (int)$getdatas[2]+ 1;
                    };

                    //資産管理番号の発行
                    $setdata =  $sarchdate.'-'.sprintf('%03d', $setcnt );
                    $this->request->data["AssetDocumentManage"]["asset_loan_no"] = $setdata;
                } 
                
                if (!$this->AssetDocumentManage->save($this->request->data["AssetDocumentManage"])) {
                    $message = "データの更新に失敗しました";
                } 
            }
            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
            $this->redirect(array('action' => 'index'));
        }
    }

    //削除処理
    function s_delete($id = null){

        $auth = $this->_checkStaffAuthority();
        // if (2 != $auth) {
        //     $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
        //     $this->redirect($this->referer());   
        // }

        $data = $this->AssetDocumentManage->find('first', array('conditions' => array('id' => $id)));
        if(!empty($data)) {
            $data['AssetDocumentManage']['enable'] = 0;
            $this->AssetDocumentManage->save($data,false);
            $this->redirect(array('action' => 'index'));
        }
    }
}
