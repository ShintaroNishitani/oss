<?php
App::uses('AppController', 'Controller');

/**
 * 案件
 */
class SaleProjectsController extends AppController {

	var $uses = array('SaleProject');

	public $paginate = array(
		'page' => 1,
		'conditions' => array(''),
		);

	/**
	 * [s_index 一覧]
	 * @return [type] [None]
	 */
	public function s_index(){

		if (0 == $this->_checkStaffAuthority()) {
				$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
				$this->redirect($this->referer());
		}

		$joins = array(
				array(
				"type" => "LEFT",
				"table" => "companies",
				"alias" => "Company",
				"conditions" => array("Company.id = SaleProject.company_id"),
			),
		);

		$con = array();

		// 項目名=>array(値,検索条件,モデル名)
		$keys = array('name'=>array('', 'like', ''),
						'company'=>array('', 'company', 'Company'),
						'required_skill'=>array('', 'like', ''),
						'status'=>array('', 'default', ''),
						// 'term_s'=>array('', 'term_s', ''),
						// 'term_e'=>array('', 'term_e', ''),
					 );

		// 検索条件作成
		$model = "SaleProject";
		if(!empty($this->params->query)){
			foreach ($keys as $key => $value) {
				if($this->params->query["sc_" . $key] != ""){
					$keys[$key][0] = $this->params->query["sc_" . $key];

					if ($value[2]) {
						$model = $value[2];
					}
					// 特殊な検索はswitch文に追加する
					switch ($keys[$key][1]) {
						case 'like':
							array_push($con, array("$model.$key LIKE"=>"%".$keys[$key][0]."%"));
							break;
						case 'term_s':
							$item = str_replace('_s', '', $key);
							array_push($con, array("$model.$item >="=>$keys[$key][0]));
							break;
						case 'term_e':
							$item = str_replace('_e', '', $key);
							array_push($con, array("$model.$item <="=>$keys[$key][0]));
							break;
						case 'company':
							array_push($con, array("$model.name LIKE"=>"%".$keys[$key][0]."%"));
							break;
						default:
							array_push($con, array("$model.$key"=>$keys[$key][0]));
							break;
					}
				}
			}
		}

		$this->SaleProject->unbindModelAll();
		$this->paginate['SaleProject'] = array('limit'=>100, "conditions"=>$con, 'fields'=>array('*'), 'order'=>array('created'=>'desc'), 'joins'=> $joins);
		$datas = $this->paginate('SaleProject');
		if(isset($this->params->query['csv'])){
			$datas = $this->SaleProject->find('all', array("conditions"=>$con, 'fields'=>array('*'), 'order'=>array('created'=>'desc'), 'joins'=> $joins));
			$this->_outputCsv($datas);
			exit();
		}

		$this->set(compact('datas', 'keys'));

		$this->set('title_for_layout', '案件一覧');
	}

	/**
	 * [s_edit 登録・編集]
	 * @param  [type]  $id    [ID]
	 * @return [type]         [None]
	 */
	function s_edit($id = null){

		$auth = $this->_checkStaffAuthority();
		if (0 == $auth) {
			echo '権限がありません';
			exit();
		}

		$this->layout = "ajax";
		$data = $this->SaleProject->find("first", array("conditions"=>array("SaleProject.id"=>$id)));
		if (!empty($data)) {
			$this->data = $data;
		}

		$companies = $this->SaleProject->Company->find('list');

		$this->set(compact('datas', 'companies'));
	}

	/**
	 * [s_update 更新]
	 * @return [type] [None]
	 */
	function s_update() {
		$error = 0;
		$message = "更新しました。";
		$auth = $this->_checkStaffAuthority();
		if (2 != $auth) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}
		if (!empty($this->request->data)) {
			if(empty($this->request->data['SaleProject']['id'])){//新規登録
				$this->SaleProject->create();
			}
			if (!$this->SaleProject->save($this->request->data)) {
				$error++;
				$message = "更新に失敗しました";
			}
		} else {
			$error++;
			$message = "不正なアクセスです。";
		}
		echo sprintf('{"error":"%d","message":"%s"}', $error, $message);
		exit();

	}

	/**
	 * [s_delete 削除]
	 * @param  [type] $id [ID]
	 * @return [type]     [None]
	 */
	function s_delete($id) {
		$error = 0;
		$message = "削除しました。";

		$auth = $this->_checkStaffAuthority();
		if (2 != $auth) {
			$error++;
			$message = "権限がありません";
		} else {
			$data = $this->SaleProject->find('first', array('conditions'=>array('SaleProject.id'=>$id)));
			if(!empty($data)){
				$data['SaleProject']['enable'] = 0;
				if (!$this->SaleProject->save($data)) {
					$error++;
					$message = "削除に失敗しました";
				}
			}
		}

		echo sprintf('{"error":"%d","message":"%s"}', $error, $message);
		exit();
	}

	/**
	 * [s_upload アップロード]
	 * @return [type]        [None]
	 */
	function s_upload(){

		if (2 != $this->_checkStaffAuthority()) {
				$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
				$this->redirect($this->referer());
		}

		$msg = "データをアップロードしました。";
		if ($this->request->is('post') || $this->request->is('put')) {
			if(!empty($this->data['SaleProject']['file'])){
				$filename =  $this->data['SaleProject']['file']['name'];
				// ファイル読み込み
				$csv = array();
				if(!$this->_readFile($this->data['SaleProject']['file']['tmp_name'], $csv, $msg)){
					$this->Session->setFlash($msg, 'default', array('class'=> 'alert alert-info'));
					$this->redirect($this->referer());
				}

				foreach ($csv as $data) {
					$this->SaleProject->create();
					$this->SaleProject->save($data);
				}
			}
		}

		$this->Session->setFlash($msg, 'default', array('class'=> 'alert alert-info'));
		$this->redirect($this->referer());
	}

	/**
	 * [_readFile ファイル読み込み]
	 * @param  [type] $filename [ファイル名]
	 * @param  [type] &$datas   [データ]
	 * @param  [type] &$msg     [メッセージ]
	 * @return [type]           [結果]
	 */
	function _readFile($filename, &$datas, &$msg){

		// エンコード
		$file = tmpfile();
		fwrite($file, mb_convert_encoding(file_get_contents($filename), 'UTF-8', 'sjis-win'));
		rewind($file);

		// CSV読み込み
		$i = 0;
		while (($csv = fgetcsv($file)) !== FALSE) {
			$i++;
			// ヘッダー部読み飛ばし
			if ($i <= 1) {
				continue;
			}
			if(16 != count($csv)){
				$msg = "ファイルフォーマットが不正です。";
				fclose($file);
				return false;
			}

			$idx = 0;
			$data = array('date'=>date('Y-m-d'),
							'name'=>trim($csv[$idx++]),
							'term_s'=>trim($csv[$idx++]),
							'term_e'=>trim($csv[$idx++]),
							'num'=>trim($csv[$idx++]),
							'place'=>trim($csv[$idx++]),
							'time_from'=>trim($csv[$idx++]),
							'time_to'=>trim($csv[$idx++]),
							'price'=>trim($csv[$idx++]),
							'process'=>trim($csv[$idx++]),
							'foreign'=>trim($csv[$idx++]),
							'age'=>trim($csv[$idx++]),
							'customer'=>trim($csv[$idx++]),
							'interview'=>trim($csv[$idx++]),
							'office_hour'=>trim($csv[$idx++]),
							'skill'=>trim($csv[$idx++]),
							'remark'=>trim($csv[$idx++]),
							'status'=>0,
							'enable'=>1,
							);

			// NULLチェック
			foreach ($data as $key => $value) {
				if (empty($value)) {
					$data[$key] = 0;
				}
			}

			$datas[] = $data;
		}
		fclose($file);
		return true;
	}

	/**
	 * [s_mail_format メールフォーマット]
	 * @param  [type]  $id    [ID]
	 * @return [type]         [None]
	 */
	function s_mail_format($id = null){

		$auth = $this->_checkStaffAuthority();
		if (0 == $auth) {
			echo '権限がありません';
			exit();
		}

		$this->layout = "ajax";
		$data = $this->SaleProject->find("first", array("conditions"=>array("SaleProject.id"=>$id)));

		$this->set(compact('data'));
	}

	/**
	 * [_outputCsv CSV出力]
	 * @param  [type] $hrs [データ]
	 * @return [type]             [None]
	 */
	function _outputCsv($sps){

		$keys = array('num'=>'No',
						'date'=>'登録日',
						'name'=>'案件名',
						'customer'=>'顧客',
						'skill'=>'スキル',
						'price'=>'単価',
						'process'=>'工程',
						'foreign'=>'外国籍',
						'term'=>'期間',
						'place'=>'勤務地',
						'age'=>'年齢',
						'time'=>'時間幅',
						'interview'=>'面談回数',
						'office_hour'=>'勤務時間',
						'remark'=>'備考',
						);

		// header
		$datas = "";
		foreach ($keys as $key => $value) {
			$datas .= ($value . ",");
		}
		$datas .= "\r\n";

		// body
		$idx = 1;
		foreach ($sps as $sp){
			foreach ($keys as $key => $value) {
				$data = '';
				// データを加工して表示するものはcase文に追加する
				switch ($key) {
					case 'num': $data = $idx; break;
					case 'foreign':
						$items = Configure::read("foreign");
						$data = $items[$sp['SaleProject']['foreign']];
						break;
					case 'term':
						$data = $sp['SaleProject']['term_s']."～".$sp['SaleProject']['term_e'];
						break;
					case 'time':
						$data = $sp['SaleProject']['time_from']."～".$sp['SaleProject']['time_to'];
						break;
					default: $data = trim($sp['SaleProject'][$key]); break;
				}
				$datas .= ("\"" . $data . "\"" . ',');
			}
			$datas .= "\r\n";

			$idx++;
		}

		Configure::write('debug', 0);
		$req_file = sprintf("案件一覧_%s.csv", date("Ymd-Hi"));
		$req_file = mb_convert_encoding($req_file, "SJIS", "UTF-8");
		header ("Content-disposition: attachment; filename=" . $req_file);
		header ("Content-type: application/octet-stream; name=" . $req_file);

		echo mb_convert_encoding($datas, "SJIS-WIN", "UTF-8");

		return;
	}


	/**
	 * [s_search_sale_project 案件検索(ajax用)]
	 * @param  [type] [None]
	 * @return [type] [None]
	 */
	public function s_search_project() {
		
		$auth = $this->_checkStaffAuthority();
		if (0 == $auth) {
			echo '権限がありません';
			exit();
		}

		// ----------------------------------------
		// 初期処理
		// ----------------------------------------
		// ビューの使用無を設定
		$this->autoRender = false;

		// ContentTypeをJSONにする
		$this->response->type('json');

//		// Ajax以外の通信の場合
//		if(!$this->request->is('ajax')) {
//			throw new BadRequestException();
//		}

		//-------------------------------------------
		// リクエストパラメータ取得
		//-------------------------------------------
		$id = $this->request->query['id'];

		//-------------------------------------------
		// 検索
		//-------------------------------------------
		$result = $this->SaleProject->find("first", array("conditions"=>array("SaleProject.id"=>$id)));

		//-------------------------------------------
		// 戻り値の設定
		//-------------------------------------------
		$status = !empty($result);
		if(!$status) {
//			throw new NotFoundException();
			$error = array(
				'message' => 'データがありません',
				'code' => 404
			);
		}

		// json_encodeを使用してJSON形式で返却
		return json_encode(compact('status', 'result', 'error'));
	}
}

?>
