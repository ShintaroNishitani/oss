<?php
App::uses("AppController", "Controller");

/**
 * 資産貸出
 */
class InnerAssetLendsController extends AppController {
    var $uses = array("InnerAssetLend","Staff");

    /**
     * [s_index 一覧]
     * @param  [type] [None]
     * @return [type] [None]
     */

    //ページネート設定 更新日の降順
    public $paginate = array(
        'page' => 1,
        'conditions' => array(''),
        );

    function s_index(){
  
        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) 
        {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }
        // 項目名=>array(値,検索条件,モデル名)
        $keys = array('request_staff_id'       =>isset($this->params->query['request_staff_id']) ? $this->params->query['request_staff_id']: "" ,
                      'include_already_return' =>isset($this->params->query['include_already_return']) ? $this->params->query['include_already_return'] : "0",
                      'include_out_range'      =>isset($this->params->query['include_out_range']) ? $this->params->query['include_out_range'] : "0",
                     );
        $joins = array(
        );    
        $con = array('InnerAssetLend.enable' => "1", "enable" == "0");
        //検索条件
        if("" != $keys['request_staff_id'])
        {
        	array_push($con, array("InnerAssetLend.rent_staff_id" => $keys['request_staff_id']));
        }
        if("1" != $keys['include_already_return'])
        {
			array_push($con, array("InnerAssetLend.status" => "0"));
		}
        if("1" == $keys['include_out_range'])
        {
        	$today = date("Y-m-d");  
        	array_push($con, array("InnerAssetLend.rent_end_date <" . "\"$today\""));
			array_push($con, array("InnerAssetLend.auto_extend" => "0"));
			array_push($con, array("InnerAssetLend.status" => "0"));
        }

        //設定20件ずつ、降順、enableが1、貸出期間が当日終了以降である。
        $this->paginate['InnerAssetLend'] = array(
            'limit' => 20,
            'order' => array('InnerAssetLend.rent_date' => 'desc'),
            'conditions' => $con,
            "recursive"=>2,
            'joins'=> $joins,
            'fields'=>array('*')
        );
        $staffs = $this->Staff->find("list");
        
        $datas = $this->paginate('InnerAssetLend');
        $this->set(compact("datas", "staffs", "keys"));
        $this->set($datas);
        $this->set("self", $this->my_staff_id);
        $this->set("staffs" ,$staffs);
        $this->set("title_for_layout", "社内資産管理台帳");
    }

    /**
     * [s_edit 詳細更新]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function s_edit($id = null){
        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        if($id != null) 
        {
            //編集
            //編集の場合取得したidから該当レコードを取得
            $data = $this->InnerAssetLend->find("first", array("conditions" => array("InnerAssetLend.id" => $id)));
            if (!empty($data)) 
            {
                $this->data = $data;
            }

            //情報管理責任者の値から、社員情報を取得
            $staffs = $this->Staff->find("list",
                array("conditions" =>
                array("Staff.no !="       => "9999",
                      "Staff.retire_date" => null)));

        } 
        else 
        {
            //新規追加
            $data = null;
            $staffs = $this->Staff->find("list",
                array("conditions" =>
                array("Staff.no !="       => "9999",
                      "Staff.retire_date" => null)));
        }
        $this->set(compact("data","staffs"));
        $this->set("title_for_layout", "社内資産貸出登録");
    }

    /**
    * [s_update ユーザ入力情報で資産情報更新]
    * @param  [type] [None]
    * @return [type] [None]
    */
    function s_update() {
        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) 
        {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        if ($this->request->is('post') || $this->request->is('put')) 
        {
            $message = "更新しました";

            if (empty($this->data))
            {
                $message = "セッションエラーです";
            }
            else
            {
                if($this->request->data['InnerAssetLend']['id'] == null) 
                {
                    //新規追加
                    $this->InnerAssetLend->create();
                }
                $this->log($this->request->data, LOG_DEBUG);
                unset($this->request->data['InnerAssetLend']['created']);
                unset($this->request->data['InnerAssetLend']['modified']);
                if (!$this->InnerAssetLend->save($this->request->data, false)) 
                {
                    $message = "同じ番号は登録できません。";
                }
            }
            $this->Session->setFlash($message, 'default', array('class' => 'alert alert-info'));
            $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * [s_delete 詳細更新]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function s_delete($id = null){
        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) 
        {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }
        $data = $this->InnerAssetLend->find('first', array('conditions' => array('InnerAssetLend.id' => $id)));
        if(!empty($data)) 
        {
            $data['InnerAssetLend']['enable'] = 0;
            $this->InnerAssetLend->save($data,false);
            $this->redirect(array('action' => 'index'));
            $this->set('title_for_layout', '社内資産管理台帳');
        }
    }
}