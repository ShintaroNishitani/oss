<?php
App::uses('AppController', 'Controller');

/**
 * 承認
 */
class ConfirmsController extends AppController {

    var $uses = array('Confirm', 'Staff', 'ApplyHoliday');

    public $paginate = array(
        'page' => 1,
        'conditions' => array(''),
        );

    /**
     * [a_index 一覧]
     * @return [type] [None]
     */
    public function a_index(){

        if (0 == $this->_checkStaffAuthority()) {
              $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
              $this->redirect($this->referer());
        }

        $con = array();

        // 項目名=>array(値,検索条件,モデル名)
        $keys = array('request_staff_id'=>array('', 'default', ''),
                      'status'=>array('', 'default', ''),
                      'type'=>array('', 'default', ''),
                      'target_date_b'=>array('', 'term_s', ''),
                      'target_date_e'=>array('', 'term_e', ''),
                      'receive'=>array('', 'default', ''),
                      'check_staff_id'=>array('', 'default', ''),
                     );

        // 検索条件作成
        $model = "Confirm";
        if(!empty($this->params->query)){
            foreach ($keys as $key => $value) {
                if($this->params->query[$key] != ""){
                    $keys[$key][0] = $this->params->query[$key];

                    if ($value[2]) {
                        $model = $value[2];
                    }
                    // 特殊な検索はswitch文に追加する
                    switch ($keys[$key][1]) {
                        case 'like':
                            array_push($con, array("$model.$key LIKE"=>"%".$keys[$key][0]."%"));
                            break;
                        case 'term_s':
                            $item = str_replace('_b', '', $key);
                            array_push($con, array("$model.$item >="=>$keys[$key][0]));
                            break;
                        case 'term_e':
                            $item = str_replace('_e', '', $key);
                            array_push($con, array("$model.$item <="=>$keys[$key][0]));
                            break;
                        default:
                            array_push($con, array("$model.$key"=>$keys[$key][0]));
                            break;
                    }
                }
            }
        }

        // 未申請、差戻は表示しない
        array_push($con, array("$model.status <>"=>array(CONFIRM_YET, CONFIRM_NO)));

        $this->paginate['Confirm'] = array('limit'=>100, "conditions"=>$con, 'fields'=>array('*'), 'order'=>array('request_date'=>'desc'));
        $datas = $this->paginate('Confirm');
        for ($i = 0; $i < count($datas); $i++) {
            $data = $datas[$i];
            if ($data['Confirm']['type'] == CONFIRM_TYPE_8) {
                $dates = $this->ApplyHoliday->ApplyHolidayDetail->ApplyHolidayDetailDate->find('list', array(
                    'fields' => array("date"),
                    'conditions'=>array('apply_holiday_detail_id'=>$data['Confirm']['type_id']),
                    'order'=>array('date'),
                ));
                $datas[$i]['Applydays'] = $dates;
            }
        }
        $staffs = $this->Staff->find('list');

        $this->set(compact('datas', 'keys', 'staffs'));

        $this->set('title_for_layout', '申請一覧');
    }

    /**
     * [s_list リスト]
     * @param  [type] $type    [種別]
     * @param  [type] $type_id [種別ID]
     * @return [type]          [None]
     */
    public function s_list($type, $type_id, $year = null, $month = null){

        if (0 == $this->_checkStaffAuthority()) {
              $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
              $this->redirect($this->referer());
        }

        $this->layout = "ajax";

        $data = $this->Confirm->find('first', array('conditions'=>array('type'=>$type, 'type_id'=>$type_id)));
        if (empty($data)) {
            $data = array('Confirm'=>array('id'=>0,
                                           'type'=>$type,
                                           'type_id'=>$type_id,
                                           'target_date'=>"$year-$month-01",
                                           'request_date'=>null,
                                           'request_staff_id'=>$this->my_staff_id,
                                           'check_date'=>null,
                                           'check_staff_id'=>0,
                                           'status'=>CONFIRM_YET,
                                           'receive'=>RECEIVE_YET,
                                           'receive_date'=>null,
                                           'remark'=>"",
                                           'enable'=>1,
                                           )
                          );
            $this->Confirm->save($data);
            $data['Confirm']['id'] = $this->Confirm->id;
        }

        $staffs = $this->Staff->find('list');

        $this->set(compact('data', 'type', 'type_id', 'staffs'));
    }

    /**
     * [a_list リスト]
     * @param  [type] $type    [種別]
     * @param  [type] $type_id [種別ID]
     * @return [type]          [None]
     */
    public function a_list($type, $type_id){

        if (0 == $this->_checkStaffAuthority()) {
              $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
              $this->redirect($this->referer());
        }

        $this->layout = "ajax";

        $data = $this->Confirm->find('first', array('conditions'=>array('type'=>$type, 'type_id'=>$type_id)));

        $staffs = $this->Staff->find('list');

        $this->set(compact('data', 'type', 'type_id', 'staffs'));
    }

    /**
     * [update 更新]
     * @param  [type] $type    [種別]
     * @param  [type] $type_id [種別ID]
     * @param  [type] $status  [ステータス]
     * @param  [type] $check_staff_id  [承認者ID]
     * @param  [type] $reason  [理由]
     * @return [type]          [None]
     */
    function update($type, $type_id, $status, $check_staff_id = "", $reason = ""){
        // 申請情報を更新
        $data = $this->Confirm->find('first', array('conditions'=>array('type'=>$type, 'type_id'=>$type_id),'order'=>array('Confirm.id'=>'desc')));
        if ($data) {
            $staff = $this->Staff->find('first', array('conditions'=>array('Staff.id'=>$data['Confirm']['request_staff_id'])));
            $types = Configure::read("confirm_type");

            $date = "";
            if ($data['Confirm']['target_date']) {
                $date = explode('-', $data['Confirm']['target_date']);
                $target_year = $date[0];
                $target_month = $date[1];
                $date = $target_year ."年".$target_month."月";
            }

            $data['Confirm']['status'] = $status;
            if ($status == CONFIRM_IN) {
                $data['Confirm']['request_date'] = date('Y-m-d H:i:s');

                if (CONFIRM_TYPE_8 == $type) {
                    // 申請先のメール・LineWorksのIDを取得する
                    if (isset($data['CheckStaff'])) {
                        $line_account_id = $data['CheckStaff']['line_account_id'];
                        $staffName = $data['Staff']['name'];
                        if ($data['PrevCheckStaff'] && $data['PrevCheckStaff']['name']) {
                            $staffName .= "（".$data['PrevCheckStaff']['name']."）";
                        }
                        $body = sprintf(CONFIRM_TYPE8_BODY, $staffName);

                        if ($line_account_id) {
                            // $this->_sendLineWorks($line_account_id, $body);
                            $this->Common->sendLineWorks($line_account_id, $body);
                        }

                    }
                } else {
                    $mail = CONFIRM_MAIL;
                    if (CONFIRM_TYPE_0 == $type) {
                        $mail = CONFIRM_MAIL2;
                    }
                    $body = sprintf(CONFIRM_BODY, $data['Staff']['name']);

                    // メール送信
                    $title = $types[$type] . " " .$staff['Staff']['name'] . " " . $date;
                    $this->_sendMail($mail,
                                    sprintf(CONFIRM_TITLE, $title),
                                    $body,
                                    null, null
                                    );
                }


            } else if ($status == CONFIRM_OK) {     // 申請済の場合は承認日を設定してステータスを承認済に変える
                $data['Confirm']['check_date'] = date('Y-m-d H:i:s');
                $data['Confirm']['check_staff_id'] = $this->my_staff_id;
            } else if ($status == CONFIRM_NO) {     // 差戻しの場合は承認日、承認者、受領をクリア
                $data['Confirm']['check_date'] = "";
                // 差戻理由を設定
                $data['Confirm']['remark'] = $reason;
                $data['Confirm']['receive_date'] = "";
                $data['Confirm']['receive'] = RECEIVE_YET;

                $confirm_body_no_text = "";
                if (CONFIRM_TYPE_8 != $type) {
                    $confirm_body_no_text = sprintf(CONFIRM_BODY_NO, $reason);
                    $data['Confirm']['check_staff_id'] = "";

                    // メール送信
                    $title = $types[$type] . " " . $date;
                    $this->_sendMail($staff['Staff']['mail'],
                                    sprintf(CONFIRM_TITLE_NO, $title),
                                    $confirm_body_no_text,
                                    null, null
                                    );

                } else {
                    $confirm_body_no_text = sprintf(CONFIRM_TYPE8_BODY_NO, $data['CheckStaff']['name'], $reason);
                }
                if ($staff['Staff']['line_account_id']) {
                    // $this->_sendLineWorks($staff['Staff']['line_account_id'], $confirm_body_no_text);
                    $this->Common->sendLineWorks($staff['Staff']['line_account_id'], $confirm_body_no_text);
                }
            }
            $this->Confirm->save($data);
        }
    }

    /**
     * [s_update 更新]
     * @param  [type] $type    [種別]
     * @param  [type] $type_id [種別ID]
     * @param  [type] $status  [ステータス]
     * @param  [type] $check_staff_id  [承認者ID]
     * @param  [type] $reason  [理由]
     * @return [type]          [None]
     */
    public function s_update($type, $type_id, $status, $check_staff_id = "", $reason = ""){

        $auth = $this->_checkStaffAuthority();
        if ($auth == 2) {
            $this->update($type, $type_id, $status, $check_staff_id, $reason);
        } else {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
        }

        $this->redirect($this->referer());
    }

    /**
     * [s_update 更新]
     * @param  [type] $type    [種別]
     * @param  [type] $type_id [種別ID]
     * @param  [type] $status  [ステータス]
     * @param  [type] $check_staff_id  [承認者ID]
     * @param  [type] $reason  [理由]
     * @return [type]          [None]
     */
    public function s_update_ajax(){

		$this->layout = "ajax";
        $error = 0;
        $message = "更新しました。";
        $auth = $this->_checkStaffAuthority();
        if ($auth == 2) {

            $type = $this->data['type'];
            $type_id = $this->data['type_id'];
            $status = $this->data['status'];
            $check_staff_id = isset($this->data['check_staff_id']) ? $this->data['check_staff_id'] : '';
            $reason = isset($this->data['reason']) ? $this->data['reason'] : '';

            $this->update($type, $type_id, $status, $check_staff_id, $reason);
        } else {
            $message = '権限がありません';
            $error = 1;
        }

        echo sprintf('{"error":"%d","message":"%s"}', $error, $message);
        $this->autoRender = false;

    }


    /**
     * [s_receive 受領]
     * @param  [type] $type    [種別]
     * @param  [type] $type_id [種別ID]
     * @return [type]          [None]
     */
    public function s_receive($type, $type_id){

        $auth = $this->_checkStaffAuthority();
        if ($auth == 2) {
            // 申請情報を更新
            $data = $this->Confirm->find('first', array('conditions'=>array('type'=>$type, 'type_id'=>$type_id)));
            if ($data) {
                $data['Confirm']['receive_date'] = date('Y-m-d H:i:s');
                $data['Confirm']['receive'] = RECEIVE_OK;

                $this->Confirm->save($data);
            }
        } else {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
        }

        $this->redirect($this->referer());
    }
}

?>
