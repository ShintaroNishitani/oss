<?php
App::uses('AppController', 'Controller');

/**
 * プロジェクト管理
 */
class ProjectsController extends AppController {
    var $uses = array('Project', 'Staff');

    public $paginate = array(
        'page' => 1,
        'conditions' => array(''),
        );

    /**
     * [a_index 一覧]
     * @return [type] [description]
     */
    function a_index(){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        $this->paginate['Project'] = array('limit'=>100, 'order'=>array('date'=>'desc'));
        $datas = $this->paginate('Project');

        $this->set(compact('datas'));

        $this->set('title_for_layout', 'プロジェクト管理');
    }

    /**
     * [a_edit 更新]
     * @param  [type] $id    [ID]
     * @return [type]        [None]
     */
    function a_edit($id = null){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            echo '権限がありません';
            exit();  
        }

        $this->layout = "ajax";
        $this->Project->unbindModelAll();
        $data = $this->Project->find("first", array("conditions" => array("Project.id" => $id), 'fields'=>array('*')));
        if (!empty($data)) {
            $this->data = $data;
        }

        // プロジェクトスタッフ取得
        $project_staff = $this->Project->ProjectStaff->find("all", array("conditions"=>array("ProjectStaff.prj_id"=>$data['Project']['id'])));

        // 社員リスト取得
        $staffs = $this->Staff->find('list', array("conditions"=>array("Staff.retire_date"=>"")));

        // 顧客リスト取得
        $customers = $this->Project->Customer->find('list', array('fields'=>array('Customer.id', 'Customer.code')));

        $this->set(compact('id', 'data', 'project_staff', 'staffs', 'customers'));
        $this->set('title_for_layout', 'プロジェクト登録・編集');
    }

    /**
     * [a_update 更新]
     * @return [type] [None]
     */
    function a_update(){

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $message = "更新しました";

$this->log($this->data);
            if (empty($this->data)) {
                $message = "セッションエラーです";
            } else {

                $this->Project->create();
                if (!$this->Project->save($this->request->data)) {
                    $message = "データの更新に失敗しました";
                }
            }
            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
            $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * [a_delete 削除]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function a_delete($id){
        $this->autoRender = false;

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());   
        }

        $this->Project->delete($id);
        $this->redirect(array('action' => 'index'));
    }
}

?>