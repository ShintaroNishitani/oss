<?php
App::uses('AppController', 'Controller');

define('ALL_STAFF', 9998);				// 全社員

/**
 * 勤怠
 */
class KintaisController extends AppController {
	var $uses = array('Kintai', 'KintaiDetail', 'KintaiTime', 'Confirm', 'Staff', 'Holiday', 'PaidHoliday', 'PaidHolidayDetail', 'TransHoliday', 'SpecialHoliday', 'Order');

	public $paginate = array(
		'page' => 1,
		'conditions' => array(''),
		);

	/**
	 * [s_index 一覧]
	 * @param  [type] $year     [年]
	 * @param  [type] $month    [月]
	 * @return [type]           [None]
	 */
	function s_index ($year = null, $month = null)
	{
		$auth = $this->_checkStaffAuthority();
		if ($auth == 0) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}

		if ($year == null || $month == null) {
			$year = date('Y');
			$month = date('n');
		}
		$today_y = date("Y");
		$today_m = date("n");
		$today_d = date("j");

		//前月、翌月を取得
		$this->Common->getBeforeMonth($year, $month, $b_year, $b_month);
		$this->Common->getNextMonth($year, $month, $n_year, $n_month);

		$staff_id = $this->Session->read('my_staff_id');

		// 当月の日数・月初・月末を取得
		$day_num =  date("t", mktime(0, 0, 0, $month, 1, $year));

		$week = array("日", "月", "火", "水", "木", "金", "土");

		// 検索条件作成
		$con = array("Kintai.year"=>$year,
					 "Kintai.month"=>$month,
					 "Kintai.staff_id"=>$staff_id);
		$datas = $this->Kintai->find('first', array('conditions'=>$con));

		if (empty($datas)) {
			// 当月の営業日数を算出
			$business_day = 0;
			for ($i = 1; $i <= $day_num; $i++) {
				$datetime = new DateTime(date("Y-m-d", mktime(0, 0, 0, $month, $i, $year)));
				$w = (int)$datetime->format('w');
				$h = 0;

				// 休日判定
				if ($w == 0 || $w == 6) {
					$h = 1;
				}
				else {
					$d = date("Y-m-d", mktime(0, 0, 0, $month, $i, $year));
					$holidays = $this->Holiday->find('first', array('conditions'=>array("Holiday.date"=>$d)));
					if (!empty($holidays)) {
						$h = 1;
					}
				}

				if ($h == 0) {
					$business_day++;
				}
			}

			// 勤怠テーブル作成
			$this->Kintai->create();
			$data['Kintai'] = array('staff_id'=>$staff_id,
									'year'=>$year,
									'month'=>$month,
									'status'=>0,
									'enable'=>1,
									'month_day'=>$day_num,
									'business_day'=>$business_day,
									'work_time'=>0.0,
									'late_time'=>0.0,
									'over_time'=>0.0,
									'extra_time'=>0.0,
									'holiday'=>0.0,
			                        'transport'=>0,
									);

			// 前月データからプロジェクト名をコピー
			$bef_con = array("Kintai.year"=>$b_year, "Kintai.month"=>$b_month, "Kintai.staff_id"=>$staff_id);

			$bef_datas = $this->Kintai->find('first', array('conditions'=>$bef_con));
			if (!empty($bef_datas)) {
				$data['Kintai'] += array('project1_name'=>$bef_datas['Kintai']['project1_name']);
				$data['Kintai'] += array('project2_name'=>$bef_datas['Kintai']['project2_name']);
				$data['Kintai'] += array('project3_name'=>$bef_datas['Kintai']['project3_name']);
				$data['Kintai'] += array('project4_name'=>$bef_datas['Kintai']['project4_name']);
				$data['Kintai'] += array('project5_name'=>$bef_datas['Kintai']['project5_name']);
				$data['Kintai'] += array('project6_name'=>$bef_datas['Kintai']['project6_name']);
				$data['Kintai'] += array('project1_kind'=>$bef_datas['Kintai']['project1_kind']);
				$data['Kintai'] += array('project2_kind'=>$bef_datas['Kintai']['project2_kind']);
				$data['Kintai'] += array('project3_kind'=>$bef_datas['Kintai']['project3_kind']);
				$data['Kintai'] += array('project4_kind'=>$bef_datas['Kintai']['project4_kind']);
				$data['Kintai'] += array('project5_kind'=>$bef_datas['Kintai']['project5_kind']);
				$data['Kintai'] += array('project6_kind'=>$bef_datas['Kintai']['project6_kind']);
			}
			else {
				$data['Kintai'] += array('project1_name'=>'');
				$data['Kintai'] += array('project2_name'=>'');
				$data['Kintai'] += array('project3_name'=>'');
				$data['Kintai'] += array('project4_name'=>'');
				$data['Kintai'] += array('project5_name'=>'');
				$data['Kintai'] += array('project6_name'=>'');
				$data['Kintai'] += array('project1_kind'=>0);
				$data['Kintai'] += array('project2_kind'=>0);
				$data['Kintai'] += array('project3_kind'=>0);
				$data['Kintai'] += array('project4_kind'=>0);
				$data['Kintai'] += array('project5_kind'=>0);
				$data['Kintai'] += array('project6_kind'=>0);
			}

			$this->Kintai->save($data);
			$id = $this->Kintai->id;

			// 勤怠詳細テーブル作成
			for ($i = 1; $i <= $day_num; $i++) {
				$datetime = new DateTime(date("Y-m-d", mktime(0, 0, 0, $month, $i, $year)));
				$w = (int)$datetime->format('w');
				$h = 0;

				// 休日判定
				if ($w == 0 || $w == 6) {
					$h = 1;
				}
				else {
					$d = date("Y-m-d", mktime(0, 0, 0, $month, $i, $year));
					$holidays = $this->Holiday->find('first', array('conditions'=>array("Holiday.date"=>$d)));
					if (!empty($holidays)) {
						$h = 1;
					}
				}

				$this->KintaiDetail->create();
				$data['KintaiDetail'] = array('kintai_id'=>$id,
											  'date'=>$i,
											  'day'=>$week[$w],
											  'holiday'=>$h,
											  'enable'=>1,
											 );
				$this->KintaiDetail->save($data);
			}

			$datas = $this->Kintai->find('first', array('conditions'=>$con));
		}
		// 2016/03/23 : [PEND] 暫定的に追加カラムについてはここで設定する。いずれ削除すること。
		else {
			// 合計時間を計算
			$datas['Kintai']['month_day']    = 0;
			$datas['Kintai']['business_day'] = 0;
			$datas['Kintai']['work_time']    = 0.0;
			$datas['Kintai']['late_time']    = 0.0;
			$datas['Kintai']['over_time']    = 0.0;
			$datas['Kintai']['extra_time']    = 0.0;
			$datas['Kintai']['holiday']      = 0.0;
			foreach ($datas['KintaiDetail'] as $detail) {
				$datas['Kintai']['month_day']++;
				$datas['Kintai']['work_time'] += $detail['work_time'];
				$datas['Kintai']['late_time'] += $detail['late_time'];

				/*
				//----- 超勤時間計算 -----//
				// 半休取得時
				if ($detail['application'] == HOLIDAY_MORNING_OFF ||
					$detail['application'] == HOLIDAY_AFTERNOON_OFF ||
					$detail['application'] == HOLIDAY_OBSERVED_HALF) {
					$datas['Kintai']['over_time'] += $detail['work_time'] - 4.0;
					$datas['Kintai']['extra_time'] += $detail['work_time'] - 4.0;
				}
				// 休日出勤時
				else if ($detail['holiday'] == 1) {
					if ($detail['work_time'] >= 8.0) {
						$datas['Kintai']['over_time'] += $detail['work_time'] - 8.0;
					}
					else {
						$datas['Kintai']['over_time'] += $detail['work_time'];
					}
					$datas['Kintai']['extra_time'] += $detail['work_time'];
				}
				// 通常勤務時
				else {
					if ($detail['work_time'] != 0.0) {
						$datas['Kintai']['over_time'] += $detail['work_time'] - 8.0;
						$datas['Kintai']['extra_time'] += $detail['work_time'] - 8.0;
					}
				}
				*/
				if (!is_null($detail['start_time'])) {
					$datas['Kintai']['over_time'] += $this->_calcOverTime($detail['work_time'], $detail['application'], $detail['holiday']);
					$datas['Kintai']['extra_time'] += $this->_calcExtraTime($staff_id,
																			$year, $month, $detail['date'],
																			$detail['work_time'], $detail['application'], $detail['holiday']);
				}

				// 営業日をカウント
				if ($detail['holiday'] != 1) {
					$datas['Kintai']['business_day']++;
				}

				// 年休日数を算出
				switch ($detail['application']) {
					case HOLIDAY_PAID_ALL:
					case HOLIDAY_SPECIAL:
						$datas['Kintai']['holiday'] += 1.0;
						break;
					case HOLIDAY_MORNING_OFF:
					case HOLIDAY_AFTERNOON_OFF:
						$datas['Kintai']['holiday'] += 0.5;
						break;
				}

				$detail['remote_work'] = 0;
			}

			// 合計計算後のレコードを一旦保存
			$this->Kintai->save($datas['Kintai']);
		}

		$id = $this->Kintai->id;
		$type = CONFIRM_TYPE_1;

		// 勤怠時間テーブル取得
		$s_times = $this->KintaiTime->find('list', array('conditions'=>array('start_enable'=>1, 'holiday_only'=>0), 'order'=>array('hour'=>'asc'), 'fields'=>array('time')));
		$e_times = $this->KintaiTime->find('list', array('conditions'=>array('end_enable'=>1), 'order'=>array('hour'=>'asc'), 'fields'=>array('time')));
		$s_times_holiday = $this->KintaiTime->find('list', array('conditions'=>array('start_enable'=>1), 'order'=>array('hour'=>'asc'), 'fields'=>array('time')));

		$con = array("Staff.id"=>$staff_id, "Staff.enable"=>1);
		$staff = $this->Staff->find('first', array('conditions'=>$con));

		// 承認状況を取得
		$status = 0;
		$confirm = $this->Confirm->find('first', array('conditions'=>array('type'=>CONFIRM_TYPE_0, 'type_id'=>$datas['Kintai']['id'])));
		if ($confirm) {
			$status = $confirm['Confirm']['status'];
		}

		// 未入力欄がないかチェック
		$comfirm_flag = 0;
		if ($status != CONFIRM_IN && $status != CONFIRM_OK) {
			$comfirm_flag = $this->_checkKintaiConfirm($id);
		}

		// 始業(平日/休日)、終業それぞれの未設定時刻IDを設定
		$s_ivd_id = $this->_getUnsetBusinessTimeID(0);
		$e_ivd_id = $this->_getUnsetBusinessTimeID(1);
		for ($i = 0; $i < $day_num; $i++) {
			if ($datas['KintaiDetail'][$i]['start_time'] == null) {
				$datas['KintaiDetail'][$i]['start_time'] = $s_ivd_id;
			}
			if ($datas['KintaiDetail'][$i]['end_time'] == null) {
				$datas['KintaiDetail'][$i]['end_time'] = $e_ivd_id;
			}
		}

		// 特別休暇残日数を取得する
		$special_remain = $this->_getSpHolidayRemain ($staff_id, $year, $month);

		// 年間有給消化数を取得する
		$paid_digest = $this->_getPaidHolidayDigest ($staff_id, $year, $month);

		// 業務形態を取得する
		$work_kinds = Configure::read ('work_kind');

		// プロジェクトのラベル取得
		$project_label = $this->Common->getProjectLabel("$year-$month-01");

		// 年間超勤時間超過回数を取得する
		$over_count = $this->_getExtraTimeOverCount($staff_id, $year, $month);

		$limit_time = $this->_getExtraTimeLimit($staff_id, $year, $month);

		$this->set(compact('year', 'month', 'b_year', 'b_month', 'n_year', 'n_month', 'today_y', 'today_m', 'today_d',
						   'datas', 'id', 'type', 'day_num', 's_times', 'e_times', 's_times_holiday', 'staff', 'status', 'special_remain', 'paid_digest',
						   'comfirm_flag', 's_ivd_id', 'e_ivd_id', 'work_kinds', 'project_label', 'over_count', 'limit_time'));

		$this->set('title_for_layout', '勤怠表');
	}

	/**
	 * [a_index 一覧]
	 * @param  [type] $year     [年]
	 * @param  [type] $month    [月]
	 * @param  [type] $staff_id [スタッフID]
	 * @return [type]           [None]
	 */
	function a_index($year, $month, $staff_id)
	{
		$auth = $this->_checkStaffAuthority();
		if (0 == $auth) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}

		// 前月、翌月を取得
		$this->Common->getBeforeMonth($year, $month, $b_year, $b_month);
		$this->Common->getNextMonth($year, $month, $n_year, $n_month);

		// 検索条件作成
		$con = array("Kintai.year"=>$year,
					 "Kintai.month"=>$month,
					 "Kintai.staff_id"=>$staff_id);

		$datas = $this->Kintai->find('first', array('conditions'=>$con));
		$id = $datas['Kintai']['id'];

		$type = CONFIRM_TYPE_0;

		// 勤怠時間テーブル取得
		$s_times = $this->KintaiTime->find('list', array('conditions'=>array('start_enable'=>1), 'fields'=>array('time')));
		$e_times = $this->KintaiTime->find('list', array('conditions'=>array('end_enable'=>1), 'fields'=>array('time')));

		$con = array("Staff.id"=>$staff_id, "Staff.enable"=>1);
		$staff = $this->Staff->find('first', array('conditions'=>$con));

		// プロジェクトのラベル取得
		$project_label = $this->Common->getProjectLabel("$year-$month-01");

		// 年間有給消化数を取得する
		$paid_digest = $this->_getPaidHolidayDigest ($staff_id, $year, $month);

		// 業務形態を取得する
		$work_kinds = Configure::read ('work_kind');

		// 当月の日数・月初・月末を取得
		$day_num =  date("t", mktime(0, 0, 0, $month, 1, $year));

		// 有休残日数を取得
		$paid_remain = $this->_getPaidHolidayRemain ($staff_id);

		// 半休残数を取得
		$half_remain = $this->_getHalfHolidayRemain ($staff_id);

		// 振休残日数を取得
		$trans_remain = $this->_getTrHolidayRemain ($staff_id);

		// 特休残日数を取得
		$special_remain = $this->_getSpHolidayRemain ($staff_id, $year, $month);

		// 特別休暇残日数を取得する
		$special_remain = $this->_getSpHolidayRemain ($staff_id, $year, $month);

		// 年間超勤時間超過回数を取得する
		$over_count = $this->_getExtraTimeOverCount($staff_id, $year, $month);

		$limit_time = $this->_getExtraTimeLimit($staff_id, $year, $month);

	   $this->set(compact('year', 'month', 'b_year', 'b_month', 'n_year', 'n_month', 'datas', 'type', 'staff_id',
						   'id', 'day_num', 's_times', 'e_times', 'staff', 'project_label', 'paid_digest', 'work_kinds',
						   'paid_remain', 'half_remain', 'trans_remain', 'special_remain', 'over_count', 'limit_time'));

		$this->set('title_for_layout', '勤怠表');
	}

	/**
	 * [a_browse 勤怠閲覧 一覧画面]
	 * @param  [type] $year      [年]
	 * @param  [type] $month     [月]
	 * @param  [type] $tgt_staff [閲覧対象社員ID]
	 * @return [type]            [None]
	 */
	function a_browse ($year = null, $month = null, $tgt_staff = null)
	{
		$auth = $this->_checkStaffAuthority();
		if (0 == $auth) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}

		// 自身のスタッフ情報を取得
		$staff_id = $this->Session->read('my_staff_id');
		$con = array("Staff.id" => $staff_id);
		$own = $this->Staff->find('first', array('conditions'=>$con));

		// 項目名=>array(値,検索条件,モデル名)
		$keys = array('request_staff_id'=>array('', 'staff_id', ''),
					  'target_date'=>array('', 'term', ''),
					 );

//        $year = "----";
//        $month = "--";
		$set_flag = 0;

		// 勤怠検索条件設定
		$con_kintai = array();
		if (!empty($this->params->query)) {
			foreach ($keys as $key => $value) {
				if ($this->params->query[$key] != "") {
					$keys[$key][0] = $this->params->query[$key];

					if ($value[2]) {
						$model = $value[2];
					}
					switch ($keys[$key][1]) {
						case 'staff_id':
							array_push($con_kintai, array("Kintai.staff_id" => $keys[$key][0]));
							$set_flag ++;
							break;
						case 'term':
							$year = substr($keys[$key][0], 0, 4);
							array_push($con_kintai, array("Kintai.year" => $year));
							$month = substr($keys[$key][0], 5, 2);
							array_push($con_kintai, array("Kintai.month" => $month));
							$set_flag ++;
							break;
						default:
							array_push($con_kintai, array("Kintai.$key" => $keys[$key][0]));
							break;
					}
				}
			}
		}

		// [従業員]が未設定で、引数設定がある場合は設定
		if ($keys['request_staff_id'][0] == "" && $tgt_staff != null) {
			$keys['request_staff_id'][0] = $tgt_staff;
			array_push($con_kintai, array("Kintai.staff_id" => $keys['request_staff_id'][0]));
			$set_flag ++;
		}

		// [対象年月]が未設定の場合、当月をデフォルトとして設定
		if ($keys['target_date'][0] == "") {
			if ($year == null || $month == null) {
				$year = date('Y');
				$month = date('n');
			}
			$keys['target_date'][0] = sprintf("%04d-%02d-01", $year, $month);
			array_push($con_kintai, array("Kintai.year" => $year));
			array_push($con_kintai, array("Kintai.month" => $month));
			$set_flag ++;
		}
		//前月、翌月を取得
		$this->Common->getBeforeMonth($year, $month, $b_year, $b_month);
		$this->Common->getNextMonth($year, $month, $n_year, $n_month);

		// 閲覧対象勤怠取得
		$id = 0;
		$confirm = null;
		if ($set_flag == 2) {
			$kintai = $this->Kintai->find('first', array('conditions'=>$con_kintai));
			if ($kintai) {
				$id = $kintai['Kintai']['id'];

				// 承認状況を取得
				$confirm = $this->Confirm->find('first', array('conditions'=>array('type'=>CONFIRM_TYPE_0, 'type_id'=>$id)));
			}
		}
		else {
			$kintai = null;
		}

		// 閲覧対象社員の一覧取得 : 役職権限が自身以下を対象
		// $authority_id = $own['Staff']['authority_id'];
		// if ($own['Staff']['authority_id'] == 5 || $own['Staff']['authority_id'] == 6) {
		// 	// 営業職 or 勤怠管理者はリーダーと同レベルの権限とする
		// 	$authority_id = 2;
		// }
		// $con_staffs = array("Staff.authority_id >= " => $authority_id,
		// 					"Staff.id != " => $staff_id,
		// 					"Staff.no != " => 9999,
		// 					'or'=>array(array('Staff.retire_date'=>null), array('Staff.retire_date !='=>null, 'Staff.retire_date >='=>"$year-$month-1")),
		// 					);
		// $staffs = $this->Staff->find('list', array('conditions'=>$con_staffs));
		$rank = $own['Authority']['rank'];
		$joins = array(
			array(
				"type" => "LEFT",
				"table" => "authorities",
				"alias" => "Authority",
				"conditions" => array("Staff.authority_id = Authority.id"),
			),
		);
		$cons = array(
					"Authority.rank >= " => $rank,
					"Authority.rank != " => 1,
					"Staff.id != " => $staff_id,
					"Staff.no != " => 9999,
					'or'=>array(array('Staff.retire_date'=>null), array('Staff.retire_date !='=>null, 'Staff.retire_date >='=>"$year-$month-1")),
							);

		$this->Staff->unbindModelAll();
		$staffs = $this->Staff->find('list', array('conditions'=>$cons, 'joins'=> $joins));

		// 有休残日数を取得
		$paid_remain = $this->_getPaidHolidayRemain ($tgt_staff);

		// 半休残数を取得
		$half_remain = $this->_getHalfHolidayRemain ($tgt_staff);

		// 振休残日数を取得
		$trans_remain = $this->_getTrHolidayRemain ($tgt_staff);

		// 特休残日数を取得
		$special_remain = $this->_getSpHolidayRemain ($tgt_staff, $year, $month);

		// 年間有給消化数を取得する
		$paid_digest = $this->_getPaidHolidayDigest ($tgt_staff, $year, $month);

		// 業務形態を取得する
		$work_kinds = Configure::read ('work_kind');

		// 勤怠時間テーブル取得
		$s_times = $this->KintaiTime->find('list', array('conditions'=>array('start_enable'=>1), 'fields'=>array('time')));
		$e_times = $this->KintaiTime->find('list', array('conditions'=>array('end_enable'=>1), 'fields'=>array('time')));

		// プロジェクトのラベル取得
		$project_label = $this->Common->getProjectLabel("$year-$month-01");


		// 年間超勤時間超過回数を取得する
		$over_count = $this->_getExtraTimeOverCount($tgt_staff, $year, $month);

		$limit_time = $this->_getExtraTimeLimit($tgt_staff, $year, $month);

		//$this->log($kintai);
		$this->set(compact('kintai', 'id', 'staffs', 'keys', 'year', 'month', 'b_year', 'b_month', 'n_year', 'n_month', 's_times', 'e_times',
			'confirm', 'paid_remain', 'half_remain', 'trans_remain', 'special_remain', 'paid_digest', 'work_kinds', 'project_label',
			'over_count', 'limit_time'));

		$this->set('title_for_layout', '勤怠閲覧');
	}

	/**
	 * [a_aggregation 勤怠集計画面]
	 * @param  [type] $from_ym      [開始年月]
	 * @param  [type] $to_ym        [終了年月]
	 * @param  [type] $tgt_staff    [集計対象社員ID]
	 * @param  [type] $other_detail [その他詳細出力]
	 * @return [type]            [None]
	 */
	function a_aggregation ($from_ym = null, $to_ym = null, $tgt_staff = null, $other_detail = null)
	{
		$auth = $this->_checkStaffAuthority();
		if ($auth == 0) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}

		// 年月リストを作成、期間は当年の前後５年
		$year = date ('Y');
		$month = date ('n');
		$terms = array ();
		for ($i = $year - 10; $i < $year + 2; $i++) {
			for ($j = 1; $j <= 12; $j++) {
				array_push ($terms, sprintf("%04d-%02d", $i, $j));
			}
		}
		$from_id = null;
		$to_id = null;
		$tgt_staff = null;

		// 項目名 => array(値,検索条件,モデル名)
		$keys = array ('term_s' => array('', 'term_s', ''),
					   'term_e' => array('', 'term_e', ''),
					   'staff' => array('', 'staff', ''),
					   );

		// 検索条件作成
		if (!empty ($this->params->query)) {
			foreach ($keys as $key => $value) {
				if ($this->params->query["sc_" . $key] != ""){
					$keys[$key][0] = $this->params->query["sc_" . $key];

					// 特殊な検索はswitch文に追加する
					switch ($keys[$key][1]) {
						case 'term_s':
							$from_id = $keys[$key][0];
							break;
						case 'term_e':
							$to_id = $keys[$key][0];
							break;
						default:
							break;
					}
				}
			}

			// その他の詳細出力についてもここで取得
			if ($this->params->query["other_detail"] != "") {
				$other_detail = $this->params->query["other_detail"];
			}
		}

		// 勤怠検索条件設定
		$con_kintais = array ();
		$tmp_kintais = array ();
		$start_y = null;
		$start_m = null;
		$end_y = null;
		$end_m = null;
		if ($keys['term_s'][0] != "" && $keys['term_e'][0] != null && $keys['staff'][0] != 0) {
			$start_y = substr($terms[$keys['term_s'][0]], 0, 4);
			$start_m = substr($terms[$keys['term_s'][0]], 5, 2);
			$end_y = substr($terms[$keys['term_e'][0]], 0, 4);
			$end_m = substr($terms[$keys['term_e'][0]], 5, 2);
			$tgt_staff = $keys['staff'][0];

			if ($keys['staff'][0] == ALL_STAFF) {
				$con = array ('Kintai.year >=' => $start_y, 'Kintai.year <=' => $end_y);
			}
			else {
				$con = array ('Kintai.year >=' => $start_y, 'Kintai.year <=' => $end_y, 'Kintai.staff_id' => $keys['staff'][0]);
			}
			$tmp_kintais = $this->Kintai->find ('all', array('conditions' => $con));
		}

		// 勤怠テーブルの検索条件数分ループ
		$tgt_datas = array();
		foreach ($tmp_kintais as $kintai) {
			// 未入力の勤怠は除外
			if (empty ($kintai['KintaiDetail'])) {
				continue;
			}
			if (count ($kintai['KintaiDetail']) == 0) {
				continue;
			}

			// 期間外の勤怠は除外
			if (($kintai['Kintai']['year'] == $start_y && $kintai['Kintai']['month'] < $start_m) ||
				($kintai['Kintai']['year'] == $end_y && $kintai['Kintai']['month'] > $end_m)) {
				continue;
			}

			// プロジェクト単位で実働時間を合計
			$work_times = array('0' => 0, '1' => 0, '2' => 0, '3' => 0, '4' => 0, '5' => 0, '6' => 0, '7' => 0, '8' => 0);
			$remarks_list = array();
			foreach ($kintai['KintaiDetail'] as $detail) {
				$work_times[0] += $detail['project1_time'];
				$work_times[1] += $detail['project2_time'];
				$work_times[2] += $detail['project3_time'];
				$work_times[3] += $detail['project4_time'];
				$work_times[4] += $detail['project5_time'];
				$work_times[5] += $detail['project6_time'];
				$work_times[6] += $detail['group_time'];
				$work_times[7] += $detail['training_time'];

				// その他の一括表示の場合
				if ($other_detail != 1) {
					$work_times[8] += $detail['other_time'];
				}
				// 個別表示の場合、備考欄と合わせて保持する
				else {
					if ($detail['other_time'] != '' && $detail['other_time'] != 0) {
						array_push($work_times, $detail['other_time']);
						array_push($remarks_list, $detail['remarks']);
					}
				}
			}

			// 未実施のプロジェクトについては非出力
			$tgt_data = array ('year' => $kintai['Kintai']['year'],
							   'month' => $kintai['Kintai']['month'],
							   'staff_name'=>$kintai['Staff']['name'],
							   'project_name'=>"",
							   'project_kind'=>0,
							   'project_time'=>0,
							   );
			for ($i = 0; $i < count($work_times); $i++) {
				if ($work_times[$i] == 0) {
					continue;
				}
				if ($i >= 0 && $i <= 5) {
					$tgt_data['project_name'] = $kintai['Kintai'][sprintf("project%d_name", $i+1)];
					$tgt_data['project_kind'] = $kintai['Kintai'][sprintf("project%d_kind", $i+1)];
				}
				else if ($i == 6) {
					$y = $kintai['Kintai']['year'];
					$m = $kintai['Kintai']['month'];
					$tgt_data['project_name'] = $this->Common->getProjectLabel("$y-$m-01");
					$tgt_data['project_kind'] = WORK_KD_TRAINING;
				}
				else if ($i == 7) {
					$tgt_data['project_name'] = "研修";
					$tgt_data['project_kind'] = WORK_KD_TRAINING;
				}
				else {
					// その他の一括表示の場合
					if ($other_detail != 1) {
						$tgt_data['project_name'] = "その他";
						$tgt_data['project_kind'] = WORK_KD_OTHER;
					}
					// 個別表示の場合、備考欄と合わせて出力
					else {
						$tgt_data['project_name'] = sprintf("その他(%s)", $remarks_list[$i - 9]);
						$tgt_data['project_kind'] = WORK_KD_OTHER;
					}
				}
				$tgt_data['project_time'] = $work_times[$i];

				// 集計対象データを格納
				array_push ($tgt_datas, $tgt_data);
			}
		}

		// 対象社員の一覧取得
		$staffs[ALL_STAFF] = "＝全社員＝";
		$staffs += $this->Staff->find('list', array("conditions"=>array("Staff.no !="=>"9999")));

		$kinds = Configure::read ('work_kind');

		if (isset ($this->params->query['csv'])) {
			$this->_outputCsv ($tgt_datas, $terms[$keys['term_s'][0]], $terms[$keys['term_e'][0]], $staffs[$tgt_staff]);
			exit ();
		}

		$this->set(compact('keys', 'staffs', 'terms', 'kintai', 'from_id', 'to_id', 'tgt_staff', 'tgt_datas', 'kinds', 'other_detail'));

		$this->set('title_for_layout', '勤怠集計');
	}

	/**
	 * [s_detail_save 更新]
	 * @return [type]            [None]
	 */
	function s_detail_save()
	{
		$this->autoRender = false;

		$auth = $this->_checkStaffAuthority();
		// if (0 == $auth) {
		//     $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
		//     $this->redirect($this->referer());
		// }

		if ($this->request->is('post') || $this->request->is('put')) {
			$message = "更新しました";
			$alert_class = 'alert alert-info';

			if (empty($this->data)) {
				$message = "セッションエラーです";
				$alert_class = 'alert alert-danger';
			}
			else {
				// 始業・終業の未設定箇所をnullへ更新
				for ($i = 1; $i <= $this->request->data['Kintai']['day_num']; $i++) {
					if ($this->request->data['KintaiDetail'][$i]['start_time'] == $this->data['Kintai']['s_ivd_id']) {
						$this->request->data['KintaiDetail'][$i]['start_time'] = null;
					}
					if ($this->request->data['KintaiDetail'][$i]['end_time'] == $this->data['Kintai']['e_ivd_id']) {
						$this->request->data['KintaiDetail'][$i]['end_time'] = null;
					}
				}

				// 休暇のチェック・最適化
				if ($this->_checkHolidayOptimize($this->data['Kintai']['year'], $this->data['Kintai']['month'])) {
					$message = "取得可能な休暇日数をオーバーしています";
					$alert_class = 'alert alert-danger';
				}
				else {
					// 過去登録時の休暇情報を一旦初期化 (末尾から逆順に削除していく)
					for ($i = $this->request->data['Kintai']['day_num']; $i > 0; $i--) {
						$this->_initPrevEntryHoliday($this->data['Kintai']['year'],
													 $this->data['Kintai']['month'],
													 $this->data['KintaiDetail'][$i]['id'],
													 $this->data['Kintai']['staff_id']);
					}

					$workday = 0;
					$inputday = 0;
					$office = 0;

					// 今回の登録データを反映
					foreach ($this->data['KintaiDetail'] as $detail) {
						$today = date('Y-m-d', mktime(0, 0, 0, $detail['month'], $detail['date'], $detail['year']));
						$day_num =  date("t", mktime(0, 0, 0, $detail['month'], 1, $detail['year']));
						$from_date = sprintf("%04d-%02d-01", $detail['year'], $detail['month']);
						$to_date = sprintf("%04d-%02d-%02d", $detail['year'], $detail['month'], $day_num);

						$paid_holiday_num = 0.0;
						$trans_holiday_num = 0.0;
						$half_holiday_num = 0;
						$sp_holiday_num = 0;
						$remarks = "";

						switch ($detail['application']) {
							// 有給休暇、振替休日、特別休暇の場合、始業・終業時間の入力は不要
							case HOLIDAY_PAID_ALL:      // 有給休暇
								$paid_holiday_num -= 1.0;
								break;
							case HOLIDAY_MORNING_OFF:   // 午前半休
							case HOLIDAY_AFTERNOON_OFF: // 午後半休
								$paid_holiday_num -= 0.5;
								$half_holiday_num--;
								break;
							case HOLIDAY_OBSERVED_ALL:  // 振替休日
								$trans_holiday_num -= 1.0;
								break;
							case HOLIDAY_OBSERVED_HALF: // 振替半休
								$trans_holiday_num -= 0.5;
								break;
							case HOLIDAY_SPECIAL:       // 特別休暇
								$sp_holiday_num -= 1.0;
								break;
						}

						// 有休情報を更新
						if ($paid_holiday_num != 0.0 || $half_holiday_num != 0.0) {
							$con = array("PaidHoliday.staff_id"=>$this->data['Kintai']['staff_id']);
							$paid_holiday = $this->PaidHoliday->find('first', array('conditions'=>$con));
							$paid_holiday['PaidHoliday']['holiday_remain'] += $paid_holiday_num;
							$paid_holiday['PaidHoliday']['half_remain'] += $half_holiday_num;
							if (!$this->PaidHoliday->save($paid_holiday)) {
								$message = "データの更新に失敗しました";
								$alert_class = 'alert alert-danger';
							}

							$use_date = sprintf("%04d-%02d-%02d", $detail['year'], $detail['month'], $detail['date']);
							$kind = NULL;
							switch ($detail['application']) {
								case HOLIDAY_PAID_ALL:      // 有給休暇
									$kind = 1;
									break;
								case HOLIDAY_MORNING_OFF:   // 午前半休
									$kind = 2;
									break;
								case HOLIDAY_AFTERNOON_OFF: // 午後半休
									$kind = 3;
									break;
							}

							$set_year = $this->Common->getYear($detail['year'], $detail['month']);
							$paid_detail['PaidHolidayDetail'] = array('staff_id'=>$this->data['Kintai']['staff_id'],
																	  'use_date'=>$use_date,
																	  'kind'=>$kind,
																	  'year'=>$set_year,
																	  'enable'=>1);
							$this->PaidHolidayDetail->create();
							$this->PaidHolidayDetail->save($paid_detail);
						}

						// 振休情報を更新
						while ($trans_holiday_num != 0.0) {
/*
							$con = array("TransHoliday.staff_id"=>$this->data['Kintai']['staff_id'],
										 "TransHoliday.digestion_date2"=>"0000-00-00");
							$trans_holiday = $this->TransHoliday->find('first', array('conditions'=>$con));
							$digestion_date = sprintf("%04d-%02d-%02d", $detail['year'], $detail['month'], $detail['date']);

							// 消化日1が未設定の場合、消化日1に設定
							if ($trans_holiday['TransHoliday']['digestion_date1'] == "0000-00-00") {
								$trans_holiday['TransHoliday']['digestion_date1'] = $digestion_date;
								if ($remarks == "") {
								    $remarks = sprintf ("%s 分消化", $trans_holiday['TransHoliday']['accrual_date']);
								}
							}
							// 消化日2が未設定の場合、消化日2に設定
							else if ($trans_holiday['TransHoliday']['digestion_date2'] == "0000-00-00") {
								$trans_holiday['TransHoliday']['digestion_date2'] = $digestion_date;
								if ($remarks == "") {
								    $remarks = sprintf ("%s 分消化", $trans_holiday['TransHoliday']['accrual_date']);
								}
							}

							// DB更新
							if (!$this->TransHoliday->save($trans_holiday)) {
								$message = "データの更新に失敗しました";
								$alert_class = 'alert alert-danger';
							}

							$trans_holiday_num += 0.5;
*/
							// 当月中に発生した振休を優先して取得
							$con = array("TransHoliday.staff_id"=>$this->data['Kintai']['staff_id'],
										 "TransHoliday.digestion_date2"=>"0000-00-00",
										 "TransHoliday.accrual_date >= "=>$from_date,
										 "TransHoliday.accrual_date <= "=>$to_date);
							$pri_trans_holiday = $this->TransHoliday->find('first', array('conditions'=>$con));
							$digestion_date = sprintf("%04d-%02d-%02d", $detail['year'], $detail['month'], $detail['date']);
							if ($pri_trans_holiday != null) {
								// 消化日1が未設定の場合、消化日1に設定
								if ($pri_trans_holiday['TransHoliday']['digestion_date1'] == "0000-00-00") {
									$pri_trans_holiday['TransHoliday']['digestion_date1'] = $digestion_date;
									if ($remarks == "") {
									    $remarks = sprintf ("%s 分消化", $pri_trans_holiday['TransHoliday']['accrual_date']);
									}
								}
								// 消化日2が未設定の場合、消化日2に設定
								else if ($pri_trans_holiday['TransHoliday']['digestion_date2'] == "0000-00-00") {
									$pri_trans_holiday['TransHoliday']['digestion_date2'] = $digestion_date;
									if ($remarks == "") {
									    $remarks = sprintf ("%s 分消化", $pri_trans_holiday['TransHoliday']['accrual_date']);
									}
								}

								// DB更新
								if (!$this->TransHoliday->save($pri_trans_holiday)) {
									$message = "データの更新に失敗しました";
									$alert_class = 'alert alert-danger';
								}

								$trans_holiday_num += 0.5;
							}
							// 当月中の振休がなかった場合
							else {
								$con = array("TransHoliday.staff_id"=>$this->data['Kintai']['staff_id'],
											 "TransHoliday.digestion_date2"=>"0000-00-00");
								$trans_holiday = $this->TransHoliday->find('first', array('conditions'=>$con));
								$digestion_date = sprintf("%04d-%02d-%02d", $detail['year'], $detail['month'], $detail['date']);

								// 消化日1が未設定の場合、消化日1に設定
								if ($trans_holiday['TransHoliday']['digestion_date1'] == "0000-00-00") {
									$trans_holiday['TransHoliday']['digestion_date1'] = $digestion_date;
									if ($remarks == "") {
										$remarks = sprintf ("%s 分消化", $trans_holiday['TransHoliday']['accrual_date']);
									}
								}
								// 消化日2が未設定の場合、消化日2に設定
								else if ($trans_holiday['TransHoliday']['digestion_date2'] == "0000-00-00") {
									$trans_holiday['TransHoliday']['digestion_date2'] = $digestion_date;
									if ($remarks == "") {
										$remarks = sprintf ("%s 分消化", $trans_holiday['TransHoliday']['accrual_date']);
									}
								}

								// DB更新
								if (!$this->TransHoliday->save($trans_holiday)) {
									$message = "データの更新に失敗しました";
									$alert_class = 'alert alert-danger';
								}

								$trans_holiday_num += 0.5;
							}
						}

						// 特別休暇を更新
						if ($sp_holiday_num != 0.0) {
							$con = array('SpecialHoliday.staff_id' => $this->data['Kintai']['staff_id'],
										 'SpecialHoliday.use_date' => null,
										 'SpecialHoliday.expiration' => 0,
										 'SpecialHoliday.start_date <=' => $today,
										 'SpecialHoliday.end_date >=' => $today);
							$order = array ('SpecialHoliday.end_date'=>'asc', 'SpecialHoliday.start_date'=>'asc');

							// 期限日 => 開始日 の順にソートをして最初に取得出来たレコードを使うように変更が必要
							$sp_holiday = $this->SpecialHoliday->find('first', array('conditions'=>$con, 'order'=>$order));
							if ($sp_holiday == null) {
								// [PEND] エラー
							}
							else {
								// 使用日を設定
								$sp_holiday['SpecialHoliday']['use_date'] = $today;

								$remarks = sprintf ("%s",$sp_holiday['SpecialHoliday']['detail']);

								// DB更新
								if (!$this->SpecialHoliday->save($sp_holiday)) {
									$message = "データの更新に失敗しました";
									$alert_class = 'alert alert-danger';
								}
							}
						}

						// 休日に勤務時間の変動があった場合、振替休日の調整を行う
						if ($detail['holiday'] == 1 && $detail['work_time'] >= 8.0) {
							$accrual_date = sprintf("%04d-%02d-%02d", $detail['year'], $detail['month'], $detail['date']);
							$trans_holiday['TransHoliday'] = array('staff_id'=>$this->data['Kintai']['staff_id'],
																   'accrual_date'=>$accrual_date,
																   'digestion_date1'=>"0000-00-00",
																   'digestion_date2'=>"0000-00-00",
																   'enable'=>1
																   );
							$this->TransHoliday->create();
							$this->TransHoliday->save($trans_holiday);
						}

						// プロジェクト時間を設定
						for ($i = 1; $i <= 6; $i++) {
							$detail[sprintf("project%d_time", $i)] = $detail['project'][$i]['_time'];
						}

						// 備考欄を更新 : 振休消化時の文字列が設定済みの場合、初期化する
						if (strstr ($detail['remarks'], " 分消化") != null) {
							$detail['remarks'] = "";
						}
						if ($remarks != "" && $detail['remarks'] == "") {
							$detail['remarks'] = $remarks;
						}

						// 平日
						if ($detail['holiday'] == 0) {
						    $workday = $workday + 1;

						    // [摘要]が未入力の場合、始業・終業時間の入力が必須
						    if (is_null($detail['application']) || $detail['application'] == '') {
						        if (!is_null($detail['start_time']) && !is_null($detail['end_time'])) {
						            $inputday = $inputday + 1;
						        }
						    }
						    else {
								switch ($detail['application']) {
									// 有給休暇、振替休日、特別休暇の場合、始業・終業時間の入力は不要
									case HOLIDAY_PAID_ALL:      // 有給休暇
									case HOLIDAY_OBSERVED_ALL:  // 振替休日
									case HOLIDAY_SPECIAL:       // 特別休暇
										if (is_null($detail['start_time']) && is_null($detail['end_time'])) {
											$inputday = $inputday + 1;
										}
										break;
									// 各種半休の場合、始業・終業時間の入力は必須
									case HOLIDAY_MORNING_OFF:   // 午前半休
									case HOLIDAY_AFTERNOON_OFF: // 午後半休
									case HOLIDAY_OBSERVED_HALF: // 振替半休
										if (!is_null($detail['start_time']) && !is_null($detail['end_time'])) {
											$inputday = $inputday + 1;
										}
										break;
								}
						    }

						    if ($detail['remote_work'] == 0) {
						        $office = $office + 1;
						    }
						}

						if (!$this->KintaiDetail->save($detail)) {
							$message = "データの更新に失敗しました";
							$alert_class = 'alert alert-danger';
						}
					}

					// 残業時間は振替休日の決定後にしか計算できないので、振替休日確定後のここで行う
					$over_time = 0;
					$extra_time = 0;
					foreach ($this->data['KintaiDetail'] as $detail) {
						// 残業時間を計算
						if (!is_null($detail['start_time'])) {
							$over_time += $this->_calcOverTime($detail['work_time'], $detail['application'], $detail['holiday']);
							$extra_time += $this->_calcExtraTime($this->data['Kintai']['staff_id'],
																$detail['year'], $detail['month'], $detail['date'],
																$detail['work_time'], $detail['application'], $detail['holiday']);
						}
					}

					// 交通費区分が未設定で、かつ全営業日の入力が完了してい場合、交通費区分を設定
					$kintai['Kintai'] = array('id'=>$this->data['Kintai']['id'],
						'year'=>$this->data['Kintai']['year'],
						'month'=>$this->data['Kintai']['month'],
						'staff_id'=>$this->data['Kintai']['staff_id'],
						'work_time'=>$this->data['Kintai']['work_time'],
						'late_time'=>$this->data['Kintai']['late_time'],
//						'over_time'=>$this->data['Kintai']['over_time'],
//						'extra_time'=>$this->data['Kintai']['extra_time'],
						'over_time'=>$over_time,
						'extra_time'=>$extra_time,
						'holiday'=>$this->data['Kintai']['holiday'],
						'day_num'=>$this->data['Kintai']['day_num'],
						's_ivd_id'=>$this->data['Kintai']['s_ivd_id'],
						'e_ivd_id'=>$this->data['Kintai']['e_ivd_id'],
						'transport'=>$this->data['Kintai']['transport'],
						'enable'=>1
					);

					if ($kintai['Kintai']['transport'] == null || $kintai['Kintai']['transport'] == 0 && $workday == $inputday) {
					    if ($office >= $workday / 2) {
					        $kintai['Kintai']['transport'] = TRANSPORT_KBN_COMMUTER;
					    }
					    else {
					        $kintai['Kintai']['transport'] = TRANSPORT_KBN_SMALL;
					    }
					}

					if (!$this->Kintai->save($kintai['Kintai'])) {
						$message = "データの更新に失敗しました";
						$alert_class = 'alert alert-danger';
					}
				}
			}
			$this->Session->setFlash($message, 'default', array('class'=> $alert_class));
			$this->redirect(array('action' => 'index', $this->data['Kintai']['year'], $this->data['Kintai']['month']));
		}
	}

	/**
	 * [s_ajax_save Ajax通信による勤怠詳細レコード単位の保存処理 (未完成、特休の処理追加も必要)]
	 * @return [type]            [None]
	 */
	public function s_ajax_save()
	{
		$this->autoRender = false;

		// ajax通信以外の場合
		if (!$this->request->is('ajax')) {
			throw new BadRequestException();
		}

		// 要求データを取得
		$detail = $this->request['data'];
//$this->log($detail);
		$today = date('Y-m-d', mktime(0, 0, 0, $detail['month'], $detail['date'], $detail['year']));

		// 値が入っているかを確認
		$status = !empty($detail);
		if (!$status) {
			$this->Session->setFlash("データがありません。", 'default', array('class'=> 'alert alert-info'));

			// ajax終了時にエラー判定を行いたい場合
			$error = array(
				'message' => 'データがありません',
				'code' => 404
			);

// 例外で落とす
//            throw new NotFoundException();
		}
		else {
			$message = "更新しました";

			// 始業・終業の未設定箇所をnullへ更新
			$s_ivd_id = $this->_getUnsetBusinessTimeID(0);
			$e_ivd_id = $this->_getUnsetBusinessTimeID(1);
			if ($detail['start_time'] == $s_ivd_id) {
				$detail['start_time'] = null;
			}
			if ($detail['end_time'] == $e_ivd_id) {
				$detail['end_time'] = null;
			}

			// 更新前のデータを取得
			$bef_data = $this->KintaiDetail->find('first', array('conditions'=>array("KintaiDetail.id"=>$detail['id'])));

			// 適用に変化がある場合、有給休暇・振替休日の調整を行う
			if ($bef_data['KintaiDetail']['application'] != $detail['application']) {
				$paid_holiday_num = 0.0;
				$trans_holiday_num = 0.0;
				$half_holiday_num = 0;
				$sp_holiday_num = 0;

				// 更新前の分の休日数を増やす
				switch ($bef_data['KintaiDetail']['application']) {
					case HOLIDAY_PAID_ALL:      // 有給休暇
						$paid_holiday_num += 1.0;
						break;
					case HOLIDAY_MORNING_OFF:   // 午前半休
						$paid_holiday_num += 0.5;
						$half_holiday_num++;
						break;
					case HOLIDAY_AFTERNOON_OFF: // 午後半休
						$paid_holiday_num += 0.5;
						$half_holiday_num++;
						break;
					case HOLIDAY_OBSERVED_ALL:  // 振替休日
						$trans_holiday_num += 1.0;
						break;
					case HOLIDAY_OBSERVED_HALF: // 振替半休
						$trans_holiday_num += 0.5;
						break;
					case HOLIDAY_SPECIAL:       // 特別休暇
						$sp_holiday_num += 1.0;
						break;
				}

				// 更新後の分の休日数を減らす
				switch ($detail['application']) {
					case HOLIDAY_PAID_ALL:      // 有給休暇
						$paid_holiday_num -= 1.0;
						break;
					case HOLIDAY_MORNING_OFF:   // 午前半休
						$paid_holiday_num -= 0.5;
						$half_holiday_num--;
						break;
					case HOLIDAY_AFTERNOON_OFF: // 午後半休
						$paid_holiday_num -= 0.5;
						$half_holiday_num--;
						break;
					case HOLIDAY_OBSERVED_ALL:  // 振替休日
						$trans_holiday_num -= 1.0;
						break;
					case HOLIDAY_OBSERVED_HALF: // 振替半休
						$trans_holiday_num -= 0.5;
						break;
					case HOLIDAY_SPECIAL:       // 特別休暇
						$sp_holiday_num -= 1.0;
						break;
				}

				// 有休情報を更新
				if ($paid_holiday_num != 0.0 || $half_holiday_num != 0.0) {
					$con = array("PaidHoliday.staff_id"=>$detail['staff_id']);
					$paid_holiday = $this->PaidHoliday->find('first', array('conditions'=>$con));
					$paid_holiday['PaidHoliday']['holiday_remain'] += $paid_holiday_num;
					$paid_holiday['PaidHoliday']['half_remain'] += $half_holiday_num;
					if (!$this->PaidHoliday->save($paid_holiday)) {
						$message = "データの更新に失敗しました";
					}

					// 有休取得時
					if ($paid_holiday_num < 0.0) {
						$use_date = sprintf("%04d-%02d-%02d", $detail['year'], $detail['month'], $detail['date']);
						$kind = NULL;
						switch ($detail['application']) {
							case HOLIDAY_PAID_ALL:      // 有給休暇
								$kind = 1;
								break;
							case HOLIDAY_MORNING_OFF:   // 午前半休
								$kind = 2;
								break;
							case HOLIDAY_AFTERNOON_OFF: // 午後半休
								$kind = 3;
								break;
						}
						$set_year = $this->Common->getYear($detail['year'], $detail['month']);
						$paid_detail['PaidHolidayDetail'] = array('staff_id'=>$detail['staff_id'],
																  'use_date'=>$use_date,
																  'kind'=>$kind,
																  'year'=>$set_year,
																  'enable'=>1);
						$this->PaidHolidayDetail->create();
						$this->PaidHolidayDetail->save($paid_detail);
					}
					// 有休修正時
					else if ($paid_holiday_num > 0.0) {
						$use_date = sprintf("%04d-%02d-%02d", $detail['year'], $detail['month'], $detail['date']);
						$con = array("PaidHolidayDetail.staff_id"=>$detail['staff_id'],
									 "PaidHolidayDetail.use_date"=>$use_date);
						$paid_detail = $this->PaidHolidayDetail->find('first', array('conditions'=>$con));

						$this->PaidHolidayDetail->delete($paid_detail['PaidHolidayDetail']['id']);
					}
				}

				// 振休情報を更新
				while ($trans_holiday_num != 0.0) {
					// 振休取得時
					if ($trans_holiday_num < 0.0) {
						$con = array("TransHoliday.staff_id"=>$detail['staff_id'],
									 "TransHoliday.digestion_date2"=>"0000-00-00");
						$trans_holiday = $this->TransHoliday->find('first', array('conditions'=>$con));
						$digestion_date = sprintf("%04d-%02d-%02d", $detail['year'], $detail['month'], $detail['date']);

						// 消化日1が未設定の場合、消化日1に設定
						if ($trans_holiday['TransHoliday']['digestion_date1'] == "0000-00-00") {
							$trans_holiday['TransHoliday']['digestion_date1'] = $digestion_date;
						}
						// 消化日2が未設定の場合、消化日2に設定
						else if ($trans_holiday['TransHoliday']['digestion_date2'] == "0000-00-00") {
							$trans_holiday['TransHoliday']['digestion_date2'] = $digestion_date;
						}

						// DB更新
						if (!$this->TransHoliday->save($trans_holiday)) {
							$message = "データの更新に失敗しました";
						}

						$trans_holiday_num += 0.5;
					}
					// 振休修正時
					else if ($trans_holiday_num > 0.0) {
						$con = array("TransHoliday.staff_id"=>$detail['staff_id'],
									 "TransHoliday.digestion_date1 !="=>"0000-00-00");
						$order = array("TransHoliday.accrual_date DESC");
						$trans_holiday = $this->TransHoliday->find('first', array('conditions'=>$con, 'order'=>$order));

						// 消化日2が設定済の場合、消化日2を初期化
						if ($trans_holiday['TransHoliday']['digestion_date2'] != "0000-00-00") {
							$trans_holiday['TransHoliday']['digestion_date2'] = "0000-00-00";
						}
						// 消化日2が未設定の場合、消化日1を初期化
						else {
							$trans_holiday['TransHoliday']['digestion_date1'] = "0000-00-00";
						}

						// DB更新
						if (!$this->TransHoliday->save($trans_holiday)) {
							$message = "データの更新に失敗しました";
						}

						$trans_holiday_num -= 0.5;
					}
				}

				// 特別休暇を更新
				if ($sp_holiday_num != 0.0) {
					// 特休取得時
					if ($sp_holiday_num < 0.0) {
						$con = array('SpecialHoliday.staff_id' => $detail['staff_id'],
									 'SpecialHoliday.use_date' => null,
									 'SpecialHoliday.expiration' => 0,
									 'SpecialHoliday.start_date <=' => $today,
									 'SpecialHoliday.end_date >=' => $today);

						// [PEND] 期限日 => 開始日 の順にソートをして最初に取得出来たレコードを使うように変更が必要
						$sp_holiday = $this->SpecialHoliday->find('first', array('conditions'=>$con));
						if ($sp_holiday == null) {
							// [PEND] エラー
						}
						else {
							// 使用日を設定
							$sp_holiday['SpecialHoliday']['use_date'] = $today;

							// DB更新
							if (!$this->SpecialHoliday->save($sp_holiday)) {
								$message = "データの更新に失敗しました";
							}
						}
					}
					// 特休修正時
					else if ($sp_holiday_num > 0.0) {
						$con = array('SpecialHoliday.staff_id' => $detail['staff_id'],
									 'SpecialHoliday.use_date' => $today);

						$sp_holiday = $this->SpecialHoliday->find('first', array('conditions'=>$con));
						if ($sp_holiday == null) {
							// [PEND] エラー
						}
						else {
							// 使用日を設定
							$sp_holiday['SpecialHoliday']['use_date'] = null;

							// DB更新
							if (!$this->SpecialHoliday->save($sp_holiday)) {
								$message = "データの更新に失敗しました";
							}
						}
					}
				}
			}

			// 休日に勤務時間の変動があった場合、振替休日の調整を行う
			if ($detail['holiday'] == 1 &&
				($bef_data['KintaiDetail']['work_time'] != $detail['work_time'])) {
				$trans_holiday_num = 0.0;

				// 更新前が振替休日対象であった場合
				if ($bef_data['KintaiDetail']['work_time'] >= 8.0) {
					$trans_holiday_num -= 1.0;
				}

				// 更新後が振替休日対象である場合
				if ($detail['work_time'] >= 8.0) {
					$trans_holiday_num += 1.0;
				}

				// 休日出勤時
				if ($trans_holiday_num > 0.0) {
					$accrual_date = sprintf("%04d-%02d-%02d", $detail['year'], $detail['month'], $detail['date']);
					$trans_holiday['TransHoliday'] = array('staff_id'=>$detail['staff_id'],
														   'accrual_date'=>$accrual_date,
														   'digestion_date1'=>"0000-00-00",
														   'digestion_date2'=>"0000-00-00",
														   'enable'=>1
														   );
					$this->TransHoliday->create();
					$this->TransHoliday->save($trans_holiday);
				}
				// 休日出勤キャンセル時
				else if ($trans_holiday_num < 0.0) {
					$accrual_date = sprintf("%04d-%02d-%02d", $detail['year'], $detail['month'], $detail['date']);
					$con = array("TransHoliday.staff_id"=>$detail['staff_id'],
								 "TransHoliday.accrual_date"=>$accrual_date);
					$trans_holiday = $this->TransHoliday->find('first', array('conditions'=>$con));

					$this->TransHoliday->delete($trans_holiday['TransHoliday']['id']);
				}
			}

			// プロジェクト時間を設定
			if (!$this->KintaiDetail->save($detail)) {
				$message = "データの更新に失敗しました";
			}
		}

		// JSON形式で返却。errorが定義されていない場合はstatusとresultの配列になる。
		return json_encode(compact('status', 'result', 'error'));
	}

	/**
	 * [s_edit_project プロジェクト名編集]
	 * @param  [type] $year      [年]
	 * @param  [type] $month     [月]
	 * @param  [type] $id        [勤怠テーブルID]
	 * @return [type]            [None]
	 */
	function s_edit_project($year, $month, $id)
	{
		$auth = $this->_checkStaffAuthority();
		if (0 == $auth) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			exit();
		}

		$this->layout = "ajax";
		$this->Kintai->unbindModelAll();
		$datas = $this->Kintai->find('first', array("conditions" => array("Kintai.id"=>$id)));
		if (!empty($datas)) {
			$this->data = $datas;
		}

		$staff_id = $this->Session->read('my_staff_id');

		// 業務形態の無効は空欄にしておく
		$kinds = Configure::read ('work_kind');
		$kinds[0] = "";

		$this->set(compact('year', 'month', 'id', 'datas', 'staff_id', 'kinds'));

		$this->set('title_for_layout', 'プロジェクト名登録・編集');
	}

	/**
	 * [s_update_project プロジェクト名更新]
	 * @return [type] [None]
	 */
	function s_update_project()
	{
		$auth = $this->_checkStaffAuthority();
		if (0 == $auth) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			exit();
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			$message = "更新しました";
			$alert_class = 'alert alert-info';

			if (empty($this->data)) {
				$message = "セッションエラーです";
				$alert_class = 'alert alert-danger';
			}
			else {
				// 見積番号の設定値をチェック
				$fault = 0;
				for ($i = 1; $i <= 6; $i++) {
					if ($this->request->data['Kintai'][sprintf("project%d_no", $i)] != NULL &&
						$this->request->data['Kintai'][sprintf("project%d_no", $i)] != "") {
						// 'Orders'テーブルから、'type'が見積で、7no'の番号が一致するレコードを検索
						$con = array("Order.type"=>0,
					 				 "Order.no"=>$this->request->data['Kintai'][sprintf("project%d_no", $i)]);
						$find_result = $this->Order->find('first', array('conditions'=>$con));
						if (empty($find_result)) {
							$fault = 1;
							break;
						}
					}
				}
				if ($fault == 1) {
					$message = "対応する見積番号が存在しません。見積番号を修正し、再登録して下さい。";
					$alert_class = 'alert alert-danger';
				}
				else if (!$this->Kintai->save($this->request->data)) {
					$message = "データの更新に失敗しました";
					$alert_class = 'alert alert-danger';
				}
				else {
					$this->_saveAutoItem(CONFIRM_TYPE_0, KINTAI_SUB_0, $this->request->data['Kintai']['project1_name']);
					$this->_saveAutoItem(CONFIRM_TYPE_0, KINTAI_SUB_0, $this->request->data['Kintai']['project2_name']);
					$this->_saveAutoItem(CONFIRM_TYPE_0, KINTAI_SUB_0, $this->request->data['Kintai']['project3_name']);
					$this->_saveAutoItem(CONFIRM_TYPE_0, KINTAI_SUB_0, $this->request->data['Kintai']['project4_name']);
					$this->_saveAutoItem(CONFIRM_TYPE_0, KINTAI_SUB_0, $this->request->data['Kintai']['project5_name']);
					$this->_saveAutoItem(CONFIRM_TYPE_0, KINTAI_SUB_0, $this->request->data['Kintai']['project6_name']);
					$this->_saveAutoItem(CONFIRM_TYPE_0, KINTAI_SUB_0, $this->request->data['Kintai']['project1_no']);
					$this->_saveAutoItem(CONFIRM_TYPE_0, KINTAI_SUB_0, $this->request->data['Kintai']['project2_no']);
					$this->_saveAutoItem(CONFIRM_TYPE_0, KINTAI_SUB_0, $this->request->data['Kintai']['project3_no']);
					$this->_saveAutoItem(CONFIRM_TYPE_0, KINTAI_SUB_0, $this->request->data['Kintai']['project4_no']);
					$this->_saveAutoItem(CONFIRM_TYPE_0, KINTAI_SUB_0, $this->request->data['Kintai']['project5_no']);
					$this->_saveAutoItem(CONFIRM_TYPE_0, KINTAI_SUB_0, $this->request->data['Kintai']['project6_no']);
				}
			}
			$this->Session->setFlash($message, 'default', array('class'=> $alert_class));
			$this->redirect(array('action' => 'index', $this->data['Kintai']['year'], $this->data['Kintai']['month']));
		}
	}

	/**
	 * [s_print 印刷]
	 * @param  [type] $id    [ID]
	 * @return [type]        [None]
	 */
	public function s_print($id)
	{
		$auth = $this->_checkStaffAuthority();
		if (0 == $auth) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}
		$staff_id = $this->Session->read('my_staff_id');

		$datas = $this->Kintai->find('first', array('conditions'=>array('Kintai.id'=>$id)));
		if ($datas['Kintai']['staff_id'] != $staff_id) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}

		$confirm = $this->Confirm->find('first', array('conditions'=>array('type'=>CONFIRM_TYPE_0, 'type_id'=>$id)));

		$staffs = $this->Staff->find('list');
		$s_times = $this->KintaiTime->find('list', array('conditions'=>array('start_enable'=>1), 'fields'=>array('time')));
		$e_times = $this->KintaiTime->find('list', array('conditions'=>array('end_enable'=>1), 'fields'=>array('time')));

		// プロジェクトのラベル取得
		$year = $datas['Kintai']['year'];
		$month = $datas['Kintai']['month'];
		$project_label = $this->Common->getProjectLabel("$year-$month-01");

		$staff = $this->Staff->find('first', array('condtions'=>array('staff_id' => $datas['Kintai']['staff_id'])));
		$this->set(compact('datas', 'confirm', 'staffs', 's_times', 'e_times', 'project_label'));
		$this->set('staff', $staff);
	}

	/**
	 * [a_print 印刷]
	 * @param  [type] $id    [ID]
	 * @return [type]        [None]
	 */
	public function a_print($id)
	{
		$auth = $this->_checkStaffAuthority();
		if (0 == $auth) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}

		$datas = $this->Kintai->find('first', array('conditions'=>array('Kintai.id'=>$id)));

		$confirm = $this->Confirm->find('first', array('conditions'=>array('type'=>CONFIRM_TYPE_0, 'type_id'=>$id)));

		$staffs = $this->Staff->find('list');
		$s_times = $this->KintaiTime->find('list', array('conditions'=>array('start_enable'=>1), 'fields'=>array('time')));
		$e_times = $this->KintaiTime->find('list', array('conditions'=>array('end_enable'=>1), 'fields'=>array('time')));

		// プロジェクトのラベル取得
		$year = $datas['Kintai']['year'];
		$month = $datas['Kintai']['month'];
		$project_label = $this->Common->getProjectLabel("$year-$month-01");

		$staff = $this->Staff->find('first', array('condtions'=>array('staff_id' => $datas['Kintai']['staff_id'])));
		$this->set(compact('datas', 'confirm', 'staffs', 's_times', 'e_times', 'project_label'));
		$this->set('staff', $staff);
	}

	/**
	 * [s_set_starttime 始業時間設定]
	 * @param  [type] $id    [勤怠ID]
	 * @return [type]        [None]
	 */
	public function s_set_starttime($id)
	{
		$auth = $this->_checkStaffAuthority();
		if (0 == $auth) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}

		// 勤怠IDと今日の日付に一致する勤怠詳細レコードを取得
		$date = date('j');
		$detail = $this->KintaiDetail->find('first', array('conditions'=>array("KintaiDetail.kintai_id"=>$id, "KintaiDetail.date"=>$date)));

		// 勤怠時間テーブル取得
//        $times = $this->KintaiTime->find('list', array('conditions'=>array('start_enable'=>1, 'holiday_only'=>0), 'fields'=>array('time')));
		$times = $this->KintaiTime->find('list', array('fields'=>array('time')));

		// 現時刻取得
		$hour = date('G');
		$min = date('i');
		$wk_time = $hour * 100 + $min;

		// 時間補正
		if ($detail['KintaiDetail']['holiday'] == 0) {
			// 平日
			if ($wk_time > 1000 && $wk_time <= 1400) {
				$hour = 14;
				$min  = 0;
			}
			else if ($wk_time > 600 && $wk_time <= 1000) {
				if ($min > 30) {
					$hour ++;
					$min  = 0;
				}
				else if ($min > 0) {
					$min  = 30;
				}
			}
			else {
				$hour = 99;
				$min  = 99;
			}
		}
		else {
			// 休日
			if ($wk_time > 1130 && $wk_time <= 1400) {
				$hour = 14;
				$min  = 0;
			}
			else if ($wk_time > 600 && $wk_time <= 1130) {
				if ($min > 30) {
					$hour ++;
					$min  = 0;
				}
				else {
					$min  = 30;
				}
			}
			else {
				$hour = 99;
				$min  = 99;
			}
		}

		$now_time = sprintf('%d:%02d', $hour, $min);

		// 出社時刻に対応するINDEXを取得
		$idx = 0;
		$loop = 1;
		foreach ($times as $time) {
			if ($time == $now_time) {
				$idx = $loop;
				break;
			}
			$loop++;
		}

		// 該当時刻が存在しなかった場合、エラー表示
		if ($idx == 0) {
			$message = "現時刻に該当する出社時間が存在しません";
			$this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
			$this->redirect(array('action' => 'index'));
		}
		else {
			// 出社時刻を設定し、データ更新
			$detail['KintaiDetail']['start_time'] = $idx;
			if (!$this->KintaiDetail->save($detail)) {
				$message = "データの更新に失敗しました";
			}
//            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
//            $this->redirect(array('action' => 'index', $this->data['Kintai']['year'], $this->data['Kintai']['month']));
			$this->redirect(array('action' => 'index'));
		}
	}

	/**
	 * [s_set_endtime 終業時間設定]
	 * @param  [type] $id    [勤怠ID]
	 * @return [type]        [None]
	 */
	public function s_set_endtime($id)
	{
		$auth = $this->_checkStaffAuthority();
		if (0 == $auth) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}

		// 勤怠IDと今日の日付に一致する勤怠詳細レコードを取得
		$date = date('j');
		$detail = $this->KintaiDetail->find('first', array('conditions'=>array("KintaiDetail.kintai_id"=>$id, "KintaiDetail.date"=>$date)));

		// 勤怠時間テーブル取得
		$times = $this->KintaiTime->find('list', array('fields'=>array('time')));

		// 現時刻
		$hour = date('G');
		$min = date('i');
		$wk_time = $hour * 100 + $min;

		// 時間補正
		if ($wk_time >= 1300 && $wk_time <= 1400) {
			$hour = 12;
			$min  = 0;
		}
		else if ($wk_time >= 1500 && $wk_time < 1800) {
			if ($min < 30) {
				$min = 0;
			}
			else {
				$min = 30;
			}
		}
		else if ($wk_time >= 1800 && $wk_time < 1845) {
			$hour = 18;
			$min  = 0;
		}
		else if ($wk_time >= 1845 && $wk_time < 2200) {
			if ($min >= 0 && $min < 15) {
				$hour --;
				$min  = 45;
			}
			else if ($min >= 45 && $min <= 59) {
				$min = 45;
			}
			else {
				$min = 15;
			}
		}
		else if ($wk_time >= 2200 && $wk_time < 2230) {
			$hour = 21;
			$min  = 45;
		}
		else if ($wk_time >= 2230 && $wk_time <= 2359) {
			if ($min < 30) {
				$min = 0;
			}
			else {
				$min = 30;
			}
		}
		else if ($wk_time >= 0 && $wk_time <= 600) {
			if ($min < 30) {
				$min = 0;
			}
			else {
				$min = 30;
			}
		}
		else {
			$hour = 99;
			$min  = 99;
		}
		$now_time = sprintf('%d:%02d', $hour, $min);

		// 出社時刻に対応するINDEXを取得
		$idx = 0;
		$loop = 1;
		foreach ($times as $time) {
			if ($time == $now_time) {
				$idx = $loop;
				break;
			}
			$loop++;
		}

		// 該当時刻が存在しなかった場合、エラー表示
		if ($idx == 0) {
			$message = "現時刻に該当する退社時間が存在しません";
			$this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
			$this->redirect(array('action' => 'index'));
		}
		else {
			// 出社時刻を設定
			$detail['KintaiDetail']['end_time'] = $idx;

			// データ更新
			if (!$this->KintaiDetail->save($detail)) {
				$message = "データの更新に失敗しました";
			}
//            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
			$this->redirect(array('action' => 'index'));
		}
	}

	/**
	 * [a_check_kintai 勤怠確認]
	 * @param  [type] $id    [勤怠ID]
	 * @return [type]        [None]
	 */
	public function a_check_kintai($id)
	{
		$auth = $this->_checkStaffAuthority();
		if (2 != $auth) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}

		$kintai = $this->Kintai->find('first', array('conditions'=>array("Kintai.id"=>$id)));
		if ($kintai) {
			if ($kintai['Kintai']['check_date']) {
				$kintai['Kintai']['check_date'] = '';
				$kintai['Kintai']['check_staff_id'] = '';
			} else {
				$kintai['Kintai']['check_date'] = date('Y-m-d H:i:s');
				$kintai['Kintai']['check_staff_id'] = $this->my_staff_id;
			}
			$kintai = $this->Kintai->save($kintai);
		}
		$this->redirect($this->referer());
	}

	/**
	 * [_checkHolidayOptimize 休暇のチェック・最適化]
	 * @param  [type] $year     [年]
	 * @param  [type] $month    [月]
	 * @return [type]           [None]
	 */
	function _checkHolidayOptimize($year, $month)
	{
		// 有給休暇残日数を取得
		$con = array('PaidHoliday.staff_id' => $this->request->data['Kintai']['staff_id']);
		$paid_holidays = $this->PaidHoliday->find('first', array('conditions'=>$con));
		$paid_remain = $paid_holidays['PaidHoliday']['holiday_remain'];
		$half_remain = $paid_holidays['PaidHoliday']['half_remain'];

		// 当月の日数・月初・月末を取得
		$day_num =  date("t", mktime(0, 0, 0, $month, 1, $year));
		$first_date = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
		$last_date = date('Y-m-d', mktime(0, 0, 0, $month + 1, 0, $year));

		// 振替休暇残日数を取得する
		$con = array('TransHoliday.staff_id' => $this->request->data['Kintai']['staff_id'],
					 'OR' => array('TransHoliday.digestion_date1' => '0000-00-00',
								   'TransHoliday.digestion_date2' => '0000-00-00'));
		$trans_holidays = $this->TransHoliday->find('all', array('conditions'=>$con));
		$trans_remain = 0.0;
		foreach ($trans_holidays as $rec) {
			if ($rec['TransHoliday']['digestion_date1'] == '0000-00-00') {
				$trans_remain += 0.5;
			}
			if ($rec['TransHoliday']['digestion_date2'] == '0000-00-00') {
				$trans_remain += 0.5;
			}
		}

		// 特別休暇残日数を取得する
		$con = array('SpecialHoliday.staff_id' => $this->request->data['Kintai']['staff_id'],
					 'SpecialHoliday.use_date' => null,
					 'SpecialHoliday.expiration' => 0,
					 'SpecialHoliday.end_date >' => $first_date);
		$special_remain = $this->SpecialHoliday->find('count', array('conditions'=>$con));
		$con = array('SpecialHoliday.staff_id' => $this->request->data['Kintai']['staff_id'],
					 'SpecialHoliday.expiration' => 0,
					 'SpecialHoliday.start_date <' => $last_date,
					 'SpecialHoliday.end_date >' => $first_date);
		$special_days = $this->SpecialHoliday->find('all', array('conditions'=>$con));
		$special_num = count($special_days);

		// 更新前に取得している休暇を一旦元に戻す
		foreach ($this->request->data['KintaiDetail'] as $detail) {

			// 更新前のデータを取得
			$bef_data = $this->KintaiDetail->find('first', array('conditions'=>array("KintaiDetail.id"=>$detail['id'])));

			$today = date('Y-m-d', mktime(0, 0, 0, $month, $bef_data['KintaiDetail']['date'], $year));

			switch ($bef_data['KintaiDetail']['application']) {
				case HOLIDAY_PAID_ALL:      // 有給休暇
					$paid_remain += 1;
					break;
				case HOLIDAY_OBSERVED_ALL:  // 振替休日
					$trans_remain += 1.0;
					break;
				case HOLIDAY_SPECIAL:       // 特別休暇
					// 使用済みの特別休暇を一旦キャンセル
					for ($i = 0; $i < $special_num; $i++) {
						if ($special_days[$i]['SpecialHoliday']['use_date'] == $today) {
							$special_days[$i]['SpecialHoliday']['use_date'] = null;
							$special_remain += 1;
							break;
						}
					}
					break;
				case HOLIDAY_MORNING_OFF:   // 午前半休
				case HOLIDAY_AFTERNOON_OFF: // 午後半休
					$paid_remain += 0.5;
					$half_remain += 1;
					break;
				case HOLIDAY_OBSERVED_HALF: // 振替半休
					$trans_remain += 0.5;
					break;
			}

			// 休日出勤日も一旦元に戻す
			if ($bef_data['KintaiDetail']['holiday'] == 1 && $bef_data['KintaiDetail']['work_time'] >= 8.0) {
				$trans_remain -= 1.0;
			}
		}

		// 更新後の休暇をチェック・最適化
		for ($i = 1; $i <= $day_num; $i++) {
			$today = date('Y-m-d', mktime(0, 0, 0,
						 $this->request->data['KintaiDetail'][$i]['month'],
						 $this->request->data['KintaiDetail'][$i]['date'],
						 $this->request->data['KintaiDetail'][$i]['year']));
			// 休日出勤を行った場合、振替休日を追加
			if ($this->request->data['KintaiDetail'][$i]['holiday'] == 1 &&
				$this->request->data['KintaiDetail'][$i]['work_time'] >= 8.0) {
				$trans_remain += 1.0;
			}

		    switch ($this->request->data['KintaiDetail'][$i]['application']) {
		        case HOLIDAY_SPECIAL:       // 特別休暇
		            //------------------------------------------------------------
		            // [全休] 特別休暇
		            //        １日分の特別休暇が残っていない場合、振替休暇/有給休暇をチェック
		            //------------------------------------------------------------
		            $sp_flag = 0;
		            for ($j = 0; $j < $special_num; $j++) {
		                if ($special_days[$j]['SpecialHoliday']['use_date'] == null &&
		                    $special_days[$j]['SpecialHoliday']['start_date'] <= $today &&
		                    $special_days[$j]['SpecialHoliday']['end_date'] >= $today) {
		                        $special_days[$j]['SpecialHoliday']['use_date'] = $today;
		                        $sp_flag = 1;
		                        break;
		                    }
		            }
		            if ($sp_flag == 1) {
		                $this->request->data['KintaiDetail'][$i]['application'] = HOLIDAY_SPECIAL;
		                break;
		            }
		            break;
		            // 特別休暇に日数が足りない場合、breakせずに有給休暇/振替休暇のチェック

		        case HOLIDAY_PAID_ALL:       // 有給休暇
		        case HOLIDAY_OBSERVED_ALL:   // 振替休暇
		            //------------------------------------------------------------
		            // [全休] 振替休暇 > 有給休暇 の優先順で消化
		            //        １日分の休暇が残っていない場合はエラー
		            //        ※「有休=0.5、振休=0.5」が残っている場合でも、エラーとする
		            //------------------------------------------------------------
		            if ($trans_remain >= 1.0) {
		                $trans_remain -= 1.0;
		                $this->request->data['KintaiDetail'][$i]['application'] = HOLIDAY_OBSERVED_ALL;
		            }
		            else if ($paid_remain >= 1) {
		                $paid_remain -= 1;
		                $this->request->data['KintaiDetail'][$i]['application'] = HOLIDAY_PAID_ALL;
		            }
		            else {
		                return 1;
		            }
		            break;

		        case HOLIDAY_MORNING_OFF:    // 午前休暇
		        case HOLIDAY_AFTERNOON_OFF:  // 午後休暇
		        case HOLIDAY_OBSERVED_HALF:  // 振替半休
		            //------------------------------------------------------------
		            // [半休] 振替休暇 > 有給休暇 の優先順で消化
		            //        半日分の休暇が残っていない場合はエラー
		            //------------------------------------------------------------
		            if ($trans_remain >= 0.5) {
		                $trans_remain -= 0.5;
		                $this->request->data['KintaiDetail'][$i]['application'] = HOLIDAY_OBSERVED_HALF;
		            }
		            else if ($paid_remain >= 0.5 && $half_remain >= 1) {
		                $paid_remain -= 0.5;
		                $half_remain -= 1;
		                if ($this->request->data['KintaiDetail'][$i]['application'] == HOLIDAY_OBSERVED_HALF) {
		                    // [PEND] メンドイので午前半休にする・・・
		                    $this->request->data['KintaiDetail'][$i]['application'] = HOLIDAY_MORNING_OFF;
		                }
		            }
		            else {
		                return 1;
		            }
		            break;

		        default:
		            break;
			}
		}

		return 0;
	}


	/**
	 * [_initPrevEntryHoliday 過去登録データの休暇情報初期化]
	 * @param  [type] $year                 [年]
	 * @param  [type] $month                [月]
	 * @param  [type] $kintai_detail_id     [初期化対象の`kinta_details`のID]
	 * @param  [type] $staff_id             [スタッフID]
	 * @return [type]                       [None]
	 */
	function _initPrevEntryHoliday($year, $month, $kintai_detail_id, $staff_id)
	{
		$ret_val = 0;

		// 過去登録データを取得
		$bef_data = $this->KintaiDetail->find('first', array('conditions'=>array("KintaiDetail.id"=>$kintai_detail_id)));
		if ($bef_data == null) {
			$this->log(sprintf("前回データが存在しません。id[%d]", $kintai_detail_id), LOG_DEBUG);
			$ret_val = 1;
			return $ret_val;
		}

		$today = date('Y-m-d', mktime(0, 0, 0, $month, $bef_data['KintaiDetail']['date'], $year));

		$paid_holiday_num = 0.0;
		$trans_holiday_num = 0.0;
		$half_holiday_num = 0;
		$sp_holiday_num = 0;

		switch ($bef_data['KintaiDetail']['application']) {
			case HOLIDAY_PAID_ALL:		// 有給休暇
				$paid_holiday_num += 1.0;
				break;
			case HOLIDAY_MORNING_OFF:	// 午前半休
				$paid_holiday_num += 0.5;
				$half_holiday_num++;
				break;
			case HOLIDAY_AFTERNOON_OFF:	// 午後半休
				$paid_holiday_num += 0.5;
				$half_holiday_num++;
				break;
			case HOLIDAY_OBSERVED_ALL:	// 振替休日
				$trans_holiday_num += 1.0;
				break;
			case HOLIDAY_OBSERVED_HALF:	// 振替半休
				$trans_holiday_num += 0.5;
				break;
			case HOLIDAY_SPECIAL:		// 特別休暇
				$sp_holiday_num += 1.0;
				break;
		}

		// 前回登録時の有休情報を削除
		if ($paid_holiday_num != 0.0 || $half_holiday_num != 0.0) {
			$con = array("PaidHoliday.staff_id"=>$staff_id);
			$paid_holiday = $this->PaidHoliday->find('first', array('conditions'=>$con));
			if ($paid_holiday == null) {
				$ret_val = 1;
			}
			else {
				$paid_holiday['PaidHoliday']['holiday_remain'] += $paid_holiday_num;
				$paid_holiday['PaidHoliday']['half_remain'] += $half_holiday_num;
				if (!$this->PaidHoliday->save($paid_holiday)) {
					$ret_val = 1;
				}
			}

			$use_date = sprintf("%04d-%02d-%02d", $year, $month, $bef_data['KintaiDetail']['date']);
			$con = array("PaidHolidayDetail.staff_id"=>$staff_id,
						 "PaidHolidayDetail.use_date"=>$use_date);
			$paid_detail = $this->PaidHolidayDetail->find('first', array('conditions'=>$con));
			$this->PaidHolidayDetail->delete($paid_detail['PaidHolidayDetail']['id']);
		}

		// 前回登録時の振休情報を削除
		while ($trans_holiday_num != 0.0) {
			/*
			$con = array("TransHoliday.staff_id"=>$staff_id,
						 "TransHoliday.digestion_date1 !="=>"0000-00-00");
			$order = array("TransHoliday.accrual_date DESC");
			$trans_holiday = $this->TransHoliday->find('first', array('conditions'=>$con, 'order'=>$order));
			if ($trans_holiday == null) {
				$ret_val = 1;
			}
			else {
				// 消化日2が設定済の場合、消化日2を初期化
				if ($trans_holiday['TransHoliday']['digestion_date2'] != "0000-00-00") {
					$trans_holiday['TransHoliday']['digestion_date2'] = "0000-00-00";
				}
				// 消化日2が未設定の場合、消化日1を初期化
				else {
					$trans_holiday['TransHoliday']['digestion_date1'] = "0000-00-00";
				}

				// DB更新
				if (!$this->TransHoliday->save($trans_holiday)) {
					$ret_val = 1;
				}
			}

			$trans_holiday_num -= 0.5;
			*/
			$con = array("TransHoliday.staff_id"=>$staff_id,
						 "OR"=>array("TransHoliday.digestion_date1"=>$today, "TransHoliday.digestion_date2"=>$today));
			$order = array("TransHoliday.accrual_date DESC");
			$trans_holiday = $this->TransHoliday->find('first', array('conditions'=>$con, 'order'=>$order));
			if ($trans_holiday == null) {
				$ret_val = 1;
			}
			else {
				// 消化日2に当日が設定されている場合、消化日2を初期化
				if ($trans_holiday['TransHoliday']['digestion_date2'] == $today) {
					$trans_holiday['TransHoliday']['digestion_date2'] = "0000-00-00";
				}
				// それ以外の場合、消化日1を初期化し、消化日2が設定されている場合は詰め替える
				else {
					if ($trans_holiday['TransHoliday']['digestion_date2'] != "0000-00-00") {
						$trans_holiday['TransHoliday']['digestion_date1'] = $trans_holiday['TransHoliday']['digestion_date2'];
						$trans_holiday['TransHoliday']['digestion_date2'] = "0000-00-00";
					}
					else {
						$trans_holiday['TransHoliday']['digestion_date1'] = "0000-00-00";
					}
				}

				// DB更新
				if (!$this->TransHoliday->save($trans_holiday)) {
					$ret_val = 1;
				}
			}

			$trans_holiday_num -= 0.5;
		}

		// 前回登録時の特休情報を削除
		if ($sp_holiday_num != 0.0) {
			$con = array('SpecialHoliday.staff_id' => $staff_id,
						 'SpecialHoliday.use_date' => $today);

			$sp_holiday = $this->SpecialHoliday->find('first', array('conditions'=>$con));
			if ($sp_holiday == null) {
				// [PEND] エラー
			}
			else {
				// 使用日を設定
				$sp_holiday['SpecialHoliday']['use_date'] = null;

				// DB更新
				if (!$this->SpecialHoliday->save($sp_holiday)) {
					$ret_val = 1;
				}
			}
		}

		// 休日に勤務時間の変動があった場合、振替休日の調整を行う
		if ($bef_data['KintaiDetail']['holiday'] == 1 &&
			$bef_data['KintaiDetail']['work_time'] >= 8.0) {
			$accrual_date = sprintf("%04d-%02d-%02d", $year, $month, $bef_data['KintaiDetail']['date']);
			$con = array("TransHoliday.staff_id"=>$this->data['Kintai']['staff_id'],
						 "TransHoliday.accrual_date"=>$accrual_date);
			$trans_holiday = $this->TransHoliday->find('first', array('conditions'=>$con));
			$this->TransHoliday->delete($trans_holiday['TransHoliday']['id']);
		}

		return $ret_val;
	}

	/**
	 * [_checkKintaiConfirm 勤怠未入力チェック]
	 * @param  [type] $id     [勤怠ID]
	 * @return [type] 0:未入力箇所なし、1:未入力箇所あり
	 */
	function _checkKintaiConfirm($id)
	{
		$ret_val = 0;

		$kintai = $this->Kintai->find('first', array('conditions'=>array("Kintai.id"=>$id)));
		foreach ($kintai['KintaiDetail'] as $detail) {
			$work_flag = 0;

			//--------------------------//
			//-----      平日      -----//
			//--------------------------//
			if ($detail['holiday'] == 0) {
				// [摘要]が未入力の場合、始業・終業時間の入力が必須
				if ($detail['application'] == null || $detail['application'] == '') {
					if (is_null($detail['start_time']) || is_null($detail['end_time'])) {
						$ret_val = 1;
						break;
					}
					$work_flag = 1;
				}
				else {
					switch ($detail['application']) {
						// 有給休暇、振替休日、特別休暇の場合、始業・終業時間の入力は不要
						case HOLIDAY_PAID_ALL:      // 有給休暇
						case HOLIDAY_OBSERVED_ALL:  // 振替休日
						case HOLIDAY_SPECIAL:       // 特別休暇
							if (!is_null($detail['start_time']) || !is_null($detail['end_time'])) {
								$ret_val = 1;
							}
							break;
						// 各種半休の場合、始業・終業時間の入力は必須
						case HOLIDAY_MORNING_OFF:   // 午前半休
						case HOLIDAY_AFTERNOON_OFF: // 午後半休
						case HOLIDAY_OBSERVED_HALF: // 振替半休
							if (is_null($detail['start_time']) || is_null($detail['end_time'])) {
								$ret_val = 1;
							}
							$work_flag = 1;
							break;
					}
				}
			}
			//--------------------------//
			//-----      休日      -----//
			//--------------------------//
			else {
				// [摘要]が選択されている場合、不正
				if ($detail['application'] != null && $detail['application'] != '') {
					$ret_val = 1;
					break;
				}
				else {
					// 休日 or 休日出勤の場合、始業・終業時間が統一されていること
					if ( (!is_null($detail['start_time']) && is_null($detail['end_time'])) ||
						 (is_null($detail['start_time']) && !is_null($detail['end_time'])) ) {
						$ret_val = 1;
					}
					// 休日出勤の場合、フラグ設定
					else if (!is_null($detail['start_time']) && !is_null($detail['end_time'])) {
						$work_flag = 1;
					}
				}
			}

			if ($ret_val == 1) {
				break;
			}
		}

		return $ret_val;
	}

	/**
	 * [_getUnsetBusinessTimeID 未設定時刻ID取得]
	 * @param  [type] $kind     [種別] 0=始業時刻, 1=終業時刻
	 * @return [type] 種別に対応する未設定時刻ID
	 */
	function _getUnsetBusinessTimeID($kind)
	{
		$ret_val = 0;
		$times = 0;

		if ($kind == 0) {
			$times = $this->KintaiTime->find('all', array('conditions'=>array('start_enable'=>1), 'order'=>array('hour'=>'asc')));
		}
		else {
			$times = $this->KintaiTime->find('all', array('conditions'=>array('end_enable'=>1), 'order'=>array('hour'=>'asc')));
		}
		foreach ($times as $rec) {
			if ($rec['KintaiTime']['time'] == "") {
				$ret_val = $rec['KintaiTime']['id'];
				break;
			}
		}

		return $ret_val;
	}

	/**
	 * [_outputCsv CSV出力]
	 * @param  [type] $targets [データ]
	 * @param  [type] $term_s [開始年月]
	 * @param  [type] $term_3 [開始年月]
	 * @param  [type] $s_name [社員名]
	 * @return [type]       [None]
	 */
	function _outputCsv ($targets, $term_s, $term_e, $s_name)
	{
		$keys = array ('date' => '年月',
					   'staff_name' => '社員氏名',
					   'project_name' => 'プロジェクト名',
					   'project_kind' => '勤務別コード',
					   'project_time' => '時間(h)',
					   );

		// header
		$datas = "";
		foreach ($keys as $key => $value) {
			$datas .= ($value . ",");
		}
		$datas .= "\r\n";

		// body
		foreach ($targets as $target){
			foreach ($keys as $key => $value) {
				$data = '';
				// データを加工して表示するものはcase文に追加する
				switch ($key) {
					case 'date':
						$data = $target['year']."/".$target['month'];
						break;
					//case 'project_kind':
					//	$items = Configure::read('work_kind');
					//	$data = $items[$target[$key]];
					//	break;
					// CSV形式なので、カンマは削除する
					default: $data = str_replace(",", "", trim ($target[$key])); break;
				}
				//$datas .= ("\"" . $data . "\"" . ',');
				$datas .= ( $data . ',');
			}
			$datas .= "\r\n";
		}

		Configure::write ('debug', 0);
		$req_file = sprintf ("%d年%d月-%d年%d月_勤務時間(%s).csv",
			substr($term_s, 0, 4),
			substr($term_s, 5, 2),
			substr($term_e, 0, 4),
			substr($term_e, 5, 2),
			$s_name);
//		$req_file = mb_convert_encoding($req_file, "SJIS", "UTF-8");
		header ("Content-disposition: attachment; filename=" . $req_file);
		header ("Content-type: application/octet-stream; name=" . $req_file);

		echo mb_convert_encoding ($datas, "SJIS-WIN", "UTF-8");
		//echo mb_convert_encoding ($datas, "UTF-8", "UTF-8");

		return;
	}


	/**
	 * [_getPdHolidayRemain 有給休暇残日数取得]
	 * @param  [type] $staff_id [取得対象社員ID]
	 * @return [type]       [None]
	 */
	function _getPaidHolidayRemain ($staff_id)
	{
		$result = 0;

		if ($staff_id != null) {
			// 対象社員の有給休暇レコードを取得
			$con = array ('PaidHoliday.staff_id' => $staff_id,
						  'PaidHoliday.enable' => 1);
			$rec = $this->PaidHoliday->find('first', array('conditions'=>$con));

			if ($rec != null) {
				$result = $rec['PaidHoliday']['holiday_remain'];
			}
		}

		return $result;
	}


	/**
	 * [_getHfHolidayRemain 半日休暇残数取得]
	 * @param  [type] $staff_id [取得対象社員ID]
	 * @return [type]       [None]
	 */
	function _getHalfHolidayRemain ($staff_id)
	{
		$result = 0;

		if ($staff_id != null) {
			// 対象社員の有給休暇レコードを取得
			$con = array ('PaidHoliday.staff_id' => $staff_id,
						  'PaidHoliday.enable' => 1);
			$rec = $this->PaidHoliday->find('first', array('conditions'=>$con));

			if ($rec != null) {
				$result = $rec['PaidHoliday']['half_remain'];
			}
		}

		return $result;
	}


	/**
	 * [_getTrHolidayRemain 振替休暇残日数取得]
	 * @param  [type] $staff_id [取得対象社員ID]
	 * @return [type]       [None]
	 */
	function _getTrHolidayRemain ($staff_id)
	{
		$result = 0;

		if ($staff_id != null) {
			// 対象社員の振替休暇レコードを取得
			$con = array ('TransHoliday.staff_id' => $staff_id,
						  'TransHoliday.enable' => 1);
			$tr_recs = $this->TransHoliday->find('all', array('conditions'=>$con));

			// 使用可能な振替休暇日数を加算
			foreach ($tr_recs as $rec) {
				if ($rec['TransHoliday']['digestion_date1'] == "0000-00-00") {
					$result += 0.5;
				}
				if ($rec['TransHoliday']['digestion_date2'] == "0000-00-00") {
					$result += 0.5;
				}
			}
		}

		return $result;
	}


	/**
	 * [_getSpHolidayRemain 特別休暇残日数取得]
	 * @param  [type] $staff_id [取得対象社員ID]
	 * @param  [type] $year     [取得対象年]
	 * @param  [type] $month    [取得対象月]
	 * @return [type]       [None]
	 */
	function _getSpHolidayRemain ($staff_id, $year = null, $month = null)
	{
		if ($year == null || $month == null) {
			$year = date('Y');
			$month = date('n');
		}

		$result = 0;

		if ($staff_id != null) {
			// 当月の月初・月末を取得
			$first_date = date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
			$last_date = date('Y-m-d', mktime(0, 0, 0, $month + 1, 0, $year));

			// 対象社員の期間内で使用可能な特別休暇日数を取得
			$con = array ('SpecialHoliday.staff_id' => $staff_id,
						  'SpecialHoliday.use_date' => null,
						  'SpecialHoliday.expiration' => 0,
						  'SpecialHoliday.start_date <=' => $last_date,
						  'SpecialHoliday.end_date >=' => $first_date);
			$result = $this->SpecialHoliday->find('count', array('conditions'=>$con));
		}

		return $result;
	}


	/**
	 * [_getPaidHolidayDigest 年間有休消化日数取得]
	 * @param  [type] $staff_id [取得対象社員ID]
	 * @param  [type] $year     [取得対象年]
	 * @param  [type] $month    [取得対象月]
	 * @return [type]       [None]
	 *
	 * 有給休暇の期間は4月～翌年3月までであり、
	 * 会社の年度(2月～翌年1月)とは異なる点に注意。
	 */
	function _getPaidHolidayDigest ($staff_id, $year = null, $month = null)
	{
		if ($year == null || $month == null) {
			$year = date('Y');
			$month = date('n');
		}

		$result = 0;

		if ($staff_id != null) {
			// 勤怠の年度期間(4月～翌年3月)に合わせる
			if ($month >= 1 && $month <= 3) {
				$year = $year - 1;
			}
			$first_date = date('Y-m-d', mktime(0, 0, 0, 4, 1, $year));
			$last_date = date('Y-m-d', mktime(0, 0, 0, 4, 0, $year+1));

			// 対象社員の期間内で使用した有給休暇を取得
			$con = array ('PaidHolidayDetail.staff_id' => $staff_id,
					      'PaidHolidayDetail.use_date >=' => $first_date,
					      'PaidHolidayDetail.use_date <=' => $last_date,
					      'PaidHolidayDetail.enable' => 1);
			$recs = $this->PaidHolidayDetail->find('all', array('conditions'=>$con));
			foreach ($recs as $rec) {
				if ($rec['PaidHolidayDetail']['kind'] == 1) {
					$result += 1;
				}
				else {
					$result += 0.5;
				}
			}
		}

		return $result;
	}

	/**
	 * [_calcOverTime 超勤時間計算]
	 * @param  [type] $work_time   [実働時間]
	 * @param  [type] $application [適用]
	 * @param  [type] $holiday     [休日判定]
	 *
	 * 実働時間と摘要欄の選択内容から超勤時間を計算する。
	 */
	function _calcOverTime ($work_time, $application, $holiday)
	{
		$over_time = 0.0;

		// 半休取得時
		if ($application == HOLIDAY_MORNING_OFF ||
			$application == HOLIDAY_AFTERNOON_OFF ||
			$application == HOLIDAY_OBSERVED_HALF) {
			$over_time = $work_time - 4.0;
		}
		// 休日時
		else if ($holiday == 1) {
			// 8.0h未満の実働時間は、休日出勤と見なさず実働時間をそのまま計上
			if ($work_time < 8.0) {
				$over_time = $work_time;
			}
			else {
				$over_time = $work_time - 8.0;
			}
		}
		// 通常営業日
		else {
			if ($work_time != 0.0) {
				$over_time = $work_time - 8.0;
			}
		}

		return $over_time;
	}

	/**
	 * [_calcExtraTime 36協定時間外労働時間計算]
	 * @param  [type] $staff_id    [社員ID]
	 * @param  [type] $year        [取得対象年]
	 * @param  [type] $month       [取得対象月]
	 * @param  [type] $date        [取得対象日]
	 * @param  [type] $work_time   [実働時間]
	 * @param  [type] $application [適用]
	 * @param  [type] $holiday     [休日判定]
	 *
	 * 実働時間と摘要欄の選択内容から超勤時間を計算する。
	 */
    function _calcExtraTime ($staff_id, $year, $month, $date, $work_time, $application, $holiday)
	{
		$extra_time = 0.0;

		// 法定休日は日曜日固定にする
		$day_of_week = date('w', strtotime(sprintf("%s-%s-%s", $year, $month, $date)));
		$legal_holiday = 0;
		if ($day_of_week == 0) {
			$legal_holiday = 1;
		}

		// 半休取得時
		if ($application == HOLIDAY_MORNING_OFF ||
			$application == HOLIDAY_AFTERNOON_OFF ||
			$application == HOLIDAY_OBSERVED_HALF) {
			$extra_time = $work_time - 4.0;
		}
		// 法定休日時
		else if ($legal_holiday == 1) {
			// 8.0h未満の実働時間は、休日出勤と見なさず残業として処理
			if ($work_time < 8.0) {
				$extra_time = $work_time;
			}
			// 休日出勤の場合、当月中に振休を取得したかをチェック
			else {
				$today = date('Y-m-d', mktime(0, 0, 0, $month, $date, $year));
				$day_num =  date("t", mktime(0, 0, 0, $month, 1, $year));
				$from_date = sprintf("%04d-%02d-01", $year, $month);
				$to_date = sprintf("%04d-%02d-%02d", $year, $month, $day_num);
				$con = array("TransHoliday.staff_id"=>$staff_id,
					"TransHoliday.accrual_date"=>$today,
					"OR"=>array(array("TransHoliday.digestion_date1 >="=>$from_date, "TransHoliday.digestion_date1 <="=>$to_date),
								array("TransHoliday.digestion_date2 >="=>$from_date, "TransHoliday.digestion_date2 <="=>$to_date)));
				// $con = array("TransHoliday.staff_id"=>$staff_id,
				// 			"TransHoliday.accrual_date"=>$today);
				$trans_holiday = $this->TransHoliday->find('first', array('conditions'=>$con));
				// 当月中に振休が取得できていない場合、実働時間をそのまま超勤時間に計上
				if (empty($trans_holiday)) {
					$extra_time = $work_time;
				}
				// 当月中に振休が取得できている場合、8.0h以上を超勤時間に計上
				else {
					$extra_time = $work_time - 8.0;
				}
			}
		}
		// 通常営業日
		else {
			if ($work_time != 0.0) {
				// 法定外休日の場合で 8.0h未満の実働時間は、36協定の残業として処理
				if ($holiday == 1  && $work_time < 8.0) {
					$extra_time = $work_time;
				} else {
					$extra_time = $work_time - 8.0;
				}
			}
		}

		return $extra_time;
	}

	/**
	 * [_getExtraTimeOverCount 超勤時間超過回数]
	 * @param  [type] $staff_id [取得対象社員ID]
	 * @param  [type] $year     [取得対象年]
	 * @param  [type] $month    [取得対象月]
	 * @return [type]       [None]
	 *
	 * 年度内で超勤時間が45.0hを超えた回数を取得する。
	 */
	function _getExtraTimeOverCount ($staff_id, $year = null, $month = null)
	{
		if ($year == null || $month == null) {
			$year = date('Y');
			$month = date('n');
		}

		$result = 0;

		if ($staff_id != null) {
			// 勤怠の年度期間(4月～翌年3月)に合わせる
			if ($month >= 1 && $month <= 3) {
				$year = $year - 1;
			}

			// 対象社員の超勤時間が45.0hを超えるレコードを取得
			$con = array ('Kintai.staff_id'=>$staff_id,
						"OR"=>array(array("Kintai.year"=>$year, "Kintai.month >="=>4),
									array("Kintai.year"=>$year+1, "Kintai.month <="=>3)),
						'Kintai.over_time >='=>45.0);
			$result = $this->Kintai->find('count', array('conditions'=>$con));
		}

		return $result;
	}

	/**
	 * [_getExtraTimeLimit 超勤時間上限取得]
	 * @param  [type] $staff_id [取得対象社員ID]
	 * @param  [type] $year     [取得対象年]
	 * @param  [type] $month    [取得対象月]
	 * @return [type]       [None]
	 *
	 * 2ヶ月ないし6ヶ月の超勤時間の平均から、今月可能な超勤時間の上限を取得します。
	 */
	function _getExtraTimeLimit ($staff_id, $year = null, $month = null)
	{
		if ($year == null || $month == null) {
			$year = date('Y');
			$month = date('n');
		}

		$result = 99.5;

		if ($staff_id != null) {
			// 対象月から過去５カ月分の勤怠情報を取得
			$sub_con = "";
			if ($month <= 5) {
				$sub_con = array("OR"=>array(array("Kintai.year"=>$year-1, "Kintai.month >="=>$month+12-5),
											 array("Kintai.year"=>$year, "Kintai.month <"=>$month, "Kintai.month >="=>1)));
			}
			else {
				$sub_con = array("Kintai.year"=>$year, "Kintai.month <"=>$month, "Kintai.month >="=>$month-5);
			}
			$con = array ('Kintai.staff_id'=>$staff_id, $sub_con);

			// 対象月から直近の順に平均を求める必要があるため、オーダーは降順
			$order = array ('Kintai.year'=>'desc', 'Kintai.month'=>'desc');
			$kintais = $this->Kintai->find('all', array('conditions'=>$con, 'order'=>$order));

			// 対象月からの超勤時間の平均が80h未満となる上限時間を産出
			$sum = 0.0;
			$count = 1;
			foreach ($kintais as $kintai) {
				$count = $count + 1;
				$sum = $sum + $kintai['Kintai']['over_time'];
				$limit = 80 * $count - $sum;
				if ($result > $limit) {
					$result = $limit;
				}
			}
		}

		return $result;
	}


}
?>
