<?php
App::uses('AppController', 'Controller');

/**
 * 企業
 */
class CompaniesController extends AppController {

    var $uses = array('Company');

    public $paginate = array(
        'page' => 1,
        'conditions' => array(''),
        );

    /**
     * [s_index 一覧]
     * @return [type] [None]
     */
    public function s_index(){

        if (0 == $this->_checkStaffAuthority()) {
              $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
              $this->redirect($this->referer());
        }

        $con = array();

        // 項目名=>array(値,検索条件,モデル名)
        $keys = array('name'=>array('', 'like', ''),
                      // 'customer'=>array('', 'like', ''),
                      // 'skill'=>array('', 'like', ''),
                      // 'status'=>array('', 'default', ''),
                      // 'term_s'=>array('', 'term_s', ''),
                      // 'term_e'=>array('', 'term_e', ''),
                     );

        // 検索条件作成
        $model = "Company";
        if(!empty($this->params->query)){
            foreach ($keys as $key => $value) {
                if($this->params->query["sc_" . $key] != ""){
                    $keys[$key][0] = $this->params->query["sc_" . $key];

                    if ($value[2]) {
                        $model = $value[2];
                    }
                    // 特殊な検索はswitch文に追加する
                    switch ($keys[$key][1]) {
                        case 'like':
                            array_push($con, array("$model.$key LIKE"=>"%".$keys[$key][0]."%"));
                            break;
                        case 'term_s':
                            $item = str_replace('_s', '', $key);
                            array_push($con, array("$model.$item >="=>$keys[$key][0]));
                            break;
                        case 'term_e':
                            $item = str_replace('_e', '', $key);
                            array_push($con, array("$model.$item <="=>$keys[$key][0]));
                            break;
                        default:
                            array_push($con, array("$model.$key"=>$keys[$key][0]));
                            break;
                    }
                }
            }
        }

        $this->paginate['Company'] = array('limit'=>100, "conditions"=>$con, 'fields'=>array('*'), 'order'=>array('created'=>'desc'));
        $datas = $this->paginate('Company');
        if(isset($this->params->query['csv'])){
            $datas = $this->Company->find('all', array("conditions"=>$con, 'fields'=>array('*'), 'order'=>array('created'=>'desc')));
            $this->_outputCsv($datas);
            exit();
        }

        $this->set(compact('datas', 'keys'));

        $this->set('title_for_layout', '企業一覧');
    }

    /**
     * [s_edit 登録・編集]
     * @param  [type]  $id    [ID]
     * @return [type]         [None]
     */
    function s_edit($id = null){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            echo '権限がありません';
            exit();
        }

        $this->layout = "ajax";
        $data = $this->Company->find("first", array("conditions"=>array("Company.id"=>$id)));
        if (!empty($data)) {
            $this->data = $data;
        }

        $this->set(compact('datas'));
    }

    /**
     * [s_update 更新]
     * @return [type] [None]
     */
    function s_update() {
        $error = 0;
        $message = "更新しました。";

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        if (!empty($this->request->data)) {
            if(empty($this->request->data['Company']['id'])){//新規登録
                $this->Company->create();
            }
            if (!$this->Company->save($this->request->data)) {
                $error++;
                $message = "更新に失敗しました";
            }
        } else {
            $error++;
            $message = "不正なアクセスです。";
        }
        echo sprintf('{"error":"%d","message":"%s"}', $error, $message);
        exit();

    }

    /**
     * [s_delete 削除]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function s_delete($id) {
        $error = 0;
        $message = "削除しました。";

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $error++;
            $message = "権限がありません";
        } else {
            $data = $this->Company->find('first', array('conditions'=>array('Company.id'=>$id)));
            if(!empty($data)){
                $data['Company']['enable'] = 0;
                if (!$this->Company->save($data)) {
                    $error++;
                    $message = "削除に失敗しました";
                }
            }
        }

        echo sprintf('{"error":"%d","message":"%s"}', $error, $message);
        exit();
    }


}

?>
