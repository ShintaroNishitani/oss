<?php
App::uses('AppController', 'Controller');

/**
 * スケジュール
 */
class SchedulesController extends AppController {

    var $uses = array('Schedule', 'Staff', 'Holiday', 'Meeting');

    /**
     * [a_calendar カレンダー]
     * @param  [type] $year  [年]
     * @param  [type] $month [月]
     * @return [type]        [None]
     */
    public function a_calendar($year = null, $month = null){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        if ($year == null || $month == null) {
            $year = date('Y');
            $month = date('n');
        }

        //前月、翌月を取得
        $this->Common->getBeforeMonth($year, $month, $b_year, $b_month);
        $this->Common->getNextMonth($year, $month, $n_year, $n_month);

        $start = "$b_year-$b_month-1";
        $end = "$n_year-$n_month-1";

        $this->Schedule->unbindModelAll();
        $datas = $this->Schedule->find('all', array('conditions'=>array('date >='=>$start,
                                                                        'date <'=>$end,
                                                                        'or'=>array(array('staff_id'=>$this->my_staff_id), array('public'=>0))),
                                                    'order'=>array('start'=>'desc')));

        // 休日情報を取得
        $holidays = $this->Holiday->find('all', array('conditions'=>array('date >='=>"$year-$month-1", 'date <'=>"$n_year-$n_month-1")));
        // 月例会情報を取得
        $meeting = $this->Meeting->find('first', array('conditions'=>array('date >='=>"$year-$month-1", 'date <'=>"$n_year-$n_month-1")));

        $this->set(compact('year', 'month', 'b_year', 'b_month', 'n_year', 'n_month', 'datas', 'holidays', 'meeting'));
        $this->set('title_for_layout', 'スケジュール');
    }

    /**
     * [a_edit 登録・編集]
     * @param  integer $id   [ID]
     * @param  [type]  $date [日付]
     * @return [type]        [None]
     */
    public function a_edit($id = -1, $date = null){

        if (0 == $this->_checkStaffAuthority()) {
            $this->Session->setFlash('権限がありません');
            $this->redirect($this->referer());
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $auth = $this->_checkStaffAuthority();
            if ($auth == 2) {
                $message = "登録しました";

                if (empty($this->data)) {
                    $message = "セッションエラーです";
                } else {
                    $data = $this->data;
                    if (!empty($data["del"])){
                        $message = "削除しました";
                        $data['Schedule']['enable'] = 0;
                    }

                    if (!$this->Schedule->save($data)) {
                        $message = "データの更新に失敗しました";
                    } else {
                        $id = $this->Schedule->id;
                    }
                }
            } else {
                $message = "登録権限がありません";
            }

            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));

            $this->redirect(array('action' => 'calendar', $this->request->data['Schedule']['year'], $this->request->data['Schedule']['month']));
        }

        $staff_id = $this->my_staff_id;

        $data = $this->Schedule->find("first", array("conditions" => array("Schedule.id" => $id)));
        if (!empty($data)) {
            $this->data = $data;
            // 非公開で別のスタッフのスケジュールは編集できない
            if ($data['Schedule']['public'] && $data['Schedule']['staff_id'] != $staff_id) {
                $this->Session->setFlash('権限がありません');
                $this->redirect($this->referer());
            }
        }

        // スタッフ
        $staffs = $this->Staff->find('list');

        $tmp = explode('-', $date);
        $year = $tmp[0];
        $month = $tmp[1];

        $date = date('Y-m-d', strtotime($date));

        $this->set(compact('id', 'date', 'staff_id', 'staffs', 'year', 'month'));
        $this->set('title_for_layout', 'スケジュール登録・編集');
    }

    /**
     * [a_detail 詳細]
     * @param  integer $id   [ID]
     * @return [type]        [None]
     */
    public function a_detail($id = -1){

        if (0 == $this->_checkStaffAuthority()) {
            $this->Session->setFlash('権限がありません');
            $this->redirect($this->referer());
        }

        $this->layout = "ajax";

        $staff_id = $this->my_staff_id;

        $data = $this->Schedule->find ("first", array("conditions" => array("Schedule.id" => $id)));
        if (!empty($data)) {
            $this->data = $data;
            // 非公開で別のスタッフのスケジュールは編集できない
            if ($data['Schedule']['public'] && $data['Schedule']['staff_id'] != $staff_id) {
                $this->Session->setFlash('非公開のため参照できません。');
                $this->redirect($this->referer());
            }
        }

        // スタッフ
        $staffs = $this->Staff->find('list');

        $this->set (compact('id', 'staff_id', 'staffs', 'data'));
        $this->set ('title_for_layout', 'イベントの詳細');
    }
}

?>
