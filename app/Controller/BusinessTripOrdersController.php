<?php
App::uses('AppController', 'Controller');

/**
 * 交通費
 */
class BusinessTripOrdersController extends AppController {
    var $uses = array('BusinessTripOrder','Confirm');

    public $paginate = array(
        'page' => 1,
        'conditions' => array(''),
        );

    /**
     * [s_index 一覧]
     * @param  [type] $year     [年]
     * @param  [type] $month    [月]
     * @param  [type] $staff_id [スタッフID]
     * @return [type]           [None]
     */
    function s_index(){


        if ($year == null || $month == null) {
            $year = date('Y');
            $month = date('n');
        }
        //前月、翌月を取得
        $this->Common->getBeforeMonth($year, $month, $b_year, $b_month);
        $this->Common->getNextMonth($year, $month, $n_year, $n_month);

        $staff_id = $this->Session->read('my_staff_id');

        $date = $year."/".$month."/"."01";
        $date = date("yy年m月d日" ,strtotime($date));

        // 検索条件作成
        // $con = array("BusinessTripOrdersController.plan_before_date"=>$date,
        //              "BusinessTripOrdersController.traveler_id"=>$staff_id,
        //              "BusinessTripOrdersController.traveler_id <>"=>9999);

        $con = array("BusinessTripOrder.traveler_id <>"=>9999);

        //$datas = $this->BusinessTripOrder->find('first', array('conditions'=>$con));
        $datas = $this->BusinessTripOrder->find('first', array('conditions'=>$con));
        if (empty($datas)) {
            // 旅費保存
            $this->BusinessTripOrder->create();
            $data['BusinessTripOrder'] = array('traveler_id'=>$staff_id,
                                       'enable'=>1,
                                      );
            $this->BusinessTripOrder->save($data);
            $id = $this->BusinessTripOrder->id;            
        } else {
            $id = $datas['BusinessTripOrder']['id'];
        }

        $status = 0;
        $confirm = $this->Confirm->find('first', array('conditions'=>array('type'=>CONFIRM_TYPE_2, 'type_id'=>$id)));
        if ($confirm) {
            $status = $confirm['Confirm']['status'];
        }

        $this->log($datas,LOG_DEBUG);
        $this->set(compact('year', 'month', 'b_year', 'b_month', 'n_year', 'n_month', 'datas', 'staff_id', 'status', 'id'));
        $this->set($datas);
        $this->set('title_for_layout', '旅費精算');
    }
}

?>