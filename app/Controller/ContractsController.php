<?php
App::uses('AppController', 'Controller');

/**
 * 契約
 */
class ContractsController extends AppController {
	var $uses = array('Contract', 'ContractDetail', 'SaleProject', 'HumanResource', 'ContractResult');

    public $paginate = array(
        'page' => 1,
        'conditions' => array(''),
        );

    /**
     * [s_index 契約一覧]
     * @return [type] [None]
     */
    public function s_index(){

        if (0 == $this->_checkStaffAuthority()) {
              $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
              $this->redirect($this->referer());
        }

        $joins = array(
            array(
                "type" => "LEFT",
                "table" => "human_resources",
                "alias" => "HumanResource",
                "conditions" => array("HumanResource.id = ContractDetail.human_resource_id"),
            ),
            array(
                "type" => "LEFT",
                "table" => "contracts",
                "alias" => "Contract",
                "conditions" => array("Contract.id = ContractDetail.contract_id"),
            ),
            array(
                "type" => "LEFT",
                "table" => "sale_projects",
                "alias" => "SaleProject",
                "conditions" => array("SaleProject.id = Contract.sale_project_id"),
            ),
            array(
                "type" => "LEFT",
                "table" => "companies",
                "alias" => "ReceiveCompany",
                "conditions" => array("ReceiveCompany.id = Contract.receive_company_id"),
            ),
            array(
                "type" => "LEFT",
                "table" => "companies",
                "alias" => "OrderCompany",
                "conditions" => array("OrderCompany.id = Contract.order_company_id"),
            ),

        );
        
        if(!isSet($this->params->query["sc_include_out_range"]))
        {
        	$this->params->query["sc_include_out_range"] = "0";
        }
        $con = array();

        // 項目名=>array(値,検索条件,モデル名)
        $keys = array('name'=>array('', 'name', 'SaleProject'),
                      'human'=>array('', 'human', 'HumanResource'),
                      'receive_company'=>array('', 'receive_company', 'ReceiveCompany'),
                      'order_company'=>array('', 'order_company', 'OrderCompany'),
                      'include_out_range'=>array('', 'include_out_range', 'Contract'),
                      // 'term_s'=>array('', 'term_s', ''),
                      // 'term_e'=>array('', 'term_e', ''),
                     );
        // 検索条件作成
        $model = "ContractDetail";

        if(!empty($this->params->query)){
            foreach ($keys as $key => $value) {
                if(isSet($this->params->query["sc_" . $key]) && ($this->params->query["sc_" . $key] != "")){
                    $keys[$key][0] = $this->params->query["sc_" . $key];

                    if ($value[2]) {
                        $model = $value[2];
                    }
                    // 特殊な検索はswitch文に追加する
                    switch ($keys[$key][1]) {
                        case 'like':
                            array_push($con, array("$model.$key LIKE"=>"%".$keys[$key][0]."%"));
                            break;
                        case 'name':
                        case 'human':
                        case 'receive_company':
                        case 'order_company':
                            array_push($con, array("$model.name LIKE"=>"%".$keys[$key][0]."%"));
                            break;
                        case 'include_out_range':
                        	if('0' == $keys[$key][0])
                        	{
                        		$today = date("Y-m-d");  
                        		array_push($con, array("$model.term_s <=" . "\"$today\""));
                        		array_push($con, array("$model.term_e >=" . "\"$today\""));
                        	}
                        	break;
                        default:
                            array_push($con, array("$model.$key"=>$keys[$key][0]));
                            break;
                    }
                }
            }
        }

        $this->ContractDetail->unbindModelAll();
//        $this->paginate['ContractDetail'] = array('limit'=>100, "conditions"=>$con, 'fields'=>array('*'), 'order'=>array('created'=>'desc'), 'recursive'=>2, 'joins'=> $joins);
        $this->paginate['ContractDetail'] = array('limit'=>100, "conditions"=>$con, 'fields'=>array('*'), 'order'=>array('HumanResource.name'=>'asc', 'Contract.term_s'=>'desc'), 'recursive'=>2, 'joins'=> $joins);
        $datas = $this->paginate('ContractDetail');


// pr($datas);exit();

        $this->set(compact('datas', 'keys'));

        $this->set('title_for_layout', '契約一覧');
    }

	/**
	 * [s_detail_edit 詳細更新]
	 * @param  [type] $id [ID]
	 * @return [type]     [None]
	 */
	function s_detail_edit($id = null){

		$auth = $this->_checkStaffAuthority();
		if (0 == $auth) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}
		$this->layout = "ajax";
		$data = $this->ContractDetail->find("first", array("conditions" => array("ContractDetail.id"=>$id)));
		if (!empty($data)) {
			$this->data = $data;
		}

		$this->set(compact('id'));
	}

	/**
	 * [s_update 更新]
	 * @param  [type] [None]
	 * @return [type] [None]
	 */
	function s_update(){
        $error = 0;
        $message = "更新しました。";

		$auth = $this->_checkStaffAuthority();
		if (2 != $auth) {
		    $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
		    $this->redirect($this->referer());
		}		
		if (!empty($this->request->data)) {
			$message = "更新しました";
			if (empty($this->data)) {
				$error++;
				$message = "セッションエラーです";
			} else {
				if (!$this->ContractDetail->save($this->request->data['ContractDetail'])) {
	                $error++;
	                $message = "更新に失敗しました";
				} else {
					if (!$this->Contract->save($this->request->data['Contract'])) {
		                $error++;
		                $message = "更新に失敗しました";
					}
				}
			}
		} else {
            $error++;
            $message = "不正なアクセスです。";
        }

        echo sprintf('{"error":"%d","message":"%s"}', $error, $message);
        exit();
	}

	/**
	 * [s_delete 削除]
	 * @param  [type] $id [ID]
	 * @return [type]     [None]
	 */
	function s_delete($id){
        $error = 0;
        $message = "削除しました。";

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $error++;
            $message = "権限がありません";
        } else {
            $data = $this->ContractDetail->find('first', array('conditions'=>array('ContractDetail.id'=>$id)));
            if(!empty($data)){
                $data['ContractDetail']['enable'] = 0;
                if (!$this->ContractDetail->save($data)) {
                    $error++;
                    $message = "削除に失敗しました";
                } else {
                	$datas = $this->ContractResult->find('all', array('conditions'=>array('contract_detail_id'=>$id)));
                	foreach ($datas as $data) {
                		$data['ContractResult']['enable'] = 0;
                		if (!$this->ContractResult->save($data)) {
		                    $error++;
		                    $message = "削除に失敗しました";
                		}
                	}
                }
            }
        }

        echo sprintf('{"error":"%d","message":"%s"}', $error, $message);
        exit();
    }
    
	/**
	 * [s_detail_clone 詳細更新]
	 * @param  [type] $id [ID]
	 * @return [type]     [None]
	 */
	function s_detail_clone($id = null){

		$auth = $this->_checkStaffAuthority();
		if (0 == $auth) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}
		$this->layout = "ajax";
		$data = $this->ContractDetail->find("first", array("conditions" => array("ContractDetail.id"=>$id)));
		if (!empty($data)) {
			unset($data['ContractResult']['id']);
			unset($data['ContractDetail']['id']);
			unset($data['Contract']['id']);
			$this->data = $data;
		}
		$this->set(compact('id'));
	}


	/**
	 * [s_clone 契約登録]
	 * @return [type] [None]
	 */
	function s_clone(){
        $error = 0;
        $message = "更新しました。";
        
		$auth = $this->_checkStaffAuthority();
		if (2 != $auth) {
		    $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
		    $this->redirect($this->referer());
		}

		// 契約情報登録
		$this->Contract->create();
		$data = $this->request->data['Contract'];
		if (!$this->Contract->save($data)) {
			$message = "契約情報の登録に失敗しました";
			$this->redirect($this->referer());
		}

		// 契約詳細登録
		$this->ContractDetail->create();
		$data = $this->request->data['ContractDetail'];
		$data['contract_id'] = $this->Contract->id;
		if (!$this->ContractDetail->save($data)) {
			$message = "契約詳細情報の登録に失敗しました";
			$this->redirect($this->referer());
		}

		// 契約実績登録
		$ymList = $this->Common->getMonthRange(strtotime($this->request->data['Contract']['term_s']), strtotime($this->request->data['Contract']['term_e']));
		$this->log($ymList);
		foreach ($ymList as $ym) {
			$data = array('contract_detail_id'=>$this->ContractDetail->id,
						  'human_resource_id'=>$this->request->data['ContractDetail']['human_resource_id'],
						  'year'=>$ym['year'],
						  'month'=>$ym['month'],
						  'enable'=>1,
							);
			$this->ContractResult->create();
			if (!$this->ContractResult->save($data)) {
				$message = "契約実績情報の登録に失敗しました";
				$this->redirect($this->referer());
			}
		}

		$this->Session->setFlash('契約が登録されました。', 'default', array('class'=> 'alert alert-info'));
		$this->redirect(array('action' => 'result_index'));

        echo sprintf('{"error":"%d","message":"%s"}', $error, $message);
        exit();
	}


	/**
	 * [s_mail_format メールフォーマット]
	 * @param  [type]  $id    [ID]
	 * @return [type]         [None]
	 */
	function s_mail_format($id = null){

		$auth = $this->_checkStaffAuthority();
		if (0 == $auth) {
		    echo '権限がありません';
		    exit();
		}

		$this->layout = "ajax";
		// $data = $this->SaleProject->find("first", array("conditions"=>array("SaleProject.id"=>$id)));

		$this->set(compact('data'));
	}

	/**
	 * [s_edit 契約登録]
	 * @return [type] [None]
	 */
	function s_edit($kind = null, $srch_id = null){

		$auth = $this->_checkStaffAuthority();
		if (2 != $auth) {
		    $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
		    $this->redirect($this->referer());
		}

		if ($this->request->is('post') || $this->request->is('put')) {
// $this->log($this->request->data);
			$sale_project_id = $this->request->data['SaleProject']['id'];
			if (empty($sale_project_id)) {
				// 案件情報登録
				$this->SaleProject->create();
				if (!$this->SaleProject->save($this->request->data['SaleProject'])) {
					$message = "案件情報の登録に失敗しました";
					$this->redirect($this->referer());
				}
				$sale_project_id = $this->SaleProject->id;
			}
			$human_resource_id = $this->request->data['HumanResource']['id'];
			if (empty($human_resource_id)) {
				// 人材情報登録
				$this->HumanResource->create();
				if (!$this->HumanResource->save($this->request->data['HumanResource'])) {
					$message = "人材情報の登録に失敗しました";
					$this->redirect($this->referer());
				}
				$human_resource_id = $this->HumanResource->id;
			}
			// 契約情報登録
			$this->Contract->create();
			$data = $this->request->data['Contract'];
			$data['sale_project_id'] = $sale_project_id;
			$data['receive_company_id'] = $this->request->data['SaleProject']['company_id'];
			$data['order_company_id'] = $this->request->data['HumanResource']['company_id'];

			if (!$this->Contract->save($data)) {
				$message = "契約情報の登録に失敗しました";
				$this->redirect($this->referer());
			}

			// 契約詳細登録
			$this->ContractDetail->create();
			$data = $this->request->data['ContractDetail'];
			$data['contract_id'] = $this->Contract->id;
			$data['human_resource_id'] = $human_resource_id;
			if (!$this->ContractDetail->save($data)) {
				$message = "契約詳細情報の登録に失敗しました";
				$this->redirect($this->referer());
			}

			// 契約実績登録
			$ymList = $this->Common->getMonthRange(strtotime($this->request->data['Contract']['term_s']), strtotime($this->request->data['Contract']['term_e']));
			$this->log($ymList);
			foreach ($ymList as $ym) {
				$data = array('contract_detail_id'=>$this->ContractDetail->id,
							  'human_resource_id'=>$human_resource_id,
							  'year'=>$ym['year'],
							  'month'=>$ym['month'],
							  'enable'=>1,
								);
				$this->ContractResult->create();
				if (!$this->ContractResult->save($data)) {
					$message = "契約実績情報の登録に失敗しました";
					$this->redirect($this->referer());
				}
			}


			$this->Session->setFlash('契約が登録されました。', 'default', array('class'=> 'alert alert-info'));
			//$this->redirect(array('action' => 'result_index'));
			$this->redirect(array('action' => 'index'));
		}
		if(!empty($kind))
		{
			if($kind == 'S')
			{
				$result = $this->SaleProject->find("first", array("conditions"=>array("SaleProject.id"=>$srch_id)));
				$this->data = $result;
			}
			else if($kind == 'H')
			{
				$result = $this->HumanResource->find("first", array("conditions"=>array("HumanResource.id"=>$srch_id)));
				$this->data = $result;
			}
		}
		

		$companies = $this->HumanResource->Company->find('list');
		$projects = $this->SaleProject->find('list');
		$humans = $this->HumanResource->find('list');

		$this->set(compact('id', 'companies', 'projects', 'humans'));
		$this->set('title_for_layout', '契約登録');
	}

	/**
	 * [s_result_index 精算一覧]
	 * @param  [type] [None]
	 * @return [type] [None]
	 */
	function s_result_index($year = null, $month = null){
		$auth = $this->_checkStaffAuthority();
		if (0 == $auth) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}


		// $datas = $this->Contract->find('all', array('order'=>array('purchase_date'=>'desc')));
		$data = array();

		if ($year == null || $month == null) {
			$year = date('Y');
			$month = date('n');
		}

		$datas = $this->ContractResult->find('all', array('conditions'=>array('year'=>$year, 'month'=>$month), 'recursive'=>3,));
// pr($datas);exit();


		//前月、翌月を取得
		$this->Common->getBeforeMonth($year, $month, $b_year, $b_month);
		$this->Common->getNextMonth($year, $month, $n_year, $n_month);

		$this->set(compact('year', 'month', 'b_year', 'b_month', 'n_year', 'n_month', 'datas'));
		$this->set('title_for_layout', '精算一覧');
	}

	/**
	 * [s_result_edit 精算編集]
	 * @param  [type] $id [ID]
	 * @return [type]     [None]
	 */
	function s_result_edit($id = null){

		$auth = $this->_checkStaffAuthority();
		if (0 == $auth) {
			$this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
			$this->redirect($this->referer());
		}
		$this->layout = "ajax";
		$data = $this->ContractResult->find("first", array("conditions" => array("ContractResult.id"=>$id)));
		if (!empty($data)) {
			$this->data = $data;
		}
		$year = $data['ContractResult']['year'];
		$month = $data['ContractResult']['month'];

		$this->set(compact('id', 'year', 'month'));
	}

	/**
	 * [s_result_update 精算更新]
	 * @param  [type] [None]
	 * @return [type] [None]
	 */
	function s_result_update(){

		$auth = $this->_checkStaffAuthority();
		if (2 != $auth) {
		    $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
		    $this->redirect($this->referer());
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$message = "更新しました";

			if (empty($this->data)) {
				$message = "セッションエラーです";
			} else {
				$this->ContractResult->create();
				if (!$this->ContractResult->save($this->request->data)) {
					$message = "データの更新に失敗しました";
				}
			}
			$this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
			$this->redirect(array('action' => 'result_index', $this->data['Contract']['ContractResult']['year'], $this->data['Contract']['ContractResult']['month']));
		}
	}

	/**
	 * [s_result_delete 精算削除]
	 * @param  [type] $id [ID]
	 * @return [type]     [None]
	 */
	function s_result_delete($id){
		$this->autoRender = false;
		$auth = $this->_checkStaffAuthority();
		if (2 != $auth) {
		    $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
		    $this->redirect($this->referer());
		}
		$data = $this->ContractResult->find('first', array('conditions'=>array('ContractResult.id'=>$id)));
		if(!empty($data)){
			$data['ContractResult']['enable'] = 0;
			$b = $this->ContractResult->save($data);
			$this->redirect(array('action' => 'result_index', $data['ContractResult']['year'], $data['ContractResult']['month']));
		}
	}
}

?>
