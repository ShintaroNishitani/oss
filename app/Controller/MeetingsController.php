<?php
App::uses('AppController', 'Controller');

/**
 * 月例会日設定
 */
class MeetingsController extends AppController {
    public $paginate = array(
        'page' => 1,
        'conditions' => array(''),
        );

    /**
     * [a_index 一覧]
     * @return [type]           [None]
     */
    function a_index(){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        $this->paginate['Meeting'] = array('limit'=>100, 'order'=>array('date'=>'desc'));
        $datas = $this->paginate('Meeting');

        $this->set(compact('datas'));

        $this->set('title_for_layout', '月例会日設定');
    }

    /**
     * [a_edit 更新]
     * @param  [type] $id    [ID]
     * @return [type]        [None]
     */
    function a_edit($id = null){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            echo '権限がありません';
            exit();
        }

        $this->layout = "ajax";
        $this->Meeting->unbindModelAll();
        $data = $this->Meeting->find("first", array("conditions" => array("Meeting.id" => $id), 'fields'=>array('*')));
        if (!empty($data)) {
            $this->data = $data;
        }

        $this->set(compact('id', 'data'));
        $this->set('title_for_layout', '月例会日登録・編集');
    }

    /**
     * [a_update 更新]
     * @return [type] [None]
     */
    function a_update(){

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $message = "更新しました";

            if (empty($this->data)) {
                $message = "セッションエラーです";
            } else {

                $this->Meeting->create();
                if (!$this->Meeting->save($this->request->data)) {
                    $message = "データの更新に失敗しました";
                }
            }
            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
            $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * [a_delete 削除]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function a_delete($id){
        $this->autoRender = false;

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        $this->Meeting->delete($id);
        $this->redirect(array('action' => 'index'));
    }

    /**
     * [a_upload_data アップロード]
     * @return [type] [None]
     */
    public function a_upload(){

        $msg = '不正なアクセスです。';
        if (!empty($this->request->data)) {
            $msg = $this->_uploadCsv($this->request->data['Meeting']['file']);
        }
        $this->Session->setFlash($msg, 'default', array('class'=> 'alert alert-info'));
        $this->redirect(array('action' => 'index'));
    }

    /**
     * [_uploadCsv CSVアプロード]
     * @param  [type] $data [データ]
     * @return [type]       [None]
     */
    public function _uploadCsv($datas){
        $error = 0;

        $fp = fopen($datas["tmp_name"], "r");
        $head = fgets($fp);
        $head = str_replace("\n", "", $head);
        $heads = explode(',', $head);

        if (count($heads) != 1) {
            return 'フォーマットが違います';
        }
        $this->Meeting->query("begin");

        $count = 0;
        while ($line = fgets($fp)) {
            $line = str_replace("\n", "", $line);
            $data = explode(',', $line);
            if (count($data) != 1) {
                $error++;
                break;
            }
            $date = $data[0];
            $meeting = $this->Meeting->find('first', array('conditions'=>array('Meeting.date'=>$date)));
            if (!$meeting) {
            	$meeting['Meeting']['date'] = $date;
	            $this->Meeting->create();
	            if(false == $this->Meeting->save($meeting)){
	                $error++;
	                break;
	            } else {
	            	$count++;
	            }
            }
        }

        if ($error) {
            $this->Meeting->query("rollback");
            return 'データの更新に失敗しました';
        } else {
            $this->Meeting->query("commit");
            return sprintf('データをアップロードしました（%d件）', $count);
        }
    }
}

?>
