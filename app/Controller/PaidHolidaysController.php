<?php
App::uses('AppController', 'Controller');
App::uses('KintaisController', 'Controller');

/**
 * 有休管理・閲覧
 */
class PaidHolidaysController extends AppController {
    var $uses = array('PaidHoliday', 'PaidHolidayDetail', 'Staff');

    public $paginate = array(
        'page' => 1,
        'conditions' => array(''),
        );

    public $holiday_kind = array('1'=>'全休' ,
                                 '2'=>'午前半休' ,
                                 '3'=>'午後半休');
    /**
     * [s_index 一覧]
     * @param  [type] $year     [年度]
     * @return [type]           [None]
     */
    function s_index($year = null)
    {
        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));            $this->redirect($this->referer());
        }

        // 今年度取得
        $now_year = $this->Common->getYear();
        $m = date ('n');
        if ($m < 4) {
            // 当月が1～3月の場合、前年度に変更する(勤怠は4月～翌年3月で管理するため)
            $now_year --;
        }

        // 年度未指定時は今年度を指定
        if ($year == null) {
            $year = $now_year;
        }

        $staff_id = $this->Session->read('my_staff_id');

        // 最新のデータのみ保持
        $datas = $this->PaidHoliday->find('first',array('conditions'=>array('PaidHoliday.enable'=>"1", 'PaidHoliday.staff_id' => $staff_id )));

        // 有休情報のレコードがない人は、表示できない
        if (NULL == $datas) {
            $this->Session->setFlash('有休情報がないため、表示できません マスター管理の有休管理でレコードを作成してください',
                                    'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        // 次の3月に消える有給日数の計算 有休残日数-今回増加する有給日数
        // 有休増加日の計算
        // 今の年と、入社年との比較を行う
        // 入社から何年switchで分岐させる
        $now_date = date("Y-m");
        //$this->log($now_date,LOG_DEBUG);
        $hire_date = $this->Staff->find('first' , array("conditions" => array("Staff.id"=> $staff_id ),'fields' => array('hire_date') , 'recursive' => -1));
        //$this->log($hire_date,LOG_DEBUG);
        $hire_date = $hire_date["Staff"]["hire_date"];
        $hire_date_ts = strtotime($hire_date);
        $hire_date = date("Y-m" , strtotime($hire_date));
        // 入社から2年6ヶ月立っていない場合有休は消滅しない
        $lost_start_date = date("Y-m", strtotime("+2 year 6 month",$hire_date_ts));
        //$this->log($lost_start_date,LOG_DEBUG);
        if( $lost_start_date > $now_date ){
             $lost_paid = 0;
        }
        else{
            $diff_year = $now_date - $hire_date - 1 ;
            //$this->log($diff_year,LOG_DEBUG);
            switch ($diff_year){
            case 2:
              $lost_paid = $datas["PaidHoliday"]["holiday_remain"] - 12;
              break;
            case 3:
              $lost_paid = $datas["PaidHoliday"]["holiday_remain"] - 14;
              break;
            case 4:
              $lost_paid = $datas["PaidHoliday"]["holiday_remain"] - 16;
              break;
            case 5;
              $lost_paid = $datas["PaidHoliday"]["holiday_remain"] - 18;
              break;
            default:
              $lost_paid = $datas["PaidHoliday"]["holiday_remain"] - 20;
            }
        }
        if ($lost_paid < 0){
            $lost_paid = 0;
        }

        // コンディション設定
        $con = array();

        // 日付指定を追加
        if ($year != 0 && $year != null) {
            $disp_from = sprintf ("%04d-04-01", $year);
            $disp_to = sprintf ("%04d-03-31", $year + 1);
            array_push ($con, array ("PaidHolidayDetail.use_date >=" => $disp_from),
                              array ("PaidHolidayDetail.use_date <=" => $disp_to));
        }
        array_push ($con, array ("PaidHolidayDetail.staff_id" => $staff_id));

        // 有休管理詳細テーブルを取得
        $paid_details = $this->PaidHolidayDetail->find ('all', array ('conditions'=>$con));

        $id = 0;

        // 選択用年度リスト
        $years = array ("0" => "全年度");
        for ($i = $now_year + 1; $i >= 2007; $i--) {
            $years += array ($i => sprintf ("%d 年度", $i));
        }

        $kind = $this->holiday_kind;
        $this->set(compact('datas', 'staff_paid', 'id', 'lost_paid', 'paid_details', 'kind', 'year', 'years'));

        $this->set('staff_paid', $datas);

        $this->set('title_for_layout', '有休閲覧');
    }

    /**
     * [a_index 一覧]
     * @param  [type] $year          [年度]
	 * @param  [type] $select_staff  [表示対象社員名]
     * @return [type]           [None]
     */
    function a_index($year = null, $select_staff = null)
    {
        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));            $this->redirect($this->referer());
        }

        // 今年度取得
        $now_year = $this->Common->getYear();
        $m = date ('n');
        if ($m < 4) {
            // 当月が1～3月の場合、前年度に変更する(勤怠は4月～翌年3月で管理するため)
            $now_year --;
        }

        // 年度未指定時は今年度を指定
        if ($year == null) {
            $year = $now_year;
        }

        // 最新のデータのみ保持
        $datas = $this->PaidHoliday->find('all', array('conditions'=>array('PaidHoliday.enable'=>"1", "Staff.no !="=>"9999", "Staff.retire_date"=>null, "Staff.authority_id <>"=>1), 'order' => array('Staff.id' =>'asc')));
        // 有給残日数取得
        $controller = new KintaisController();
        for ($i = 0; $i < count($datas); $i++) {
            $datas[$i]['PaidHoliday']['paid_digest'] = $controller->_getPaidHolidayDigest ($datas[$i]['Staff']['id']);
        }

        // スタッフ情報
        $staffs = $this->Staff->find('list', array('conditions'=>array("Staff.no !="=>"9999", "Staff.retire_date"=>null, "Staff.authority_id <>"=>1)));

        // globalの変数を設定
        $option_list= $this->option_list;

        // コンディション設定
        $con = array();

        // 日付指定を追加
        if ($year != 0 && $year != null) {
            $disp_from = sprintf ("%04d-04-01", $year);
            $disp_to = sprintf ("%04d-03-31", $year + 1);
            array_push ($con, array ("PaidHolidayDetail.use_date >=" => $disp_from),
                              array ("PaidHolidayDetail.use_date <=" => $disp_to));
        }

        // 対象スタッフを追加
        if ($select_staff != null) {
            array_push ($con, array ("PaidHolidayDetail.staff_id" => $select_staff));
        }

        // 有休管理詳細テーブルを取得
        $paid_details = $this->PaidHolidayDetail->find ('all', array ("conditions"=>$con));

        // idの初期化
        $id = 0;

        // 選択用年度リスト
        $years = array ("0" => "全年度");
        for ($i = $now_year + 1; $i >= 2007; $i--) {
            $years += array ($i => sprintf ("%d 年度", $i));
        }

        $kind = $this->holiday_kind;
        $this->set(compact('datas', 'staffs','id', 'paid_details', 'kind', 'year', 'select_staff', 'years'));

        $this->set('holiday_data', $datas);

        $this->set('title_for_layout', '有休管理');
    }

    /**
     * [a_edit 登録・編集]
     * @param  [type] $id       [description]
     * @return [type]           [description]
     */
    function a_edit($id){

        $auth = $this->_checkStaffAuthority();
        if (0 == $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        $this->layout = "ajax";
        $this->PaidHoliday->unbindModelAll();
        $data = $this->PaidHoliday->find("first", array("conditions" => array("PaidHoliday.id"=>$id)));
        if (!empty($data)) {
            $this->data = $data;
        }
        $year = $this->Common->getYear(); //年取得

        if(!empty($data)){
            $hire_date = $this->Staff->find('first' , array("conditions" => array("Staff.id"=>$data["PaidHoliday"]["staff_id"])));
            $hire_date_ts = strtotime($hire_date["Staff"]['hire_date']);

            // 更新日の計算
            $start_day = date("Y-m-d", mktime(0, 0, 0, 4, 1, $year));
            $start_day_ts = strtotime($start_day);
            $half_year_day = date("Y-m-01",strtotime("-6 month"));
            $half_year_day_ts = strtotime($half_year_day);

            if( $hire_date_ts <= $half_year_day_ts) {
                // 半年以上
                if( strtotime("today") <= $start_day_ts) {
                    //4月1日より手前
                    $refresh_date = $start_day;
                }else{
                    //4月1日よりうしろ
                    $refresh_date_ts = strtotime( "+1 year" , $start_day_ts);
                    $refresh_date    = date("Y-m-d", $refresh_date_ts);
                }
            }else{
                // 半年以下
                $refresh_date_ts = strtotime("+6 month", $hire_date);
                $refresh_date    = date("Y-m-d", $refresh_date_ts);
            }
        }else{
            // 今月1日の日付を入れる
            $refresh_date = date("Y-m-01");;
        }

        $staffs = $this->Staff->find('list');

        $this->set(compact('data', 'staffs','hire_date','refresh_date','lost_paid'));

        $this->set('title_for_layout',  '有休登録・編集');
    }

    /**
     * [a_update 更新]
     * @return [type] [None]
     */
    function a_update( $id=null ){

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }

        //$this->log($this->request->data,LOG_DEBUG);

        if ($this->request->is('post') || $this->request->is('put')) {
            $message = "更新しました";

            if (empty($this->data)) {
                $message = "セッションエラーです";
            } else {


                if (empty($this->request->data['PaidHoliday']['id'])) {
                    $this->request->data['PaidHoliday']['id'] = 0;
                }
                if( $this->request->data['PaidHoliday']['id'] != 0 ){
                    $data = array('PaidHoliday','id' => $id, 'enable' => 0);
                    $fields = array('enable');

                    // データを新たに追加するので、既存レコードのenableを消す
                    // enableのみデータを更新する
                    if (!$this->PaidHoliday->save($data,false,$fields)) {
                        //$this->log($this->request->data,LOG_DEBUG);
                        $message = "データの更新に失敗しました";
                    }
                }
                // 新レコードの作成
                $this->PaidHoliday->create();
                $this->data['PaidHoliday']['id'];

                // 年を設定する
                $this->request->data['PaidHoliday']['year'] = $this->Common->getYear();

                // 新たにデータを追加するのでidに0を設定
                $this->request->data['PaidHoliday']['id'] = 0;
                //$this->log($this->request->data);

                // データを更新(id=0なので新規レコード追加)する
                if (!$this->PaidHoliday->save($this->request->data)) {
                    //$this->log($this->request->data,LOG_DEBUG);
                    $message = "データの更新に失敗しました";
                }
                //$this->log($this->request->data,LOG_DEBUG);
            }
            $this->Session->setFlash($message, 'default', array('class'=> 'alert alert-info'));
            $this->redirect(array('action' => 'index', $this->data['PaidHoliday']['id']));
        }
    }

    /**
     * [a_delete 削除]
     * @param  [type] $id [ID]
     * @return [type]     [None]
     */
    function a_delete($id){
        $this->autoRender = false;

        $auth = $this->_checkStaffAuthority();
        if (2 != $auth) {
            $this->Session->setFlash('権限がありません', 'default', array('class'=> 'alert alert-info'));
            $this->redirect($this->referer());
        }
        $data = $this->PaidHoliday->find('first', array('conditions'=>array('PaidHoliday.id'=>$id)));
        if(!empty($data)){
            $data['PaidHoliday']['enable'] = 0;
            $this->PaidHoliday->save($data);
            $this->redirect($this->referer());
        }
    }




}

?>
