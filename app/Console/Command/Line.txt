// アクセストークン
$accessToken = 'cYaPdX+M6xAFhQX3hPmt1gBw7mucf5/vcJjUVZUpLuIz29CyCSiO5hHw8sAxYp1Cj01bn5Hm3rTpOhaSujLURHx3ghe9Ik+wZ+3uqOETTYF9k8RR69WT0iw7emABQTrK9KSommTTnit6hexBPpAjdAdB04t89/1O/w1cDnyilFU=';

    // LINE Messaging API プッシュメッセージを送る
    $LINE_PUSH_URL = "https://api.line.me/v2/bot/message/push";

    $LINE_CHANNEL_ACCESS_TOKEN = 'cYaPdX+M6xAFhQX3hPmt1gBw7mucf5/vcJjUVZUpLuIz29CyCSiO5hHw8sAxYp1Cj01bn5Hm3rTpOhaSujLURHx3ghe9Ik+wZ+3uqOETTYF9k8RR69WT0iw7emABQTrK9KSommTTnit6hexBPpAjdAdB04t89/1O/w1cDnyilFU=';

    $LINE_USER_ID = "U9448d83307cdf17783b611324574bfbd";

    // 送信するメッセージ
    $message_text_1 = "PHP LINE API TEST PUSH MESSAGE";
    $message_text_2 = "PHPでPUSH送信！！！\r\n２行目！！！";

    // リクエストヘッダ
    $header = [
        'Authorization: Bearer ' . $LINE_CHANNEL_ACCESS_TOKEN,
        'Content-Type: application/json'
    ];

    // 送信するメッセージの下準備
    $post_values = array(
        [
        "type" => "text",
        "text" => $message_text_1
        ],
        [
        "type" => "text",
        "text" => $message_text_2
        ]
    );

    // 送信するデータ
    $post_data = [
        "to" => $LINE_USER_ID,
        "messages" => $post_values
        ];

    // cURLを使った送信処理の時は true
    // file_get_contentsを使った送信処理の時は false
    $USE_CURL = true;

    if ($USE_CURL) {
        // cURLを使った送信処理
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $LINE_PUSH_URL);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($post_data));
        $result = curl_exec($curl);
        curl_close($curl);
    }
    else
    {
        // file_get_contentsを使った送信処理
        $context = stream_context_create(array(
            'http' => array(
                'method' => 'POST',
                'header' => implode("\r\n", $header),
                'content'=>  json_encode($post_data),
                'ignore_errors' => true
            )
        ));

        $result = file_get_contents(
            $LINE_PUSH_URL,
            false,
            $context
            );
    }
