<?php
/**
 * 見積／請求書番号管理表インポート用シェル
 * ImportOrderShell manual type(Constのオーダー種別参照) year
 */
class ImportOrderShell extends AppShell {

	var $uses = array('Order', 'Staff', 'Customer');

    function main() {

    }


    function arg() {

		$type = $this->args[0];
		$year = $this->args[1];
		if($type == null || empty($year)){
			echo "[USAGE : php app/Console/cake.php ImportOrder arg type year]";
			return false;
		}

		$filename = "order.csv";

		//ファイル読み込み
		$file_in = fopen($filename, "r");
		if(empty($file_in)){
			echo mb_convert_encoding('読み込みファイルがオープンできませんでした。', "SJIS", "UTF-8");
			return false;
		}

		$customers = $this->Customer->find('list', array('fields'=>array('id', 'code')));

		//トランザクション開始
       	//使用するmodel群
		$models = array($this->Order);

	 	//modelにトランザクション開始処理
		$this->_begin($models);

		$num = 0;
		try{
			$bError = false;

			//ヘッダ部を読み込み
			$allstr = fgets($file_in);

			//body部を読み込み
			while(!feof($file_in)){
				$allstr = fgets($file_in);
				$allstr = str_replace('"', '', $allstr);
				if(empty($allstr)){
					break;
				}

				$str = explode(',', $allstr);

				$temp['Order'] = array();
				$temp['Order']['year'] = $year;
				$temp['Order']['type'] = $type;
				$temp['Order']['enable'] = 1;

				if(!empty($str)){
					$num++;

					$idx = 0;

					// No
					$temp['Order']['no']  = $str[$idx++];

					// 使用状況
					$idx++;

					// 使用者
					$name = mb_convert_encoding($str[$idx++], "UTF-8", "SJIS-WIN");
					$staff = $this->Staff->find('first', array('conditions'=>array('Staff.name like'=>"$name%%")));
					if (empty($staff)) {
						echo mb_convert_encoding($num .' スタッフ['.$name.']が見つかりません。', "SJIS-WIN", "UTF-8") . "\n";
						continue;
					}
					$temp['Order']['staff_id']  = $staff['Staff']['id'];

					// 取得日
					$temp['Order']['date']  = $str[$idx++];

					// 顧客コード
					$temp['Order']['customer_id'] = 0;
					$code = mb_convert_encoding($str[$idx++], "UTF-8", "SJIS-WIN");

					foreach ($customers as $item) {
						if ($code == $item['Customer']['code']) {
							$temp['Order']['customer_id'] = $item['Customer']['id'];
							break;
						}
					}
					if (empty($temp['Order']['customer_id'])) {
						echo mb_convert_encoding($num .' 顧客コード['.$code.']が見つかりません。', "SJIS-WIN", "UTF-8") . "\n";
					}

					// 備考
					$temp['Order']['remark']  = mb_convert_encoding($str[$idx++], "UTF-8", "SJIS-WIN");

					// pr($temp);
					// 保存
					$this->Order->create();
					if(false == $this->Order->save($temp['Order'])){
						echo mb_convert_encoding('番号['.$temp['Order']['no'].']の登録に失敗しました。', "SJIS", "UTF-8");
						$this->_rollback($models);
						$bError = true;
						break;
					}

				}
			}

			if(false == $bError){
				$this->_commit($models);
 				echo mb_convert_encoding("データのインポートが完了しました。[$num]\n", "SJIS", "UTF-8");
			}else{
				$this->_rollback($models);
			}

			fclose($file_in);
		} catch (Exception $e) {
			$this->_rollback($models);

			print_r($e);
			fclose($file_in);
			return false;
		}
    }

	private function _begin($models) {
	    //各モデル毎にトランザクションを開始
	    foreach ($models as $model) {
	        if ($model->begin()) {
	            //トランザクション開始失敗時の処理
	            break;
	        }
	    }
	}

	private function _commit($models) {
	    //各モデル毎にトランザクションを開始
	    foreach ($models as $model) {
	        if ($model->commit()) {
	            //トランザクション開始失敗時の処理
	            break;
	        }
	    }
	}

	private function _rollback($models) {
	    //各モデル毎にトランザクションを開始
	    foreach ($models as $model) {
	        if ($model->rollback()) {
	            //トランザクション開始失敗時の処理
	            break;
	        }
	    }
	}

}

?>
