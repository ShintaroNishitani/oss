<?php
App::import('Core', 'Controller');
App::import('Controller', 'App');
App::uses('CommonComponent', 'Controller/Component');

/**
 * 交通費請求未申請メール送信
 * 毎日1回本シェルを実行し、月例会日の指定日前に交通費請求が行われていない場合は、通知メールを送信する
 */
class SendTransportMailShell extends AppShell {

	var $uses = array('Transport', 'Confirm', 'Meeting', 'Staff');

	function startup() {
		$this->Controller = new Controller();
		$this->AppController = new AppController();
		$collection = new ComponentCollection();
        $this->Common = new CommonComponent($collection);
	}

    function main() {
    	// 当月取得
        $year = date('Y');
        $month = date('n');

        // 翌月を取得
        $this->getNextMonth($year, $month, $n_year, $n_month);

        // 当月の月例会日を取得
    	$meeting = $this->Meeting->find('first', array('conditions'=>array('date >='=>"$year-$month-1", 'date <'=>"$n_year-$n_month-1")));
    	if (!$meeting) {
    		echo mb_convert_encoding($year .'年'. $month . '月の月例会日が設定されていません'."\n", "SJIS", "UTF-8");
    		return;
    	}

    	$send_staffs = array();	// 送信対象のスタッフ

    	// 通知日を計算(今日から○日後が月例会日か？)
    	$date = strtotime(date('Y-n-j') . TRANSPORT_MAIL_DAY . ' day');
    	if (strtotime($meeting['Meeting']['date']) == $date) {
    		// 在籍中のスタッフを取得
    		$staffs = $this->Staff->find('all', array('conditions'=>array('Staff.retire_date'=>null)));
    		foreach ($staffs as $staff) {
                if ($staff['Authority']['id'] == 1) {
                    continue;   // 役員権限には送信しない
                }

    			// 交通費情報があるか？
    			$transport = $this->Transport->find('first', array('conditions'=>array('staff_id'=>$staff['Staff']['id'],
    																				   'year'=>$year, 'month'=>$month)));
    			if (empty($transport)) {
    				$send_staffs[] = $staff;	// ない場合は送信対象
    				continue;
    			}
    			// ある場合は申請済または承認済みになっているか
    			$confirm = $this->Confirm->find('first', array('conditions'=>array('type'=>CONFIRM_TYPE_1,
    														   					   'type_id'=>$transport['Transport']['id'],
    														   					   'status'=>array(CONFIRM_IN, CONFIRM_OK))));
    			if (empty($confirm)) {
    				$send_staffs[] = $staff;	// ない場合は送信対象
    			}
    		}
            // 対象者にメール送信
            foreach ($send_staffs as $staff) {
                $this->AppController->_sendMail($staff['Staff']['mail'],
                                                TRANSPORT_MAIL_TITLE,
                                                TRANSPORT_MAIL_BODY,
                                                null, null
                                                );
				if ($staff['Staff']['line_account_id']) {
					// $this->AppController->_sendLineWorks($staff['Staff']['line_account_id'], TRANSPORT_MAIL_BODY);
					$this->Common->sendLineWorks($staff['Staff']['line_account_id'], TRANSPORT_MAIL_BODY);
				}
            }
    		echo mb_convert_encoding('メールを送信しました。'."\n", "SJIS", "UTF-8");
    	}
	}

    function getNextMonth($year, $month, &$next_year, &$next_month) {
        $next_year = $year;
        $next_month = $month + 1;
        if ($next_month > 12){
            $next_month -= 12;
            $next_year += 1;
        }
    }
}

?>
