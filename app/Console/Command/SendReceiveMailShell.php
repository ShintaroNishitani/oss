<?php
App::import('Core', 'Controller');
App::import('Controller', 'App');
App::uses('CommonComponent', 'Controller/Component');

/**
 * 受領ボタン未押下メール送信
 * 毎日1回本シェルを実行し、月例会日の指定時刻に受領ボタンを押していない場合は、通知メールを送信する
 */
class SendReceiveMailShell extends AppShell {

	var $uses = array('Transport', 'Cost', 'Confirm', 'Meeting', 'Staff');

	function startup() {
		$this->Controller = new Controller();
		$this->AppController = new AppController();
        $collection = new ComponentCollection();
        $this->Common = new CommonComponent($collection);
	}

    function main() {
        $confirm_type = Configure::read("confirm_type");

    	// 当月取得
        $year = date('Y');
        $month = date('n');

        // 翌月を取得
        $this->getNextMonth($year, $month, $n_year, $n_month);

        // 当月の月例会日を取得
    	$meeting = $this->Meeting->find('first', array('conditions'=>array('date >='=>"$year-$month-1", 'date <'=>"$n_year-$n_month-1")));
    	if (!$meeting) {
    		echo mb_convert_encoding($year .'年'. $month . '月の月例会日が設定されていません'."\n", "SJIS", "UTF-8");
    		return;
    	}

    	$send_trans_staffs = array();	// 送信対象のスタッフ

    	$date = date('Y-n-j');
    	if (strtotime($meeting['Meeting']['date']) == strtotime($date)) {
    		// 在籍中のスタッフを取得
    		$staffs = $this->Staff->find('all', array('conditions'=>array('Staff.retire_date'=>null)));
    		foreach ($staffs as $staff) {
                if ($staff['Authority']['id'] == 1) {
                    continue;   // 役員権限には送信しない
                }

                $items = array(CONFIRM_TYPE_1=>array('model'=>'Transport'),    // 交通費
                               CONFIRM_TYPE_2=>array('model'=>'Cost'),         // 経費
                    );

                foreach ($items as $type => $item) {
                    $model = $item['model'];
                    $data = $this->$model->find('first', array('conditions'=>array('staff_id'=>$staff['Staff']['id'],
                                                                                   'year'=>$year, 'month'=>$month),
                                                               'fields'=>array('id')
                                                                ));
                    if ($data) {
                        // ある場合は承認済かつ未受領の場合は送信対象
                        $confirm = $this->Confirm->find('first', array('conditions'=>array('type'=>$type,
                                                                                           'type_id'=>$data[$model]['id'],
                                                                                           'status'=>CONFIRM_OK,
                                                                                           'receive'=>RECEIVE_YET,
                                                                                           )));
                        if ($confirm) {
                            // メール送信
                            $this->AppController->_sendMail($staff['Staff']['mail'],
                                                            sprintf(RECEIVE_MAIL_TITLE, $confirm_type[$type]),
                                                            sprintf(RECEIVE_MAIL_BODY, $confirm_type[$type]),
                                                            null, null
                                                            );
                            if ($staff['Staff']['line_account_id']) {
                                // $this->AppController->_sendLineWorks($staff['Staff']['line_account_id'], sprintf(RECEIVE_MAIL_BODY, $confirm_type[$type]));
                                $this->Common->sendLineWorks($staff['Staff']['line_account_id'], sprintf(RECEIVE_MAIL_BODY, $confirm_type[$type]));
                            }
                        }
                    }
                }
    		}
    		echo mb_convert_encoding('メールを送信しました。'."\n", "SJIS", "UTF-8");
    	}

        // セキュリティ資産貸出書
        $week = date('w');
        $day = date('j');

        // 1日で土日以外または2日が月曜日または3日が月曜日
    	if ($day == 1 && !($week == 0 || $week == 6) ||
            $day == 2 && $week == 1 ||
            $day == 3 && $week == 1
        ) {
            $staffs = $this->Staff->find('all', array('conditions'=>array('Staff.retire_date'=>null)));
            foreach ($staffs as $staff) {
                if ($staff['Staff']['line_account_id']) {
                    $this->Common->sendLineWorks($staff['Staff']['line_account_id'], SECURITY_ASSET_ALERT);
                }
            }
        }
    }

    function getNextMonth($year, $month, &$next_year, &$next_month) {
        $next_year = $year;
        $next_month = $month + 1;
        if ($next_month > 12){
            $next_month -= 12;
            $next_year += 1;
        }
    }
}

?>
