<?php
App::uses('ComponentCollection', 'Controller');
App::uses('CommonComponent', 'Controller/Component');

/**
 * 外部APIアクセストークン取得（1日1回実行）
 * <LineWorks有効期間>
 * アクセストークン：1日 リフレッシュトークン：90日
 */
class GetApiLWTokenShell extends AppShell {

	var $uses = array('LineworksToken');
    // var $components = array('Common');

    function startup() {
        $collection = new ComponentCollection();
        $this->Common = new CommonComponent($collection);
    }

    function main() {
        $jwt = $this->Common->createJwt();
        $body = 'assertion='.$jwt;
        $body .= '&';
        $body .= 'grant_type=urn:ietf:params:oauth:grant-type:jwt-bearer';
        $body .= '&';
        $body .= 'client_id='.LW_CLIENTID;
        $body .= '&';
        $body .= 'client_secret='.LW_CLIENTSECRET;
        $body .= '&';
        $body .= 'scope=bot,bot.read';

        $now = strtotime('now');
        $data = $this->LineworksToken->find('first');
        if (!empty($data))
        {
            $expire = strtotime($data['LineworksToken']['refresh_expired_date']);
            // トークン有効期限チェック
            if ($now < $expire)
            {
                $token = $data['LineworksToken']['refresh_token'];
                $body = 'grant_type=refresh_token';
                $body .= '&';
                $body .= 'refresh_token='.urlencode($token);
                $body .= '&';
                $body .= 'client_id='.LW_CLIENTID;
                $body .= '&';
                $body .= 'client_secret='.LW_CLIENTSECRET;
            }
        }
        
        $curl = curl_init(LW_URL_TOKEN);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            'Content-Type: application/x-www-form-urlencoded',
        ]);
        $res = curl_exec($curl);
        // $info = curl_getinfo($curl);
        // $error = curl_error($curl);
        curl_close($curl);

        $res_decode = json_decode($res, true);
        if (array_key_exists("access_token", $res_decode))
        {
            $token =  $res_decode["access_token"];
            $expire = date("Y-m-d H:i:s", strtotime("+1 day", $now));
            $data['LineworksToken']['access_token'] = $token;
            $data['LineworksToken']['access_expired_date'] = $expire;
            if (array_key_exists("refresh_token", $res_decode))
            {
                $token =  $res_decode["refresh_token"];
                $expire = date("Y-m-d H:i:s", strtotime("+90 day", $now));
                $data['LineworksToken']['refresh_token'] = $token;
                $data['LineworksToken']['refresh_expired_date'] = $expire;
            }
            $this->LineworksToken->save($data, true);
        }

        $this->out('アクセストークン更新完了.');
    }
}

?>
