#!/bin/sh

# ----- 設定 -----

# バックアップ元のデータベース
DBNAME=oceansoftware_oss # ←DB名
DBLOGINID=oceansoftware # ←ユーザ名
DBPASS=oceansoftware2013 # ←パスワード
DBSERVER=mysql455.db.sakura.ne.jp # ←DBが乗っているホスト名

PATH=/bin:/usr/bin:/usr/local/bin


# ---------- ファイル名を設定 ----------
# タイムスタンプを取得
TSNOW=`date +%Y%m%d`
TSOLD=`date -v -14d +%Y%m%d`

# バックアップ先のディレクトリ・ファイル
bk_dir=/home/oceansoftware/backup/oss/db

# ファイル名を設定
file_temp=oss_db_$TSNOW.sql
file_backup=oss_db_$TSNOW.tar.gz
file_remove=oss_db_$TSOLD.tar.gz

# ---------- バックアップ処理 ----------
# バックアップ先のディレクトリに移動
cd $bk_dir
if [ $? != 0 ]; then
echo "Backup directory does not exist."
exit 1
fi

# データベースをダンプ
mysqldump -Q -h $DBSERVER -u $DBLOGINID -p$DBPASS $DBNAME > $file_temp
if [ $? != 0 -o ! -e $file_temp ]; then
echo "Cannot dump database."
exit 1
fi

# アーカイブを作成
tar cfz $file_backup $file_temp
if [ $? != 0 -o ! -e $file_backup ]; then
echo "Cannot archive files."
exit 1
fi

# テンポラリファイルを削除
rm -f $file_temp

# ローテーション処理
if [ -e $file_remove ]; then
rm -f $file_remove
fi

exit 0
